BattleCry
CardMover/RANDOM/3/1111/1111/111/FRIENDLY/DECK/FRIENDLY/HAND

Spell
Attacker/ALL/1/1111/1111/000/300/01
Healer/ALL/1/1111/1111/000/300/0
ApChanger/ALL/1/1111/1111/000/300/FRIENDLY

Will
CardMover/RANDOM/2/1111/1111/000/ENEMY/MONSTERFIELD/ENEMY/GRAVEYARD

Cerberus, Gatekeeper of Hell/10/3000/2000/Hero/01/DEMONIC/Open the Gate: Draw three cards from deck to hand/Hellfire: Deal 300 damage to all enemy monster cards and Increase HP and AP of all friendly monster cards by 300/Revenge of the Two Heads: Send two random enemy monster cards from field to garveyard