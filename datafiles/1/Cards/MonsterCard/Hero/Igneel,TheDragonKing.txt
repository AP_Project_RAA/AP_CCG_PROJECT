BattleCry
CardMover/ALL/1/1111/1110/000/BOTH/MONSTERFIELD/BOTH/GRAVEYARD

Spell
Attacker/ALL/1/1111/1111/000/600/11

Will
ApChanger/ALL/1/1111/1111/000/-400/ENEMY

Igneel, The Dragon King/10/4000/800/Hero/00/DRAGONBREED/King’s Grace: Send all non-Hero monster cards on both sides of field to their graveyards/King’s Wing Slash: Deal 600 damage to all enemy monster cards and player/King’s Wail: Decrease all enemy monster cards’ AP by 400