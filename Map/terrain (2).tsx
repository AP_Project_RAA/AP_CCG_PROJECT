<?xml version="1.0" encoding="UTF-8"?>
<tileset name="terrain (2)" tilewidth="32" tileheight="32" tilecount="1024" columns="32">
 <image source="Sprites/terrain (2).png" width="1024" height="1024"/>
 <tile id="0">
  <properties>
   <property name="Start" type="int" value="484"/>
  </properties>
 </tile>
 <tile id="187">
  <animation>
   <frame tileid="187" duration="250"/>
   <frame tileid="188" duration="250"/>
  </animation>
 </tile>
 <tile id="188">
  <animation>
   <frame tileid="188" duration="250"/>
   <frame tileid="189" duration="250"/>
  </animation>
 </tile>
 <tile id="189">
  <animation>
   <frame tileid="189" duration="250"/>
   <frame tileid="187" duration="250"/>
  </animation>
 </tile>
</tileset>
