<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Hero" tilewidth="32" tileheight="32" tilecount="3168" columns="48">
 <image source="Sprites/Hero.png" trans="ffffff" width="1536" height="2112"/>
 <tile id="1056">
  <animation>
   <frame tileid="1056" duration="100"/>
   <frame tileid="1058" duration="100"/>
   <frame tileid="1060" duration="100"/>
   <frame tileid="1062" duration="100"/>
   <frame tileid="1064" duration="100"/>
   <frame tileid="1066" duration="100"/>
   <frame tileid="1068" duration="100"/>
   <frame tileid="1070" duration="100"/>
   <frame tileid="1072" duration="100"/>
  </animation>
 </tile>
 <tile id="1057">
  <animation>
   <frame tileid="1057" duration="100"/>
   <frame tileid="1059" duration="100"/>
   <frame tileid="1061" duration="100"/>
   <frame tileid="1063" duration="100"/>
   <frame tileid="1065" duration="100"/>
   <frame tileid="1067" duration="100"/>
   <frame tileid="1069" duration="100"/>
   <frame tileid="1071" duration="100"/>
   <frame tileid="1073" duration="100"/>
  </animation>
 </tile>
 <tile id="1104">
  <animation>
   <frame tileid="1104" duration="100"/>
   <frame tileid="1106" duration="100"/>
   <frame tileid="1108" duration="100"/>
   <frame tileid="1110" duration="100"/>
   <frame tileid="1112" duration="100"/>
   <frame tileid="1114" duration="100"/>
   <frame tileid="1116" duration="100"/>
   <frame tileid="1118" duration="100"/>
   <frame tileid="1120" duration="100"/>
  </animation>
 </tile>
 <tile id="1105">
  <animation>
   <frame tileid="1105" duration="100"/>
   <frame tileid="1107" duration="100"/>
   <frame tileid="1109" duration="100"/>
   <frame tileid="1111" duration="100"/>
   <frame tileid="1113" duration="100"/>
   <frame tileid="1115" duration="100"/>
   <frame tileid="1117" duration="100"/>
   <frame tileid="1119" duration="100"/>
   <frame tileid="1121" duration="100"/>
  </animation>
 </tile>
</tileset>
