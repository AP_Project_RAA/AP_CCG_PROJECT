<?xml version="1.0" encoding="UTF-8"?>
<tileset name="lava" tilewidth="32" tileheight="32" tilecount="30" columns="5">
 <image source="Sprites/lava.png" trans="ffffff" width="160" height="192"/>
 <tile id="25">
  <animation>
   <frame tileid="25" duration="500"/>
   <frame tileid="26" duration="500"/>
  </animation>
 </tile>
 <tile id="26">
  <animation>
   <frame tileid="26" duration="500"/>
   <frame tileid="27" duration="500"/>
  </animation>
 </tile>
 <tile id="27">
  <animation>
   <frame tileid="27" duration="500"/>
   <frame tileid="25" duration="500"/>
  </animation>
 </tile>
</tileset>
