package network;

import enums.Field;
import enums.SpellType;
import gameObjects.Item;
import gameObjects.SpellPack;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.General;
import gameObjects.cards.monsterCards.Hero;
import gameObjects.cards.monsterCards.MonsterCard;
import gameObjects.cards.monsterCards.SpellCaster;
import gameObjects.cards.spellCards.AuraSpell;
import gameObjects.cards.spellCards.ContinuousSpell;
import gameObjects.cards.spellCards.InstantSpell;
import gameObjects.cards.spellCards.SpellCard;
import gameObjects.spells.Spell;
import javafx.animation.PathTransition;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import main.*;
import states.CardShop;
import states.MainMenu;
import states.State;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class ClientBattle implements State {
    private Client client;
    private UI ui = UI.instance;

    private Player player;
    private Player enemy;
    private CardShop cardShop;
    private MainMenu mainMenu;
    private ArrayList<Item> playerItemsCopy;
    private ArrayList<Item> enemyItemsCopy;
    private boolean isGameOver;
    private GameManager gameManager;
    private Stage stage;
    private Scene scene;
    private Group root;
    private ScrollPane sideScrollPane;
    private VBox sideScrollPaneContent;
    private HBox pMonsterCardsBox, pSpellCardsBox, eMonsterCardsBox, eSpellCardsBox;
    private ImageView handButtonImageView, itemsButtonImageView;
    private ImageView drawCardButtonImageView, useItemButtonImageView, graveyardButtonImageView;
    private MonsterCard selectedPMonsterCard;
    private Card selectedCardFromHand;
    private Item selectedItem;
    private ImageView castSpellButton, useCardButton;
    private boolean isUsingCard = false;
    private ScrollPane consolePane;
    private Rectangle consolebg;
    private Text consoleContent;
    private ImageView consoleButton;
    private ScrollPane spellTargetsPane;
    private HBox spellTargetsBox;
    transient private Label spellTargetsTitle;
    private Rectangle spellTargetsbg;
    private boolean isCastingSpell = false;
    private ArrayList<Card> spellTargetsArray = new ArrayList<>();
    private Rectangle cardInfobg;
    transient private Label cardInfoContent;
    private boolean isShowingInfo;
    private Button closeInfoButton;
    private boolean isFinishedCasting = true;
    private GamePlace playerDeckCopy, enemyDeckCopy;
    private int round, turn;

    public MainMenu getMainMenu(){
        return mainMenu;
    }

    public void setMainMenu(MainMenu mainMenu){
        this.mainMenu = mainMenu;
    }

    public Player getPlayer(){
        return player;
    }

    public void setPlayer(Player player){
        this.player = player;
    }

    public Player getEnemy(){
        return enemy;
    }

    public void setEnemy(Player enemy){
        this.enemy = enemy;
    }

    public CardShop getCardShop(){
        return cardShop;
    }

    public void setCardShop(CardShop cardShop){
        this.cardShop = cardShop;
    }

    public void setupScene(){
        root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        ImageView battlebg = new ImageView(Dictionary.BATTLE_BG);
        battlebg.fitWidthProperty().bind(scene.widthProperty());
        battlebg.fitHeightProperty().bind(scene.heightProperty());
        root.getChildren().add(battlebg);

        Group enemyAvatar = enemy.getAvatar();
        updateEnemyAvatarEvent();
        root.getChildren().add(enemyAvatar);

        Group playerAvatar = player.getAvatar();
        root.getChildren().add(playerAvatar);

        // player avatars
        enemyAvatar.relocate(1050, 20);
        playerAvatar.relocate(1050, scene.getHeight() - 140);

        // left side scroll pane
        ImageView sideScrollPaneImageView = new ImageView(Dictionary.SIDE_PANE);
        sideScrollPaneImageView.setFitWidth(160);
        sideScrollPaneImageView.setFitHeight(560);
        sideScrollPaneImageView.setLayoutX(40);
        sideScrollPaneImageView.setLayoutY(30);
        sideScrollPane = new ScrollPane();
        sideScrollPaneContent = new VBox();
        sideScrollPaneContent.setSpacing(10);
        sideScrollPane.setLayoutY(60);
        sideScrollPane.setLayoutX(60);
        sideScrollPane.setMaxHeight(500);
        sideScrollPane.setPrefWidth(128);
        sideScrollPane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        sideScrollPane.setContent(sideScrollPaneContent);
        root.getChildren().addAll(sideScrollPaneImageView, sideScrollPane);

        // hand button, items button and graveyard button
        handButtonImageView = new ImageView(Dictionary.HAND_BUTTON_OFF);
        itemsButtonImageView = new ImageView(Dictionary.ITEMS_BUTTON_OFF);
        graveyardButtonImageView = new ImageView(Dictionary.GRAVEYARD_BUTTON_OFF);
        // hand
        handButtonImageView.setLayoutX(60);
        handButtonImageView.setLayoutY(600);
        handButtonImageView.setPreserveRatio(true);
        handButtonImageView.setFitWidth(120);
        handButtonImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            if(handButtonImageView.getImage().equals(Dictionary.HAND_BUTTON_OFF)) {
                showHand();
                handButtonImageView.setImage(Dictionary.HAND_BUTTON_ON);
                itemsButtonImageView.setImage(Dictionary.ITEMS_BUTTON_OFF);
                graveyardButtonImageView.setImage(Dictionary.GRAVEYARD_BUTTON_OFF);
                selectedItem = null;
                refreshDrawCardAndUseItemButtons();
            }
        });
        handButtonImageView.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        handButtonImageView.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        // items
        itemsButtonImageView.setLayoutX(60);
        itemsButtonImageView.setLayoutY(650);
        itemsButtonImageView.setPreserveRatio(true);
        itemsButtonImageView.setFitWidth(120);
        itemsButtonImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            if(itemsButtonImageView.getImage().equals(Dictionary.ITEMS_BUTTON_OFF)) {
                showItems();
                handButtonImageView.setImage(Dictionary.HAND_BUTTON_OFF);
                itemsButtonImageView.setImage(Dictionary.ITEMS_BUTTON_ON);
                graveyardButtonImageView.setImage(Dictionary.GRAVEYARD_BUTTON_OFF);
                selectedCardFromHand = null;
                refreshDrawCardAndUseItemButtons();
            }
        });
        itemsButtonImageView.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        itemsButtonImageView.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        // graveyard
        graveyardButtonImageView.setLayoutX(60);
        graveyardButtonImageView.setLayoutY(700);
        graveyardButtonImageView.setPreserveRatio(true);
        graveyardButtonImageView.setFitWidth(120);
        graveyardButtonImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            if(graveyardButtonImageView.getImage().equals(Dictionary.GRAVEYARD_BUTTON_OFF)) {
                showGraveyard();
                handButtonImageView.setImage(Dictionary.HAND_BUTTON_OFF);
                itemsButtonImageView.setImage(Dictionary.ITEMS_BUTTON_OFF);
                graveyardButtonImageView.setImage(Dictionary.GRAVEYARD_BUTTON_ON);
                selectedItem = null;
                selectedCardFromHand = null;
                refreshDrawCardAndUseItemButtons();
            }
        });
        graveyardButtonImageView.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        graveyardButtonImageView.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        root.getChildren().addAll(handButtonImageView, itemsButtonImageView, graveyardButtonImageView);

        //Enemy SpellField
        eSpellCardsBox = new HBox();
        eSpellCardsBox.setLayoutX(300);
        eSpellCardsBox.setLayoutY(50);
        eSpellCardsBox.setPrefWidth(scene.getWidth() - 600);
        eSpellCardsBox.setPrefHeight(130);
        eSpellCardsBox.setAlignment(Pos.CENTER);
        eSpellCardsBox.setSpacing(16);
//        eSpellCardsBox.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        root.getChildren().add(eSpellCardsBox);

        //Enemy MonsterField
        eMonsterCardsBox = new HBox();
        eMonsterCardsBox.setLayoutX(200);
        eMonsterCardsBox.setLayoutY(190);
        eMonsterCardsBox.setPrefWidth(scene.getWidth() - 400);
        eMonsterCardsBox.setPrefHeight(130);
        eMonsterCardsBox.setAlignment(Pos.CENTER);
        eMonsterCardsBox.setSpacing(16);
//        eMonsterCardsBox.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        root.getChildren().add(eMonsterCardsBox);

        //Player MonsterField
        pMonsterCardsBox = new HBox();
        pMonsterCardsBox.setLayoutX(200);
        pMonsterCardsBox.setLayoutY(scene.getHeight() - 320);
        pMonsterCardsBox.setPrefWidth(scene.getWidth() - 400);
        pMonsterCardsBox.setPrefHeight(130);
        pMonsterCardsBox.setAlignment(Pos.CENTER);
        pMonsterCardsBox.setSpacing(16);
//        pMonsterCardsBox.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        root.getChildren().add(pMonsterCardsBox);

        //Player SpellField
        pSpellCardsBox = new HBox();
        pSpellCardsBox.setLayoutX(300);
        pSpellCardsBox.setLayoutY(scene.getHeight() - 180);
        pSpellCardsBox.setPrefWidth(scene.getWidth() - 600);
        pSpellCardsBox.setPrefHeight(130);
        pSpellCardsBox.setAlignment(Pos.CENTER);
        pSpellCardsBox.setSpacing(16);
//        pSpellCardsBox.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        root.getChildren().add(pSpellCardsBox);

        // draw card button
        drawCardButtonImageView = new ImageView(Dictionary.DRAW_CARD_BUTTON);
        drawCardButtonImageView.setLayoutX(210);
        drawCardButtonImageView.setLayoutY(550);
        drawCardButtonImageView.setPreserveRatio(true);
        drawCardButtonImageView.setFitWidth(120);
        drawCardButtonImageView.setOnMouseEntered(event -> {
            if(!drawCardButtonImageView.isDisable()) {
                drawCardButtonImageView.setImage(Dictionary.DRAW_CARD_BUTTON_HOVER);
                scene.setCursor(Cursor.HAND);
            }
        });
        drawCardButtonImageView.setOnMouseExited(event -> {
            if(!drawCardButtonImageView.isDisable()) {
                drawCardButtonImageView.setImage(Dictionary.DRAW_CARD_BUTTON);
                scene.setCursor(Cursor.DEFAULT);
            }
        });
        drawCardButtonImageView.setOnMouseClicked(event -> {
            if(turn == 0) {
                if(!drawCardButtonImageView.isDisable()) {
                    if(selectedCardFromHand instanceof MonsterCard) {
                        Dictionary.playDrawMonsterCardSound();
                    } else {
                        Dictionary.playDrawSpellCardSound();
                    }
                    setCard(selectedCardFromHand);
                    selectedCardFromHand = null;
                    refreshDrawCardAndUseItemButtons();

                    client.sendPlayer(enemy);
                    client.sendPlayer(player);
                }
            } else
                ui.showOutput("\n> " + "Not Your Turn!!");
        });
        root.getChildren().add(drawCardButtonImageView);

        //use card button
        useCardButton = new ImageView(Dictionary.USE_CARD_BUTTON);
        useCardButton.setLayoutX(210);
        useCardButton.setLayoutY(600);
        useCardButton.setPreserveRatio(true);
        useCardButton.setFitWidth(120);
        useCardButton.setOnMouseEntered(event -> {
            scene.setCursor(Cursor.HAND);
        });
        useCardButton.setOnMouseExited(event -> {
            scene.setCursor(Cursor.DEFAULT);
        });
        useCardButton.setOnMouseClicked(event -> {
            if(turn == 0) {
                if(isUsingCard) {
                    isUsingCard = false;
                    useCardButton.setImage(Dictionary.USE_CARD_BUTTON);
                } else {
                    isUsingCard = true;
                    useCardButton.setImage(Dictionary.USE_CARD_BUTTON_HOVER);
                }
                if(selectedPMonsterCard != null) {

                }
                client.sendPlayer(enemy);
                client.sendPlayer(player);
            } else
                ui.showOutput("\n> " + "Not Your Turn!!");
        });
        root.getChildren().add(useCardButton);

        //Cast Spell Button
        castSpellButton = new ImageView(Dictionary.CAST_SPELL_BUTTON);
        castSpellButton.setLayoutX(210);
        castSpellButton.setLayoutY(650);
        castSpellButton.setPreserveRatio(true);
        castSpellButton.setFitWidth(120);
        castSpellButton.setOnMouseEntered(event -> {
            castSpellButton.setImage(Dictionary.CAST_SPELL_BUTTON_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        castSpellButton.setOnMouseExited(event -> {
            castSpellButton.setImage(Dictionary.CAST_SPELL_BUTTON);
            scene.setCursor(Cursor.DEFAULT);
        });
        castSpellButton.setOnMouseClicked(event -> {
            if(turn == 0) {
                if(selectedPMonsterCard instanceof SpellCaster) {
                    SpellCaster spellCaster = (SpellCaster) selectedPMonsterCard;
                    if(!spellCaster.isUsedSpell()) {
                        spellCaster.setUsedSpell(true);
                        castSpell(spellCaster.getSpell(), spellCaster.getName());
                        //castSpellButton.setDisable(true);
                    }
                } else if(selectedPMonsterCard instanceof Hero) {
                    Hero hero = (Hero) selectedPMonsterCard;
                    if(!hero.isUsedSpell()) {
                        hero.setUsedSpell(true);
                        castSpell(hero.getSpell(), hero.getName());
                        //castSpellButton.setDisable(true);
                    }
                }
                client.sendPlayer(enemy);
                client.sendPlayer(player);
            } else
                ui.showOutput("\n> " + "Not Your Turn!!");
        });
        root.getChildren().add(castSpellButton);

        // use item button
        useItemButtonImageView = new ImageView(Dictionary.USE_ITEM_BUTTON);
        useItemButtonImageView.setLayoutX(210);
        useItemButtonImageView.setLayoutY(700);
        useItemButtonImageView.setPreserveRatio(true);
        useItemButtonImageView.setFitWidth(120);
        useItemButtonImageView.setOnMouseEntered(event -> {
            if(!useItemButtonImageView.isDisable()) {
                useItemButtonImageView.setImage(Dictionary.USE_ITEM_BUTTON_HOVER);
                scene.setCursor(Cursor.HAND);
            }
        });
        useItemButtonImageView.setOnMouseExited(event -> {
            if(!useItemButtonImageView.isDisable()) {
                useItemButtonImageView.setImage(Dictionary.USE_ITEM_BUTTON);
                scene.setCursor(Cursor.DEFAULT);
            }
        });
        useItemButtonImageView.setOnMouseClicked(event -> {
            Dictionary.playUseItemSound();
            if(turn == 0) {
                useItem(selectedItem);
                selectedItem = null;
                useItemButtonImageView.setImage(Dictionary.USE_ITEM_BUTTON);
                useItemButtonImageView.setOpacity(0.5);
                useItemButtonImageView.setDisable(true);
                showItems();
                client.sendPlayer(enemy);
                client.sendPlayer(player);
            } else
                ui.showOutput("\n> " + "Not Your Turn!!");
        });
        root.getChildren().add(useItemButtonImageView);

        //console
        Group consoleGroup = new Group();
        consoleGroup.setLayoutX(1040);
        consoleGroup.setLayoutY(300);

        consolebg = new Rectangle(300, 200);
        consolebg.setFill(Color.BLACK);
        consolebg.setOpacity(0.5);
        consolebg.setArcWidth(15);
        consolebg.setArcHeight(15);
        consoleGroup.getChildren().add(consolebg);

        consoleContent = new Text();
        consoleContent.setWrappingWidth(295);
        consoleContent.setTextAlignment(TextAlignment.LEFT);
        consoleContent.setFont(Font.font("Consolas", 12));
        consoleContent.setStroke(Color.WHITE);
        ui.setConsoleContent(consoleContent);

        consolePane = new ScrollPane();
        consolePane.setContent(consoleContent);
        consolePane.setPrefViewportWidth(300);
        consolePane.setPrefViewportHeight(180);
        consolePane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        ui.setConsolePane(consolePane);
        consoleGroup.getChildren().add(consolePane);

        root.getChildren().addAll(consoleGroup);

        // making paths for console
        Line path1 = new Line(490, 100, 160, 100);
        Line path2 = new Line(160, 100, 490, 100);
        PathTransition transition = new PathTransition();
        transition.setPath(path1);
        transition.setDuration(Duration.millis(500));
        transition.setNode(consoleGroup);
        transition.setAutoReverse(false);

        //console Button
        consoleButton = new ImageView(Dictionary.CONSOLE_BUTTON_OFF);
        consoleButton.setLayoutX(scene.getWidth() - 110);
        consoleButton.setLayoutY(scene.getHeight() - 50);
        consoleButton.setPreserveRatio(true);
        consoleButton.setFitWidth(100);
        consoleButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        consoleButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        consoleButton.setOnMouseClicked(event -> {
            if(transition.getPath().equals(path1)) {
                transition.setPath(path2);
            } else {
                transition.setPath(path1);
            }
            transition.play();
        });
        consoleButton.setOnMouseEntered(event -> {
            consoleButton.setImage(Dictionary.CONSOLE_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        consoleButton.setOnMouseExited(event -> {
            consoleButton.setImage(Dictionary.CONSOLE_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        root.getChildren().add(consoleButton);

        //spell targets box
        spellTargetsbg = new Rectangle(300, scene.getHeight() / 2 - 80, scene.getWidth() - 600, 160);
        spellTargetsbg.setFill(Color.BLACK);
        spellTargetsbg.setOpacity(0.5);
        spellTargetsbg.setVisible(false);
        root.getChildren().add(spellTargetsbg);

        spellTargetsTitle = new Label("Title");
        spellTargetsTitle.setLayoutX(300);
        spellTargetsTitle.setLayoutY(scene.getHeight() / 2 - 80);
        spellTargetsTitle.setPrefWidth(scene.getWidth() - 600);
        spellTargetsTitle.setAlignment(Pos.CENTER);
        spellTargetsTitle.setFont(Font.font("Times New Roman", FontWeight.BOLD, 14));
        spellTargetsTitle.setTextFill(Color.WHITE);
        spellTargetsTitle.setVisible(false);
        root.getChildren().add(spellTargetsTitle);

        spellTargetsPane = new ScrollPane();
        spellTargetsPane.setLayoutX(300);
        spellTargetsPane.setPrefViewportWidth(scene.getWidth() - 600);
        spellTargetsPane.setLayoutY(scene.getHeight() / 2 - 80);
        spellTargetsPane.setPrefViewportHeight(160);
        spellTargetsPane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        spellTargetsPane.setVisible(false);

        spellTargetsBox = new HBox();
        spellTargetsBox.setLayoutX(300);
        spellTargetsBox.setLayoutY(scene.getHeight() / 2 - 79);
        spellTargetsBox.setPrefWidth(scene.getWidth() - 600);
        spellTargetsBox.setPrefHeight(158);
        spellTargetsBox.setAlignment(Pos.CENTER);
        spellTargetsBox.setVisible(false);

        spellTargetsPane.setContent(spellTargetsBox);
        root.getChildren().add(spellTargetsPane);

        //card info popup
        cardInfobg = new Rectangle(scene.getWidth() / 2 - 100, scene.getHeight() / 2 - 150, 200, 300);
        cardInfobg.setFill(Color.BLACK);
        cardInfobg.setOpacity(0.7);
        cardInfobg.setVisible(false);
        root.getChildren().add(cardInfobg);

        cardInfoContent = new Label("card info");
        cardInfoContent.setLayoutX(scene.getWidth() / 2 - 90);
        cardInfoContent.setLayoutY(scene.getHeight() / 2 - 140);
        cardInfoContent.setPrefWidth(180);
        cardInfoContent.setPrefHeight(280);
        cardInfoContent.setFont(Font.font("Times New Roman", 12));
        cardInfoContent.setTextFill(Color.WHITE);
        cardInfoContent.setAlignment(Pos.CENTER_LEFT);
        cardInfoContent.setWrapText(true);
        cardInfoContent.setVisible(false);
        root.getChildren().add(cardInfoContent);

        closeInfoButton = new Button("X");
        closeInfoButton.setLayoutX(scene.getWidth() / 2 + 77);
        closeInfoButton.setLayoutY(scene.getHeight() / 2 - 150);
        closeInfoButton.setOnMouseClicked(event -> {
            runCardInfoPopup(null);
        });
        closeInfoButton.setVisible(false);
        root.getChildren().add(closeInfoButton);

        ImageView endTurnButton = new ImageView(Dictionary.END_TURN_BUTTON_OFF);
        endTurnButton.setLayoutX(scene.getWidth() - 110);
        endTurnButton.setLayoutY(scene.getHeight() - 110);
        endTurnButton.setPreserveRatio(true);
        endTurnButton.setFitWidth(100);
        endTurnButton.setOnMouseEntered(event -> {
            endTurnButton.setImage(Dictionary.END_TURN_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        endTurnButton.setOnMouseExited(event -> {
            endTurnButton.setImage(Dictionary.END_TURN_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        endTurnButton.setOnMouseClicked(event -> {
            if(turn == 0) {
                turn = 1;
                client.sendPlayer(enemy);
                client.sendPlayer(player);

                client.trigger();
            } else
                ui.showOutput("\n> " + "Not Your Turn!!");
        });
        root.getChildren().add(endTurnButton);

        refreshDrawCardAndUseItemButtons();
    }

    private void updateEnemyAvatarEvent(){
        enemy.getAvatar().setOnMouseClicked(event -> {
            // attacking enemy by clicking on it
            if(isUsingCard && selectedPMonsterCard != null) {
                runSpelledAttackMenu(selectedPMonsterCard, null, enemy, "");
                client.sendPlayer(enemy);
                client.sendPlayer(player);
            }
        });
    }

    private void showHand(){
        selectedCardFromHand = null;
        sideScrollPaneContent.getChildren().clear();
        for(Card card : player.getHand().getCards()){
            sideScrollPaneContent.getChildren().add(card.getCardGroup());
            card.getCardGroup().setOnMouseClicked(event -> {
                Dictionary.playClickSound();
                if(event.getButton().equals(MouseButton.PRIMARY)) {
                    selectedCardFromHand = card;
                    refreshDrawCardAndUseItemButtons();
                }
                if(event.getButton().equals(MouseButton.SECONDARY)) {
                    runCardInfoPopup(card);
                }
            });
        }
    }

    private void showItems(){
        sideScrollPaneContent.getChildren().clear();
        for(Item item : player.getInventory().getItems()){
            sideScrollPaneContent.getChildren().add(item.getItemGroup());
            item.getItemGroup().setOnMouseClicked(event1 -> {
                Dictionary.playClickSound();
                selectedItem = item;
                refreshDrawCardAndUseItemButtons();
            });
        }
    }

    private void showGraveyard(){
        sideScrollPaneContent.getChildren().clear();
        for(Card card : player.getGraveYard().getCards()){
            sideScrollPaneContent.getChildren().add(card.getCardGroup());
            card.getCardGroup().setOnMouseClicked(event -> {
                Dictionary.playClickSound();
                if(event.getButton().equals(MouseButton.SECONDARY)) {
                    runCardInfoPopup(card);
                }
            });
        }
    }

    private void refreshDrawCardAndUseItemButtons(){
        if(handButtonImageView.getImage().equals(Dictionary.HAND_BUTTON_ON)
                && selectedCardFromHand != null
                && selectedCardFromHand.getMP() <= player.MP
                && ((selectedCardFromHand instanceof MonsterCard && pMonsterCardsBox.getChildren().size() < 5)
                || (selectedCardFromHand instanceof SpellCard && pSpellCardsBox.getChildren().size() < 3))) {
            drawCardButtonImageView.setOpacity(1);
            drawCardButtonImageView.setDisable(false);
        } else {
            drawCardButtonImageView.setImage(Dictionary.DRAW_CARD_BUTTON);
            drawCardButtonImageView.setOpacity(0.5);
            drawCardButtonImageView.setDisable(true);
        }
        if(itemsButtonImageView.getImage().equals(Dictionary.ITEMS_BUTTON_ON) && selectedItem != null) {
            useItemButtonImageView.setOpacity(1);
            useItemButtonImageView.setDisable(false);
        } else {
            useItemButtonImageView.setImage(Dictionary.USE_ITEM_BUTTON);
            useItemButtonImageView.setOpacity(0.5);
            useItemButtonImageView.setDisable(true);
        }
    }

    public void updateMonsterCardsBoxes(){
        pMonsterCardsBox.getChildren().clear();
        for(Card card : player.getMonsterField().getCards()){
            MonsterCard monsterCard = (MonsterCard) card;
            monsterCard.updateGFX();
            pMonsterCardsBox.getChildren().add(card.getCardGroup());
            card.getCardGroup().setOnMouseClicked(event -> {
                if(event.getButton().equals(MouseButton.PRIMARY)) {
                    selectedPMonsterCard = (MonsterCard) card;
                }
                if(event.getButton().equals(MouseButton.SECONDARY)) {
                    runCardInfoPopup(card);
                }
            });
        }

        eMonsterCardsBox.getChildren().clear();
        for(Card card : enemy.getMonsterField().getCards()){
            eMonsterCardsBox.getChildren().add(card.getCardGroup());
            MonsterCard monsterCard = (MonsterCard) card;
            monsterCard.updateGFX();
            card.getCardGroup().setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event){
                    if(event.getButton().equals(MouseButton.PRIMARY)) {
                        //choosing enemy monstercard to attack
                        MonsterCard pMonsterCard = (MonsterCard) selectedPMonsterCard;
                        MonsterCard eMonsterCard = (MonsterCard) card;

                        if(pMonsterCard != null && !pMonsterCard.hasAttacked && isUsingCard && !isCastingSpell) {
                            runSpelledAttackMenu(pMonsterCard, eMonsterCard, enemy, "");
                        }
                        if(isCastingSpell) {
                            spellTargetsArray.add(card);
                        }
                    } else if(event.getButton().equals(MouseButton.SECONDARY)) {
                        runCardInfoPopup(card);
                    }
                }
            });
        }
    }

    public void updateSpellCardsBoxes(){
        pSpellCardsBox.getChildren().clear();
        for(Card card : player.getSpellField().getCards()){
            pSpellCardsBox.getChildren().add(card.getCardGroup());
            card.getCardGroup().setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event){
                    if(event.getButton().equals(MouseButton.PRIMARY)) {

                    }
                    if(event.getButton().equals(MouseButton.SECONDARY)) {
                        runCardInfoPopup(card);
                    }
                }
            });
        }

        eSpellCardsBox.getChildren().clear();
        for(Card card : enemy.getSpellField().getCards()){
            eSpellCardsBox.getChildren().add(card.getCardGroup());
            card.getCardGroup().setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event){
                    if(event.getButton().equals(MouseButton.PRIMARY)) {

                    }
                    if(event.getButton().equals(MouseButton.SECONDARY)) {
                        runCardInfoPopup(card);
                    }
                }
            });
        }
    }

    private void runCardInfoPopup(Card card){
        if(isShowingInfo) {
            isShowingInfo = false;
            cardInfobg.setVisible(false);
            cardInfoContent.setVisible(false);
            closeInfoButton.setVisible(false);
        } else {
            isShowingInfo = true;
            cardInfobg.setVisible(true);
            cardInfoContent.setVisible(true);
            cardInfoContent.setText(card.getInfo());
            closeInfoButton.setVisible(true);
        }
    }

    private void showTargets(Spell spell, SpellPack spellPack, String name){
        spellTargetsbg.setVisible(true);
        spellTargetsPane.setVisible(true);
        spellTargetsTitle.setVisible(true);
        spellTargetsBox.setVisible(true);
        spellTargetsBox.getChildren().clear();

        spellTargetsTitle.setText(name);

        ArrayList<Card> spellTargets = spell.getTargets(player, enemy);
        for(int i = 0; i < spellTargets.size(); i++){
            Card card = spellTargets.get(i);
            spellTargetsBox.getChildren().add(card.getCardGroup());
            card.getCardGroup().setOnMouseClicked(event -> {
                updateSpellTargetsArray(card, spell, spellPack);
            });
        }
        if(spell.isEffectsPlayer()) {
            Card playerCard = new Card();
            playerCard.setName("__Player_");
            spellTargetsBox.getChildren().add(player.getAvatar());
            player.getAvatar().setOnMouseClicked(event -> {
                updateSpellTargetsArray(playerCard, spell, spellPack);
            });
        }
        if(spell.isEffectsEnemy()) {
            Card enemyCard = new Card();
            enemyCard.setName("__Enemy_");
            spellTargetsBox.getChildren().add(enemy.getAvatar());
            enemy.getAvatar().setOnMouseClicked(event -> {
                updateSpellTargetsArray(enemyCard, spell, spellPack);
            });
        }

    }

    public void updateSpellTargetsArray(Card card, Spell spell, SpellPack spellPack){
        if(isCastingSpell && !spellTargetsArray.contains(card) && spellTargetsArray.size() < spell.getNoOfCasts()) {
            spellTargetsArray.add(card);
        }
        if(isCastingSpell && spellTargetsArray.size() == spell.getNoOfCasts()) {
            spellTargetsbg.setVisible(false);
            spellTargetsPane.setVisible(false);
            spellTargetsTitle.setVisible(false);
            spellTargetsBox.setVisible(false);
            isFinishedCasting = true;

            affectSpellCasting(spellPack);

            for(int i = 0; i < spellTargetsArray.size(); i++){
                Card tmpCard = spellTargetsArray.get(i);
                if(!root.getChildren().contains(player.getAvatar())) {
                    player.getAvatar().setLayoutX(1050);
                    player.getAvatar().setLayoutY(scene.getHeight() - 140);
                    root.getChildren().add(player.getAvatar());
                    player.updateAvatar();
                }
                if(!root.getChildren().contains(enemy.getAvatar())) {
                    enemy.getAvatar().setLayoutX(1050);
                    enemy.getAvatar().setLayoutY(20);
                    root.getChildren().add(enemy.getAvatar());
                    enemy.updateAvatar();
                    updateEnemyAvatarEvent();
                }
                updateSpellCardsBoxes();
                updateMonsterCardsBoxes();
                if(handButtonImageView.getImage().equals(Dictionary.HAND_BUTTON_ON))
                    showHand();
                isCastingSpell = false;
                isFinishedCasting = true;
                spellTargetsArray = new ArrayList<>();
            }
        }
    }

    @Override
    public void run(){
        setupScene();
        stage.setScene(scene);
        stage.show();

        player.updateAvatar();
        enemy.updateAvatar();

        playerDeckCopy = player.getDeck().getCopy();
        enemyDeckCopy = enemy.getDeck().getCopy();

        playerItemsCopy = new ArrayList<>();
        enemyItemsCopy = new ArrayList<>();
        for(int i = 0; i < player.getItems().size(); i++)
            playerItemsCopy.add(player.getItems().get(i).getCopy());
        for(int i = 0; i < enemy.getItems().size(); i++)
            enemyItemsCopy.add(enemy.getItems().get(i).getCopy());

        //Randomizing Decks
        player.setDeck(getRandomDeck(player.getDeck()));
        enemy.setDeck(getRandomDeck((enemy.getDeck())));

        //setting initial hands
        for(int i = 0; i < 5; i++){
            Card card = player.getDeck().getCards().get(i);
            if(card != null) {
                player.getHand().getCards().add(card);
                player.getDeck().getCards().remove(i);
            }
            card = enemy.getDeck().getCards().get(i);
            if(card != null) {
                enemy.getHand().getCards().add(card);
                enemy.getDeck().getCards().remove(i);
            }
        }

        ui.showBattleStart(enemy);

        isGameOver = false;

        //affecting amulets
        if(player.getAmulet() != null) {
            player.setMaxHP(player.getMaxHP() + player.getAmulet().getMaxHP_Effect());
            player.setHP(player.getMaxHP());
            player.setMaxMP(player.getMaxMP() + player.getAmulet().getMaxMP_Effect());
        }
//        if (enemy.getAmulet() != null) {
//            enemy.setMaxHP(enemy.getMaxHP() + enemy.getAmulet().getMaxHP_Effect());
//            enemy.setHP(enemy.getMaxHP());
//            enemy.setMaxMP(enemy.getMaxMP() + enemy.getAmulet().getMaxMP_Effect());
//        }

        //setting turn
        if(turn == 0) {
            ui.showBattleTurn(player);
            round = 1;
            startRound();
        } else {
            round = 2;
            ui.showBattleTurn(enemy);
        }
    }

    private void setCard(Card card){

        if(card instanceof MonsterCard) {
            MonsterCard monsterCard = (MonsterCard) card;

            // Conditions
            if(player.getMonsterField().getCards().size() == 5) {
                return;
            }
            if(card.getMP() > player.MP) {
                ui.showOutput("\n> " + Dictionary.NOT_ENOUGH_MP);
                return;
            }

            // Conditions passed and putting the card on the field
            player.MP -= card.getMP();
            player.updateAvatar();

            // removing the card from hand and adding it to monster field
            player.getMonsterField().getCards().add(card);
            updateMonsterCardsBoxes();
            player.getHand().getCards().remove(card);
            showHand();

            // running BattleCry
            if(card instanceof General) {
                castSpell(((General) card).getBattleCry(), card.getName());
            } else if(card instanceof Hero) {
                castSpell(((Hero) card).getBattleCry(), card.getName());
            }

            monsterCard.setRound(0);

            // casting aura spell on it
            for(AuraSpell auraSpell : player.getAuraSpells()){
                for(Spell spell : auraSpell.spellPack.getSpells()){
                    spell.castSpellOn(monsterCard, player, enemy);
                }
            }

        } else if(card instanceof SpellCard) {
            SpellCard spellCard = (SpellCard) card;
            if(player.getSpellField().getCards().size() == 3) {
                ui.showFullSpellField();
                return;
            }
            if(spellCard.getMP() > player.MP) {
                ui.showOutput("\n> " + Dictionary.NOT_ENOUGH_MP);
                return;
            }
            player.MP -= spellCard.getMP();
            player.updateAvatar();

            // handling instant spells
            if(spellCard instanceof InstantSpell) {
                castSpell(spellCard.spellPack, spellCard.getName());
                player.getGraveYard().getCards().add(spellCard);
                player.getHand().getCards().remove(card);
                showHand();
                return;
            }

            // handling aura spells
            if(spellCard instanceof AuraSpell) {
                castSpell(spellCard.spellPack, spellCard.getName());
            }

            // removing the card from hand and putting it to spell field
            player.getSpellField().getCards().add(card);
            updateSpellCardsBoxes();
            player.getHand().getCards().remove(card);
            showHand();
        }
        client.sendPlayer(enemy);
        client.sendPlayer(player);
    }

    private GamePlace getRandomDeck(GamePlace deck){
        ArrayList<Card> cards = deck.getCards();
        ArrayList<Card> randomCards = new ArrayList<>();

        Random random = new Random();
        while(cards.size() > 0) {
            int index = random.nextInt(cards.size());
            Card card = cards.get(index);
            randomCards.add(card);
            cards.remove(index);
        }
        GamePlace ret = new GamePlace();
        ret.setCards(randomCards);
        return ret;
    }

    void startRound(){
        //server.getClientThreads().get(0).sendPlayers();
        ui.showBattleRound(round);
        ui.showTurn(player);
        player.MP = Math.min((round + 1) / 2, player.getMaxMP());
        player.updateAvatar();

        if(player.getDeck().getCards().size() > 0 && player.getHand().getCards().size() < 10) {
            Card card = player.getDeck().getCards().get(0);
            player.getHand().getCards().add(card);
            player.getDeck().getCards().remove(0);
            if(handButtonImageView.getImage().equals(Dictionary.HAND_BUTTON_ON))
                showHand();
        }
        ui.showMPStats(player);
        startRoundContd(player);

        round += 2;
    }

    private void startRoundContd(Player player){
        //waking up and updating monsterCards
        ArrayList<Card> monsterCards = player.getMonsterField().getCards();
        for(int i = 0; i < monsterCards.size(); i++){
            MonsterCard monsterCard = (MonsterCard) monsterCards.get(i);
            monsterCard.setRound(monsterCard.getRound() + 1);
            monsterCard.hasAttacked = false;
        }

        //affecting continuous Spells
        ArrayList<ContinuousSpell> Cspells = player.getContinuousSpells();
        for(int i = 0; i < Cspells.size(); i++){
            ContinuousSpell continuousSpell = Cspells.get(i);
            client.sendInfo(ui.showSpellInfo(continuousSpell.getName(), continuousSpell.getInfo()));
            castSpell(continuousSpell.spellPack, continuousSpell.getName());

            if(this.player.getHP() <= 0)
                gameOver(false);
            if(enemy.getHP() <= 0)
                gameOver(true);
            handleDeadCards(this.player);
            handleDeadCards(this.enemy);
        }
        client.sendPlayer(enemy);
        client.sendPlayer(player);
    }

    private void castSpell(SpellPack spellPack, String name){
        boolean hasSelective = false;
        Spell selectiveSpell = null;
        for(int i = 0; i < spellPack.getSpells().size(); i++){
            Spell spell = spellPack.getSpells().get(i);
            if(spell.getSpellType() == SpellType.SELECTIVE) {
                hasSelective = true;
                selectiveSpell = spell;
            }
        }

        if(hasSelective) {
            isCastingSpell = true;
            isFinishedCasting = false;
            showTargets(selectiveSpell, spellPack, name);
            return;
        }

        affectSpellCasting(spellPack);
        client.sendPlayer(enemy);
        client.sendPlayer(player);
    }

    private void affectSpellCasting(SpellPack spellPack){
        for(int i = 0; i < spellPack.getSpells().size(); i++){
            Spell spell = spellPack.getSpells().get(i);
            switch(spell.getSpellType()) {
                case RANDOM:
                    spell.castSpellToRandom(player, enemy);
                    break;
                case ALL:
                    spell.castSpellToAll(player, enemy);
                    break;
                case SELECTIVE:
                    spell.castSpellToSelected(player, enemy, spellTargetsArray, false, false);
                    break;
            }
        }

        //checking for dead Players
        if(player.getHP() <= 0)
            gameOver(false);
        if(enemy.getHP() <= 0)
            gameOver(true);

        //checking for dead cards
        handleDeadCards(this.player);
        handleDeadCards(this.enemy);

        player.updateAvatar();
        enemy.updateAvatar();

        client.sendPlayer(enemy);
        client.sendPlayer(player);
    }

    private void handleDeadCards(Player player){
        GamePlace monsterField = player.getMonsterField();
        for(int i = 0; i < monsterField.getCards().size(); i++){
            MonsterCard monsterCard = (MonsterCard) monsterField.getCards().get(i);
            if(monsterCard.getHP() <= 0) {
                if(monsterCard instanceof General) {
                    General general = (General) monsterCard;
                    castSpell(general.getWill(), general.getName());
                } else if(monsterCard instanceof Hero) {
                    Hero hero = (Hero) monsterCard;
                    castSpell(hero.getWill(), hero.getName());
                }

                monsterField.getCards().remove(i);
                monsterCard = monsterCard.getResetCopy();
                player.getGraveYard().getCards().add(monsterCard);
                i = 0;
            }
        }
        updateMonsterCardsBoxes();
        updateSpellCardsBoxes();
        if(graveyardButtonImageView.getImage().equals(Dictionary.GRAVEYARD_BUTTON_ON)) {
            showGraveyard();
        }
    }

    private void useItem(Item item){
        player.setHP(Math.min(player.getMaxHP(), player.getHP() + item.getHP_Effect()));
        player.MP = Math.min(player.getMaxMP(), player.MP + item.getMP_Effect());
        player.updateAvatar();
        player.getItems().remove(item);
        player.getInventory().getItems().remove(item);
        showItems();
    }

    private void runSpelledAttackMenu(MonsterCard pMonsterCard, MonsterCard eMonsterCard, Player enemy, String info){
        if(pMonsterCard.getRound() == 0 && !pMonsterCard.isNimble) {
            ui.showOutput("\n> " + pMonsterCard.getName() + " is sleeping. zZ");
            return;
        }

        if(eMonsterCard == null && enemy != null) {
            if(enemy.getDefenderCards().size() != 0) {
                ui.showOutput("\n> A defender card is on the way");
                return;
            }
            if(pMonsterCard.hasAttacked) {
                ui.showOutput("\n> A card can attack only once in each turn.");
                return;
            }

            //checking for a decrease of incoming damages by an amulet
            if(enemy.getAmulet() != null && enemy.getAmulet().getArmorEffect() != 0) {
                int armorEffect = enemy.getAmulet().getArmorEffect();
                enemy.setHP(enemy.getHP() - pMonsterCard.getAP() * (100 - armorEffect) / 100);
            } else
                pMonsterCard.attack(enemy);

            pMonsterCard.hasAttacked = true;

            //updating gfx
            enemy.updateAvatar();
            String display = "\n> " + pMonsterCard.getName() + " attacked " + enemy.getName();
            ui.showOutput(display);
            client.sendInfo(display);

            selectedPMonsterCard = null;
            useCardButton.setImage(Dictionary.USE_CARD_BUTTON);
            isUsingCard = false;
        } else {
            if(pMonsterCard.hasAttacked) {
                ui.showOutput("\n> A card can attack only once in each turn.");
                return;
            }

            //Attacking
            ArrayList<MonsterCard> defenderCards = enemy.getDefenderCards();
            if(defenderCards.size() != 0) {
                if(!defenderCards.contains(eMonsterCard)) {
                    ui.showOutput("\n> A defender card is on the way.");
                    return;
                }
            }

            //checking for a decrease of incoming damages by an amulet
            if(enemy.getAmulet() != null && enemy.getAmulet().getArmorEffect() != 0) {
                int armorEffect = enemy.getAmulet().getArmorEffect();
                int hp = eMonsterCard.getHP() - pMonsterCard.getAP() * (100 - armorEffect) / 100;
                pMonsterCard.attack(eMonsterCard);
                eMonsterCard.setHP(hp);
            } else
                pMonsterCard.attack(eMonsterCard);

            pMonsterCard.hasAttacked = true;

            //updating gfx
            String display = "\n> " + pMonsterCard.getName() + " attacked " + eMonsterCard.getName();
            ui.showOutput(display);
            client.sendInfo(display);

            pMonsterCard.updateGFX();
            eMonsterCard.updateGFX();

            selectedPMonsterCard = null;
            useCardButton.setImage(Dictionary.USE_CARD_BUTTON);
            isUsingCard = false;
        }
        //checking for dead Players
        if(player.getHP() <= 0)
            gameOver(false);
        if(enemy.getHP() <= 0)
            gameOver(true);

        //Handling dead cards
        handleDeadCards(this.player);
        handleDeadCards(this.enemy);

        client.sendPlayer(enemy);
        client.sendPlayer(player);
    }

    private void gameOver(boolean result){
        //result = true : win
        //result = false : lose
        isGameOver = true;
        player.setHP(20000);
        enemy.setHP(20000);

        //reseting decks
        player.getPlaces().put(Field.DECK, playerDeckCopy);
        enemy.getPlaces().put(Field.DECK, enemyDeckCopy);

        player.setHand(new GamePlace());
        enemy.setHand(new GamePlace());

        player.setMonsterField(new GamePlace());
        enemy.setMonsterField(new GamePlace());

        player.setSpellField(new GamePlace());
        enemy.setSpellField(new GamePlace());

        player.setGraveYard(new GamePlace());
        enemy.setGraveYard(new GamePlace());

        player.MP = 0;
        enemy.MP = 0;

        player.updateAvatar();
        enemy.updateAvatar();
        updateSpellCardsBoxes();
        updateMonsterCardsBoxes();

        //GFX
        Rectangle backGND = new Rectangle(scene.getWidth(), scene.getHeight());
        backGND.setFill(Color.BLACK);
        backGND.setOpacity(0.75);
        root.getChildren().add(backGND);

        Rectangle menu = new Rectangle(scene.getWidth()/4, scene.getHeight()/4);
        menu.setArcHeight(15);
        menu.setArcWidth(15);
        menu.setFill(Color.ORANGE);
        menu.setLayoutX(3 * scene.getWidth()/8);
        menu.setLayoutY(3 * scene.getHeight()/8);
        root.getChildren().add(menu);

        Text text = new Text();
        text.setFont(new Font(18));
        text.setTextAlignment(TextAlignment.CENTER);
        text.setLayoutX(3 * scene.getWidth()/8);
        text.setLayoutY(3 * scene.getHeight()/8 + 20);
        text.setWrappingWidth(scene.getWidth()/4);
        root.getChildren().add(text);

        ImageView okBTN = new ImageView(Dictionary.OK_BTN);
        okBTN.setFitWidth(100);
        okBTN.setFitHeight(50);
        okBTN.setLayoutX(scene.getWidth()/2 - okBTN.getFitWidth()/2);
        okBTN.setLayoutY(5 * scene.getHeight()/8 - okBTN.getFitHeight());
        okBTN.setOnMouseEntered(event -> {
            okBTN.setImage(Dictionary.OK_BTN_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        okBTN.setOnMouseExited(event -> {
            okBTN.setImage(Dictionary.OK_BTN);
            scene.setCursor(Cursor.DEFAULT);
        });
        root.getChildren().add(okBTN);

        if(result) {
            text.setFill(Color.GREEN);
            player.setCredit(player.getCredit() + 3000);
            text.setText("You Won!\n" +
                    "You Earn" +3000 + " Gills!!!\n" +
                    "You Will get back to MainMenu!");
            okBTN.setOnMouseClicked(event -> {
                Dictionary.playClickSound();
                mainMenu.run();
            });
        } else {
            text.setFill(Color.RED);
            ArrayList<Item> temp = new ArrayList<>();
            for(Item item : playerItemsCopy)
                temp.add(item.getCopy());
            player.setItems(temp);

            ArrayList<Item> temp2 = new ArrayList<>();
            for(Item item : enemyItemsCopy)
                temp2.add(item.getCopy());
            enemy.setItems(temp2);


            text.setText("You Loose :(\n" +
                    "Game Over!");

            okBTN.setOnMouseClicked(event -> {
                Dictionary.playClickSound();
                System.exit(0);
            });
        }
    }

    public void setStage(Stage stage){
        this.stage = stage;
    }

    public void setClient(Client client){
        this.client = client;
    }

    public Client getClient(){
        return client;
    }

    public void setTurn(int turn){
        this.turn = turn;
    }

    public UI getUi(){
        return ui;
    }

    public void setGameManager(GameManager gameManager){
        this.gameManager = gameManager;
    }
}