package network.Peer_to_Peer;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * This class defines the different type of messages that will be exchanged between the
 * Clients and the Server.
 * When talking from a Java Client to a Java Server a lot easier to pass Java objects, no
 * need to count bytes or to wait for a line feed at the end of the frame
 */
public class ChatMessage implements Serializable {

    protected static final long serialVersionUID = 1112122200L;

    // The different types of message sent by the Client
    // WHOISIN to receive the list of the users connected
    // MESSAGE an ordinary message
    // LOGOUT to disconnect from the Server
    static final int WHOISIN = 0, MESSAGE = 1, LOGOUT = 2;
    private int type;
    private String message;
    private ArrayList<String> messageList = new ArrayList<>();

    // constructor
    public ChatMessage(int type, String message){
        this.type = type;
        this.message = message;
        if(message != null) {
            String[] temp = message.split(",");
            for(int i = 0; i < temp.length; i++)
                messageList.add(temp[i]);
        }
    }

    // getters
    int getType(){
        return type;
    }

    String getMessage(){
        return message;
    }

    public ArrayList<String> getMessageList(){
        return messageList;
    }
}
