package network.Peer_to_Peer;

import enums.Field;
import gameObjects.Amulet;
import gameObjects.Item;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.MonsterCard;
import javafx.application.Platform;
import main.GamePlace;
import main.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

/*
 * The Client that can be run both as a console or a GUI
 */
public class Client extends Thread {
    private ClientBattle clientBattle;
    // for I/O
    private ObjectInputStream sInput;        // to read from the socket
    private ObjectOutputStream sOutput;        // to write on the socket
    private Socket socket;

    // the server, the port and the username
    private String server;
    private int port;

    public Client(String server, int port, ClientBattle clientBattle){
        this.server = server;
        this.port = port;
        this.clientBattle = clientBattle;
    }

    /*
     * To start the dialog
     */
    public void run(){
        // try to connect to the server
        try {
            socket = new Socket(server, port);
        }
        // if it failed not much I can so
        catch (Exception ec) {
            //ec.printStackTrace();
            display("Error connecting to server:" + ec);
            return;
        }

        String msg = "Connection accepted " + socket.getInetAddress() + ":" + socket.getPort();
        display(msg);

        /* Creating both Data Stream */
        try {
            sInput = new ObjectInputStream(socket.getInputStream());
            sOutput = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException eIO) {
            display("Exception creating new Input/output Streams: " + eIO);
            return;
        }
        // creates the Thread to listen from the server
        ListenToServer listen = new ListenToServer();
        listen.setDaemon(true);
        listen.start();

        // Send our username to the server this is the only message that we
        // will send as a String. All other messages will be ChatMessage objects
        try {
            sendPlayer(clientBattle.getPlayer());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * To send a message to the console or the GUI
     */
    private void display(String msg){
        System.out.println(msg);
    }

    /*
     * When something goes wrong
     * Close the Input/Output streams and disconnect not much to do in the catch clause
     */
    private void disconnect(){
        try {
            if(sInput != null) sInput.close();
        } catch (Exception e) {
        } // not much else I can do
        try {
            if(sOutput != null) sOutput.close();
        } catch (Exception e) {
        } // not much else I can do
        try {
            if(socket != null) socket.close();
        } catch (Exception e) {
        } // not much else I can do

        // inform the GUI
//        if(cg != null)
//            cg.connectionFailed();

    }

    /*
     * a class that waits for the message from the server and append them to the JTextArea
     * if we have a GUI or simply System.out.println() it in console mode
     */
    class ListenToServer extends Thread {

        public void run(){
            try {
                Object object = sInput.readObject();
                updateServerPlayer(object);
                //set turn
                clientBattle.setTurn((Integer) sInput.readObject());
                Platform.runLater(() -> {
                    clientBattle.run();
                });
            } catch (Exception e) {
                //display("Exception doing login : " + e);
            }

            Object object;
            while(true) {
                try {
                    object = sInput.readObject();
                    if(object instanceof Boolean) {
                        if((Boolean) object) {
                            clientBattle.setTurn(0);
                            Platform.runLater(() -> {
                                clientBattle.startRound();
                            });
                        }
                    } else if(object instanceof Integer) {
                        updateServerPlayer(object);
                        updateClientPlayer();

                        Platform.runLater(() -> {
                            //clientBattle.setupScene();
                            clientBattle.updateMonsterCardsBoxes();
                            clientBattle.updateSpellCardsBoxes();
                        });
                    } else if(object instanceof String)
                        displayInfo(object);
                } catch (IOException e) {
                    display("Server has close the connection: " + e);
//                    if(cg != null)
//                        cg.connectionFailed();
                    break;
                }
                // can't happen with a String object but need the catch anyhow
                catch (ClassNotFoundException e2) {
                }
            }
        }
    }

    private void updateClientPlayer(){
        try {
            clientBattle.getPlayer().setHP((Integer) sInput.readObject());
            clientBattle.getPlayer().setName((String) sInput.readObject());
            clientBattle.getPlayer().MP = (Integer) sInput.readObject();
            clientBattle.getPlayer().setMaxMP((Integer) sInput.readObject());
            clientBattle.getPlayer().setAmulet((Amulet) sInput.readObject());
            //Items
            int size = (Integer) sInput.readObject();
            ArrayList<Item> itemArrayList = new ArrayList<>();
            for(int i = 0; i < size; i++)
                itemArrayList.add((Item) sInput.readObject());
            clientBattle.getPlayer().setItems(itemArrayList);
            //GamePlaces
            //Deck
            size = (Integer) sInput.readObject();
            ArrayList<Card> deckCardArrayList = new ArrayList<>();
            GamePlace deck = new GamePlace();
            for(int i = 0; i < size; i++)
                deckCardArrayList.add((Card) sInput.readObject());
            deck.setCards(deckCardArrayList);
            clientBattle.getPlayer().setDeck(deck);
            //Hand
            size = (Integer) sInput.readObject();
            ArrayList<Card> handCardArrayList = new ArrayList<>();
            GamePlace hand = new GamePlace();
            for(int i = 0; i < size; i++)
                handCardArrayList.add((Card) sInput.readObject());
            hand.setCards(handCardArrayList);
            clientBattle.getPlayer().setHand(hand);
            //GraveYard
            size = (Integer) sInput.readObject();
            ArrayList<Card> graveYardCardArrayList = new ArrayList<>();
            GamePlace graveYard = new GamePlace();
            for(int i = 0; i < size; i++)
                graveYardCardArrayList.add((Card) sInput.readObject());
            graveYard.setCards(graveYardCardArrayList);
            clientBattle.getPlayer().setGraveYard(graveYard);
            //MonsterField
            size = (Integer) sInput.readObject();
            ArrayList<Card> monsterFieldCardArrayList = new ArrayList<>();
            GamePlace monsterField = new GamePlace();
            for(int i = 0; i < size; i++){
                Card card = (Card) sInput.readObject();
                ((MonsterCard) card).setHP((Integer) sInput.readObject());
                ((MonsterCard) card).setAP((Integer) sInput.readObject());
                monsterFieldCardArrayList.add(card);
            }
            monsterField.setCards(monsterFieldCardArrayList);
            clientBattle.getPlayer().setMonsterField(monsterField);
            //SpellField
            size = (Integer) sInput.readObject();
            ArrayList<Card> spellFieldCardArrayList = new ArrayList<>();
            GamePlace spellField = new GamePlace();
            for(int i = 0; i < size; i++)
                spellFieldCardArrayList.add((Card) sInput.readObject());
            spellField.setCards(spellFieldCardArrayList);
            clientBattle.getPlayer().setSpellField(spellField);
            //Inventory
            //Item
            size = (Integer) sInput.readObject();
            ArrayList<Item> inventoryItemArrayList = new ArrayList<>();
            for(int i = 0; i < size; i++)
                inventoryItemArrayList.add((Item) sInput.readObject());
            clientBattle.getPlayer().getInventory().setItems(inventoryItemArrayList);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Platform.runLater(() -> {
            clientBattle.getPlayer().updateAvatar();
            for(Card c : clientBattle.getPlayer().getDeck().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getPlayer().getHand().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getPlayer().getGraveYard().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getPlayer().getMonsterField().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getPlayer().getSpellField().getCards()){
                c.setupGraphics();
            }
            for(Item item : clientBattle.getPlayer().getInventory().getItems())
                item.updateGraphics();
            for(Amulet amulet : clientBattle.getPlayer().getInventory().getAmulets())
                amulet.updateGraphics();
        });
    }

    private void updateServerPlayer(Object object){
        try {
            clientBattle.getEnemy().setHP((Integer) object);
            clientBattle.getEnemy().setName((String) sInput.readObject());
            clientBattle.getEnemy().MP = (Integer) sInput.readObject();
            clientBattle.getEnemy().setMaxMP((Integer) sInput.readObject());
            clientBattle.getEnemy().setAmulet((Amulet) sInput.readObject());
            //Items
            int size = (Integer) sInput.readObject();
            ArrayList<Item> itemArrayList = new ArrayList<>();
            for(int i = 0; i < size; i++)
                itemArrayList.add((Item) sInput.readObject());
            clientBattle.getEnemy().setItems(itemArrayList);
            //GamePlaces
            //Deck
            size = (Integer) sInput.readObject();
            ArrayList<Card> deckCardArrayList = new ArrayList<>();
            GamePlace deck = new GamePlace();
            for(int i = 0; i < size; i++)
                deckCardArrayList.add((Card) sInput.readObject());
            deck.setCards(deckCardArrayList);
            clientBattle.getEnemy().setDeck(deck);
            //Hand
            size = (Integer) sInput.readObject();
            ArrayList<Card> handCardArrayList = new ArrayList<>();
            GamePlace hand = new GamePlace();
            for(int i = 0; i < size; i++)
                handCardArrayList.add((Card) sInput.readObject());
            hand.setCards(handCardArrayList);
            clientBattle.getEnemy().setHand(hand);
            //GraveYard
            size = (Integer) sInput.readObject();
            ArrayList<Card> graveYardCardArrayList = new ArrayList<>();
            GamePlace graveYard = new GamePlace();
            for(int i = 0; i < size; i++)
                graveYardCardArrayList.add((Card) sInput.readObject());
            graveYard.setCards(graveYardCardArrayList);
            clientBattle.getEnemy().setGraveYard(graveYard);
            //MonsterField
            size = (Integer) sInput.readObject();
            ArrayList<Card> monsterFieldCardArrayList = new ArrayList<>();
            GamePlace monsterField = new GamePlace();
            for(int i = 0; i < size; i++){
                Card card = (Card) sInput.readObject();
                ((MonsterCard) card).setHP((Integer) sInput.readObject());
                ((MonsterCard) card).setAP((Integer) sInput.readObject());
                monsterFieldCardArrayList.add(card);
            }
            monsterField.setCards(monsterFieldCardArrayList);
            clientBattle.getEnemy().setMonsterField(monsterField);
            //SpellField
            size = (Integer) sInput.readObject();
            ArrayList<Card> spellFieldCardArrayList = new ArrayList<>();
            GamePlace spellField = new GamePlace();
            for(int i = 0; i < size; i++)
                spellFieldCardArrayList.add((Card) sInput.readObject());
            spellField.setCards(spellFieldCardArrayList);
            clientBattle.getEnemy().setSpellField(spellField);
            //Inventory
            //Item
            size = (Integer) sInput.readObject();
            ArrayList<Item> inventoryItemArrayList = new ArrayList<>();
            for(int i = 0; i < size; i++)
                inventoryItemArrayList.add((Item) sInput.readObject());
            clientBattle.getEnemy().getInventory().setItems(inventoryItemArrayList);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Platform.runLater(() -> {
            clientBattle.getEnemy().updateAvatar();
            for(Card c : clientBattle.getEnemy().getDeck().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getEnemy().getHand().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getEnemy().getGraveYard().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getEnemy().getMonsterField().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getEnemy().getSpellField().getCards()){
                c.setupGraphics();
            }
            for(Item item : clientBattle.getEnemy().getInventory().getItems())
                item.updateGraphics();
            for(Amulet amulet : clientBattle.getEnemy().getInventory().getAmulets())
                amulet.updateGraphics();
        });
    }

    void sendPlayer(Player player){
        try {
            sOutput.writeObject(player.getHP());
            sOutput.writeObject(player.getName());
            sOutput.writeObject(player.MP);
            sOutput.writeObject(player.getMaxMP());
            sOutput.writeObject(player.getAmulet());
            //items
            sOutput.writeObject(player.getItems().size());
            for(Item item : player.getItems())
                sOutput.writeObject(item);
            //GamePlaces
            //Deck
            sOutput.writeObject(player.getPlaces().get(Field.DECK).getCards().size());
            for(Card card : player.getPlaces().get(Field.DECK).getCards())
                sOutput.writeObject(card);
            //Hand
            sOutput.writeObject(player.getPlaces().get(Field.HAND).getCards().size());
            for(Card card : player.getPlaces().get(Field.HAND).getCards())
                sOutput.writeObject(card);
            //GraveYard
            sOutput.writeObject(player.getPlaces().get(Field.GRAVEYARD).getCards().size());
            for(Card card : player.getPlaces().get(Field.GRAVEYARD).getCards())
                sOutput.writeObject(card);
            //MonsterField
            sOutput.writeObject(player.getPlaces().get(Field.MONSTERFIELD).getCards().size());
            for(Card card : player.getPlaces().get(Field.MONSTERFIELD).getCards()){
                sOutput.writeObject(card);
                sOutput.writeObject(((MonsterCard) card).getHP());
                sOutput.writeObject(((MonsterCard) card).getAP());
            }
            //SpellField
            sOutput.writeObject(player.getPlaces().get(Field.SPELLFIELD).getCards().size());
            for(Card card : player.getPlaces().get(Field.SPELLFIELD).getCards())
                sOutput.writeObject(card);
            //Inventory
            //Item
            sOutput.writeObject(player.getInventory().getItems().size());
            for(Item item : player.getInventory().getItems())
                sOutput.writeObject(item);
        } catch (IOException e) {
            display("Exception writing to server: " + e);

        }
    }

    void sendInfo(String string){
        try {
            sOutput.writeObject(string);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void displayInfo(Object object){
        clientBattle.getUi().showOutput((String) object);
    }

    void trigger(){
        try {
            sOutput.writeObject(Boolean.TRUE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setPort(int port){
        this.port = port;
    }

    public void setServer(String server){
        this.server = server;
    }
}
