package network.Peer_to_Peer;

import enums.Field;
import gameObjects.Amulet;
import gameObjects.Item;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.MonsterCard;
import javafx.application.Platform;
import main.GamePlace;
import main.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/*
 * The server that can be run both as a console application or a GUI
 */
public class Server extends Thread {
    private ServerBattle serverBattle;
    // a unique ID for each connection
    private static int uniqueId;
    // an ArrayList to keep the list of the Client
    private ArrayList<ClientThread> al;
    // the port number to listen for connection
    private int port;
    // the boolean that will be turned of to stop the server
    private boolean keepGoing;

    /*
     *  server constructor that receive the port to listen to for connection as parameter
     *  in console
     */
    public Server(int port, ServerBattle serverBattle){
        this.serverBattle = serverBattle;
        // the port
        this.port = port;
        // ArrayList for the Client list
        al = new ArrayList<ClientThread>();
    }

    public void run(){
        keepGoing = true;
        /* create socket server and wait for connection requests */
        try {
            // the socket used by the server
            ServerSocket serverSocket = new ServerSocket(port);

            // infinite loop to wait for connections
            while(keepGoing) {
                // format message saying we are waiting
                display("Server waiting for Clients on port " + port + ".");

                Socket socket = serverSocket.accept();
                // if I was asked to stop
                if(!keepGoing)
                    break;
                ClientThread t = new ClientThread(socket);  // make a thread of it
                t.setDaemon(true);
                al.add(t);                                    // save it in the ArrayList
                t.start();
            }
            // I was asked to stop
            try {
                serverSocket.close();
                for(int i = 0; i < al.size(); ++i){
                    ClientThread tc = al.get(i);
                    try {
                        tc.sInput.close();
                        tc.sOutput.close();
                        tc.socket.close();
                    } catch (IOException ioE) {
                        // not much I can do
                    }
                }
            } catch (Exception e) {
                display("Exception closing the server and clients: " + e);
            }
        }
        // something went bad
        catch (IOException e) {
            String msg = " Exception on new ServerSocket: " + e + "\n";
            display(msg);
        }
    }

    /*
     * For the GUI to stop the server
     */
//    public void stop() {
//        keepGoing = false;
//        // connect to myself as Client to exit statement
//        // Socket socket = serverSocket.accept();
//        try {
//            new Socket("localhost", port);
//        }
//        catch(Exception e) {
//            // nothing I can really do
//        }
//    }
    /*
     * Display an event (not a message) to the console or the GUI
     */
    private void display(String msg){
        System.out.println(msg);
    }
    /*
     *  to broadcast a message to all Clients
     */
    /*private synchronized void broadcast(Player player) {
        // display message on console or GUI


        // we loop in reverse order in case we would have to remove a Client
        // because it has disconnected
        for(int i = al.size(); --i >= 0;) {
            ClientThread ct = al.get(i);
            // try to write to the Client if it fails remove it from the list
            if(!ct.sendPlayer(player)) {
                al.remove(i);
                display("Disconnected Client ");
            }
        }
    }*/

    // for a client who logoff using the LOGOUT message
    synchronized void remove(int id){
        // scan the array list until we found the Id
        for(int i = 0; i < al.size(); ++i){
            ClientThread ct = al.get(i);
            // found it
            if(ct.id == id) {
                al.remove(i);
                return;
            }
        }
    }


    /**
     * One instance of this thread will run for each client
     */
    class ClientThread extends Thread {
        // the socket where to listen/talk
        Socket socket;
        ObjectInputStream sInput;
        ObjectOutputStream sOutput;
        // my unique id (easier for disconnection)
        int id;
        // the date I connect
        String date;

        // Constructor
        ClientThread(Socket socket){
            // a unique id
            id = ++uniqueId;
            this.socket = socket;
            /* Creating both Data Stream */
            System.out.println("Thread trying to create Object Input/Output Streams");
            try {
                // create output first
                sOutput = new ObjectOutputStream(socket.getOutputStream());
                sInput = new ObjectInputStream(socket.getInputStream());
                //get client player
                updateClientPlayer();

                Platform.runLater(() -> {
                    serverBattle.run();
                });
            } catch (IOException e) {
                e.printStackTrace();
                //display("Exception creating new Input/output Streams: " + e);
                return;
            }
            // have to catch ClassNotFoundException
            // but I read a String, I am sure it will work
            date = new Date().toString() + "\n";
        }

        // what will run forever
        public void run(){
            //send server player
            try {
                sendPlayer(serverBattle.getPlayer());
                //set turns
                Random random = new Random();
                int turn = random.nextInt(2);
                serverBattle.setTurn(turn);
                sOutput.writeObject(1 - turn);

            } catch (Exception e) {
            }

            // to loop until LOGOUT
            boolean keepGoing = true;
            Object object;
            while(keepGoing) {
                // read a String (which is an object)
                try {
                    object = sInput.readObject();
                    if(object instanceof Boolean) {
                        serverBattle.setTurn(0);
                        if((Boolean) object) {
                            Platform.runLater(() -> {
                                serverBattle.startRound();
                            });
                        }
                    } else if(object instanceof Integer) {
                        updateServerPlayer(object);
                        updateClientPlayer();

                        Platform.runLater(() -> {
                            //serverBattle.setupScene();
                            serverBattle.updateMonsterCardsBoxes();
                            serverBattle.updateSpellCardsBoxes();
                        });
                    } else if(object instanceof String)
                        displayInfo(object);
                } catch (IOException e) {
                    display(" Exception reading Streams: " + e);
                    break;
                } catch (ClassNotFoundException e2) {
                    break;
                }
                // the message part of the ChatMessage
            }
            // remove myself from the arrayList containing the list of the
            // connected Clients
            remove(id);
            close();
        }

        // try to close everything
        private void close(){
            // try to close the connection
            try {
                if(sOutput != null) sOutput.close();
            } catch (Exception e) {
            }
            try {
                if(sInput != null) sInput.close();
            } catch (Exception e) {
            }
            ;
            try {
                if(socket != null) socket.close();
            } catch (Exception e) {
            }
        }

        /*
         * Write a String to the Client output stream
         */
        void sendPlayer(Player player){
            try {
                sOutput.writeObject(player.getHP());
                sOutput.writeObject(player.getName());
                sOutput.writeObject(player.MP);
                sOutput.writeObject(player.getMaxMP());
                sOutput.writeObject(player.getAmulet());
                //items
                sOutput.writeObject(player.getItems().size());
                for(Item item : player.getItems())
                    sOutput.writeObject(item);
                //GamePlaces
                //Deck
                sOutput.writeObject(player.getPlaces().get(Field.DECK).getCards().size());
                for(Card card : player.getPlaces().get(Field.DECK).getCards())
                    sOutput.writeObject(card);
                //Hand
                sOutput.writeObject(player.getPlaces().get(Field.HAND).getCards().size());
                for(Card card : player.getPlaces().get(Field.HAND).getCards())
                    sOutput.writeObject(card);
                //GraveYard
                sOutput.writeObject(player.getPlaces().get(Field.GRAVEYARD).getCards().size());
                for(Card card : player.getPlaces().get(Field.GRAVEYARD).getCards())
                    sOutput.writeObject(card);
                //MonsterField
                sOutput.writeObject(player.getPlaces().get(Field.MONSTERFIELD).getCards().size());
                for(Card card : player.getPlaces().get(Field.MONSTERFIELD).getCards()){
                    sOutput.writeObject(card);
                    sOutput.writeObject(((MonsterCard) card).getHP());
                    sOutput.writeObject(((MonsterCard) card).getAP());
                }
                //SpellField
                sOutput.writeObject(player.getPlaces().get(Field.SPELLFIELD).getCards().size());
                for(Card card : player.getPlaces().get(Field.SPELLFIELD).getCards())
                    sOutput.writeObject(card);
                //Inventory
                //Item
                sOutput.writeObject(player.getInventory().getItems().size());
                for(Item item : player.getInventory().getItems())
                    sOutput.writeObject(item);
            } catch (IOException e) {
                display("Exception writing to server: " + e);

            }
        }

        void sendInfo(String string){
            try {
                sOutput.writeObject(string);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void displayInfo(Object object){
            serverBattle.getUi().showOutput((String) object);
        }

        private void updateServerPlayer(Object object){
            try {
                serverBattle.getPlayer().setHP((Integer) object);
                serverBattle.getPlayer().setName((String) sInput.readObject());
                serverBattle.getPlayer().MP = (Integer) sInput.readObject();
                serverBattle.getPlayer().setMaxMP((Integer) sInput.readObject());
                serverBattle.getPlayer().setAmulet((Amulet) sInput.readObject());
                //Items
                int size = (Integer) sInput.readObject();
                ArrayList<Item> itemArrayList = new ArrayList<>();
                for(int i = 0; i < size; i++)
                    itemArrayList.add((Item) sInput.readObject());
                serverBattle.getPlayer().setItems(itemArrayList);
                //GamePlaces
                //Deck
                size = (Integer) sInput.readObject();
                ArrayList<Card> deckCardArrayList = new ArrayList<>();
                GamePlace deck = new GamePlace();
                for(int i = 0; i < size; i++)
                    deckCardArrayList.add((Card) sInput.readObject());
                deck.setCards(deckCardArrayList);
                serverBattle.getPlayer().setDeck(deck);
                //Hand
                size = (Integer) sInput.readObject();
                ArrayList<Card> handCardArrayList = new ArrayList<>();
                GamePlace hand = new GamePlace();
                for(int i = 0; i < size; i++)
                    handCardArrayList.add((Card) sInput.readObject());
                hand.setCards(handCardArrayList);
                serverBattle.getPlayer().setHand(hand);
                //GraveYard
                size = (Integer) sInput.readObject();
                ArrayList<Card> graveYardCardArrayList = new ArrayList<>();
                GamePlace graveYard = new GamePlace();
                for(int i = 0; i < size; i++)
                    graveYardCardArrayList.add((Card) sInput.readObject());
                graveYard.setCards(graveYardCardArrayList);
                serverBattle.getPlayer().setGraveYard(graveYard);
                //MonsterField
                size = (Integer) sInput.readObject();
                ArrayList<Card> monsterFieldCardArrayList = new ArrayList<>();
                GamePlace monsterField = new GamePlace();
                for(int i = 0; i < size; i++){
                    Card card = (Card) sInput.readObject();
                    ((MonsterCard) card).setHP((Integer) sInput.readObject());
                    ((MonsterCard) card).setAP((Integer) sInput.readObject());
                    monsterFieldCardArrayList.add(card);
                }
                monsterField.setCards(monsterFieldCardArrayList);
                serverBattle.getPlayer().setMonsterField(monsterField);
                //SpellField
                size = (Integer) sInput.readObject();
                ArrayList<Card> spellFieldCardArrayList = new ArrayList<>();
                GamePlace spellField = new GamePlace();
                for(int i = 0; i < size; i++)
                    spellFieldCardArrayList.add((Card) sInput.readObject());
                spellField.setCards(spellFieldCardArrayList);
                serverBattle.getPlayer().setSpellField(spellField);
                //Inventory
                //Item
                size = (Integer) sInput.readObject();
                ArrayList<Item> inventoryItemArrayList = new ArrayList<>();
                for(int i = 0; i < size; i++)
                    inventoryItemArrayList.add((Item) sInput.readObject());
                serverBattle.getPlayer().getInventory().setItems(inventoryItemArrayList);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                serverBattle.getPlayer().updateAvatar();
                for(Card c : serverBattle.getPlayer().getDeck().getCards()){
                    c.setupGraphics();
                }
                for(Card c : serverBattle.getPlayer().getHand().getCards()){
                    c.setupGraphics();
                }
                for(Card c : serverBattle.getPlayer().getGraveYard().getCards()){
                    c.setupGraphics();
                }
                for(Card c : serverBattle.getPlayer().getMonsterField().getCards()){
                    c.setupGraphics();
                }
                for(Card c : serverBattle.getPlayer().getSpellField().getCards()){
                    c.setupGraphics();
                }
                for(Item item : serverBattle.getPlayer().getInventory().getItems())
                    item.updateGraphics();
                for(Amulet amulet : serverBattle.getPlayer().getInventory().getAmulets())
                    amulet.updateGraphics();
            });
        }

        private void updateClientPlayer(){
            try {
                serverBattle.getEnemy().setHP((Integer) sInput.readObject());
                serverBattle.getEnemy().setName((String) sInput.readObject());
                serverBattle.getEnemy().MP = (Integer) sInput.readObject();
                serverBattle.getEnemy().setMaxMP((Integer) sInput.readObject());
                serverBattle.getEnemy().setAmulet((Amulet) sInput.readObject());
                //Items
                int size = (Integer) sInput.readObject();
                ArrayList<Item> itemArrayList = new ArrayList<>();
                for(int i = 0; i < size; i++)
                    itemArrayList.add((Item) sInput.readObject());
                serverBattle.getEnemy().setItems(itemArrayList);
                //GamePlaces
                //Deck
                size = (Integer) sInput.readObject();
                ArrayList<Card> deckCardArrayList = new ArrayList<>();
                GamePlace deck = new GamePlace();
                for(int i = 0; i < size; i++)
                    deckCardArrayList.add((Card) sInput.readObject());
                deck.setCards(deckCardArrayList);
                serverBattle.getEnemy().setDeck(deck);
                //Hand
                size = (Integer) sInput.readObject();
                ArrayList<Card> handCardArrayList = new ArrayList<>();
                GamePlace hand = new GamePlace();
                for(int i = 0; i < size; i++)
                    handCardArrayList.add((Card) sInput.readObject());
                hand.setCards(handCardArrayList);
                serverBattle.getEnemy().setHand(hand);
                //GraveYard
                size = (Integer) sInput.readObject();
                ArrayList<Card> graveYardCardArrayList = new ArrayList<>();
                GamePlace graveYard = new GamePlace();
                for(int i = 0; i < size; i++)
                    graveYardCardArrayList.add((Card) sInput.readObject());
                graveYard.setCards(graveYardCardArrayList);
                serverBattle.getEnemy().setGraveYard(graveYard);
                //MonsterField
                size = (Integer) sInput.readObject();
                ArrayList<Card> monsterFieldCardArrayList = new ArrayList<>();
                GamePlace monsterField = new GamePlace();
                for(int i = 0; i < size; i++){
                    Card card = (Card) sInput.readObject();
                    ((MonsterCard) card).setHP((Integer) sInput.readObject());
                    ((MonsterCard) card).setAP((Integer) sInput.readObject());
                    monsterFieldCardArrayList.add(card);
                }
                monsterField.setCards(monsterFieldCardArrayList);
                serverBattle.getEnemy().setMonsterField(monsterField);
                //SpellField
                size = (Integer) sInput.readObject();
                ArrayList<Card> spellFieldCardArrayList = new ArrayList<>();
                GamePlace spellField = new GamePlace();
                for(int i = 0; i < size; i++)
                    spellFieldCardArrayList.add((Card) sInput.readObject());
                spellField.setCards(spellFieldCardArrayList);
                serverBattle.getEnemy().setSpellField(spellField);
                //Inventory
                //Item
                size = (Integer) sInput.readObject();
                ArrayList<Item> inventoryItemArrayList = new ArrayList<>();
                for(int i = 0; i < size; i++)
                    inventoryItemArrayList.add((Item) sInput.readObject());
                serverBattle.getEnemy().getInventory().setItems(inventoryItemArrayList);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                serverBattle.getEnemy().updateAvatar();
                for(Card c : serverBattle.getEnemy().getDeck().getCards()){
                    c.setupGraphics();
                }
                for(Card c : serverBattle.getEnemy().getHand().getCards()){
                    c.setupGraphics();
                }
                for(Card c : serverBattle.getEnemy().getGraveYard().getCards()){
                    c.setupGraphics();
                }
                for(Card c : serverBattle.getEnemy().getMonsterField().getCards()){
                    c.setupGraphics();
                }
                for(Card c : serverBattle.getEnemy().getSpellField().getCards()){
                    c.setupGraphics();
                }
                for(Item item : serverBattle.getEnemy().getInventory().getItems())
                    item.updateGraphics();
                for(Amulet amulet : serverBattle.getEnemy().getInventory().getAmulets())
                    amulet.updateGraphics();
            });
        }

        void trigger(){
            try {
                sOutput.writeObject(Boolean.TRUE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<ClientThread> getClientThreads(){
        return al;
    }

    public void setPort(int port){
        this.port = port;
    }
}

