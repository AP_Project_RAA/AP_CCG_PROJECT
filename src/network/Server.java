package network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/*
 * The server that can be run both as a console application or a GUI
 */
public class Server extends Thread implements Serializable{
    private static final long serialVersionUID = 1000L;
    //for MainServer I/O
    transient private ObjectOutputStream out;
    transient private Socket mainServerSocket;
    // a unique ID for each connection
    private static int uniqueId;
    // an ArrayList to keep the list of the Client
    private ArrayList<ClientThread> clientThreads;
    // the port number to listen for connection
    private int port;
    private String address, name;

    private AtomicBoolean running = new AtomicBoolean(true);

    public Server(int port, String address, String name){
        this.address = address;
        this.name = name;
        // the port
        this.port = port;
        // ArrayList for the Client list
        clientThreads = new ArrayList<ClientThread>();
    }

    public void run(){
        try {
            mainServerSocket = new Socket("localhost", 8080);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        try {
            out = new ObjectOutputStream(mainServerSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        try {
            out.writeObject("Server");
            out.writeObject(this.address);
            out.writeObject(port);
            out.writeObject(name);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // the socket used by the server
            ServerSocket serverSocket = new ServerSocket(port);

            // infinite loop to wait for connections
            while(true) {
                display("Server waiting for Clients on port " + port + ".");

                Socket socket = serverSocket.accept();
                ClientThread t = new ClientThread(socket);  // make a thread of it
                t.setDaemon(true);
                clientThreads.add(t);                                    // save it in the ArrayList
                t.start();
            }
        }
        // something went bad
        catch (IOException e) {
            String msg = " Exception on new ServerSocket: " + e + "\n";
            display(msg);
        }
    }

    private void display(String msg){
        System.out.println(msg);
    }

    private synchronized void broadcastToClients(Object object, int id) {
        for(int i = clientThreads.size(); --i >= 0;) {
            ClientThread temp = clientThreads.get(i);
            if(id == -1){
                temp.send(object);
            }else if(id == temp.id){
                temp.send(object);
                break;
            }
        }
    }

    // for a client who logoff using the LOGOUT message
    synchronized void remove(int id){
        // scan the array list until we found the Id
        for(int i = 0; i < clientThreads.size(); ++i){
            ClientThread ct = clientThreads.get(i);
            // found it
            if(ct.id == id) {
                clientThreads.remove(i);
                return;
            }
        }
    }

    /**
     * One instance of this thread will run for each client
     */
    public class ClientThread extends Thread implements Serializable {
        private static final long serialVersionUID = 1001L;
        // the socket where to listen/talk
        transient Socket socket;
        transient ObjectInputStream sInput;
        transient ObjectOutputStream sOutput;
        // my unique id (easier for disconnection)
        int id;
        String name;

        // Constructor
        ClientThread(Socket socket){
            // a unique id
            id = ++uniqueId;
            this.socket = socket;

            System.out.println("Thread trying to create Object Input/Output Streams");
            try {
                // create output first
                sOutput = new ObjectOutputStream(socket.getOutputStream());
                sInput = new ObjectInputStream(socket.getInputStream());

            } catch (Exception e) {
                e.printStackTrace();
                //display("Exception creating new Input/output Streams: " + e);
                return;
            }
        }

        // what will run forever
        public void run(){

            try {
                //send client id
                sOutput.writeObject(id);
                //get Client Name
                name = (String) sInput.readObject();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            Thread sendClients = new Thread(()->{
                while(running.get()) {
                    try {
                        sOutput.writeUnshared(clientThreads);

                        Thread.sleep(5000);
                    } catch (IOException | InterruptedException e) {
//                        e.printStackTrace();
//                        break;
                    }
                }
            });
            sendClients.setDaemon(true);
            sendClients.start();

            Object object;
            int hostId = -100;
            while(true) {
                try {
                    object = sInput.readUnshared();
                    //System.out.println(object);
                    if(object instanceof Boolean)
                        running.set(!(Boolean) object);
                    else if(object instanceof Integer){
                        if(hostId == -100)
                            hostId = (Integer) object;
                        else
                            broadcastToClients(object, hostId);
                    }
                    else {
                        broadcastToClients(object, hostId);
                    }

//                    else if(sendClients.getState().equals(State.BLOCKED))
//                        sendClients.start();

                } catch (IOException | ClassNotFoundException e) {
                    display(" Exception reading Streams: " + e);
                    break;
                }
                // the message part of the ChatMessage
            }
            // remove myself from the arrayList containing the list of the
            // connected Clients
            remove(id);
            close();
        }

        // try to close everything
        private void close(){
            // try to close the connection
            try {
                if(sOutput != null) sOutput.close();
            } catch (Exception e) {
            }
            try {
                if(sInput != null) sInput.close();
            } catch (Exception e) {
            }
            ;
            try {
                if(socket != null) socket.close();
            } catch (Exception e) {
            }
        }

        //send objects
        void send(Object object){
            try {
                sOutput.writeUnshared(object);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public String toString(){
            return name;
        }

        @Override
        public long getId(){
            return id;
        }
    }

    public ArrayList<ClientThread> getClientThreads(){
        return clientThreads;
    }

    public void setPort(int port){
        this.port = port;
    }

    public String name(){
        return name;
    }
}

