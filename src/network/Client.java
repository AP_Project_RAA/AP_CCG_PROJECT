package network;

import gameObjects.Amulet;
import gameObjects.Item;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.MonsterCard;
import javafx.application.Platform;
import main.GamePlace;
import main.Player;
import network.Peer_to_Peer.ChatMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/*
 * The Client that can be run both as a console or a GUI
 */
public class Client extends Thread {
    private ClientBattle clientBattle;
    //for MainServer I/O
    private ObjectOutputStream out;
    private ObjectInputStream in;
    //for Server I/O
    private ObjectInputStream sInput;        // to read from the socket
    private ObjectOutputStream sOutput;        // to write on the socket

    private String serverAddress;
    private int serverPort, id, hostId;
    private Boolean isConnectToHost = false;

    private ArrayList<MainServer.ServerThread> servers = new ArrayList<>();
    private ArrayList<Server.ClientThread> clients;

    public Client(ClientBattle clientBattle){
        this.clientBattle = clientBattle;
        clients = new ArrayList<>();
        servers = new ArrayList<>();
    }

    public void run(){
        ListenToMainServer listen = new ListenToMainServer();
        listen.setDaemon(true);
        listen.start();
    }

    private void display(String msg){
        System.out.println(msg);
    }

    /*
     * When something goes wrong
     * Close the Input/Output streams and disconnect not much to do in the catch clause
     */
    private void disconnect(){
        try {
            if(sInput != null) sInput.close();
        } catch (Exception e) {
        } // not much else I can do
        try {
            if(sOutput != null) sOutput.close();
        } catch (Exception e) {
        } // not much else I can do
    }

    class ListenToMainServer extends Thread{
        Socket socket;
        ListenToMainServer(){
            try {
                socket = new Socket("localhost", 8080);
                out = new ObjectOutputStream(socket.getOutputStream());

                out.writeObject("Client");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run(){
            try {
                in = new ObjectInputStream(socket.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            while(true) {
                try {

                    servers = (ArrayList<MainServer.ServerThread>) in.readUnshared();

                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }

    class ListenToServer extends Thread {
        Socket socket;

        ListenToServer(){
            // try to connect to the server
            try {
                socket = new Socket(serverAddress, serverPort);
            }
            catch (Exception ec) {
                //ec.printStackTrace();
                display("Error connecting to server:" + ec);
                return;
            }

            String msg = "Connection accepted " + socket.getInetAddress() + ":" + socket.getPort();
            display(msg);

            /* Creating both Data Stream */
            try {
                sInput = new ObjectInputStream(socket.getInputStream());
                sOutput = new ObjectOutputStream(socket.getOutputStream());
            } catch (IOException eIO) {
                display("Exception creating new Input/output Streams: " + eIO);
                return;
            }
        }

        @Override
        public void run(){
            try {
                id = (Integer) sInput.readObject();
                sOutput.writeObject(clientBattle.getPlayer().getName());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            //Get Clients List
            while(!isConnectToHost){
                Object object;
                try {
                    object = sInput.readUnshared();

                    if(object instanceof ArrayList)
                        clients = (ArrayList<Server.ClientThread>) object;
                    else if(object instanceof ChatMessage)
                        acceptConnection();

                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }

    class Play extends Thread{
        int hostId;

        Play(int hostId){
            this.hostId = hostId;
            try {
                send(clientBattle.getPlayer(), hostId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run(){
            try {
                Object object = sInput.readUnshared();
                clientBattle.setEnemy((Player) object);
                //updateHostPlayer(object);

                Platform.runLater(() -> {
                    clientBattle.run();
                });
            } catch (Exception e) {
                //display("Exception doing login : " + e);
            }

            Object object;
            while(true) {
                try {
                    object = sInput.readUnshared();
                    if(object instanceof Boolean) {
                        if((Boolean) object) {
                            clientBattle.setTurn(0);
                            Platform.runLater(() -> {
                                clientBattle.startRound();
                            });
                        }
                    } else if(object instanceof Player) {
                        clientBattle.setEnemy((Player) object);
                        clientBattle.setPlayer((Player) sInput.readUnshared());
//                        updateHostPlayer(object);
//                        updateClientPlayer();

                        Platform.runLater(() -> {
                            clientBattle.updateMonsterCardsBoxes();
                            clientBattle.updateSpellCardsBoxes();
                        });
                    } else if(object instanceof String)
                        displayInfo(object);
                } catch (IOException e) {
                    display("Server has close the connection: " + e);
                    break;
                }
                // can't happen with a String object but need the catch anyhow
                catch (ClassNotFoundException e2) {
                }
            }
        }
    }

    public void connectToServer(int id){
        for(MainServer.ServerThread serverThread : servers){
            if(serverThread.getId() == id){
                serverPort = serverThread.port;
                serverAddress = serverThread.address;
            }
        }
        ListenToServer listen = new ListenToServer();
        listen.setDaemon(true);
        listen.start();
        //new Thread(this::acceptConnection).start();
    }

    public void connectToHost(int hostId){
            this.hostId = hostId;
            try {
                sOutput.writeUnshared(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            isConnectToHost = true;

            send(hostId, hostId);
            send(new ChatMessage(0, ""), hostId);
            //send id
            send(id, hostId);
            send("Do You Want to Play With " + clientBattle.getPlayer().getName() + "? y/n", hostId);

            Object object;
            try {
                //System.out.println("OKKK");
                object = sInput.readUnshared();
                //System.out.println("OKKK");
                if(((String)object).equals("y")){
                    System.out.println("OKKK");
                    isConnectToHost = true;
                    //set turns
                    Random random = new Random();
                    int turn = random.nextInt(2);
                    clientBattle.setTurn(turn);
                    send(1-turn, hostId);
                    //sOutput.writeObject(1 - turn);
                    //start Game
                    Play play = new Play(hostId);
                    play.setDaemon(true);
                    play.start();
                }else {
                    display("Connection Refuse!");
                    sOutput.writeUnshared(false);
                    isConnectToHost = false;
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
    }

    private void acceptConnection(){

            try {
                sOutput.writeUnshared(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            isConnectToHost = true;
            try {
                //get hostID
                hostId = (Integer) sInput.readUnshared();
                System.out.println("OKKK");
                send(hostId, hostId);

                System.out.println((String) sInput.readUnshared());
                String s = new Scanner(System.in).nextLine();

                send(s, hostId);
                if(s.equals("y")) {
                    isConnectToHost = true;
                    //set turn
                    clientBattle.setTurn((Integer) sInput.readUnshared());

                    //Start Game
                    Play play = new Play(hostId);
                    play.setDaemon(true);
                    play.start();
                }
                else {
                    sOutput.writeUnshared(false);
                    isConnectToHost = false;
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
    }

    private void updateClientPlayer(){
        try {
            clientBattle.getPlayer().setHP((Integer) sInput.readObject());
            clientBattle.getPlayer().setName((String) sInput.readObject());
            clientBattle.getPlayer().MP = (Integer) sInput.readObject();
            clientBattle.getPlayer().setMaxMP((Integer) sInput.readObject());
            clientBattle.getPlayer().setAmulet((Amulet) sInput.readObject());
            //Items
            int size = (Integer) sInput.readObject();
            ArrayList<Item> itemArrayList = new ArrayList<>();
            for(int i = 0; i < size; i++)
                itemArrayList.add((Item) sInput.readObject());
            clientBattle.getPlayer().setItems(itemArrayList);
            //GamePlaces
            //Deck
            size = (Integer) sInput.readObject();
            ArrayList<Card> deckCardArrayList = new ArrayList<>();
            GamePlace deck = new GamePlace();
            for(int i = 0; i < size; i++)
                deckCardArrayList.add((Card) sInput.readObject());
            deck.setCards(deckCardArrayList);
            clientBattle.getPlayer().setDeck(deck);
            //Hand
            size = (Integer) sInput.readObject();
            ArrayList<Card> handCardArrayList = new ArrayList<>();
            GamePlace hand = new GamePlace();
            for(int i = 0; i < size; i++)
                handCardArrayList.add((Card) sInput.readObject());
            hand.setCards(handCardArrayList);
            clientBattle.getPlayer().setHand(hand);
            //GraveYard
            size = (Integer) sInput.readObject();
            ArrayList<Card> graveYardCardArrayList = new ArrayList<>();
            GamePlace graveYard = new GamePlace();
            for(int i = 0; i < size; i++)
                graveYardCardArrayList.add((Card) sInput.readObject());
            graveYard.setCards(graveYardCardArrayList);
            clientBattle.getPlayer().setGraveYard(graveYard);
            //MonsterField
            size = (Integer) sInput.readObject();
            ArrayList<Card> monsterFieldCardArrayList = new ArrayList<>();
            GamePlace monsterField = new GamePlace();
            for(int i = 0; i < size; i++){
                Card card = (Card) sInput.readObject();
                ((MonsterCard) card).setHP((Integer) sInput.readObject());
                ((MonsterCard) card).setAP((Integer) sInput.readObject());
                monsterFieldCardArrayList.add(card);
            }
            monsterField.setCards(monsterFieldCardArrayList);
            clientBattle.getPlayer().setMonsterField(monsterField);
            //SpellField
            size = (Integer) sInput.readObject();
            ArrayList<Card> spellFieldCardArrayList = new ArrayList<>();
            GamePlace spellField = new GamePlace();
            for(int i = 0; i < size; i++)
                spellFieldCardArrayList.add((Card) sInput.readObject());
            spellField.setCards(spellFieldCardArrayList);
            clientBattle.getPlayer().setSpellField(spellField);
            //Inventory
            //Item
            size = (Integer) sInput.readObject();
            ArrayList<Item> inventoryItemArrayList = new ArrayList<>();
            for(int i = 0; i < size; i++)
                inventoryItemArrayList.add((Item) sInput.readObject());
            clientBattle.getPlayer().getInventory().setItems(inventoryItemArrayList);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Platform.runLater(() -> {
            clientBattle.getPlayer().updateAvatar();
            for(Card c : clientBattle.getPlayer().getDeck().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getPlayer().getHand().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getPlayer().getGraveYard().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getPlayer().getMonsterField().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getPlayer().getSpellField().getCards()){
                c.setupGraphics();
            }
            for(Item item : clientBattle.getPlayer().getInventory().getItems())
                item.updateGraphics();
            for(Amulet amulet : clientBattle.getPlayer().getInventory().getAmulets())
                amulet.updateGraphics();
        });
    }

    private void updateHostPlayer(Object object){
        try {
            clientBattle.getEnemy().setHP((Integer) object);
            clientBattle.getEnemy().setName((String) sInput.readObject());
            clientBattle.getEnemy().MP = (Integer) sInput.readObject();
            clientBattle.getEnemy().setMaxMP((Integer) sInput.readObject());
            clientBattle.getEnemy().setAmulet((Amulet) sInput.readObject());
            //Items
            int size = (Integer) sInput.readObject();
            ArrayList<Item> itemArrayList = new ArrayList<>();
            for(int i = 0; i < size; i++)
                itemArrayList.add((Item) sInput.readObject());
            clientBattle.getEnemy().setItems(itemArrayList);
            //GamePlaces
            //Deck
            size = (Integer) sInput.readObject();
            ArrayList<Card> deckCardArrayList = new ArrayList<>();
            GamePlace deck = new GamePlace();
            for(int i = 0; i < size; i++)
                deckCardArrayList.add((Card) sInput.readObject());
            deck.setCards(deckCardArrayList);
            clientBattle.getEnemy().setDeck(deck);
            //Hand
            size = (Integer) sInput.readObject();
            ArrayList<Card> handCardArrayList = new ArrayList<>();
            GamePlace hand = new GamePlace();
            for(int i = 0; i < size; i++)
                handCardArrayList.add((Card) sInput.readObject());
            hand.setCards(handCardArrayList);
            clientBattle.getEnemy().setHand(hand);
            //GraveYard
            size = (Integer) sInput.readObject();
            ArrayList<Card> graveYardCardArrayList = new ArrayList<>();
            GamePlace graveYard = new GamePlace();
            for(int i = 0; i < size; i++)
                graveYardCardArrayList.add((Card) sInput.readObject());
            graveYard.setCards(graveYardCardArrayList);
            clientBattle.getEnemy().setGraveYard(graveYard);
            //MonsterField
            size = (Integer) sInput.readObject();
            ArrayList<Card> monsterFieldCardArrayList = new ArrayList<>();
            GamePlace monsterField = new GamePlace();
            for(int i = 0; i < size; i++){
                Card card = (Card) sInput.readObject();
                ((MonsterCard) card).setHP((Integer) sInput.readObject());
                ((MonsterCard) card).setAP((Integer) sInput.readObject());
                monsterFieldCardArrayList.add(card);
            }
            monsterField.setCards(monsterFieldCardArrayList);
            clientBattle.getEnemy().setMonsterField(monsterField);
            //SpellField
            size = (Integer) sInput.readObject();
            ArrayList<Card> spellFieldCardArrayList = new ArrayList<>();
            GamePlace spellField = new GamePlace();
            for(int i = 0; i < size; i++)
                spellFieldCardArrayList.add((Card) sInput.readObject());
            spellField.setCards(spellFieldCardArrayList);
            clientBattle.getEnemy().setSpellField(spellField);
            //Inventory
            //Item
            size = (Integer) sInput.readObject();
            ArrayList<Item> inventoryItemArrayList = new ArrayList<>();
            for(int i = 0; i < size; i++)
                inventoryItemArrayList.add((Item) sInput.readObject());
            clientBattle.getEnemy().getInventory().setItems(inventoryItemArrayList);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Platform.runLater(() -> {
            clientBattle.getEnemy().updateAvatar();
            for(Card c : clientBattle.getEnemy().getDeck().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getEnemy().getHand().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getEnemy().getGraveYard().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getEnemy().getMonsterField().getCards()){
                c.setupGraphics();
            }
            for(Card c : clientBattle.getEnemy().getSpellField().getCards()){
                c.setupGraphics();
            }
            for(Item item : clientBattle.getEnemy().getInventory().getItems())
                item.updateGraphics();
            for(Amulet amulet : clientBattle.getEnemy().getInventory().getAmulets())
                amulet.updateGraphics();
        });
    }

    void sendPlayer(Player player){
        //try {
              send(player, hostId);
//            sOutput.writeObject(player.getHP());
//            sOutput.writeObject(player.getName());
//            sOutput.writeObject(player.MP);
//            sOutput.writeObject(player.getMaxMP());
//            sOutput.writeObject(player.getAmulet());
//            //items
//            sOutput.writeObject(player.getItems().size());
//            for(Item item : player.getItems())
//                sOutput.writeObject(item);
//            //GamePlaces
//            //Deck
//            sOutput.writeObject(player.getPlaces().get(Field.DECK).getCards().size());
//            for(Card card : player.getPlaces().get(Field.DECK).getCards())
//                sOutput.writeObject(card);
//            //Hand
//            sOutput.writeObject(player.getPlaces().get(Field.HAND).getCards().size());
//            for(Card card : player.getPlaces().get(Field.HAND).getCards())
//                sOutput.writeObject(card);
//            //GraveYard
//            sOutput.writeObject(player.getPlaces().get(Field.GRAVEYARD).getCards().size());
//            for(Card card : player.getPlaces().get(Field.GRAVEYARD).getCards())
//                sOutput.writeObject(card);
//            //MonsterField
//            sOutput.writeObject(player.getPlaces().get(Field.MONSTERFIELD).getCards().size());
//            for(Card card : player.getPlaces().get(Field.MONSTERFIELD).getCards()){
//                sOutput.writeObject(card);
//                sOutput.writeObject(((MonsterCard) card).getHP());
//                sOutput.writeObject(((MonsterCard) card).getAP());
//            }
//            //SpellField
//            sOutput.writeObject(player.getPlaces().get(Field.SPELLFIELD).getCards().size());
//            for(Card card : player.getPlaces().get(Field.SPELLFIELD).getCards())
//                sOutput.writeObject(card);
//            //Inventory
//            //Item
//            sOutput.writeObject(player.getInventory().getItems().size());
//            for(Item item : player.getInventory().getItems())
//                sOutput.writeObject(item);
//        } catch (IOException e) {
//            display("Exception writing to server: " + e);
//
//        }
    }

    void sendInfo(String string){
        send(string, hostId);
    }

    void displayInfo(Object object){
        clientBattle.getUi().showOutput((String) object);
    }

    void trigger(){
        send(Boolean.TRUE, hostId);
    }

    private void send(Object object, int destId){
        try {
            sOutput.writeUnshared(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<MainServer.ServerThread> getServers(){
        return servers;
    }

    public ArrayList<Server.ClientThread> getClients(){
        return clients;
    }
}
