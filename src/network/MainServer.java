package network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class MainServer extends Thread implements Serializable{
    private static final long serialVersionUID = 1100L;
    transient private ObjectInputStream in;
    // a unique ID for each connection
    private static int uniqueClientId, uniqueServerId;
    // an ArrayList to keep the list of the Client
    private ArrayList<ClientThread> clientThreads;
    private ArrayList<ServerThread> serverThreads;
    // the port number to listen for connection
    private int port;

    public MainServer(int port){
        this.port = port;
        clientThreads = new ArrayList<>();
        serverThreads = new ArrayList<>();
    }

    public void run(){
        /* create socket server and wait for connection requests */
        try {
            // the socket used by the server
            ServerSocket serverSocket = new ServerSocket(port);

            // infinite loop to wait for connections
            while(true) {
                // format message saying we are waiting
                display("MainServer waiting for Clients on port " + port + ".");

                Socket socket = serverSocket.accept();
                in = new ObjectInputStream(socket.getInputStream());

                try {
                    if(((String)in.readObject()).equals("Client")){
                        ClientThread t = new ClientThread(socket);
                        t.setDaemon(true);
                        clientThreads.add(t);
                        t.start();
                        display("A Client Connected");
                    }else{
                        String  address = (String)in.readObject();
                        int port = (Integer)in.readObject();
                        String name = (String)in.readObject();

                        ServerThread s = new ServerThread(socket, address, port, name);
                        s.setDaemon(true);
                        serverThreads.add(s);
                        s.start();
                        display("A Server Connected");
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        // something went bad
        catch (IOException e) {
            e.printStackTrace();
//            String msg = " Exception on new ServerSocket: " + e + "\n";
//            display(msg);
        }
    }
    /*
     * Display an event (not a message) to the console or the GUI
     */
    private void display(String msg){
        System.out.println(msg);
    }

    private synchronized void broadcastToServers(Object object, int id) {
        for(int i = serverThreads.size(); --i >= 0;) {
            ServerThread temp = serverThreads.get(i);
            if(id == -1){
                temp.send(object);
            }else if(id == temp.id){
                temp.send(object);
                break;
            }
        }
    }
    private synchronized void broadcastToClients(Object object, int id) {
        for(int i = clientThreads.size(); --i >= 0;) {
            ClientThread temp = clientThreads.get(i);
            if(id == -1){
                temp.send(object);
            }else if(id == temp.id){
                temp.send(object);
                break;
            }
        }
    }

    // for a client who logoff using the LOGOUT message
    private synchronized void remove(int id){
        // scan the array list until we found the Id
        for(int i = 0; i < clientThreads.size(); ++i){
            ClientThread ct = clientThreads.get(i);
            // found it
            if(ct.id == id) {
                clientThreads.remove(i);
                return;
            }
        }
    }


    /**
     * One instance of this thread will run for each client
     */
    public class ClientThread extends Thread implements Serializable{
        private static final long serialVersionUID = 1101L;
        // the socket where to listen/talk
        transient Socket socket;
        transient ObjectOutputStream out;
        // my unique id (easier for disconnection)
        int id;

        // Constructor
        ClientThread(Socket socket){
            // a unique id
            id = ++uniqueClientId;
            this.socket = socket;
            /* Creating both Data Stream */
            System.out.println("Thread trying to create Object Input/Output Streams");
            try {
                // create output first
                out = new ObjectOutputStream(socket.getOutputStream());

            } catch (IOException e) {
                e.printStackTrace();
                //display("Exception creating new Input/output Streams: " + e);
            }
        }

        // what will run forever
        public void run(){
            while(true) {
                try {
                    out.writeUnshared(serverThreads);
                    //in.readUnshared();

                    Thread.sleep(5000);
                } catch (IOException e) {
                    display(" Exception reading Streams: " + e);
                    break;
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
//                catch (ClassNotFoundException e2) {
//                    break;
//                }
            }
            // remove myself from the arrayList containing the list of the
            // connected Clients
            remove(id);
            close();
        }

        // try to close everything
        private void close(){
            // try to close the connection
            try {
                if(out != null) out.close();
                if(socket != null) socket.close();
            } catch (Exception e) {
            }
        }

        //send objects
        void send(Object object){
            try {
                out.writeUnshared(object);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class ServerThread extends Thread implements Serializable {
        private static final long serialVersionUID = 1110L;
        int id, port;
        transient Socket socket;
        String address, name;
        transient ObjectOutputStream out;

        ServerThread(Socket socket, String address, int port, String name){
            this.socket = socket;
            this.port = port;
            this.address = address;
            this.name = name;
            id = ++uniqueServerId;

            try {
                out = new ObjectOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run(){
            super.run();
        }

        //send objects
        void send(Object object){
            try {
                out.writeUnshared(object);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public int getPort(){
            return port;
        }

        public String getAddress(){
            return address;
        }

        public String name(){
            return name;
        }

        @Override
        public String toString(){
            return name;
        }
    }

    public ArrayList<ClientThread> getClientThreads(){
        return clientThreads;
    }

    public ArrayList<ServerThread> getServerThreads(){
        return serverThreads;
    }

    public void setPort(int port){
        this.port = port;
    }
}
