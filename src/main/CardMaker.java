package main;//import com.sun.xml.internal.bind.v2.TODO;

import enums.Breed;
import enums.Field;
import enums.Side;
import enums.SpellType;
import gameObjects.SpellPack;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.General;
import gameObjects.cards.monsterCards.Hero;
import gameObjects.cards.monsterCards.Normal;
import gameObjects.cards.monsterCards.SpellCaster;
import gameObjects.cards.spellCards.AuraSpell;
import gameObjects.cards.spellCards.ContinuousSpell;
import gameObjects.cards.spellCards.InstantSpell;
import gameObjects.spells.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;

public class CardMaker {
    public static void make(CheckPoint checkPoint) {
        ArrayList<Spell> spell = new ArrayList<Spell>();
        ArrayList<Spell> battleCry = new ArrayList<Spell>();
        ArrayList<Spell> will = new ArrayList<Spell>();
        ArrayList<Card> cards = new ArrayList<Card>();

        cards.add(new Normal("Elven Ranger", 1, 300, 400, "00", Breed.ELVEN));
        cards.add(new Normal("Elven Hunter", 3, 800, 600, "00", Breed.ELVEN));
        cards.add( new Normal("Elven Guardsman", 5, 1500, 500, "10", Breed.ELVEN));
        cards.add( new Normal("Elven Assassin", 5, 800, 1200, "01", Breed.ELVEN));
        cards.add( new Normal("Lesser Whelp", 1, 500, 300, "00", Breed.DRAGONBREED));
        cards.add( new Normal("Dragonling", 3, 700, 700, "00", Breed.DRAGONBREED));
        cards.add( new Normal("Armored Dragon", 5, 2000, 400, "10", Breed.DRAGONBREED));
        cards.add( new Normal("Yellow Drake", 5, 800, 1000, "01", Breed.DRAGONBREED));
        cards.add( new Normal("Murloc Crawler", 1, 200, 500, "00", Breed.ATLANTIAN));
        cards.add( new Normal("Murloc Warrior", 2, 600, 600, "00", Breed.ATLANTIAN));
        cards.add( new Normal("Giant Crab", 4, 1500, 300, "10", Breed.ATLANTIAN));
        cards.add( new Normal("Shark Man", 5, 900, 1200, "01", Breed.ATLANTIAN));
        cards.add( new Normal("Goblin Smuggler", 2, 600, 400, "00", Breed.DEMONIC));
        cards.add( new Normal("Ogre Warrior", 3, 800, 500, "00", Breed.DEMONIC));
        cards.add( new Normal("Ogre Frontliner", 5, 1800, 600, "10", Breed.DEMONIC));
        cards.add( new Normal("Undead", 1, 200, 400, "00", Breed.DEMONIC));
        cards.add( new Normal("Giant Bat", 3, 500, 900, "01", Breed.DEMONIC));
        cards.add( new Normal("Stout Undead", 4, 1200, 600, "10", Breed.DEMONIC));
        cards.add( new Normal("Imp", 2, 300, 500, "00", Breed.DEMONIC));
        cards.add( new Normal("Shade", 3, 500, 800, "00", Breed.DEMONIC));
        cards.add( new Normal("Living Armor", 5, 1500, 400, "10", Breed.DEMONIC));
        cards.add( new Normal("Hellhound", 5, 800, 1000, "01", Breed.DEMONIC));
        /**SpellCaster**/
        spell.add(new Healer(SpellType.SELECTIVE, 1, "1111", "1111", "000", 500, "0"));
        spell.add(new ApChanger(SpellType.SELECTIVE, 1, "1111", "1111", "000", 300, Side.FRIENDLY));
        cards.add(new SpellCaster("Elven Druid", 5, 1200, 600, "00", Breed.ELVEN, new SpellPack(spell), "Rejuvenation: Increase a selected friendly monster card’s HP by 500 and AP by 300"));
        spell.clear();

        spell.add(new Attacker(SpellType.ALL, 1, "1111", "1111", "000", 400, "01"));
        spell.add(new CardMover(SpellType.RANDOM, 1, "0000", "0000", "111", Side.ENEMY, Field.SPELLFIELD, Side.ENEMY, Field.GRAVEYARD));
        cards.add(new SpellCaster("Elven Sorceress", 7, 1000, 1000, "01", Breed.ELVEN, new SpellPack(spell), "Arcane Explosion: Deal 400 damage to all enemy monster cards and remove a random spell card from enemy field and move it to graveyard."));
        spell.clear();

        spell.add(new CardMover(SpellType.RANDOM, 1, "1111", "1111", "000", Side.ENEMY, Field.MONSTERFIELD, Side.ENEMY, Field.GRAVEYARD));
        cards.add(new SpellCaster("Blue Dragon", 5, 800, 1200, "00", Breed.DRAGONBREED, new SpellPack(spell), "Magical Fire: Move an enemy monster card from field to graveyard"));
        spell.clear();

        spell.add(new Attacker(SpellType.SELECTIVE, 1, "1111", "1111", "000", 500, "01"));
        spell.add(new ApChanger(SpellType.SELECTIVE, 1, "1111", "1111", "000", -500, Side.ENEMY));
        cards.add(new SpellCaster("Volcanic Dragon", 8, 2500, 700, "10", Breed.DRAGONBREED, new SpellPack(spell), "Lava Spit: Deal 500 damage to an enemy monster card and reduce its AP by 500"));
        spell.clear();

        spell.add(new Healer(SpellType.ALL, 1, "1111", "1111", "000", 300, "0"));
        spell.add(new ApChanger(SpellType.ALL, 1, "1111", "1111", "000", 200, Side.FRIENDLY));
        cards.add(new SpellCaster("Naga Siren", 6, 1200, 600, "00", Breed.ATLANTIAN, new SpellPack(spell), "Song of the Siren: Increase HP of all friendly monster cards by 300 and their AP by 200"));
        spell.clear();

        spell.add(new Attacker(SpellType.RANDOM, 1, "1111", "1111", "000", 1000, "11"));
        cards.add(new SpellCaster("Sea Serpent", 7, 1200, 1500, "01", Breed.ATLANTIAN, new SpellPack(spell), "Serpent’s Bite: Deal 1000 damage to an enemy monster card or player"));
        spell.clear();

        spell.add(new Healer(SpellType.RANDOM, 1, "1111", "1111", "000", 400, "1"));
        cards.add(new SpellCaster("Goblin Shaman", 4, 1000, 500, "00", Breed.DEMONIC, new SpellPack(spell), "Mend: Increase a friendly monster card or player’s HP by 400"));
        spell.clear();

        spell.add(new ApChanger(SpellType.RANDOM, 1, "1111", "1111", "00", 400, Side.FRIENDLY));
        cards.add(new SpellCaster("Ogre Magi", 5, 1500, 800, "00", Breed.DEMONIC, new SpellPack(spell), "Enrage: Increase a friendly monster card’s AP by 400"));
        spell.clear();

        spell.add(new ApChanger(SpellType.RANDOM, 1, "1111", "1111", "000", -500, Side.ENEMY));
        cards.add(new SpellCaster("Undead Mage", 6, 800, 1000, "00", Breed.DEMONIC, new SpellPack(spell), "Curse: Reduce an enemy monster card’s AP by 500"));
        spell.clear();

        spell.add(new Attacker(SpellType.ALL, 1, "1111", "1111", "000", 300, "01"));
        spell.add(new Healer(SpellType.ALL, 1, "1111", "1111", "000", 300, "0"));
        cards.add(new SpellCaster("Vampire Acolyte", 7, 1500, 1500, "01", Breed.DEMONIC, new SpellPack(spell), "Black Wave: Deal 300 damage to all enemy monster cards and heal all friendly monster cards for 300 HP"));
        spell.clear();

        spell.add(new Attacker(SpellType.ALL, 1, "1111", "1111", "000", 800, "11"));
        cards.add(new SpellCaster("Evil Eye", 6, 400, 400, "00", Breed.DEMONIC, new SpellPack(spell), "Evil Gaze: Deal 800 damage to all enemy monster cards and player"));
        spell.clear();

        spell.add(new CardMover(SpellType.RANDOM, 1, "1111", "1111", "111", Side.FRIENDLY, Field.GRAVEYARD, Side.FRIENDLY, Field.HAND));
        cards.add(new SpellCaster("Necromancer", 7, 1200, 1500, "01", Breed.DEMONIC, new SpellPack(spell), "Raise Dead: Move a random card from your graveyard to hand"));
        spell.clear();
        /**General**/
        battleCry.add(new CardMover(SpellType.ALL, 1, "0000", "0000", "111", Side.ENEMY, Field.SPELLFIELD, Side.ENEMY, Field.HAND));
        will.add(new Healer(SpellType.SELECTIVE, 1, "1000", "1111", "000", 800, "0"));
        will.add(new ApChanger(SpellType.SELECTIVE, 1, "1000", "1111", "000", 600, Side.FRIENDLY));
        cards.add(new General("Noble Elf", 9, 2000, 2400, "00", Breed.ELVEN, new SpellPack(battleCry), new SpellPack(will), "Purge: Remove all enemy spell cards on the field and move them to hand", "Increase a random friendly Elven monster card on the field’s HP by 800 and AP by 600"));
        battleCry.clear();will.clear();

        battleCry.add(new CardMover(SpellType.RANDOM, 1, "1111", "1111", "000", Side.ENEMY, Field.MONSTERFIELD, Side.ENEMY, Field.GRAVEYARD));
        will.add(new CardMover(SpellType.RANDOM, 2, "1111", "1111", "111", Side.FRIENDLY, Field.DECK, Side.FRIENDLY, Field.HAND));
        cards.add(new General("Greater Dragon", 8, 2000, 1800, "00", Breed.DRAGONBREED, new SpellPack(battleCry), new SpellPack(will), "Devour: Send a random enemy monster card from field to graveyard", "Dragon’s Call: draw two cards from deck to hand"));
        battleCry.clear();will.clear();

        battleCry.add(new ApChanger(SpellType.ALL, 1, "1111", "1111", "000", -200, Side.ENEMY));
        battleCry.add(new CardMover(SpellType.RANDOM, 1, "1111", "1111", "000", Side.ENEMY, Field.MONSTERFIELD, Side.ENEMY, Field.HAND));
        will.add(new Attacker(SpellType.ALL, 0, "1111", "1111", "000", 400, "11"));
        cards.add(new General("Kraken", 8, 1800, 2200, "00", Breed.ATLANTIAN, new SpellPack(battleCry), new SpellPack(will), "Titan’s Presence: Return one random enemy monster card from field to hand and reduce all enemy monsters’ AP by 200", "Titan’s Fall: Deal 400 damage to all enemy monster cards and player"));
        battleCry.clear();will.clear();

        battleCry.add(new Attacker(SpellType.ALL, 1, "1111", "1111", "000", 400, "11"));
        will.add(new ApChanger(SpellType.ALL, 1, "1111", "1111", "000", 300, Side.FRIENDLY));
        cards.add(new General("Ogre Warchief", 7, 2500, 1500, "00", Breed.DEMONIC, new SpellPack(battleCry), new SpellPack(will), "War Stomp: Deal 400 damage to all enemy monster cards and player", "Last Order: Increase all friendly monster cards’ AP by 300"));
        battleCry.clear();will.clear();

        battleCry.add(new CardMover(SpellType.RANDOM, 2, "1111", "1111", "000", Side.ENEMY, Field.MONSTERFIELD, Side.ENEMY, Field.HAND));
        will.add(new ApChanger(SpellType.ALL, 1, "1111", "1111", "000", 200, Side.FRIENDLY));
        will.add(new ApChanger(SpellType.ALL, 1, "1111", "1111", "000", -200, Side.ENEMY));
        cards.add(new General("Vampire Prince", 9, 2000, 2200, "00", Breed.DEMONIC, new SpellPack(battleCry), new SpellPack(will), "Fear: Return two random enemy monster cards from field to hand", "Darkness: Reduce all enemy monster cards’ AP by 200 and increase friendly monster cards’ AP by 200"));
        battleCry.clear();will.clear();

        battleCry.add(new CardMover(SpellType.SELECTIVE, 1, "1111", "1111", "111", Side.ENEMY, Field.HAND, Side.ENEMY, Field.GRAVEYARD));
        will.add(new Healer(SpellType.RANDOM, 1, "0000", "0000", "000", 1000, "1"));
        cards.add(new General("Dark Knight", 8, 2500, 2500, "00", Breed.DEMONIC, new SpellPack(battleCry), new SpellPack(will), "Sacrifice: Select and move a card from hand to graveyard", "Loyalty: Heal your player for 1000 HP"));
        battleCry.clear();will.clear();
        /**Hero**/
        battleCry.add(new CardMover(SpellType.RANDOM, 2, "1111", "1111", "111", Side.FRIENDLY, Field.GRAVEYARD, Side.FRIENDLY, Field.HAND));
        spell.add(new Healer(SpellType.RANDOM, 1, "1111", "1111", "000", 2500, "1"));
        will.add(new Healer(SpellType.ALL, 1, "1111", "1111", "000", 500, "1"));
        will.add(new ApChanger(SpellType.ALL, 1, "1111", "1111", "000", 200, Side.FRIENDLY));
        cards.add(new Hero("Luthien, The High Priestess", 9, 2500, 2000, "00", Breed.ELVEN, new SpellPack(battleCry), new SpellPack(spell), new SpellPack(will),
                "Revive Allies: move two random cards from your graveyard to hand",
                "Divine Blessing: Increase HP of a friendly monster card or player by 2500",
                "Burst of Light: Increase HP of all friendly monster cards and player by 500 and increase AP of all friendly monster cards by 200"));
        battleCry.clear();will.clear();spell.clear();

        battleCry.add(new CardMover(SpellType.ALL, 1, "1111", "1110", "000", Side.BOTH, Field.MONSTERFIELD, Side.BOTH, Field.GRAVEYARD));
        spell.add(new Attacker(SpellType.ALL, 1, "1111", "1111", "000", 600, "11"));
        will.add(new ApChanger(SpellType.ALL, 1, "1111", "1111", "000", -400, Side.ENEMY));
        cards.add(new Hero("Igneel, The Dragon King", 10, 4000, 800, "00", Breed.DRAGONBREED, new SpellPack(battleCry), new SpellPack(spell), new SpellPack(will),
                "King’s Grace: Send all non-Hero monster cards on both sides of field to their graveyards",
                "King’s Wing Slash: Deal 600 damage to all enemy monster cards and player",
                "King’s Wail: Decrease all enemy monster cards’ AP by 400"));
        battleCry.clear();will.clear();spell.clear();

        battleCry.add(new CardMover(SpellType.SELECTIVE, 1, "1111", "1111", "111", Side.FRIENDLY, Field.HAND, Side.FRIENDLY, Field.PLAYFIELD));
        spell.add(new Attacker(SpellType.RANDOM, 3, "1111", "1111", "000", 800, "11"));
        will.add(new Attacker(SpellType.ALL, 1, "1111", "1111", "000", 400, "11"));
        will.add(new ApChanger(SpellType.ALL, 1, "0010", "1111", "000", 500, Side.FRIENDLY));
        cards.add(new Hero("Neptune, King Of Atlantis", 10, 2200, 2500, "01", Breed.ATLANTIAN, new SpellPack(battleCry), new SpellPack(spell), new SpellPack(will),
                "Call to Arms: Select and move a card from hand to play field",
                "Trident Beam: Deal 800 damage to three random enemy monster cards or player",
                "Ocean’s Cry: Deal 400 damage to all enemy monster cards and player and increase all friendly Atlantian monster cards’ AP by 500"));
        battleCry.clear();will.clear();spell.clear();

        battleCry.add(new CardMover(SpellType.RANDOM, 3, "1111", "1111", "111", Side.FRIENDLY,Field.DECK, Side.FRIENDLY, Field.HAND));
        spell.add(new Attacker(SpellType.ALL, 1, "1111", "1111", "000", 300, "01"));
        spell.add(new Healer(SpellType.ALL, 1, "1111", "1111", "000", 300, "0"));
        spell.add(new ApChanger(SpellType.ALL, 1, "1111", "1111", "000", 300, Side.FRIENDLY));
        will.add(new CardMover(SpellType.RANDOM, 2, "1111", "1111", "000", Side.ENEMY, Field.MONSTERFIELD, Side.ENEMY, Field.GRAVEYARD));
        cards.add(new Hero("Cerberus, Gatekeeper Of Hell", 10, 3000, 2000, "01", Breed.DEMONIC, new SpellPack(battleCry), new SpellPack(spell), new SpellPack(will), "" +
                "Open the Gate: Draw three cards from deck to hand",
                "Hellfire: Deal 300 damage to all enemy monster cards and Increase HP and AP of all friendly monster cards by 300",
                "Revenge of the Two Heads: Send two random enemy monster cards from field to graveyard"));
        battleCry.clear();will.clear();spell.clear();

        //making directories
        File file = new File("datafiles/" + checkPoint.getName());
        file.mkdir();

        file = new File("datafiles/" + checkPoint.getName() + "/Paths");
        file.mkdir();


        try {

            PrintWriter pathOut = new PrintWriter("datafiles/" + checkPoint.getName() + "/Paths/MonsterPaths.txt");

            File file1 = new File("datafiles/" + checkPoint.getName() + "/Cards");
            file1.mkdir();
            file1 = new File("datafiles/" + checkPoint.getName() + "/Cards/MonsterCard");
            file1.mkdir();

            for(int i = 0 ; i < cards.size() ; i++)
            {
                pathOut.write("datafiles/" + checkPoint.getName() + "/Cards/MonsterCard/"+cards.get(i).getClass().getSimpleName()+"/"+cards.get(i).getName()+".txt\n");

                file1 = new File("datafiles/" + checkPoint.getName() + "/Cards/MonsterCard/"+cards.get(i).getClass().getSimpleName());
                file1.mkdir();

                try {
                    PrintWriter fout = new PrintWriter("datafiles/" + checkPoint.getName() + "/Cards/MonsterCard/"+cards.get(i).getClass().getSimpleName()+"/"+cards.get(i).getName()+".txt");
                    fout.write(cards.get(i).toString());
                    fout.close();
                } catch (FileNotFoundException e) {
                    System.out.println(cards.get(i).getName() + ".txt not found");
                }
            }

            pathOut.close();
        } catch (FileNotFoundException e1) {
            System.out.println("Fatal Error! Paths.txt was not found");
        }


        cards.clear();


        spell.add(new Attacker(SpellType.SELECTIVE, 1, "1111", "1111", "000", 500, "01"));
        cards.add(new InstantSpell("Throwing Knives", 3, new SpellPack(spell), "Deal 500 damage to a selected enemy monster card on the field or to enemy player"));
        spell.clear();

        spell.add(new Attacker(SpellType.ALL, 1, "1111", "1111", "000", 100, "01"));
        cards.add(new ContinuousSpell("Poisonous Cauldron", 4, new SpellPack(spell), "Deal 100 damage to all enemy monster cards and enemy player"));
        spell.clear();

        spell.add(new Healer(SpellType.SELECTIVE, 1, "1111", "1111", "000", 500, "1"));
        cards.add(new InstantSpell("First Aid Kit", 3, new SpellPack(spell), "Increase HP of a selected friendly monster card or player by 500"));
        spell.clear();

        spell.add(new CardMover(SpellType.RANDOM, 1, "1111", "1111", "111", Side.ENEMY, Field.PLAYFIELD, Side.ENEMY, Field.GRAVEYARD));
        cards.add(new InstantSpell("Reaper’s Scythe", 4, new SpellPack(spell), "Send an enemy monster or spell card from field to graveyard"));
        spell.clear();

        spell.add(new Attacker(SpellType.RANDOM, 1, "1111", "1111", "000", 800, "01"));
        cards.add(new ContinuousSpell("Meteor Shower", 8, new SpellPack(spell), "Deal 800 damage to a random enemy monster card on field or player"));
        spell.clear();

        spell.add(new Healer(SpellType.RANDOM, 1, "1000", "1111", "000", 300, "0"));
        spell.add(new ApChanger(SpellType.RANDOM, 1, "1000", "1111", "000", 300, Side.FRIENDLY));
        cards.add(new AuraSpell("Lunar Blessing", 6, new SpellPack(spell), "Increase AP and HP of friendly Elven monster cards by 300"));
        spell.clear();

        spell.add(new CardMover(SpellType.SELECTIVE, 1, "1111", "1111", "000", Side.FRIENDLY, Field.MONSTERFIELD, Side.FRIENDLY, Field.HAND));
        spell.add(new CardMover(SpellType.RANDOM, 1, "1111", "1111", "111", Side.FRIENDLY, Field.DECK, Side.FRIENDLY, Field.HAND));
        cards.add(new InstantSpell("Strategic Retreat", 6, new SpellPack(spell), "Select and move a monster card from field to hand and draw one card from deck"));
        spell.clear();

        spell.add(new ApChanger(SpellType.ALL, 1, "1111", "1111", "000", 300, Side.FRIENDLY));
        cards.add(new AuraSpell("War Drum", 6, new SpellPack(spell), "Increase all friendly monster cards’ AP by 300"));
        spell.clear();

        spell.add(new Healer(SpellType.ALL, 1, "1111", "1111", "000", 200, "0"));
        cards.add(new ContinuousSpell("Healing Ward", 5, new SpellPack(spell), "Increase all friendly monster cards’ HP by 200"));
        spell.clear();

        spell.add(new Attacker(SpellType.ALL, 1, "0000", "0000", "000", 500, "01"));
        spell.add(new Healer(SpellType.ALL, 1, "0000", "0000", "000", 500, "1"));
        cards.add(new InstantSpell("Blood Feast", 4, new SpellPack(spell), "Deal 500 damage to enemy player and heal your player for 500 HP"));
        spell.clear();


        spell.add(new Attacker(SpellType.ALL, 1, "1101", "1111", "000", 500, "10"));
        cards.add(new InstantSpell("Tsunami", 6, new SpellPack(spell), "Deal 500 damage to all non-Atlantian monster cards on both sides of field"));
        spell.clear();

        spell.add(new Healer(SpellType.ALL, 1, "1111", "1000", "000", 400, "0"));
        spell.add(new ApChanger(SpellType.ALL, 1, "1111", "1000", "000", 400, Side.FRIENDLY));
        cards.add(new AuraSpell("Take All You Can", 7, new SpellPack(spell), "Increase all friendly normal monster cards’ HP and AP by 400"));
        spell.clear();

        spell.add(new CardMover(SpellType.SELECTIVE, 1, "0000", "0000", "111", Side.ENEMY, Field.SPELLFIELD, Side.ENEMY, Field.GRAVEYARD));
        spell.add(new Attacker(SpellType.ALL, 1, "0000", "0000", "000", 500, "01"));
        cards.add(new InstantSpell("Arcane Bolt", 5, new SpellPack(spell), "Deal 500 damage to enemy player and select and move an enemy spell card from field to graveyard"));
        spell.clear();

        spell.add(new CardMover(SpellType.ALL, 1, "0000", "0000", "111", Side.BOTH, Field.SPELLFIELD, Side.BOTH, Field.HAND));
        cards.add(new InstantSpell("Greater Purge", 7, new SpellPack(spell), "Remove all spell cards on field from both sides and move them to hand"));
        spell.clear();

        spell.add(new CardMover(SpellType.ALL, 1, "0000", "0000", "111", Side.ENEMY, Field.SPELLFIELD, Side.ENEMY, Field.HAND));
        cards.add(new ContinuousSpell("Magic Seal", 9, new SpellPack(spell), "Remove all enemy spell cards from field and move them to graveyard"));
        spell.clear();


        try {
            PrintWriter pathOut = new PrintWriter("datafiles/" + checkPoint.getName() + "/Paths/SpellPaths.txt");

            File file1 = new File("datafiles/" + checkPoint.getName() + "/Cards/SpellCard");
            file1.mkdir();

            for(int i = 0 ; i < cards.size() ; i++)
            {
                file1 = new File("datafiles/" + checkPoint.getName() + "/Cards/SpellCard/"+cards.get(i).getClass().getSimpleName());
                file1.mkdir();

                pathOut.write("datafiles/" + checkPoint.getName() + "/Cards/SpellCard/"+cards.get(i).getClass().getSimpleName()+"/"+cards.get(i).getName()+".txt\n");

                try {
                    PrintWriter fout = new PrintWriter("datafiles/" + checkPoint.getName() + "/Cards/SpellCard/"+cards.get(i).getClass().getSimpleName()+"/"+cards.get(i).getName()+".txt");
                    fout.write(cards.get(i).toString());
                    fout.close();
                } catch (FileNotFoundException e) {
                    System.out.println(cards.get(i).getName() + ".txt not found");
                }
            }

            pathOut.close();
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            System.out.println("Fatal Error! Paths.txt was not found");
        }


        try
        {
            File source , dest;
            source = new File("datafiles\\1\\amuletsData.txt");
            dest = new File("datafiles\\" + checkPoint.getName() +"\\amuletsData.txt");
            Files.copy(source.toPath(), dest.toPath());

            source = new File("datafiles\\1\\itemsData.txt");
            dest = new File("datafiles\\" + checkPoint.getName() +"\\itemsData.txt");
            Files.copy(source.toPath(), dest.toPath());

            dest = new File("datafiles\\" + checkPoint.getName() +"\\Players");
            dest.mkdir();

            source = new File("datafiles\\1\\Players\\Player.txt");
            dest = new File("datafiles\\" + checkPoint.getName() +"\\Players\\Player.txt");
            Files.copy(source.toPath(), dest.toPath());

            source = new File("datafiles\\1\\Players\\Enemy1.txt");
            dest = new File("datafiles\\" + checkPoint.getName() +"\\Players\\Enemy1.txt");
            Files.copy(source.toPath(), dest.toPath());

            source = new File("datafiles\\1\\Players\\Enemy2.txt");
            dest = new File("datafiles\\" + checkPoint.getName() +"\\Players\\Enemy2.txt");
            Files.copy(source.toPath(), dest.toPath());

            source = new File("datafiles\\1\\Players\\Enemy3.txt");
            dest = new File("datafiles\\" + checkPoint.getName() +"\\Players\\Enemy3.txt");
            Files.copy(source.toPath(), dest.toPath());

            source = new File("datafiles\\1\\Players\\Enemy4.txt");
            dest = new File("datafiles\\" + checkPoint.getName() +"\\Players\\Enemy4.txt");
            Files.copy(source.toPath(), dest.toPath());

            File file1 = new File("datafiles\\" + checkPoint.getName() + "\\playersave");
            file1.mkdir();

            File file2 = new File("datafiles\\" + checkPoint.getName() + "\\SpellPacks");
            file2.mkdir();

            PrintWriter spellPackWriter = new PrintWriter("datafiles\\" + checkPoint.getName() + "\\Paths\\SpellPackPaths.txt");
            spellPackWriter.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

//        ArrayList<Card> test = new ArrayList<>();
//        System.out.println(test.size());
//        GameManager gameManager = new GameManager();

//        gameManager.init();
        //System.out.println(gameManager.loadPlayer().getInventory().getCards().size());
        //test = GameManager.loadMonsterCards();
        //test = GameManager.loadSpellCards();
        /*for (int i = 0 ; i < test.size() ; i++)
            System.out.println(test.get(i) + "\n***********************************");
        //System.out.println("khdbhfbdhf");*/
    }
}
