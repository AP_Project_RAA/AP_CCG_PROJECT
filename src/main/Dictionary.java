package main;

import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class Dictionary {

    public static Dictionary instance = new Dictionary();

    public static final int WINDOW_WIDTH = 1366;
    public static final int WINDOW_HEIGHT = 768;
    public static final String NOT_ENOUGH_MP = "Not enough MP!";
    public static final String FULL_SPELLFIELD = "Your spellField is full.";
    public static final String NON_PURCHASABLE = "The \"Demon King's Crown\" is not purchasable!";

    public static final Image BACK_BUTTON = new Image(new File("images\\back.png").toURI().toString());
    public static final Image MAIN_MENU_BACKGROUND = new Image(new File("images\\main-menu-background.jpg").toURI().toString());
    public static final Image SMOKE_GIF = new Image(new File("images\\smoke.gif").toURI().toString());
    public static final Image MAIN_MENU_BTN1 = new Image(new File("images\\single-player.png").toURI().toString());
    public static final Image MAIN_MENU_BTN1_HOVER = new Image(new File("images\\single-player-hover.png").toURI().toString());
    public static final Image MAIN_MENU_BTN2 = new Image(new File("images\\custom-game.png").toURI().toString());
    public static final Image MAIN_MENU_BTN2_HOVER = new Image(new File("images\\custom-game-hover.png").toURI().toString());
    public static final Image MAIN_MENU_BTN3 = new Image(new File("images\\multiplayer.png").toURI().toString());
    public static final Image MAIN_MENU_BTN3_HOVER = new Image(new File("images\\multiplayer-hover.png").toURI().toString());
    public static final Image MAIN_MENU_BTN4 = new Image(new File("images\\settings.png").toURI().toString());
    public static final Image MAIN_MENU_BTN4_HOVER = new Image(new File("images\\settings-hover.png").toURI().toString());
    public static final Image MAIN_MENU_BTN5 = new Image(new File("images\\exit.png").toURI().toString());
    public static final Image MAIN_MENU_BTN5_HOVER = new Image(new File("images\\exit-hover.png").toURI().toString());

    public static final Image ONLINE = new Image(new File("images\\online1.png").toURI().toString());
    public static final Image ONLINE_HOVER = new Image(new File("images\\online2.png").toURI().toString());
    public static final Image WITH_FRIENDS = new Image(new File("images\\playwithfriends1.png").toURI().toString());
    public static final Image WITH_FRIENDS_HOVER = new Image(new File("images\\playwithfriends2.png").toURI().toString());

    public static final Image MULTIPLAYER_BACKGROUND = new Image(new File("images\\multiplayer_background.jpg").toURI().toString());
    public static final Image SERVER_BTN = new Image(new File("images\\host1.png").toURI().toString());
    public static final Image SERVER_BTN_HOVER = new Image(new File("images\\host2.png").toURI().toString());
    public static final Image CLIENT_BTN = new Image(new File("images\\client1.png").toURI().toString());
    public static final Image CLIENT_BTN_HOVER = new Image(new File("images\\client2.png").toURI().toString());
    public static final Image OK_BTN = new Image(new File("images\\okbutton1.png").toURI().toString());
    public static final Image OK_BTN_HOVER = new Image(new File("images\\okbutton2.png").toURI().toString());

    public static final Image SHOP_MENU_BACKGROUND = new Image(new File("images\\shop-menu-background.jpg").toURI().toString());
    public static final Image SHOP_MENU_BTN1 = new Image(new File("images\\shop-menu-btn1.png").toURI().toString());
    public static final Image SHOP_MENU_BTN1_HOVER = new Image(new File("images\\shop-menu-btn1-hover.png").toURI().toString());
    public static final Image SHOP_MENU_BTN2 = new Image(new File("images\\shop-menu-btn2.png").toURI().toString());
    public static final Image SHOP_MENU_BTN2_HOVER = new Image(new File("images\\shop-menu-btn2-hover.png").toURI().toString());
    public static final Image SHOP_MENU_BTN3 = new Image(new File("images\\shop-menu-btn3.png").toURI().toString());
    public static final Image SHOP_MENU_BTN3_HOVER = new Image(new File("images\\shop-menu-btn3-hover.png").toURI().toString());

    public static final Image COIN_IMAGE = new Image(new File("images\\coin.png").toURI().toString());
    public static final Image SCROLL_PAPER_1 = new Image(new File("images\\scroll-paper.png").toURI().toString());
    public static final Image SCROLL_PAPER_2 = new Image(new File("images\\scroll-paper2.png").toURI().toString());
    public static final Image BUY_BUTTON = new Image(new File("images\\buy-button.png").toURI().toString());
    public static final Image BUY_BUTTON_HOVER = new Image(new File("images\\buy-button-hover.png").toURI().toString());
    public static final Image BUY_BUTTON_DISABLE = new Image(new File("images\\buy-button-disable.png").toURI().toString());
    public static final Image SELL_BUTTON = new Image(new File("images\\sell-button.png").toURI().toString());
    public static final Image SELL_BUTTON_HOVER = new Image(new File("images\\sell-button-hover.png").toURI().toString());
    public static final Image SELL_BUTTON_DISABLE = new Image(new File("images\\sell-button-disable.png").toURI().toString());

    public static final Image EQUIP_BUTTON_1 = new Image(new File("images\\equipbutton1.crop.png").toURI().toString());
    public static final Image EQUIP_BUTTON_2 = new Image(new File("images\\equipbutton2.crop.png").toURI().toString());

    public static final Image CARD_INVENTORY_BUTTON_1 = new Image(new File("images\\cardinventory1.crop.png").toURI().toString());
    public static final Image CARD_INVENTORY_BUTTON_2 = new Image(new File("images\\cardinventory2.crop.png").toURI().toString());
    public static final Image ITEM_INVENTORY_BUTTON_1 = new Image(new File("images\\iteminventory1.crop.png").toURI().toString());
    public static final Image ITEM_INVENTORY_BUTTON_2 = new Image(new File("images\\iteminventory2.crop.png").toURI().toString());
    public static final Image AMULET_INVENTORY_BUTTON_1 = new Image(new File("images\\amuletinventory1.crop.png").toURI().toString());
    public static final Image AMULET_INVENTORY_BUTTON_2 = new Image(new File("images\\amuletinventory2.crop.png").toURI().toString());

    //Add to Deck : atd
    public static final Image atdButton_1 = new Image(new File("images\\atdbutton1.png").toURI().toString());
    public static final Image atdButton_2 = new Image(new File("images\\atdbutton2.png").toURI().toString());

    public static final Image rfdButton_1 = new Image(new File("images\\rfdbutton1.png").toURI().toString());
    public static final Image rfdButton_2 = new Image(new File("images\\rfdbutton2.png").toURI().toString());

    public static final Image BATTLE_BG = new Image(new File("images\\battlebg1.jpg").toURI().toString());
    public static final Image BATTLE_BG_2 = new Image(new File("images\\battlebg2.jpg").toURI().toString());
    public static final Image BATTLE_BG_3 = new Image(new File("images\\battlebg3.jpg").toURI().toString());

    public static final Image HAND_BUTTON_OFF = new Image(new File("images\\hand-button.png").toURI().toString());
    public static final Image HAND_BUTTON_ON = new Image(new File("images\\hand-button-hover.png").toURI().toString());
    public static final Image GRAVEYARD_BUTTON_OFF = new Image(new File("images\\graveyard-button.png").toURI().toString());
    public static final Image GRAVEYARD_BUTTON_ON = new Image(new File("images\\graveyard-button-hover.png").toURI().toString());
    public static final Image ITEMS_BUTTON_OFF = new Image(new File("images\\items-button.png").toURI().toString());
    public static final Image ITEMS_BUTTON_ON = new Image(new File("images\\items-button-hover.png").toURI().toString());

    public static final Image SIDE_PANE = new Image(new File("images\\stone-slab.png").toURI().toString());

    public static final Image CAST_SPELL_BUTTON = new Image(new File("images\\castspellbutton1.png").toURI().toString());
    public static final Image CAST_SPELL_BUTTON_HOVER = new Image(new File("images\\castspellbutton2.png").toURI().toString());

    public static final Image USE_CARD_BUTTON = new Image(new File("images\\usecardbutton1.png").toURI().toString());
    public static final Image USE_CARD_BUTTON_HOVER = new Image(new File("images\\usecardbutton2.png").toURI().toString());

    public static final Image DRAW_CARD_BUTTON = new Image(new File("images\\drawcardbutton1.png").toURI().toString());
    public static final Image DRAW_CARD_BUTTON_HOVER = new Image(new File("images\\drawcardbutton2.png").toURI().toString());

    public static final Image USE_ITEM_BUTTON = new Image(new File("images\\useitembutton1.png").toURI().toString());
    public static final Image USE_ITEM_BUTTON_HOVER = new Image(new File("images\\useitembutton2.png").toURI().toString());

    public static final Image AVATAR_IMAGE = new Image(new File("images\\avatar-frame.png").toURI().toString());

    public static final Image CONSOLE_BUTTON_OFF = new Image(new File("images\\consolebutton1.png").toURI().toString());
    public static final Image CONSOLE_BUTTON_ON = new Image(new File("images\\consolebutton2.png").toURI().toString());

    public static final Image END_TURN_BUTTON_OFF = new Image(new File("images\\endturnbutton1.png").toURI().toString());
    public static final Image END_TURN_BUTTON_ON = new Image(new File("images\\endturnbutton2.png").toURI().toString());

    public static final Image CUSTOM_CARD_BG_1 = new Image(new File("images\\customcardbg1.jpg").toURI().toString());
    public static final Image CUSTOM_ITEM_BACKGROUND = new Image(new File("images\\customItemBackground.jpg").toURI().toString());
    public static final Image SCROLL_PAPER_3 = new Image(new File("images\\scroll-paper-3.png").toURI().toString());
    public static final Image CREATE_BUTTON_OFF = new Image(new File("images\\create-button-off.png").toURI().toString());
    public static final Image CREATE_BUTTON_ON = new Image(new File("images\\create-button-on.png").toURI().toString());
    public static final Image CUSTOM_CARD_BG_3 = new Image(new File("images\\customcardbg3.1.png").toURI().toString());
    public static final Image CUSTOM_AMULET_BACKGROUND = new Image(new File("images\\customAmuletBackground.jpg").toURI().toString());

    public static final Image CUSTOM_CARD_BUTTON_ON = new Image(new File("images\\customcardon.png").toURI().toString());
    public static final Image CUSTOM_CARD_BUTTON_OFF = new Image(new File("images\\customcardoff.png").toURI().toString());
    public static final Image CUSTOM_ITEM_BUTTON_OFF = new Image(new File("images\\customitemoff.png").toURI().toString());
    public static final Image CUSTOM_ITEM_BUTTON_ON = new Image(new File("images\\customitemon.png").toURI().toString());
    public static final Image CUSTOM_AMULET_BUTTON_OFF = new Image(new File("images\\customamuletoff.png").toURI().toString());
    public static final Image CUSTOM_AMULET_BUTTON_ON = new Image(new File("images\\customamuleton.png").toURI().toString());

    public static final Image MONSTERCARD_BUTTON_OFF = new Image(new File("images\\monstercardbuttonoff.png").toURI().toString());
    public static final Image MONSTERCARD_BUTTON_ON = new Image(new File("images\\monstercardbuttonon.png").toURI().toString());
    public static final Image SPELLCARD_BUTTON_OFF = new Image(new File("images\\spellcardbuttonoff.png").toURI().toString());
    public static final Image SPELLCARD_BUTTON_ON = new Image(new File("images\\spellcardbuttonon.png").toURI().toString());
    public static final Image NORMAL_BUTTON_OFF = new Image(new File("images\\normalbuttonoff.png").toURI().toString());
    public static final Image NORMAL_BUTTON_ON = new Image(new File("images\\normalbuttonon.png").toURI().toString());
    public static final Image SPELLCASTER_BUTTON_OFF = new Image(new File("images\\spellcasterbuttonoff.png").toURI().toString());
    public static final Image SPELLCASTER_BUTTON_ON = new Image(new File("images\\spellcasterbuttonon.png").toURI().toString());
    public static final Image HERO_BUTTON_OFF = new Image(new File("images\\herobuttonoff.png").toURI().toString());
    public static final Image HERO_BUTTON_ON = new Image(new File("images\\herobuttonon.png").toURI().toString());
    public static final Image GENERAL_BUTTON_OFF = new Image(new File("images\\generalbuttonoff.png").toURI().toString());
    public static final Image GENERAL_BUTTON_ON = new Image(new File("images\\generalbuttonon.png").toURI().toString());
    public static final Image AURA_BUTTON_OFF = new Image(new File("images\\aurabuttonoff.png").toURI().toString());
    public static final Image AURA_BUTTON_ON = new Image(new File("images\\aurabuttonon.png").toURI().toString());
    public static final Image INSTANT_BUTTON_OFF = new Image(new File("images\\instantbuttonoff.png").toURI().toString());
    public static final Image INSTANT_BUTTON_ON = new Image(new File("images\\instantbuttonon.png").toURI().toString());
    public static final Image CONTS_BUTTON_OFF = new Image(new File("images\\continuousbuttonoff.png").toURI().toString());
    public static final Image CONTS_BUTTON_ON = new Image(new File("images\\continuousbuttonon.png").toURI().toString());
    public static final Image AURA_BUTTON_OFF_2 = new Image(new File("images\\aurabuttonoff2.png").toURI().toString());
    public static final Image AURA_BUTTON_ON_2 = new Image(new File("images\\aurabuttonon2.png").toURI().toString());
    public static final Image INSTANT_BUTTON_OFF_2 = new Image(new File("images\\instantbuttonoff2.png").toURI().toString());
    public static final Image INSTANT_BUTTON_ON_2 = new Image(new File("images\\instantbuttonon2.png").toURI().toString());
    public static final Image CONTS_BUTTON_OFF_2 = new Image(new File("images\\continuousbuttonoff2.png").toURI().toString());
    public static final Image CONTS_BUTTON_ON_2 = new Image(new File("images\\continuousbuttonon2.png").toURI().toString());
    public static final Image HEALER_BUTTON_OFF = new Image(new File("images\\healerbuttonoff.png").toURI().toString());
    public static final Image HEALER_BUTTON_ON = new Image(new File("images\\healerbuttonon.png").toURI().toString());
    public static final Image ATTACKER_BUTTON_OFF = new Image(new File("images\\attackerbuttonoff.png").toURI().toString());
    public static final Image ATTACKER_BUTTON_ON = new Image(new File("images\\attackerbuttonon.png").toURI().toString());
    public static final Image CARDMOVER_BUTTON_OFF = new Image(new File("images\\cardmoverbuttonoff.png").toURI().toString());
    public static final Image CARDMOVER_BUTTON_ON = new Image(new File("images\\cardmoverbuttonon.png").toURI().toString());
    public static final Image APCHANGER_BUTTON_OFF = new Image(new File("images\\apchangerbuttonoff.png").toURI().toString());
    public static final Image APCHANGER_BUTTON_ON = new Image(new File("images\\apchangerbuttonon.png").toURI().toString());
    public static final Image ADDSPELL_BUTTON_OFF = new Image(new File("images\\addspellbuttonoff.png").toURI().toString());
    public static final Image ADDSPELL_BUTTON_ON = new Image(new File("images\\addspellbuttonon.png").toURI().toString());
    public static final Image MAKECARD_BUTTON_OFF = new Image(new File("images\\makecardbuttonoff.png").toURI().toString());
    public static final Image MAKECARD_BUTTON_ON = new Image(new File("images\\makecardbuttonon.png").toURI().toString());
    public static final Image ADD_EXISTING_BUTTON_OFF = new Image(new File("images\\addebuttonoff.png").toURI().toString());
    public static final Image ADD_EXISTING_BUTTON_ON = new Image(new File("images\\addebuttonon.png").toURI().toString());
    public static final Image CREATE_NEW_BUTTON_OFF = new Image(new File("images\\createnewbuttonoff.png").toURI().toString());
    public static final Image CREATE_NEW_BUTTON_ON = new Image(new File("images\\createnewbuttonon.png").toURI().toString());
    public static final Image ADD_BATTLECRY_BUTTON_OFF = new Image(new File("images\\addbattlecrybuttonoff.png").toURI().toString());
    public static final Image ADD_BATTLECRY_BUTTON_ON = new Image(new File("images\\addbattlecrybuttonon.png").toURI().toString());
    public static final Image ADD_WILL_BUTTON_OFF = new Image(new File("images\\addwillbuttonoff.png").toURI().toString());
    public static final Image ADD_WILL_BUTTON_ON = new Image(new File("images\\addwillbuttonon.png").toURI().toString());
    public static final Image RANDOM_BUTTON_OFF = new Image(new File("images\\randomoff.png").toURI().toString());
    public static final Image RANDOM_BUTTON_ON = new Image(new File("images\\randomon.png").toURI().toString());
    public static final Image SELECTIVE_BUTTON_OFF = new Image(new File("images\\selectiveoff.png").toURI().toString());
    public static final Image SELECTIVE_BUTTON_ON = new Image(new File("images\\selectiveon.png").toURI().toString());
    public static final Image ALL_BUTTON_OFF = new Image(new File("images\\alloff.png").toURI().toString());
    public static final Image ALL_BUTTON_ON = new Image(new File("images\\allon.png").toURI().toString());
    public static final Image ELVEN_BUTTON_OFF = new Image(new File("images\\elvenoff.png").toURI().toString());
    public static final Image ELVEN_BUTTON_ON = new Image(new File("images\\elvenon.png").toURI().toString());
    public static final Image ATLANTIAN_BUTTON_OFF = new Image(new File("images\\atlantianoff.png").toURI().toString());
    public static final Image ATLANTIAN_BUTTON_ON = new Image(new File("images\\atlantianon.png").toURI().toString());
    public static final Image DRAGONBREED_BUTTON_OFF = new Image(new File("images\\dragonbreedoff.png").toURI().toString());
    public static final Image DRAGONBREED_BUTTON_ON = new Image(new File("images\\dragonbreedon.png").toURI().toString());
    public static final Image DEMONIC_BUTTON_OFF = new Image(new File("images\\demonicoff.png").toURI().toString());
    public static final Image DEMONIC_BUTTON_ON = new Image(new File("images\\demonicon.png").toURI().toString());
    public static final Image FRIENDLY_BUTTON_OFF = new Image(new File("images\\friendlyoff.png").toURI().toString());
    public static final Image FRIENDLY_BUTTON_ON  = new Image(new File("images\\friendlyon.png").toURI().toString());
    public static final Image ENEMY_BUTTON_OFF = new Image(new File("images\\enemyoff.png").toURI().toString());
    public static final Image ENEMY_BUTTON_ON = new Image(new File("images\\enemyon.png").toURI().toString());
    public static final Image BOTH_BUTTON_OFF = new Image(new File("images\\bothoff.png").toURI().toString());
    public static final Image BOTH_BUTTON_ON = new Image(new File("images\\bothon.png").toURI().toString());
    public static final Image CREATE_SPELL_BUTTON_OFF = new Image(new File("images\\createspellbuttonoff.png").toURI().toString());
    public static final Image CREATE_SPELL_BUTTON_ON = new Image(new File("images\\createspellbuttonon.png").toURI().toString());
    public static final Image CREATE_BLUE_BUTTON_OFF = new Image(new File("images\\createbluebuttonoff.png").toURI().toString());
    public static final Image CREATE_BLUE_BUTTON_ON = new Image(new File("images\\createbluebuttonon.png").toURI().toString());


    public static final Media MAIN_MENU_BACKGROUND_MUSIC = new Media(new File("audio\\mainMenuBackgroundMusic.wav").toURI().toString());
    public static final Media SHOP_BACKGROUND_MUSIC = new Media(new File("audio\\shopBackgroundMusic.wav").toURI().toString());
    public static final Media CUSTOM_BACKGROUND_MUSIC = new Media(new File("audio\\custom-background-music.mp3").toURI().toString());
    private static final Media CLICK_SOUND = new Media(new File("sounds\\click.wav").toURI().toString());
    private static final Media BUY_SOUND = new Media(new File("sounds\\buy-sound.wav").toURI().toString());
    private static final Media SELL_SOUND = new Media(new File("sounds\\sell-sound.wav").toURI().toString());
    private static final Media TURN_PAGE_SOUND = new Media(new File("sounds\\turn-page.wav").toURI().toString());
    private static final Media DRAW_MONSTER_CARD_SOUND = new Media(new File("sounds\\drawMonsterCardSound.wav").toURI().toString());
    private static final Media DRAW_SPELL_CARD_SOUND = new Media(new File("sounds\\drawSpellCardSound.wav").toURI().toString());
    private static final Media USE_ITEM_SOUND = new Media(new File("sounds\\useItemSound.wav").toURI().toString());
    private static final Media CREATE_ITEM_SOUND = new Media(new File("sounds\\creating-custom-item-sound.wav").toURI().toString());
    private static final Media CREATE_AMULET_SOUND = new Media(new File("sounds\\creating-custom-amulet-sound.mp3").toURI().toString());

    public static void playClickSound(){
        MediaPlayer player = new MediaPlayer(CLICK_SOUND);
        player.play();
    }

    public static void playBuySound(){
        MediaPlayer player = new MediaPlayer(BUY_SOUND);
        player.setVolume(0.6);
        player.play();
    }

    public static void playSellSound(){
        MediaPlayer player = new MediaPlayer(SELL_SOUND);
        player.play();
    }

    public static void playTurnPageSound(){
        MediaPlayer player = new MediaPlayer(TURN_PAGE_SOUND);
        player.play();
    }

    public static void playDrawMonsterCardSound(){
        MediaPlayer player = new MediaPlayer(DRAW_MONSTER_CARD_SOUND);
        player.setVolume(0.8);
        player.play();
    }

    public static void playDrawSpellCardSound(){
        MediaPlayer player = new MediaPlayer(DRAW_SPELL_CARD_SOUND);
        player.setVolume(0.5);
        player.play();
    }

    public static void playUseItemSound(){
        MediaPlayer player = new MediaPlayer(USE_ITEM_SOUND);
        player.setVolume(0.5);
        player.play();
    }

    public static void playCreateItemSound(){
        MediaPlayer player = new MediaPlayer(CREATE_ITEM_SOUND);
        player.play();
    }

    public static void playCreateAmuletSound(){
        MediaPlayer player = new MediaPlayer(CREATE_AMULET_SOUND);
        player.play();
    }
}
