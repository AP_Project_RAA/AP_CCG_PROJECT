package main;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.*;
import java.util.Scanner;

public class Main extends Application {
    Group root;
    Scene MainScene;
    public static GameManager gameManager = new GameManager();

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setResizable(false);

        root = new Group();
        MainScene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        ImageView mainbg = new ImageView(Dictionary.MAIN_MENU_BACKGROUND);
        mainbg.setPreserveRatio(true);
        mainbg.fitWidthProperty().bind(MainScene.widthProperty());
        root.getChildren().add(mainbg);

        ImageView smokeGifImageView1 = new ImageView(Dictionary.SMOKE_GIF);
        smokeGifImageView1.setFitWidth(Dictionary.WINDOW_WIDTH);
        smokeGifImageView1.setFitHeight(Dictionary.WINDOW_HEIGHT);
        smokeGifImageView1.setOpacity(0.18);
        ImageView smokeGifImageView2 = new ImageView(Dictionary.SMOKE_GIF);
        smokeGifImageView2.setFitWidth(Dictionary.WINDOW_WIDTH);
        smokeGifImageView2.setFitHeight(Dictionary.WINDOW_HEIGHT);
        smokeGifImageView2.setOpacity(0.18);
        smokeGifImageView2.setRotate(180);
        root.getChildren().addAll(smokeGifImageView1, smokeGifImageView2);

        Label loadFilesTitle = new Label("Saved Game CheckPoints:");
        loadFilesTitle.setAlignment(Pos.CENTER);
        loadFilesTitle.setPrefWidth(500);
        loadFilesTitle.setLayoutX(MainScene.getWidth() / 2 - 250);
        loadFilesTitle.setLayoutY(100);
        loadFilesTitle.setFont(Font.font("Algerian", FontWeight.BOLD, 32));
        loadFilesTitle.setTextFill(Color.WHITE);
        root.getChildren().add(loadFilesTitle);

        InputStream loadFiles = new FileInputStream("datafiles/checkpoints.txt");
        Scanner scanner = new Scanner(loadFiles);
        String read;
        int fileNo = 0;
        while(scanner.hasNext()) {
            read = scanner.nextLine();
            Label fileLabel = new Label(read);
            fileLabel.setFont(Font.font("Times New Roman", 14));
            fileLabel.setTextAlignment(TextAlignment.CENTER);
            fileLabel.setAlignment(Pos.CENTER);
            fileLabel.setPrefWidth(200);
            fileLabel.setLayoutX(MainScene.getWidth() / 2 - 100);
            fileLabel.setLayoutY(200 + fileNo * 40);
            fileLabel.setStyle("-fx-background-color: White");
            fileLabel.setOpacity(0.7);
            fileNo++;
            fileLabel.setOnMouseEntered(event -> {
                fileLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
                fileLabel.setOpacity(1);
                MainScene.setCursor(Cursor.HAND);
            });
            fileLabel.setOnMouseExited(event -> {
                fileLabel.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 14));
                fileLabel.setOpacity(0.7);
                MainScene.setCursor(Cursor.DEFAULT);
            });
            fileLabel.setOnMouseClicked(event -> {
                gameManager.checkPoint = new CheckPoint();
                gameManager.checkPoint.setName(fileLabel.getText());
                gameManager.init(primaryStage);
            });
            root.getChildren().add(fileLabel);
        }

        Button newGameButton = new Button("New Game");
        newGameButton.setPrefWidth(200);
        newGameButton.setTranslateX(MainScene.getWidth() / 2 - 200 - 10);
        newGameButton.setTranslateY(200 + fileNo * 40 + 50);
        root.getChildren().add(newGameButton);

        TextField fileName = new TextField("New Game " + (fileNo+1));
        fileName.setPrefWidth(200);
        fileName.setTranslateX(MainScene.getWidth() / 2 + 10);
        fileName.setTranslateY(200 + fileNo * 40 + 50);
        root.getChildren().add(fileName);

        newGameButton.setOnMouseClicked(event -> {
            if(!fileName.getText().equals("")) {
                gameManager.checkPoint = new CheckPoint();
                gameManager.checkPoint.setName(fileName.getText());
                CardMaker.make(gameManager.checkPoint);
                try {
                    PrintWriter printWriter = new PrintWriter(new FileOutputStream(new File("datafiles/checkpoints.txt"), true));
                    printWriter.append("\n").append(fileName.getText());
                    printWriter.flush();
                    printWriter.close();
                } catch (FileNotFoundException e) {
                    System.out.println("Couldn't make load file directory/files");
                }
                gameManager.init(primaryStage);
            }
        });

        primaryStage.setScene(MainScene);
        primaryStage.show();
    }

    public static void main(String[] args){
        launch(args);
    }
}
