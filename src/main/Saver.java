package main;

import gameObjects.SpellPack;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.*;
import gameObjects.cards.spellCards.SpellCard;
import gameObjects.spells.Spell;
import states.CardShop;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Saver {
    private static CheckPoint checkPoint;
    private static Player player;
    private static GameManager gameManager;
    private static CardShop cardShop;

    public static CheckPoint getCheckPoint() {
        return checkPoint;
    }

    public static void setCheckPoint(CheckPoint checkPoint) {
        Saver.checkPoint = checkPoint;
    }

    public static Player getPlayer() {
        return player;
    }

    public static void setPlayer(Player player) {
        Saver.player = player;
    }

    public static GameManager getGameManager() {
        return gameManager;
    }

    public static void setGameManager(GameManager gameManager) {
        Saver.gameManager = gameManager;
    }

    public static CardShop getCardShop() {
        return cardShop;
    }

    public static void setCardShop(CardShop cardShop) {
        Saver.cardShop = cardShop;
    }

    public static void save() {
        try {
            FileOutputStream fout = new FileOutputStream("datafiles\\" + checkPoint.getName() + "\\playersave\\player.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeUnshared(player);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Couldn't save player");
        }

    }

    public static void saveMonsterCard(MonsterCard monsterCard) {
        try {
            PrintWriter pathOut = new PrintWriter(new FileOutputStream(new File("datafiles\\" + checkPoint.getName() + "\\Paths\\MonsterPaths.txt"), true));
            pathOut.append("datafiles/" + checkPoint.getName() + "/Cards/MonsterCard/" + monsterCard.getClass().getSimpleName() + "/" + monsterCard.getName() + ".txt\n");
            pathOut.flush();
            pathOut.close();

            File dir = new File("datafiles/" + checkPoint.getName() + "/Cards/MonsterCard/" + monsterCard.getClass().getSimpleName());
            dir.mkdir();

            PrintWriter fout = new PrintWriter("datafiles/" + checkPoint.getName() + "/Cards/MonsterCard/" + monsterCard.getClass().getSimpleName() + "/" + monsterCard.getName() + ".txt");
            fout.write(monsterCard.toString());
            fout.close();
        } catch (IOException e) {
            System.out.println(monsterCard.getName() + ".txt Not Found");
        }

    }

    public static void saveSpellCard(SpellCard spellCard) {
        try {
            PrintWriter pathOut = new PrintWriter(new FileOutputStream(new File("datafiles\\" + checkPoint.getName() + "\\Paths\\SpellPaths.txt"), true));
            pathOut.append("datafiles/" + checkPoint.getName() + "/Cards/SpellCard/" + spellCard.getClass().getSimpleName() + "/" + spellCard.getName() + ".txt\n");
            pathOut.flush();
            pathOut.close();

            File dir = new File("datafiles/" + checkPoint.getName() + "/Cards/SpellCard/" + spellCard.getClass().getSimpleName());
            dir.mkdir();

            PrintWriter fout = new PrintWriter("datafiles/" + checkPoint.getName() + "/Cards/SpellCard/" + spellCard.getClass().getSimpleName() + "/" + spellCard.getName() + ".txt");
            fout.write(spellCard.toString());
            fout.close();
        } catch (IOException e) {
            System.out.println(spellCard.getName() + ".txt Not Found");
        }
    }

    public static void saveSpellPack(SpellPack spellPack) {
        try {
            PrintWriter pathOut = new PrintWriter(new FileOutputStream(new File("datafiles\\" + checkPoint.getName() + "\\Paths\\SpellPackPaths.txt"), true));
            pathOut.append("datafiles\\" + checkPoint.getName() + "\\SpellPacks\\" + spellPack.getName() + "\\spellPackData.ser" + "\n");
            pathOut.flush();
            pathOut.close();

            File dir = new File("datafiles\\" + checkPoint.getName() + "\\SpellPacks\\" + spellPack.getName());
            dir.mkdir();

            FileOutputStream fout = new FileOutputStream("datafiles\\" + checkPoint.getName() + "\\SpellPacks\\" + spellPack.getName() + "\\spellPackData.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeUnshared(spellPack);
        } catch (IOException e) {
            System.out.println(spellPack.getName() + ".txt Not Found");
        }
    }

    public static ArrayList<SpellPack> loadSpellPacks() {
        ArrayList<SpellPack> spellPacks = new ArrayList<>();

        try {
            BufferedReader pathIn = new BufferedReader(new FileReader("datafiles\\" + checkPoint.getName() + "\\Paths\\SpellPackPaths.txt"));
            String path;
            while ((path = pathIn.readLine()) != null) {
                FileInputStream fin = new FileInputStream(path);
                ObjectInputStream ois = new ObjectInputStream(fin);
                SpellPack loadedSpellPack = (SpellPack) ois.readObject();
                spellPacks.add(loadedSpellPack);
            }
        } catch (Exception e) {
            System.out.println("Couldn't load spell packs.");
            e.printStackTrace();
        }

        return spellPacks;
    }

    public static void loadCards() {
        ArrayList<Card> totalCards = new ArrayList<>();
        totalCards.addAll(gameManager.loadMonsterCards());
        totalCards.addAll(gameManager.loadSpellCards());
        CardShop cardShop = new CardShop(totalCards);
        gameManager.setCardShop(cardShop);
    }
}
