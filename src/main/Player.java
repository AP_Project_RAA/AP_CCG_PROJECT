package main;

import enums.Field;
import gameObjects.Amulet;
import gameObjects.GameObject;
import gameObjects.Item;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.MonsterCard;
import gameObjects.cards.spellCards.AuraSpell;
import gameObjects.cards.spellCards.ContinuousSpell;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import states.Inventory;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Player extends GameObject implements Serializable {
    private static final long serialVersionUID = 1L;

    /**/private int HP;
    private int credit;
    private int level;
    private int maxMP;
    private int maxHP;
    private int mysticHourGlass = 0;
    /**/private Amulet amulet;
    /**/private ArrayList<Item> items;
    private String name;
    /**/private HashMap<Field, GamePlace> places = new HashMap<>();
    /**/private Inventory inventory;
    /**/public int MP;
    transient private Group avatar;
    transient private Label playerName, hp;
    transient private Rectangle MPbg, MPbar;
    transient private Label MPLabel;
    transient private ImageView playerImage;

    public void setMysticHourGlass(int mysticHourGlass) {
        this.mysticHourGlass = mysticHourGlass;
    }

    public int getMysticHourGlass() {
        return mysticHourGlass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        playerImage = new ImageView(new Image(new File("images\\" + getName() + ".jpg").toURI().toString()));
        playerImage.setLayoutX(15);
        playerImage.setLayoutY(15);
        playerImage.setFitWidth(90);
        playerImage.setFitHeight(90);
        avatar.getChildren().add(playerImage);

        ImageView frame = new ImageView(Dictionary.AVATAR_IMAGE);
        frame.setLayoutX(5);
        frame.setLayoutY(5);
        frame.setFitWidth(110);
        frame.setFitHeight(110);
        avatar.getChildren().add(frame);
    }

    public int getMaxMP() {
        return maxMP;
    }

    public void setMaxMP(int maxMP) {
        this.maxMP = maxMP;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

    public HashMap<Field, GamePlace> getPlaces() {
        return places;
    }

    public Player(int initialHP, int initialCredit) {
        this.HP = initialHP;
        this.maxHP = initialHP;
        this.credit = initialCredit;
        this.level = 1;
        this.amulet = null;
        items = new ArrayList<>();

        places.put(Field.DECK, new GamePlace());
        places.put(Field.GRAVEYARD, new GamePlace());
        places.put(Field.HAND, new GamePlace());
        places.put(Field.MONSTERFIELD, new GamePlace());
        places.put(Field.SPELLFIELD, new GamePlace());
        places.put(Field.PLAYFIELD, new GamePlace());
        inventory = new Inventory();
        maxMP = 10;

        //applying gfx
        avatar = new Group();
        Rectangle frameBackground = new Rectangle(0, 0, 200, 120);
        frameBackground.setFill(Color.BLACK);
        frameBackground.setOpacity(0.8);
        avatar.getChildren().add(frameBackground);

        playerName = new Label(getName());
        playerName.setFont(Font.font("Algerian", FontWeight.BOLD, 14));
        playerName.setLayoutY(20);
        playerName.setLayoutX(120);
        playerName.setPrefWidth(80);
        playerName.setWrapText(true);
        playerName.setTextFill(Color.WHITE);
        playerName.setAlignment(Pos.CENTER_LEFT);
        avatar.getChildren().add(playerName);

        hp = new Label("HP: " + getHP());
        hp.setFont(Font.font("Times New Roman", FontWeight.BOLD, 12));
        hp.setLayoutX(120);
        hp.setLayoutY(70);
        hp.setTextFill(Color.WHITE);
        avatar.getChildren().add(hp);

        MPbg = new Rectangle(0, 122, 200, 20);
        MPbg.setFill(Color.BLACK);
        avatar.getChildren().add(MPbg);

        MPLabel = new Label("MP");
        MPLabel.setLayoutX(0);
        MPLabel.setLayoutY(122);
        MPLabel.setPrefWidth(30);
        MPLabel.setPrefHeight(20);
        MPLabel.setAlignment(Pos.CENTER_LEFT);
        MPLabel.setFont(Font.font("Times New Roman", 11));
        MPLabel.setTextFill(Color.WHITE);
        avatar.getChildren().add(MPLabel);

        MPbar = new Rectangle(30, 128, 10, 8);
        MPbar.setFill(Color.GREEN);
        avatar.getChildren().add(MPbar);
    }

    public GamePlace getMonsterField() {
        return places.get(Field.MONSTERFIELD);
    }

    public void setMonsterField(GamePlace monsterField) {
        places.put(Field.MONSTERFIELD, monsterField);
    }

    public GamePlace getSpellField() {
        return places.get(Field.SPELLFIELD);
    }

    public void setSpellField(GamePlace spellField) {
        places.put(Field.SPELLFIELD, spellField);
    }

    public GamePlace getGraveYard() {
        return places.get(Field.GRAVEYARD);
    }

    public void setGraveYard(GamePlace graveYard) {
        places.put(Field.GRAVEYARD, graveYard);
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = Math.min(HP, maxHP);
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Amulet getAmulet() {
        return amulet;
    }

    public void setAmulet(Amulet amulet) {
        this.amulet = amulet;
    }

    public GamePlace getHand() {
        return places.get(Field.HAND);
    }

    public void setHand(GamePlace hand) {
        places.put(Field.HAND, hand);
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public GamePlace getDeck() {
        return places.get(Field.DECK);
    }

    public void setDeck(GamePlace deck) {
        places.put(Field.DECK, deck);
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public ArrayList<AuraSpell> getAuraSpells() {
        ArrayList<Card> spellCards = getPlaces().get(Field.SPELLFIELD).getCards();
        ArrayList<AuraSpell> ret = new ArrayList<>();
        for (Card spellCard : spellCards) {
            if (spellCard instanceof AuraSpell) {
                AuraSpell auraSpell = (AuraSpell) spellCard;
                ret.add(auraSpell);
            }
        }
        return ret;
    }

    public ArrayList<MonsterCard> getDefenderCards() {
        ArrayList<Card> monsterCards = getPlaces().get(Field.MONSTERFIELD).getCards();
        ArrayList<MonsterCard> ret = new ArrayList<>();
        for (Card monsterCard1 : monsterCards) {
            MonsterCard monsterCard = (MonsterCard) monsterCard1;
            if (monsterCard.isDefensive)
                ret.add(monsterCard);
        }
        return ret;
    }

    public ArrayList<ContinuousSpell> getContinuousSpells() {
        ArrayList<Card> spellCards = getPlaces().get(Field.SPELLFIELD).getCards();
        ArrayList<ContinuousSpell> continuousSpells = new ArrayList<>();
        for (Card spellCard : spellCards) {
            if (spellCard instanceof ContinuousSpell) {
                ContinuousSpell continuousSpell = (ContinuousSpell) spellCard;
                continuousSpells.add(continuousSpell);
            }
        }
        return continuousSpells;
    }

    public ArrayList<Card> getPossibleCardsToDraw() {
        ArrayList<Card> ret = new ArrayList<>();
        ArrayList<Card> hand = getHand().getCards();
        for (Card card : hand) {
            if (card.getMP() <= this.MP)
                ret.add(card);
        }
        return ret;
    }

    public ArrayList<MonsterCard> getPossibleMonsterCardsToUse() {
        ArrayList<MonsterCard> ret = new ArrayList<>();
        ArrayList<Card> monsterCards = getMonsterField().getCards();
        for (Card monsterCard1 : monsterCards) {
            MonsterCard monsterCard = (MonsterCard) monsterCard1;
            if (monsterCard.getRound() > 0 || monsterCard.isNimble) {
                ret.add(monsterCard);
            }
        }
        return ret;
    }

    public Player getCopy() {
        Player player = new Player(HP, credit);
        player.setInventory(inventory.getCopy());
        player.setDeck(places.get(Field.DECK).getCopy());
        player.setHand(places.get(Field.HAND).getCopy());
        player.setMonsterField(places.get(Field.MONSTERFIELD).getCopy());
        player.setSpellField(places.get(Field.SPELLFIELD).getCopy());
        player.setGraveYard(places.get(Field.GRAVEYARD).getCopy());
        if (amulet != null)
            player.setAmulet(amulet.getCopy());
        player.setLevel(level);
        player.setMaxMP(maxMP);

        //copying items
        ArrayList<Item> itemCopy = new ArrayList<>();
        for (Item item : items) {
            itemCopy.add(item.getCopy());
        }
        player.setItems(itemCopy);
        player.setName(name);

        return player;
    }

    public Group getAvatar() {
        return avatar;
    }

    public void updateAvatar() {
        playerName.setText(getName());
        hp.setText("HP: " + getHP());
        MPLabel.setText("MP:" + MP);
        MPbar.setWidth(MP * 170 / maxMP);
    }

    public void removeAmuletAndApplyChanges() {
        maxHP -= amulet.getMaxHP_Effect();
        maxMP -= amulet.getMaxMP_Effect();
        amulet = null;
    }
}