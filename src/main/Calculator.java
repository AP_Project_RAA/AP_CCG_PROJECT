package main;

import gameObjects.cards.Card;

import java.util.ArrayList;

public class Calculator {

    public static Calculator instance = new Calculator();

    public int numberOfCardInArray(ArrayList<Card> array, Card card){
        int result = 0;
        for(Card c : array){
            if(card.equals(c))
                result++;
        }
        return result;
    }
}