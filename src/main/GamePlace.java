package main;

import gameObjects.GameObject;
import gameObjects.cards.Card;

import java.io.Serializable;
import java.util.ArrayList;

public class GamePlace extends GameObject implements Serializable {
    private static final long serialVersionUID = 5L;

    private ArrayList<Card> cards;

    public GamePlace(){
        cards = new ArrayList<>();
    }

    public ArrayList<Card> getCards(){
        return cards;
    }

    public void setCards(ArrayList<Card> cards){
        this.cards = cards;
    }

    public GamePlace getCopy(){
        GamePlace copy = new GamePlace();
        for(Card card : cards){
            copy.getCards().add(card.getCopy());
        }
        return copy;
    }
}