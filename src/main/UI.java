package main;

import gameObjects.SpellPack;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.MonsterCard;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;

public class UI {

    public static UI instance = new UI();

    private Text consoleContent;
    private ScrollPane consolePane;

    public void showFullSpellField() {
        System.out.println(Dictionary.FULL_SPELLFIELD);
    }

    public void showOutput(String output) {
        consoleContent.setText(consoleContent.getText() + "\n> " + output);
        consolePane.layout();
        consolePane.setVvalue(1);
    }

    public void showBattleRound(int round) {
        showOutput("Turn " + round + " Started");
    }

    public void showMPStats(Player player) {
        showOutput("MP status: " + player.MP + "/" + player.getMaxMP());
    }

    public void showBattleStart(Player enemy) {
        showOutput("Battle against " + enemy.getName() + " Started");
    }

    public void showBattleTurn(Player player) {
        showOutput(player.getName() + " Starts the battle.");
    }

    public void showAttackEndDialog(MonsterCard monsterCard, MonsterCard enemyCard) {
        String res;
        if (enemyCard.isDefensive) {
            res = "\n> " + "Attacker " + monsterCard.getName() + " Clashed with Defender: " + enemyCard.getName();
            showOutput(res);
        } else {
            res = "\n> " + "Attacker " + monsterCard.getName() + " Clashed with Target: " + enemyCard.getName();
            showOutput(res);
        }
    }

    public void showAttackEndDialog(MonsterCard monsterCard, Player enemy) {
        showOutput("\n> " + "Attacker " + monsterCard.getName() + " Clashed with Enemy: " + enemy.getName());
    }

    public String showSpellInfo(String name, String info) {
        String res = "\n> " + "SpellCard " + name + " has cast a spell:\n> " + info;
        showOutput(res);
        return res;
    }

    public void showTurn(Player player) {
        showOutput("\n> " + player.getName() + "\'s Turn");
    }

    public void showEnemySetCard(Player enemy, Card card) {
        showOutput("\n> " + enemy.getName() + " Set card " + card.getName() + " on the field.");
    }

    public void showBattleCry(Card card, SpellPack battleCry) {
        showOutput("\n> " + card.getName() + " BattleCry");
    }

    public void showEnemyUseCard(Player enemy, MonsterCard monsterCard) {
        showOutput("\n> " + enemy.getName() + " is using card: " + monsterCard.getName());
    }

    public void setConsoleContent(Text consoleContent) {
        this.consoleContent = consoleContent;
    }

    public void setConsolePane(ScrollPane consolePane) {
        this.consolePane = consolePane;
    }
}