package main;

import enums.Breed;
import enums.Field;
import enums.Side;
import enums.SpellType;
import gameObjects.Amulet;
import gameObjects.Item;
import gameObjects.SpellPack;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.General;
import gameObjects.cards.monsterCards.Hero;
import gameObjects.cards.monsterCards.Normal;
import gameObjects.cards.monsterCards.SpellCaster;
import gameObjects.cards.spellCards.AuraSpell;
import gameObjects.cards.spellCards.ContinuousSpell;
import gameObjects.cards.spellCards.InstantSpell;
import gameObjects.spells.*;
import javafx.stage.Stage;
import map.Map;
import network.Peer_to_Peer.Client;
import network.Peer_to_Peer.ClientBattle;
import network.Peer_to_Peer.Server;
import network.Peer_to_Peer.ServerBattle;
import states.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class GameManager {

    private Player player;
    private Player[] enemies = new Player[4];
    public CheckPoint checkPoint;
    private MainMenu mainMenu;
    private MultiPlayer multiPlayer;
    private Online online;
    private ShopMenu shopMenu;
    private CardShop cardShop;
    private ItemShop itemShop;
    private AmuletShop amuletShop;
    private Inventory inventory;
    private Battle battle;
    private Map map;
    private Stage stage;
    private Saver saver;

    private ServerBattle serverBattle;
    private ClientBattle clientBattle;
    private network.ClientBattle onlineClientBattle;

    private CustomMenu customMenu;
    private CustomCard customCard;
    private CustomItem customItem;
    private CustomAmulet customAmulet;

    public void init(Stage stage) {
        Saver.setCheckPoint(checkPoint);
        Saver.setGameManager(this);

        ArrayList<Card> totalCards = loadMonsterCards();
        totalCards.addAll(loadSpellCards());
        mainMenu = new MainMenu();
        multiPlayer = new MultiPlayer();
        online = new Online();
        onlineClientBattle = new network.ClientBattle();
        shopMenu = new ShopMenu();
        cardShop = new CardShop(totalCards);

        itemShop = new ItemShop(loadItems());
        amuletShop = new AmuletShop(loadAmulets());
        battle = new Battle();
        serverBattle = new ServerBattle();
        clientBattle = new ClientBattle();
        map = new Map("map/1.tmx");
        player = loadPlayer();

        //custom initialization
        customMenu = new CustomMenu();
        customCard = new CustomCard();
        customItem = new CustomItem();
        customAmulet = new CustomAmulet();

        inventory = player.getInventory();
        enemies = loadEnemies();
        this.stage = stage;

        setMainMenuReferences();
        setShopMenuReferences();
        setCardShopReferences();
        setItemShopReferences();
        setAmuletShopReferences();
        setInventoryReferences();
        setBattleReferences();
        setMapReferences();
        setServerBattleReferences();
        setClientBattleReferences();
        setMultiPlayerReferences();
        setOnlineReferences();
        setOnlineClientBattleReferences();

        //custom references
        setCustomMenuReferences();
        setCustomCardReferences();
        setCustomItemReferences();
        setCustomAmuletReferences();

        setupScenes();

        mainMenu.run();
    }

    private void setupScenes() {
        mainMenu.setupScene();
        shopMenu.setupScene();
        cardShop.setupScene();
        itemShop.setupScene();
        amuletShop.setupScene();
        inventory.setupScene();
        battle.setupScene();
        multiPlayer.setupScene();
        online.setupScene();
        customMenu.setupScene();
        customCard.setupScene();
        customItem.setupScene();
        customAmulet.setupScene();
        onlineClientBattle.setupScene();
    }

    public Player loadPlayer() {
        Player player = new Player(20000, 10000);
        player.setName("Player");

        //if a save of player exists, loading it
        try {
            FileInputStream fin = new FileInputStream("datafiles\\" + checkPoint.getName() + "\\playersave\\player.ser");
            ObjectInputStream ois = new ObjectInputStream(fin);
            Player loadedPlayer = (Player) ois.readObject();

            System.out.println("loaded player from file");
            player = loadedPlayer.getCopy();

            saver.setPlayer(player);
        } catch (FileNotFoundException f) {
            //a save of player doesn't exist
            try {
                InputStream playerInputStream = new FileInputStream("datafiles/" + checkPoint.getName() + "/Players/Player.txt");
                Scanner scanner = new Scanner(playerInputStream);
                String read;
                while ((scanner.hasNext())) {
                    read = scanner.nextLine();
                    String[] data = read.split("/");

                    if (data[0].equals("Mystic_Hourglass"))
                        player.setMysticHourGlass(Integer.parseInt(data[1]));

                    for (Card c : cardShop.getCardsInShop()) {
                        if (c.getName().equals(data[0])) {
                            for (int j = 0; j < Integer.parseInt(data[1]); j++) {
                                /**/
                                player.getPlaces().get(Field.DECK).getCards().add(c.getCopy());
                            }
                        }
                    }
                    for (Item item : itemShop.getItemsInShop()) {
                        if (item.getName().equals(data[0])) {
                            for (int j = 0; j < Integer.parseInt(data[1]); j++) {
                                player.getInventory().getItems().add(item.getCopy());
                                player.getItems().add(item.getCopy());
                            }
                        }
                    }
                    for (Amulet amulet : amuletShop.getAmuletsInShop()) {
                        if (amulet.getName().equals(data[0])) {
                            for (int j = 0; j < Integer.parseInt(data[1]); j++) {
                                player.getInventory().getAmulets().add(amulet.getCopy());
                            }
                        }
                    }
                }

                saver.setPlayer(player);

            } catch (IOException e) {
                System.out.println("Fatal Error! Player.txt was not found // couldn't save player");
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        Saver.save();

        return player;
    }

    public void savePlayer(Player player) throws IOException {
        FileOutputStream fout = new FileOutputStream("datafiles\\" + checkPoint.getName() + "\\playersave\\player.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fout);
        oos.writeObject(player);
    }

    private Player[] loadEnemies() {

        Player[] enemy = new Player[4];

        for (int i = 0; i < 4; i++) {
            enemy[i] = new Player(20000, 0);
            enemy[i].setLevel(i + 1);
            try {
                InputStream enemyInputStream = new FileInputStream("datafiles/" + checkPoint.getName() + "/Players/Enemy" + String.valueOf(i + 1) + ".txt");
                Scanner scanner = new Scanner(enemyInputStream);
                String read;
                while (scanner.hasNext()) {
                    read = scanner.nextLine();
                    String[] data = read.split("/");

                    for (Card c : cardShop.getCardsInShop()) {
                        if (c.getName().equals(data[0])) {
                            for (int j = 0; j < Integer.parseInt(data[1]); j++) {
                                enemy[i].getDeck().getCards().add(c.getCopy());
                            }
                        }
                    }
                    for (Item item : itemShop.getItemsInShop()) {
                        if (item.getName().equals(data[0])) {
                            for (int j = 0; j < Integer.parseInt(data[1]); j++) {
                                enemy[i].getInventory().getItems().add(item.getCopy());
                                enemy[i].getItems().add(item.getCopy());
                            }
                        }
                    }
                    for (Amulet amulet : amuletShop.getAmuletsInShop()) {
                        if (amulet.getName().equals(data[0])) {
                            for (int j = 0; j < Integer.parseInt(data[1]); j++) {
                                enemy[i].getInventory().getAmulets().add(amulet.getCopy());
                            }
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println("Fatal Error! Enemy" + String.valueOf(i + 1) + ".txt was not found");
            }
        }
        enemy[0].setName("Goblin Chieftain");
        enemy[1].setName("Ogre Warlord");
        enemy[2].setName("Vampire Lord");
        enemy[3].setName("Lucifer");

        return enemy;
    }

    public ArrayList<Card> loadMonsterCards() {
        HashMap<String, Field> field = new HashMap<String, Field>();
        field.put("GRAVEYARD", Field.GRAVEYARD);
        field.put("DECK", Field.DECK);
        field.put("HAND", Field.HAND);
        field.put("SPELLFIELD", Field.SPELLFIELD);
        field.put("MONSTERFIELD", Field.MONSTERFIELD);
        field.put("PLAYFIELD", Field.PLAYFIELD);

        HashMap<String, Breed> breed = new HashMap<String, Breed>();
        breed.put("ELVEN", Breed.ELVEN);
        breed.put("DRAGONBREED", Breed.DRAGONBREED);
        breed.put("ATLANTIAN", Breed.ATLANTIAN);
        breed.put("DEMONIC", Breed.DEMONIC);

        HashMap<String, SpellType> spellType = new HashMap<String, SpellType>();
        spellType.put("RANDOM", SpellType.RANDOM);
        spellType.put("ALL", SpellType.ALL);
        spellType.put("SELECTIVE", SpellType.SELECTIVE);

        HashMap<String, Side> side = new HashMap<String, Side>();
        side.put("FRIENDLY", Side.FRIENDLY);
        side.put("ENEMY", Side.ENEMY);
        side.put("BOTH", Side.BOTH);

        ArrayList<Card> cards = new ArrayList<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader("datafiles/" + checkPoint.getName() + "/Paths/MonsterPaths.txt"));
            String pathIn;
            int i = 0;
            while ((pathIn = in.readLine()) != null) {
                try {
                    BufferedReader fin = new BufferedReader(new FileReader(pathIn));
                    String read;
                    String type = new String();
                    String[] data;
                    ArrayList<Spell> spell = new ArrayList<Spell>();
                    ArrayList<Spell> battleCry = new ArrayList<Spell>();
                    ArrayList<Spell> will = new ArrayList<Spell>();

                    while ((read = fin.readLine()) != null) {
                        if (read.length() != 0) {
                            data = read.split("/");
                            if (data.length == 1) {
                                type = data[0];
                            } else {
                                switch (data[0]) {
                                    case "Healer":
                                        if (type.equals("Spell")) {
                                            spell.add(new Healer(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), data[7]));
                                        } else if (type.equals("BattleCry")) {
                                            battleCry.add(new Healer(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), data[7]));
                                        } else if (type.equals("Will")) {
                                            will.add(new Healer(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), data[7]));
                                        }
                                        break;
                                    case "Attacker":
                                        if (type.equals("Spell")) {
                                            spell.add(new Attacker(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), data[7]));
                                        } else if (type.equals("BattleCry")) {
                                            battleCry.add(new Attacker(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), data[7]));
                                        } else if (type.equals("Will")) {
                                            will.add(new Attacker(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), data[7]));
                                        }
                                        break;
                                    case "CardMover":
                                        if (type.equals("Spell")) {
                                            spell.add(new CardMover(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], side.get(data[6]), field.get(data[7]), side.get(data[8]), field.get(data[9])));
                                        } else if (type.equals("BattleCry")) {
                                            battleCry.add(new CardMover(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], side.get(data[6]), field.get(data[7]), side.get(data[8]), field.get(data[9])));
                                        } else if (type.equals("Will")) {
                                            will.add(new CardMover(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], side.get(data[6]), field.get(data[7]), side.get(data[8]), field.get(data[9])));
                                        }
                                        break;
                                    case "ApChanger":
                                        if (type.equals("Spell")) {
                                            spell.add(new ApChanger(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), side.get(data[7])));
                                        } else if (type.equals("BattleCry")) {
                                            battleCry.add(new ApChanger(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), side.get(data[7])));
                                        } else if (type.equals("Will")) {
                                            will.add(new ApChanger(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), side.get(data[7])));
                                        }
                                        break;
                                    default:
                                        switch (data[4]) {
                                            case "Normal":
                                                cards.add(new Normal(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]), Integer.parseInt(data[3]), data[5], breed.get(data[6])));
                                                break;
                                            case "SpellCaster":
                                                cards.add(new SpellCaster(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]), Integer.parseInt(data[3]), data[5], breed.get(data[6]), new SpellPack(spell), data[7]));
                                                break;
                                            case "General":
                                                cards.add(new General(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]), Integer.parseInt(data[3]), data[5], breed.get(data[6]), new SpellPack(battleCry), new SpellPack(will), data[7], data[8]));
                                                break;
                                            case "Hero":
                                                cards.add(new Hero(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]), Integer.parseInt(data[3]), data[5], breed.get(data[6]), new SpellPack(battleCry), new SpellPack(spell), new SpellPack(will), data[7], data[8], data[9]));
                                        }
                                }
                            }
                        }
                    }

                    fin.close();
                } catch (FileNotFoundException e) {
                    System.out.println(pathIn + " not found!!");
                }
                i++;
                //System.out.println(pathIn);
            }

            in.close();
        } catch (Exception e) {
            System.out.println("Fatal Error! MonsterPaths.txt was not found");
        }
        return cards;
    }

    public void setCardShop(CardShop cardShop) {
        this.cardShop = cardShop;

        setMainMenuReferences();
        setShopMenuReferences();
        setCardShopReferences();
        setItemShopReferences();
        setAmuletShopReferences();
        setInventoryReferences();
        setBattleReferences();
        setMapReferences();
        setServerBattleReferences();
        setClientBattleReferences();
        setMultiPlayerReferences();
        setOnlineReferences();
        setOnlineClientBattleReferences();

        //custom references
        setCustomMenuReferences();
        setCustomCardReferences();
        setCustomItemReferences();
        setCustomAmuletReferences();

        cardShop.setupScene();
    }

    public ArrayList<Card> loadSpellCards() {
        HashMap<String, Field> field = new HashMap<String, Field>();
        field.put("GRAVEYARD", Field.GRAVEYARD);
        field.put("DECK", Field.DECK);
        field.put("HAND", Field.HAND);
        field.put("SPELLFIELD", Field.SPELLFIELD);
        field.put("MONSTERFIELD", Field.MONSTERFIELD);
        field.put("PLAYFIELD", Field.PLAYFIELD);

        HashMap<String, Breed> breed = new HashMap<String, Breed>();
        breed.put("ELVEN", Breed.ELVEN);
        breed.put("DRAGONBREED", Breed.DRAGONBREED);
        breed.put("ATLANTIAN", Breed.ATLANTIAN);
        breed.put("DEMONIC", Breed.DEMONIC);

        HashMap<String, SpellType> spellType = new HashMap<String, SpellType>();
        spellType.put("RANDOM", SpellType.RANDOM);
        spellType.put("ALL", SpellType.ALL);
        spellType.put("SELECTIVE", SpellType.SELECTIVE);

        HashMap<String, Side> side = new HashMap<String, Side>();
        side.put("FRIENDLY", Side.FRIENDLY);
        side.put("ENEMY", Side.ENEMY);
        side.put("BOTH", Side.BOTH);

        ArrayList<Card> cards = new ArrayList<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader("datafiles/" + checkPoint.getName() + "/Paths/SpellPaths.txt"));
            String pathIn;
            int i = 0;
            while ((pathIn = in.readLine()) != null) {
                try {
                    BufferedReader fin = new BufferedReader(new FileReader(pathIn));
                    String read;
                    String type = new String();
                    String[] data;
                    ArrayList<Spell> spell = new ArrayList<Spell>();

                    while ((read = fin.readLine()) != null) {
                        if (read.length() != 0) {
                            data = read.split("/");
                            switch (data[0]) {
                                case "Healer":
                                    spell.add(new Healer(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), data[7]));
                                    break;
                                case "Attacker":
                                    spell.add(new Attacker(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), data[7]));
                                    break;
                                case "CardMover":
                                    spell.add(new CardMover(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], side.get(data[6]), field.get(data[7]), side.get(data[8]), field.get(data[9])));
                                    break;
                                case "ApChanger":
                                    spell.add(new ApChanger(spellType.get(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], Integer.parseInt(data[6]), side.get(data[7])));
                                    break;
                                default:
                                    switch (data[2]) {
                                        case "InstantSpell":
                                            cards.add(new InstantSpell(data[0], Integer.parseInt(data[1]), new SpellPack(spell), data[3]));
                                            break;
                                        case "AuraSpell":
                                            cards.add(new AuraSpell(data[0], Integer.parseInt(data[1]), new SpellPack(spell), data[3]));
                                            break;
                                        case "ContinuousSpell":
                                            cards.add(new ContinuousSpell(data[0], Integer.parseInt(data[1]), new SpellPack(spell), data[3]));
                                            break;
                                    }
                            }
                        }
                    }

                    fin.close();
                } catch (FileNotFoundException e) {
                    System.out.println(pathIn + " not found!!");
                }
                i++;
                //System.out.println(pathIn);
            }

            in.close();
        } catch (Exception e) {
            System.out.println("Fatal Error! SpellPaths.txt was not found");
        }
        return cards;
    }

    public ArrayList<Item> loadItems() {

        ArrayList<Item> items = new ArrayList<>();
        String line;
        String fileName = "datafiles/" + checkPoint.getName() + "/itemsData.txt";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split("/");
                items.add(new Item(data[0],
                        Integer.parseInt(data[1]),
                        Integer.parseInt(data[2]),
                        Integer.parseInt(data[3]),
                        null));
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");
        } catch (IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        }

        return items;
    }

    public ArrayList<Amulet> loadAmulets() {

        ArrayList<Amulet> amulets = new ArrayList<>();
        String line;
        String fileName = "datafiles/" + checkPoint.getName() + "/amuletsData.txt";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split("/");
                amulets.add(new Amulet(data[0],
                        Integer.parseInt(data[1]),
                        Integer.parseInt(data[2]),
                        Integer.parseInt(data[3]),
                        Integer.parseInt(data[4])));
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");
        } catch (IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        }

        return amulets;
    }

    public void setMainMenuReferences() {
        mainMenu.setShopmenu(shopMenu);
        mainMenu.setInventory(inventory);
        mainMenu.setMap(map);
        mainMenu.setBattle(battle);
        mainMenu.setStage(stage);
        mainMenu.setMultiPlayer(multiPlayer);
        mainMenu.setOnline(online);
        mainMenu.setCustomMenu(customMenu);
    }

    public void setShopMenuReferences() {
        shopMenu.setMainMenu(mainMenu);
        shopMenu.setCardShop(cardShop);
        shopMenu.setItemShop(itemShop);
        shopMenu.setAmuletShop(amuletShop);
        shopMenu.setStage(stage);
    }

    public void setMapReferences() {
        map.setMapPath("Map/" + String.valueOf(player.getLevel()) + ".tmx");
        map.setShopMenu(shopMenu);
        map.setBattle(battle);
        map.setMainMenu(mainMenu);
    }

    public void setCardShopReferences() {
        cardShop.setShopMenu(shopMenu);
        cardShop.setPlayer(player);
        cardShop.setStage(stage);
    }

    public void setItemShopReferences() {
        itemShop.setPlayer(player);
        itemShop.setShopMenu(shopMenu);
        itemShop.setStage(stage);
    }

    public void setAmuletShopReferences() {
        amuletShop.setPlayer(player);
        amuletShop.setShopMenu(shopMenu);
        amuletShop.setStage(stage);
    }

    public void setInventoryReferences() {
        inventory.setPlayer(player);
        inventory.setMainMenu(mainMenu);
        inventory.setStage(stage);
    }

    public void setBattleReferences() {
        battle.setCardShop(cardShop);
        battle.setEnemy(enemies[player.getLevel() - 1]);
        battle.setPlayer(player);
        battle.setMainMenu(mainMenu);
        battle.setStage(stage);
        battle.setGameManager(this);
    }

    public void setServerBattleReferences() {
        serverBattle.setCardShop(cardShop);
        serverBattle.setMainMenu(mainMenu);
        serverBattle.setStage(stage);
        serverBattle.setPlayer(player);
        serverBattle.setEnemy(enemies[0]);
        serverBattle.setServer(new Server(8080, serverBattle));
        serverBattle.setGameManager(this);
    }

    public void setClientBattleReferences() {
        clientBattle.setCardShop(cardShop);
        clientBattle.setMainMenu(mainMenu);
        clientBattle.setStage(stage);
        clientBattle.setPlayer(player);
        clientBattle.setEnemy(enemies[0]);
        clientBattle.setClient(new Client("localhost", 8080, clientBattle));
        clientBattle.setGameManager(this);
    }

    public void setMultiPlayerReferences() {
        multiPlayer.setStage(stage);
        multiPlayer.setServerBattle(serverBattle);
        multiPlayer.setClientBattle(clientBattle);
        multiPlayer.setPreState(mainMenu);
    }

    public void setOnlineReferences() {
        online.setStage(stage);
        online.setServerBattle(serverBattle);
        online.setClientBattle(onlineClientBattle);
        online.setPreState(mainMenu);
    }

    public void setOnlineClientBattleReferences() {
        onlineClientBattle.setCardShop(cardShop);
        onlineClientBattle.setMainMenu(mainMenu);
        onlineClientBattle.setStage(stage);
        onlineClientBattle.setPlayer(player);
        onlineClientBattle.setEnemy(enemies[0]);
        onlineClientBattle.setClient(new network.Client(onlineClientBattle));
        onlineClientBattle.setGameManager(this);
    }

    private void setCustomMenuReferences() {
        customMenu.setStage(stage);
        customMenu.setCustomCard(customCard);
        customMenu.setCustomItem(customItem);
        customMenu.setCustomAmulet(customAmulet);
        customMenu.setMainMenu(mainMenu);
    }

    private void setCustomCardReferences() {
        customCard.setStage(stage);
        customCard.setCustomMenu(customMenu);
        //todo
    }

    private void setCustomItemReferences() {
        customItem.setStage(stage);
        customItem.setCustomMenu(customMenu);
        customItem.setPlayer(player);
        customItem.setItemShop(itemShop);
    }

    private void setCustomAmuletReferences() {
        customAmulet.setStage(stage);
        customAmulet.setCustomMenu(customMenu);
        customAmulet.setPlayer(player);
    }

}