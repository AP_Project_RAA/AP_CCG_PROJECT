package gameObjects.spells;

import enums.Side;
import enums.SpellType;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.MonsterCard;
import main.Player;

import java.util.ArrayList;
import java.util.Random;

public class ApChanger extends Spell {
    private static final long serialVersionUID = 81L;

    private int Apchange;
    private Side side;

    public ApChanger(SpellType spellType, int noOfCasts, String breed, String monsterCardType, String spellCardType, int apchange, Side side) {
        super(spellType, noOfCasts, breed, monsterCardType, spellCardType);
        Apchange = apchange;
        this.side = side;
    }

    public void castSpellToAll(Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        for (int i = 0; i < targets.size(); i++) {
            Card card = targets.get(i);
            MonsterCard monsterCard;
            if (card instanceof MonsterCard)
                monsterCard = (MonsterCard) card;
            else
                continue;
            monsterCard.setAP(monsterCard.getAP() + Apchange);
        }
    }

    public void castReverseSpellToAll(Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        for (int i = 0; i < targets.size(); i++) {
            Card card = targets.get(i);
            MonsterCard monsterCard;
            if (card instanceof MonsterCard)
                monsterCard = (MonsterCard) card;
            else
                continue;
            monsterCard.setAP(monsterCard.getAP() - Apchange);
        }
    }

    public void castSpellToRandom(Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        Random random = new Random();
        for (int i = 0; i < noOfCasts; i++) {
            int id = random.nextInt(targets.size());
            Card card = targets.get(id);
            MonsterCard monsterCard;
            if (card instanceof MonsterCard)
                monsterCard = (MonsterCard) card;
            else
                continue;
            monsterCard.setAP(monsterCard.getAP() + Apchange);
        }
    }

    public void castSpellToSelected(Player player, Player enemy, ArrayList<Card> selectedCards, boolean selectedPlayer, boolean selectedEnemy) {
        for (int i = 0; i < selectedCards.size(); i++) {
            Card card = selectedCards.get(i);
            MonsterCard monsterCard;
            if (card instanceof MonsterCard)
                monsterCard = (MonsterCard) card;
            else
                continue;
            monsterCard.setAP(monsterCard.getAP() + Apchange);
        }
    }

    public ArrayList<Card> getTargets(Player player, Player enemy) {
        ArrayList<Card> targets = new ArrayList<Card>();
        switch (side) {
            case FRIENDLY:
                ArrayList<Card> cards = player.getMonsterField().getCards();
                for (int i = 0; i < cards.size(); i++) {
                    MonsterCard monsterCard;
                    if (cards.get(i) instanceof MonsterCard)
                        monsterCard = (MonsterCard) cards.get(i);
                    else
                        continue;
                    if (isValid(monsterCard)) {
                        targets.add(monsterCard);
                    }
                }
                break;
            case ENEMY:
                ArrayList<Card> enemyCards = enemy.getMonsterField().getCards();
                for (int i = 0; i < enemyCards.size(); i++) {
                    MonsterCard monsterCard;
                    if (enemyCards.get(i) instanceof MonsterCard)
                        monsterCard = (MonsterCard) enemyCards.get(i);
                    else
                        continue;
                    if (isValid(monsterCard)) {
                        targets.add(monsterCard);
                    }
                }
                break;
            case BOTH:
                side = Side.FRIENDLY;
                ArrayList<Card> targets1 = getTargets(player, enemy);
                side = Side.ENEMY;
                ArrayList<Card> targets2 = getTargets(player, enemy);
                side = Side.BOTH;
                targets.addAll(targets1);
                targets.addAll(targets2);
                break;
        }
        return targets;
    }

    public void castSpellOn(MonsterCard monsterCard, Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        if (targets.contains(monsterCard)) {
            monsterCard.setAP(monsterCard.getAP() + Apchange);
        }
    }

    public void liftSpellFrom(MonsterCard monsterCard, Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        if (targets.contains(monsterCard)) {
            monsterCard.setAP(monsterCard.getAP() - Apchange);
        }
    }

    public ApChanger getCopy() {
        return new ApChanger(spellType, noOfCasts, breed, monsterCardType, spellCardType, Apchange, side);
    }

    @Override
    public String toString() {
        String out = "ApChanger/";
        out += super.toString();
        out += String.valueOf(Apchange) + "/" + side + "\n";
        return out;
    }
}