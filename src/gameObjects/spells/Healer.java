package gameObjects.spells;

import enums.SpellType;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.MonsterCard;
import main.Player;

import java.util.ArrayList;
import java.util.Random;

public class Healer extends Spell {
    private static final long serialVersionUID = 84L;

    private int healAmount;
    private String effect;

    public Healer(SpellType spellType, int noOfCasts, String breed, String monsterCardType, String spellCardType, int healAmount, String effectsPlayer) {
        super(spellType, noOfCasts, breed, monsterCardType, spellCardType);
        this.healAmount = healAmount;
        effect = effectsPlayer;
        this.effectsPlayer = (effectsPlayer.charAt(0) == '1');
    }

    public void castSpellToAll(Player player, Player enemy) {
        if (effectsPlayer)
            player.setHP(player.getHP() + healAmount);

        ArrayList<Card> targets = getTargets(player, enemy);
        for (Card target : targets) {
            MonsterCard monsterCard;
            if (target instanceof MonsterCard)
                monsterCard = (MonsterCard) target;
            else
                continue;
            monsterCard.setHP(monsterCard.getHP() + healAmount);
        }
    }

    public void castReverseSpellToAll(Player player, Player enemy) {
        if (effectsPlayer)
            player.setHP(player.getHP() - healAmount);

        ArrayList<Card> targets = getTargets(player, enemy);
        for (Card target : targets) {
            MonsterCard monsterCard;
            if (target instanceof MonsterCard)
                monsterCard = (MonsterCard) target;
            else
                continue;
            monsterCard.setHP(monsterCard.getHP() - healAmount);
        }
    }

    public void castSpellToRandom(Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        for (int i = 0; i < noOfCasts; i++) {
            Random random = new Random();
            if (effectsPlayer) {
                int id = random.nextInt(targets.size() + 1);
                if (id == targets.size())
                    player.setHP(player.getHP() + healAmount);
                else {
                    Card card = targets.get(id);
                    MonsterCard monsterCard;
                    if (card instanceof MonsterCard)
                        monsterCard = (MonsterCard) card;
                    else
                        continue;

                    monsterCard.setHP(monsterCard.getHP() + healAmount);
                }
            } else {
                int id = random.nextInt(targets.size());

                Card card = targets.get(id);
                MonsterCard monsterCard;
                if (card instanceof MonsterCard)
                    monsterCard = (MonsterCard) card;
                else
                    continue;

                monsterCard.setHP(monsterCard.getHP() + healAmount);
            }

        }
    }

    public void castSpellToSelected(Player player, Player enemy, ArrayList<Card> selectedCards, boolean selectedPlayer, boolean selectedEnemy) {
        for (Card card : selectedCards) {
            if (card.getName().equals("__Player_")) {
                player.setHP(player.getHP() + healAmount);
                continue;
            }
            if (card.getName().equals("__Enemy_")) {
                enemy.setHP(enemy.getHP() + healAmount);
                continue;
            }
            MonsterCard monsterCard;
            if (card instanceof MonsterCard)
                monsterCard = (MonsterCard) card;
            else
                continue;
            monsterCard.setHP(monsterCard.getHP() + healAmount);
        }
    }

    public ArrayList<Card> getTargets(Player player, Player enemy) {
        ArrayList<Card> targets = new ArrayList<>();
        ArrayList<Card> cards = player.getMonsterField().getCards();
        for (Card card : cards) {
            MonsterCard monsterCard;
            if (card instanceof MonsterCard)
                monsterCard = (MonsterCard) card;
            else
                continue;

            if (isValid(monsterCard))
                targets.add(monsterCard);
        }
        return targets;
    }

    public void castSpellOn(MonsterCard monsterCard, Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        if (targets.contains(monsterCard)) {
            monsterCard.setHP(monsterCard.getHP() + healAmount);
        }
    }

    public void liftSpellFrom(MonsterCard monsterCard, Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        if (targets.contains(monsterCard)) {
            monsterCard.setHP(monsterCard.getHP() - healAmount);
        }
    }

    public Healer getCopy() {
        String effectsP;
        if (effectsPlayer)
            effectsP = "1";
        else
            effectsP = "0";
        return new Healer(this.spellType, noOfCasts, breed, monsterCardType, spellCardType, healAmount, effectsP);
    }

    @Override
    public String toString() {
        String out = "Healer/";
        out += super.toString();
        out += String.valueOf(healAmount) + "/" + effect + "\n";
        return out;
    }

}