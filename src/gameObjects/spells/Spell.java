package gameObjects.spells;

import enums.SpellType;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.*;
import main.Player;

import java.io.Serializable;
import java.util.ArrayList;

public class Spell implements Serializable {
    private static final long serialVersionUID = 8L;

    SpellType spellType;
    int noOfCasts;
    String breed = "0000";
    //Elven|DragoonBreed|Atlantian|Demonic
    String monsterCardType = "0000";
    //Normal|spellCaster|general|hero
    String spellCardType = "000";
    //instant|continuous|Aura
    boolean effectsPlayer, effectsEnemy;


    public Spell(SpellType spellType, int noOfCasts, String breed, String monsterCardType, String spellCardType) {
        this.spellType = spellType;
        this.noOfCasts = noOfCasts;
        this.breed = breed;
        this.monsterCardType = monsterCardType;
        this.spellCardType = spellCardType;
    }

    public SpellType getSpellType() {
        return spellType;
    }

    public int getNoOfCasts() {
        return noOfCasts;
    }

    public String getMonsterCardType() {
        return monsterCardType;
    }

    public String getSpellCardType() {
        return spellCardType;
    }

    public boolean isEffectsPlayer() {
        return effectsPlayer;
    }

    public boolean isEffectsEnemy() {
        return effectsEnemy;
    }

    public void castSpellToAll(Player player, Player enemy) {

    }

    public void castReverseSpellToAll(Player player, Player enemy) {

    }

    public void castSpellToRandom(Player player, Player enemy) {

    }

    public void castSpellToSelected(Player player, Player enemy, ArrayList<Card> selectedCards, boolean selectedPlayer, boolean selectedEnemy) {

    }

    public ArrayList<Card> getTargets(Player player, Player enemy) {
        return null;
    }

    public boolean isValid(MonsterCard monsterCard) {
        switch (monsterCard.getBreed()) {
            case ELVEN:
                if (breed.charAt(0) != '1')
                    return false;
                break;
            case DRAGONBREED:
                if (breed.charAt(1) != '1')
                    return false;
                break;
            case ATLANTIAN:
                if (breed.charAt(2) != '1')
                    return false;
                break;
            case DEMONIC:
                if (breed.charAt(3) != '1')
                    return false;
                break;
        }

        if (monsterCard instanceof Normal && monsterCardType.charAt(0) != '1')
            return false;
        else if (monsterCard instanceof SpellCaster && monsterCardType.charAt(1) != '1')
            return false;
        else if (monsterCard instanceof General && monsterCardType.charAt(2) != '1')
            return false;
        else if (monsterCard instanceof Hero && monsterCardType.charAt(3) != '1')
            return false;

        return true;
    }

    public void castSpellOn(MonsterCard monsterCard, Player player, Player enemy) {
    }

    @Override
    public String toString() {
        return spellType + "/" + String.valueOf(noOfCasts) + "/" + breed + "/" + monsterCardType + "/" + spellCardType + "/";
    }

    public Spell getCopy() {
        return new Spell(this.spellType, noOfCasts, this.breed, this.monsterCardType, this.spellCardType);
    }
}