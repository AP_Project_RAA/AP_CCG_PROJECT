package gameObjects.spells;

import enums.SpellType;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.MonsterCard;
import main.Player;

import java.util.ArrayList;
import java.util.Random;

public class Attacker extends Spell {
    private static final long serialVersionUID = 82L;

    private int damage;
    private String effect;

    public Attacker(SpellType spellType, int noOfCasts, String breed, String monsterCardType, String spellCardType, int damage, String playerOrEnemyEffect) {
        super(spellType, noOfCasts, breed, monsterCardType, spellCardType);
        this.damage = damage;
        this.effect = playerOrEnemyEffect;
        this.effectsPlayer = (effect.charAt(0) == '1');
        this.effectsEnemy = (effect.charAt(1) == '1');
    }

    public void castSpellToAll(Player player, Player enemy) {
        if (effectsEnemy)
            enemy.setHP(enemy.getHP() - damage);

        ArrayList<Card> targets = getTargets(player, enemy);
        for (int i = 0; i < targets.size(); i++) {
            MonsterCard monsterCard;
            if (targets.get(i) instanceof MonsterCard)
                monsterCard = (MonsterCard) targets.get(i);
            else
                continue;
            monsterCard.setHP(monsterCard.getHP() - damage);
        }
    }

    public void castReverseSpellToAll(Player player, Player enemy) {
        if (effectsEnemy)
            enemy.setHP(enemy.getHP() + damage);

        ArrayList<Card> targets = getTargets(player, enemy);
        for (int i = 0; i < targets.size(); i++) {
            MonsterCard monsterCard;
            if (targets.get(i) instanceof MonsterCard)
                monsterCard = (MonsterCard) targets.get(i);
            else
                continue;
            monsterCard.setHP(monsterCard.getHP() + damage);
        }
    }

    public void castSpellToRandom(Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        for (int i = 0; i < noOfCasts; i++) {
            Random random = new Random();
            if (effectsEnemy) {
                int id = random.nextInt(targets.size() + 1);
                if (id == targets.size() || targets.size() == 0)
                    enemy.setHP(enemy.getHP() - damage);
                else {
                    Card card = targets.get(id);
                    MonsterCard monsterCard;
                    if (card instanceof MonsterCard)
                        monsterCard = (MonsterCard) card;
                    else
                        continue;

                    monsterCard.setHP(monsterCard.getHP() - damage);
                }
            } else {
                int id = random.nextInt(targets.size());

                Card card = targets.get(id);
                MonsterCard monsterCard;
                if (card instanceof MonsterCard)
                    monsterCard = (MonsterCard) card;
                else
                    continue;

                monsterCard.setHP(monsterCard.getHP() - damage);
            }

        }
    }

    public void castSpellToSelected(Player player, Player enemy, ArrayList<Card> selectedCards, boolean selectedPlayer, boolean selectedEnemy) {
        for (int i = 0; i < selectedCards.size(); i++) {
            Card card = selectedCards.get(i);
            if (card.getName().equals("__Player_")) {
                player.setHP(player.getHP() - damage);
                continue;
            }
            if (card.getName().equals("__Enemy_")) {
                enemy.setHP(enemy.getHP() - damage);
                continue;
            }
            MonsterCard monsterCard;
            if (card instanceof MonsterCard)
                monsterCard = (MonsterCard) card;
            else
                continue;
            monsterCard.setHP(monsterCard.getHP() - damage);
        }
    }

    public ArrayList<Card> getTargets(Player player, Player enemy) {
        ArrayList<Card> targets = new ArrayList<Card>();
        ArrayList<Card> cards = enemy.getMonsterField().getCards();
        for (int i = 0; i < cards.size(); i++) {
            MonsterCard monsterCard;
            if (cards.get(i) instanceof MonsterCard)
                monsterCard = (MonsterCard) cards.get(i);
            else
                continue;

            if (isValid(monsterCard))
                targets.add(monsterCard);
        }

        return targets;
    }

    public void castSpellOn(MonsterCard monsterCard, Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        if (targets.contains(monsterCard)) {
            monsterCard.setHP(monsterCard.getHP() - damage);
        }
    }

    public void liftSpellFrom(MonsterCard monsterCard, Player player, Player enemy) {
        ArrayList<Card> targets = getTargets(player, enemy);
        if (targets.contains(monsterCard)) {
            monsterCard.setHP(monsterCard.getHP() + damage);
        }
    }

    @Override
    public Spell getCopy() {
        return new Attacker(spellType, noOfCasts, breed, monsterCardType, spellCardType, damage, effect);
    }

    @Override
    public String toString() {
        String out = "Attacker/";
        out += super.toString();
        out += String.valueOf(damage) + "/" + effect + "\n";
        return out;
    }
}