package gameObjects.spells;

import enums.Field;
import enums.Side;
import enums.SpellType;
import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.MonsterCard;
import gameObjects.cards.monsterCards.SpellCaster;
import gameObjects.cards.spellCards.AuraSpell;
import gameObjects.cards.spellCards.ContinuousSpell;
import gameObjects.cards.spellCards.InstantSpell;
import gameObjects.cards.spellCards.SpellCard;
import main.Player;

import java.util.ArrayList;
import java.util.Random;

public class CardMover extends Spell {
    private static final long serialVersionUID = 83L;

    private Side originSide;
    private Field originField;
    private Side destinationSide;
    private Field destinationField;


    public Side getOriginSide(){
        return originSide;
    }

    public void setOriginSide(Side originSide){
        this.originSide = originSide;
    }

    public Field getOriginField(){
        return originField;
    }

    public void setOriginField(Field originField){
        this.originField = originField;
    }

    public Side getDestinationSide(){
        return destinationSide;
    }

    public void setDestinationSide(Side destinationSide){
        this.destinationSide = destinationSide;
    }

    public Field getDestinationField(){
        return destinationField;
    }

    public void setDestinationField(Field destinationField){
        this.destinationField = destinationField;
    }

    public CardMover(SpellType spellType, int noOfCasts, String breed, String monsterCardType, String spellCardType, Side originSide, Field originField, Side destinationSide, Field destinationField){
        super(spellType, noOfCasts, breed, monsterCardType, spellCardType);
        this.originSide = originSide;
        this.originField = originField;
        this.destinationSide = destinationSide;
        this.destinationField = destinationField;
    }

    public void castSpellToAll(Player player, Player enemy){
        ArrayList<Card> targets = getTargets(player, enemy);
        for(int i = 0; i < targets.size(); i++){
            Card movingCard = targets.get(i);
            checkForAura(movingCard, player, enemy);
            moveCard(movingCard, player, enemy);
        }
    }


    public void castSpellToRandom(Player player, Player enemy){
        ArrayList<Card> targets = getTargets(player, enemy);
        Random random = new Random();
        for(int i = 0; i < noOfCasts; i++){
            int id = random.nextInt(targets.size());
            Card movingCard = targets.get(id);
            checkForAura(movingCard, player, enemy);
            moveCard(movingCard, player, enemy);
            targets.remove(id);
        }
    }

    public void castSpellToSelected(Player player, Player enemy, ArrayList<Card> selectedCards, boolean selectedPlayer, boolean selectedEnemy){
        ArrayList<Card> targets = selectedCards;
        for(int i = 0; i < targets.size(); i++){
            Card movingCard = targets.get(i);
            if(movingCard.getName().equals("__Player_") || movingCard.getName().equals("__Enemy_"))
                continue;
            checkForAura(movingCard, player, enemy);
            moveCard(movingCard, player, enemy);
        }
    }


    private void checkForAura(Card movingCard, Player player, Player enemy){
        if(movingCard instanceof AuraSpell && originField.equals(Field.SPELLFIELD)) {
            AuraSpell auraSpell = (AuraSpell) movingCard;
            ArrayList<Spell> spells = auraSpell.spellPack.getSpells();
            for(int j = 0; j < spells.size(); j++)
                spells.get(j).castReverseSpellToAll(player, enemy);
        }
    }

    private void moveCard(Card movingCard, Player player, Player enemy){
        int index;
        if(originField == Field.PLAYFIELD)
        {
            if(movingCard instanceof MonsterCard)
                originField = Field.MONSTERFIELD;
            if(movingCard instanceof SpellCard)
                originField = Field.SPELLFIELD;
        }
        switch(originSide) {
            case FRIENDLY:
                index = player.getPlaces().get(originField).getCards().indexOf(movingCard);
                player.getPlaces().get(originField).getCards().remove(index);
                if(destinationSide == Side.FRIENDLY)
                    player.getPlaces().get(destinationField).getCards().add(movingCard);
                else
                    enemy.getPlaces().get(destinationField).getCards().add(movingCard);
                break;
            case ENEMY:
                index = enemy.getPlaces().get(originField).getCards().indexOf(movingCard);
                enemy.getPlaces().get(originField).getCards().remove(index);
                if(destinationSide == Side.FRIENDLY)
                    player.getPlaces().get(destinationField).getCards().add(movingCard);
                else
                    enemy.getPlaces().get(destinationField).getCards().add(movingCard);
                break;
            case BOTH:
                index = enemy.getPlaces().get(originField).getCards().indexOf(movingCard);
                enemy.getPlaces().get(originField).getCards().remove(index);
                if(destinationSide == Side.FRIENDLY)
                    player.getPlaces().get(destinationField).getCards().add(movingCard);
                else
                    enemy.getPlaces().get(destinationField).getCards().add(movingCard);

                index = player.getPlaces().get(originField).getCards().indexOf(movingCard);
                player.getPlaces().get(originField).getCards().remove(index);
                if(destinationSide == Side.FRIENDLY)
                    player.getPlaces().get(destinationField).getCards().add(movingCard);
                else
                    enemy.getPlaces().get(destinationField).getCards().add(movingCard);
                break;
        }
    }

    public ArrayList<Card> getTargets(Player player, Player enemy){
        ArrayList<Card> targets = new ArrayList<Card>();
        ArrayList<Card> cards;
        switch(originSide) {
            case FRIENDLY:
                if(originField == Field.PLAYFIELD)
                {
                    cards = new ArrayList<>();
                    cards.addAll(player.getPlaces().get(Field.MONSTERFIELD).getCards());
                    cards.addAll(player.getPlaces().get(Field.SPELLFIELD).getCards());
                }
                else
                    cards = player.getPlaces().get(originField).getCards();
                for(int i = 0; i < cards.size(); i++){
                    if(isValid(cards.get(i)))
                        targets.add(cards.get(i));
                }
                break;
            case ENEMY:
                if(originField == Field.PLAYFIELD)
                {
                    cards = new ArrayList<>();
                    cards.addAll(enemy.getPlaces().get(Field.MONSTERFIELD).getCards());
                    cards.addAll(enemy.getPlaces().get(Field.SPELLFIELD).getCards());
                }
                else
                    cards = enemy.getPlaces().get(originField).getCards();
                for(int i = 0; i < cards.size(); i++){
                    if(isValid(cards.get(i)))
                        targets.add(cards.get(i));
                }
                break;
            case BOTH:
                if(originField == Field.PLAYFIELD)
                {
                    cards = new ArrayList<>();
                    cards.addAll(enemy.getPlaces().get(Field.MONSTERFIELD).getCards());
                    cards.addAll(enemy.getPlaces().get(Field.SPELLFIELD).getCards());
                }
                else
                    cards = enemy.getPlaces().get(originField).getCards();
                cards = enemy.getPlaces().get(originField).getCards();
                for(int i = 0; i < cards.size(); i++){
                    if(isValid(cards.get(i)))
                        targets.add(cards.get(i));
                }
                if(originField == Field.PLAYFIELD)
                {
                    cards = new ArrayList<>();
                    cards.addAll(player.getPlaces().get(Field.MONSTERFIELD).getCards());
                    cards.addAll(player.getPlaces().get(Field.SPELLFIELD).getCards());
                }
                else
                    cards = player.getPlaces().get(originField).getCards();

                for(int i = 0; i < cards.size(); i++){
                    if(isValid(cards.get(i)))
                        targets.add(cards.get(i));
                }
                break;
        }
        return targets;
    }

    boolean isValid(Card card){
        if(card instanceof MonsterCard) {
            MonsterCard monsterCard = (MonsterCard) card;
            return super.isValid(monsterCard);
        } else if(card instanceof SpellCard) {
            SpellCard spellCard = (SpellCard) card;
            if(spellCard instanceof InstantSpell && spellCardType.charAt(0) == '0')
                return false;
            else if(spellCard instanceof ContinuousSpell && spellCardType.charAt(1) == '0')
                return false;
            else if(spellCard instanceof AuraSpell && spellCardType.charAt(2) == '0')
                return false;
            return true;
        }
        return false;
    }

    public CardMover getCopy(){
        return new CardMover(spellType, noOfCasts, breed, monsterCardType, spellCardType, originSide, originField, destinationSide, destinationField);
    }

    @Override
    public String toString(){
        String out = "CardMover/";
        out += super.toString();
        out += originSide + "/" + originField + "/" + destinationSide + "/" + destinationField + "\n";
        return out;
    }
}