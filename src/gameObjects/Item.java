package gameObjects;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class Item extends GameObject {
    private static final long serialVersionUID = 3L;

    private String name;
    private int price;
    private int HP_Effect;
    private int MP_Effect;
    private String customItemColor;
    private String description;
    transient private Group itemGroup;

    public Item(String name, int price, int HP_effect, int MP_effect, String color) {
        this.name = name;
        this.price = price;
        this.HP_Effect = HP_effect;
        this.MP_Effect = MP_effect;
        this.customItemColor = color;
        // initializing descreption of the item
        description = "Increase Player's ";
        if (HP_effect != 0 && MP_effect != 0) {
            description = description + "HP by " + Integer.toString(HP_effect)
                    + " and MP by " + Integer.toString(MP_effect);
        } else {
            if (HP_effect != 0) {
                description = description + "HP by " + Integer.toString(HP_effect);
            }
            if (MP_effect != 0) {
                description = description + "MP by " + Integer.toString(MP_effect);
            }
        }

        // applying gfx
        updateGraphics();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public int getHP_Effect() {
        return this.HP_Effect;
    }

    public int getMP_Effect() {
        return this.MP_Effect;
    }

    public Item getCopy() {
        return new Item(name, price, HP_Effect, MP_Effect, customItemColor);
    }

    public String getInfo() {
        return "\"" + this.name + "\" Info:\n" + this.description;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof Item && obj.hashCode() == this.hashCode();
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    public Group getItemGroup() {
        return itemGroup;
    }

    public void updateGraphics() {
        // if customItemColor is not null, item is custom-made
        itemGroup = new Group();
        ImageView itemImageView = new ImageView();
        if (customItemColor == null) {
            itemImageView.setImage(new Image(new File("images\\items\\" + name + ".png").toURI().toString()));
        } else {
            itemImageView.setImage(new Image(new File("images\\custom items\\" + customItemColor + " custom item.png").toURI().toString()));
        }
        itemImageView.setPreserveRatio(true);
        itemImageView.setFitWidth(92);
        itemGroup.getChildren().add(itemImageView);
    }
}