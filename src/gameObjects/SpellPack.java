package gameObjects;

import gameObjects.spells.Spell;

import java.io.Serializable;
import java.util.ArrayList;

public class SpellPack implements Serializable {
    private static final long serialVersionUID = 9L;

    private String name;
    private ArrayList<Spell> spells = new ArrayList<>();

    public SpellPack(ArrayList<Spell> spells) {
        this.spells.addAll(spells);
    }

    public ArrayList<Spell> getSpells() {
        return spells;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public SpellPack getCopy() {
        ArrayList<Spell> ret = new ArrayList<>();
        for (Spell spell : spells) {
            ret.add(spell.getCopy());
        }
        return new SpellPack(ret);
    }

    @Override
    public String toString() {
        String out = "";
        for (Spell s : spells) {
            out += s.toString();
        }

        out += "\n";
        return out;
    }
}