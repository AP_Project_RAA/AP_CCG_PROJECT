package gameObjects;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class Amulet extends GameObject {
    private static final long serialVersionUID = 2L;

    private String name;
    private int price;
    private int maxHP_Effect;
    private int maxMP_Effect;
    private int armorEffect;
    private String description;
    transient private Group amuletGroup;

    public Amulet(String name, int price, int maxHP_Effect, int maxMP_Effect, int armorEffect) {
        this.name = name;
        this.price = price;
        this.maxHP_Effect = maxHP_Effect;
        this.maxMP_Effect = maxMP_Effect;
        this.armorEffect = armorEffect;
        if (armorEffect == 0) {
            description = "Increase Player's ";
            if (maxHP_Effect != 0) {
                description = description + "Max HP by " + Integer.toString(maxHP_Effect);
            }
            if (maxMP_Effect != 0) {
                description = description + "Max MP by " + Integer.toString(maxMP_Effect);
            }
        } else {
            description = "Decrease All incoming Damages by " + Integer.toString(armorEffect) + "%";
        }

        // applying gfx
        updateGraphics();
    }

    public int getPrice() {
        return price;
    }

    public int getMaxHP_Effect() {
        return this.maxHP_Effect;
    }

    public int getMaxMP_Effect() {
        return this.maxMP_Effect;
    }

    public int getArmorEffect() {
        return this.armorEffect;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Amulet getCopy() {
        return new Amulet(name, price, maxHP_Effect, maxMP_Effect, armorEffect);
    }

    public String getInfo() {
        return "\"" + name + "\" Info:\n" + description;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof Amulet && obj.hashCode() == this.hashCode();
    }

    public Group getAmuletGroup() {
        return amuletGroup;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public void updateGraphics() {
        boolean isAmuletInShop = false;
        String[] namesOfAmuletsInShop = {"Iron Pendant", "Gold Pendant", "Diamond Pendant",
                "Iron Ring", "Gold Ring", "Diamond Ring", "Demon King's Crown"};
        for (String amuletName : namesOfAmuletsInShop) {
            if (amuletName.equals(name)) {
                isAmuletInShop = true;
                break;
            }
        }
        amuletGroup = new Group();
        ImageView amuletImageView = new ImageView();
        amuletGroup.getChildren().add(amuletImageView);
        amuletImageView.setImage(new Image(new File("images\\amulets\\" + name + ".png").toURI().toString()));
        if (isAmuletInShop) {
            amuletImageView.setPreserveRatio(true);
            amuletImageView.setFitWidth(116);
        } else {
            amuletImageView.setFitWidth(110);
            amuletImageView.setFitHeight(110);
            amuletImageView.setLayoutX(3);
            amuletImageView.setLayoutY(9);
            ImageView frameImageView = new ImageView(new File("images\\amulets\\amulet-frame.png").toURI().toString());
            frameImageView.setFitWidth(116);
            frameImageView.setFitHeight(128);
            amuletGroup.getChildren().addAll(frameImageView);
        }
    }

}