package gameObjects.cards.monsterCards;

import enums.Breed;
import gameObjects.cards.Card;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import main.Player;

import java.io.File;

public class MonsterCard extends Card {
    private static final long serialVersionUID = 71L;

    private int HP;
    private int initialHP;
    private int initialAP;
    private int AP;
    public boolean isDefensive;
    public boolean isNimble;
    protected Breed breed;
    private int round;
    public boolean hasAttacked;

    transient private Label hpLabel, apLabel;

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    String speciality;

    public MonsterCard(String name, int MP, int price, int HP, int AP, String speciality, Breed breed) {
        super(name, MP, price);
        this.HP = HP;
        initialHP = HP;
        this.AP = AP;
        initialAP = AP;
        this.speciality = speciality;
        this.isDefensive = (speciality.charAt(0) == '1');
        this.isNimble = (speciality.charAt(1) == '1');
        this.breed = breed;
        this.round = 0;
        hasAttacked = false;

        //setting up card graphics
        setupGraphics();
    }

    public MonsterCard getCopy() {
        return new MonsterCard(name, MP, price, HP, AP, speciality, breed);
    }

    public MonsterCard getResetCopy() {
        MonsterCard monsterCard = getCopy();
        monsterCard.setHP(initialHP);
        monsterCard.setAP(initialAP);
        return monsterCard;
    }

    @Override
    public String toString() {
        String out;
        out = super.getName() + "/" + String.valueOf(super.getMP()) + "/" + String.valueOf(HP) + "/" + String.valueOf(AP) + "/" + this.getClass().getSimpleName() + "/" + speciality + "/" + breed;
        return out;
    }

    @Override
    public String getInfo() {
        String out = "Name: " + "\"" + name + "\"" + "\n";
        out += "HP: " + HP + "\n";
        out += "AP: " + AP + "\n";
        out += "MP Cost: " + MP + "\n";
        out += "Card Type: " + this.getClass().getSimpleName() + "\n";
        out += "Card Tribe: " + breed + "\n";
        out += "IsNimble: " + String.valueOf(isNimble) + "\n";
        out += "IsDefensive: " + String.valueOf(isDefensive);
        return out;
    }

    public int getHP() {
        return this.HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getAP() {
        return this.AP;
    }

    public void setAP(int AP) {
        this.AP = AP;
    }

    public void attack(MonsterCard other) {
        other.setHP(other.getHP() - this.AP);
        HP -= other.getAP();
    }

    public void attack(Player player) {
        player.setHP(player.getHP() - this.AP);
    }

    public void updateGFX() {
        hpLabel.setText("HP: " + getHP());
        apLabel.setText("AP: " + getAP());
    }

    public void setupGraphics() {
        cardGroup = new Group();

        ImageView cardbg = new ImageView();
        Image cardbgImage = new Image(new File("images\\cardbackground2.jpg").toURI().toString());
        cardbg.setImage(cardbgImage);
        cardbg.setX(20);
        cardbg.setY(18);
        cardbg.setFitWidth(80);
        cardbg.setFitHeight(108);
        cardGroup.getChildren().add(cardbg);

        ImageView frame = new ImageView(new Image(new File("images\\monsterframe2.png").toURI().toString()));
        frame.setX(0);
        frame.setY(0);
        frame.setPreserveRatio(true);
        frame.setFitWidth(100);
        cardGroup.getChildren().add(frame);

        hpLabel = new Label("HP:" + getHP());
        hpLabel.setTranslateY(109);
        hpLabel.setTranslateX(35);
        hpLabel.setFont(Font.font("Times New Roman", 11));
        cardGroup.getChildren().add(hpLabel);

        apLabel = new Label("AP:" + getAP());
        apLabel.setTranslateY(99);
        apLabel.setTranslateX(35);
        apLabel.setFont(Font.font("Times New Roman", 11));
        cardGroup.getChildren().add(apLabel);

        Label nameLabel = new Label(getName());
        nameLabel.setTranslateY(56);
        nameLabel.setTranslateX(30);
        nameLabel.setPrefWidth(62);
        nameLabel.setTextAlignment(TextAlignment.CENTER);
        nameLabel.setAlignment(Pos.CENTER);
        nameLabel.setMaxWidth(62);
        nameLabel.setWrapText(true);
        nameLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 11));
        cardGroup.getChildren().add(nameLabel);

        String breedPath = "";
        switch (breed) {
            case ATLANTIAN:
                breedPath = "atlantian.png";
                break;
            case DEMONIC:
                breedPath = "demonic.png";
                break;
            case DRAGONBREED:
                breedPath = "dragonbreed.png";
                break;
            case ELVEN:
                breedPath = "elf.png";
                break;
        }

        ImageView breedView = new ImageView(new Image(new File("images\\" + breedPath).toURI().toString()));
        breedView.setPreserveRatio(true);
        breedView.setFitWidth(28);
        breedView.setX(31);
        breedView.setY(20);
        cardGroup.getChildren().add(breedView);

        if (isDefensive) {
            ImageView defender = new ImageView(new Image(new File("images\\defensive.png").toURI().toString()));
            defender.setPreserveRatio(true);
            defender.setFitWidth(20);
            defender.setX(76);
            defender.setY(100);
            cardGroup.getChildren().add(defender);
        }
    }

}