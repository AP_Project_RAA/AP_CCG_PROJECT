package gameObjects.cards.monsterCards;

import enums.Breed;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class Normal extends MonsterCard {
    private static final long serialVersionUID = 711L;

    public Normal(String name, int MP, int HP, int AP, String speciality, Breed breed) {
        super(name, MP, MP * 300, HP, AP, speciality, breed);
    }

    @Override
    public String toString() {
        return (super.toString() + "\n");
    }

    @Override
    public MonsterCard getCopy() {
        return new Normal(name, MP, super.getHP(), super.getAP(), speciality, breed);
    }

    @Override
    public void setupGraphics() {
        super.setupGraphics();
        ImageView type = new ImageView(new Image(new File("images\\normal.png").toURI().toString()));
        type.setPreserveRatio(true);
        type.setFitWidth(28);
        type.setX(63);
        type.setY(20);
        cardGroup.getChildren().add(type);
    }
}