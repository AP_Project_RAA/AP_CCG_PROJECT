package gameObjects.cards.monsterCards;

import enums.Breed;
import gameObjects.SpellPack;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class General extends MonsterCard {
    private static final long serialVersionUID = 713L;

    private SpellPack battleCry;
    private SpellPack will;

    private String battleCryInfo, willInfo;

    public General(String name, int MP, int HP, int AP, String speciality, Breed breed, SpellPack battleCry, SpellPack will, String battleCryInfo, String willInfo) {
        super(name, MP, MP * 700, HP, AP, speciality, breed);
        this.battleCry = battleCry;
        this.will = will;

        setBattleCryInfo(battleCryInfo);
        setWillInfo(willInfo);

        setupGraphics();
    }


    public SpellPack getBattleCry() {
        return battleCry;
    }

    public void setBattleCry(SpellPack battleCry) {
        this.battleCry = battleCry;
    }

    public SpellPack getWill() {
        return will;
    }

    public void setWill(SpellPack will) {
        this.will = will;
    }

    public void setBattleCryInfo(String battleCryInfo) {
        this.battleCryInfo = battleCryInfo;
    }

    public void setWillInfo(String willInfo) {
        this.willInfo = willInfo;
    }

    public String getBattleCryInfo() {
        return battleCryInfo;
    }

    public String getWillInfo() {
        return willInfo;
    }

    @Override
    public String toString() {
        String out = "BattleCry\n" + battleCry.toString() + "Will\n" + will.toString();

        out += super.toString() + "/" + battleCryInfo + "/" + willInfo;
        return out;
    }

    @Override
    public String getInfo() {
        String out = super.getInfo() + "\n";
        out += "BattleCry Details: \n" + battleCryInfo + "\n";
        out += "Will Details: \n" + willInfo;
        return out;
    }

    @Override
    public MonsterCard getCopy() {
        return new General(name, MP, super.getHP(), super.getAP(), speciality, super.getBreed(), battleCry.getCopy(), will.getCopy(), battleCryInfo, willInfo);
    }

    @Override
    public void setupGraphics() {
        super.setupGraphics();
        ImageView type = new ImageView(new Image(new File("images\\general.png").toURI().toString()));
        type.setPreserveRatio(true);
        type.setFitWidth(28);
        type.setX(63);
        type.setY(20);
        cardGroup.getChildren().add(type);
    }
}