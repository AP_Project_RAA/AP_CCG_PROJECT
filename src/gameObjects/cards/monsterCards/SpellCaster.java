package gameObjects.cards.monsterCards;

import enums.Breed;
import gameObjects.SpellPack;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class SpellCaster extends MonsterCard {
    private static final long serialVersionUID = 712L;

    private SpellPack spell;
    private String spellInfo;
    private boolean usedSpell;

    public SpellPack getSpell() {
        return spell;
    }

    public void setSpell(SpellPack spell) {
        this.spell = spell;
    }

    public void setSpellInfo(String spellInfo) {
        this.spellInfo = spellInfo;
    }

    public boolean isUsedSpell() {
        return usedSpell;
    }

    public void setUsedSpell(boolean usedSpell) {
        this.usedSpell = usedSpell;
    }

    public SpellCaster(String name, int MP, int HP, int AP, String speciality, Breed breed, SpellPack spell, String spellInfo) {
        super(name, MP, MP * 500, HP, AP, speciality, breed);
        this.spell = spell;
        this.usedSpell = false;
        setSpellInfo(spellInfo);

        setupGraphics();
    }

    @Override
    public String toString() {
        String out = "Spell\n" + spell.toString();
        out += super.toString() + "/" + spellInfo;
        return out;
    }

    @Override
    public String getInfo() {
        String out = super.getInfo() + "\n";
        out += "Spell Details: \n" + spellInfo;
        return out;
    }

    @Override
    public MonsterCard getCopy() {

        return new SpellCaster(name, super.MP, super.getHP(), super.getAP(), speciality, super.getBreed(), spell.getCopy(), spellInfo);
    }

    @Override
    public void setupGraphics() {
        super.setupGraphics();
        ImageView type = new ImageView(new Image(new File("images\\spell caster.png").toURI().toString()));
        type.setPreserveRatio(true);
        type.setFitWidth(28);
        type.setX(63);
        type.setY(20);
        cardGroup.getChildren().add(type);
    }
}