package gameObjects.cards.monsterCards;

import enums.Breed;
import gameObjects.SpellPack;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class Hero extends MonsterCard {
    private static final long serialVersionUID = 714L;

    private SpellPack battleCry;
    private SpellPack will;
    private SpellPack spell;
    private boolean usedSpell;

    private String battleCryInfo, spellInfo, willInfo;

    public Hero(String name, int MP, int HP, int AP, String speciality, Breed breed, SpellPack battleCry, SpellPack spell, SpellPack will, String battleCryInfo, String spellInfo, String willInfo) {
        super(name, MP, MP * 1000, HP, AP, speciality, breed);
        this.battleCry = battleCry;
        this.spell = spell;
        this.will = will;
        this.usedSpell = false;

        setBattleCryInfo(battleCryInfo);
        setSpellInfo(spellInfo);
        setWillInfo(willInfo);

        setupGraphics();
    }

    public SpellPack getBattleCry() {
        return battleCry;
    }

    public SpellPack getWill() {
        return will;
    }

    public void setWill(SpellPack will) {
        this.will = will;
    }

    public SpellPack getSpell() {
        return spell;
    }

    public void setSpell(SpellPack spell) {
        this.spell = spell;
    }

    public boolean isUsedSpell() {
        return usedSpell;
    }

    public void setUsedSpell(boolean usedSpell) {
        this.usedSpell = usedSpell;
    }

    public void setBattleCryInfo(String battleCryInfo) {
        this.battleCryInfo = battleCryInfo;
    }

    public void setSpellInfo(String spellInfo) {
        this.spellInfo = spellInfo;
    }

    public void setWillInfo(String willInfo) {
        this.willInfo = willInfo;
    }

    @Override
    public String toString() {
        String out = "BattleCry\n" + battleCry.toString() + "Spell\n" + spell.toString() + "Will\n" + will.toString();

        out += super.toString() + "/" + battleCryInfo + "/" + spellInfo + "/" + willInfo;

        return out;
    }

    @Override
    public String getInfo() {
        String out = super.getInfo() + "\n";
        out += "BattleCry Details: \n" + battleCryInfo + "\n";
        out += "Spell Details: \n" + spellInfo + "\n";
        out += "Will Details: \n" + willInfo;
        return out;
    }

    public MonsterCard getCopy() {
        Hero hero = new Hero(name, MP, super.getHP(), super.getAP(), speciality, super.getBreed(), battleCry.getCopy(), spell.getCopy(), will.getCopy(), battleCryInfo, spellInfo, willInfo);
        return hero;
    }

    @Override
    public void setupGraphics() {
        super.setupGraphics();
        ImageView type = new ImageView(new Image(new File("images\\hero.png").toURI().toString()));
        type.setPreserveRatio(true);
        type.setFitWidth(28);
        type.setX(63);
        type.setY(20);
        cardGroup.getChildren().add(type);
    }
}