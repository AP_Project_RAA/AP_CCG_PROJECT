package gameObjects.cards;

import gameObjects.GameObject;
import javafx.scene.Group;

public class Card extends GameObject {
    private static final long serialVersionUID = 7L;

    protected String name;
    protected int MP;
    protected int price;
    transient protected Group cardGroup;

    public Card(String name, int MP, int price) {
        this.name = name;
        this.MP = MP;
        this.price = price;
    }

    public Card() {
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMP() {
        return this.MP;
    }

    public void setMP(int MP) {
        this.MP = MP;
    }

    public Card getCopy() {
        return new Card(this.name, this.MP, this.price);
    }

    public String getInfo() {
        return "";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Card) {
            return obj.hashCode() == this.hashCode();
        } else {
            return false;
        }
    }

    public Group getCardGroup() {
        return cardGroup;
    }

    public void setupGraphics() {
    }
}