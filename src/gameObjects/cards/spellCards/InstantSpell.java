package gameObjects.cards.spellCards;

import gameObjects.SpellPack;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.File;

public class InstantSpell extends SpellCard {
    private static final long serialVersionUID = 721L;

    public InstantSpell(String name, int MP, SpellPack spellPack, String spellInfo) {
        super(name, MP, spellPack, spellInfo);

        //applying gfx
        setupGraphics();
    }

    public InstantSpell getCopy() {
        return new InstantSpell(name, MP, spellPack.getCopy(), getSpellInfo());
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void setupGraphics() {
        super.setupGraphics();
        ImageView type = new ImageView(new Image(new File("images\\instant-spell.png").toURI().toString()));
        type.setPreserveRatio(true);
        type.setFitWidth(28);
        type.setX(45);
        type.setY(20);
        cardGroup.getChildren().add(type);

        Label typeLabel = new Label("Instant");
        typeLabel.setFont(Font.font("Algerian", FontWeight.BOLD, 11));
        typeLabel.setLayoutX(28);
        typeLabel.setLayoutY(100);
        cardGroup.getChildren().add(typeLabel);
    }
}