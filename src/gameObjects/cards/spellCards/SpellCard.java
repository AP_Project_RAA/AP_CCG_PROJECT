package gameObjects.cards.spellCards;

import gameObjects.SpellPack;
import gameObjects.cards.Card;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import main.Player;

import java.io.File;

public class SpellCard extends Card {
    private static final long serialVersionUID = 72L;

    public SpellPack spellPack;
    private String spellInfo;

    public SpellCard(String name, int MP, SpellPack spellPack, String spellInfo) {
        super(name, MP, MP * 700);
        this.spellPack = spellPack;

        setSpellInfo(spellInfo);

        //Applying card gfx
        setupGraphics();
    }

    public String getSpellInfo() {
        return spellInfo;
    }

    public void setSpellInfo(String spellInfo) {
        this.spellInfo = spellInfo;
    }

    public SpellCard getCopy() {
        return new SpellCard(name, MP, spellPack.getCopy(), spellInfo);
    }

    @Override
    public String toString() {
        String out = spellPack.toString();
        out += super.getName() + "/" + String.valueOf(super.getMP()) + "/" + this.getClass().getSimpleName() + "/" + spellInfo;
        return out;
    }

    @Override
    public String getInfo() {
        String out = "Name: " + "\"" + name + "\"" + "\n";
        out += "MP Cost: " + MP + "\n";
        out += "Card Type: " + this.getClass().getSimpleName() + "\n";
        out += "Spell Details: \n" + spellInfo;
        return out;
    }

    public void setupGraphics() {
        cardGroup = new Group();
        //width = 100 height = 127

        ImageView cardbg = new ImageView();
        Image cardbgImage = new Image(new File("images\\spellbg.png").toURI().toString());
        cardbg.setImage(cardbgImage);
        cardbg.setX(20);
        cardbg.setY(16);
        cardbg.setFitWidth(80);
        cardbg.setFitHeight(108);
        cardGroup.getChildren().add(cardbg);

        ImageView frame = new ImageView();
        Image frameImage = new Image(new File("images\\spellframe3.png").toURI().toString());
        frame.setImage(frameImage);
        frame.setX(0);
        frame.setY(0);
        cardGroup.getChildren().add(frame);

        Label nameLabel = new Label(getName());
        nameLabel.setTranslateY(60);
        nameLabel.setTranslateX(26);
        nameLabel.setPrefWidth(65);
        nameLabel.setTextAlignment(TextAlignment.CENTER);
        nameLabel.setAlignment(Pos.CENTER);
        nameLabel.setWrapText(true);
        nameLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 12));
        cardGroup.getChildren().add(nameLabel);
    }
}