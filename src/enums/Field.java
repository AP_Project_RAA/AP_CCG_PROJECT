package enums;

public enum Field {
    PLAYFIELD, MONSTERFIELD, SPELLFIELD, GRAVEYARD, HAND, DECK;
    private static final long serialVersionUID = 40L;
}
