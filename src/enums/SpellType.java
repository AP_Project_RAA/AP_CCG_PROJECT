package enums;

public enum SpellType {
    SELECTIVE, RANDOM, ALL;
    private static final long serialVersionUID = 44L;
}
