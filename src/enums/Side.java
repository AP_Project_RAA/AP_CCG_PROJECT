package enums;

public enum Side {
    FRIENDLY, ENEMY, BOTH;
    private static final long serialVersionUID = 43L;
}
