package states;

import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import main.Dictionary;

public class CustomMenu implements State {
    private Stage stage;
    private Group root;
    private Scene scene;
    private CustomCard customCard;
    private CustomItem customItem;
    private CustomAmulet customAmulet;
    private MainMenu mainMenu;
    private MediaPlayer backgroundMusic;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setCustomCard(CustomCard customCard) {
        this.customCard = customCard;
    }

    public void setCustomItem(CustomItem customItem) {
        this.customItem = customItem;
    }

    public void setCustomAmulet(CustomAmulet customAmulet) {
        this.customAmulet = customAmulet;
    }

    public void setMainMenu(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    public void run() {
        stage.setScene(scene);
        backgroundMusic.play();
        stage.show();
    }

    public void setupScene() {
        root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        ImageView custumMenubg = new ImageView(Dictionary.CUSTOM_CARD_BG_1);
        custumMenubg.fitWidthProperty().bind(scene.widthProperty());
        custumMenubg.fitHeightProperty().bind(scene.heightProperty());
        root.getChildren().add(custumMenubg);

        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            backgroundMusic.stop();
            mainMenu.run();
        });
        root.getChildren().add(backButtonImageView);

        Label title = new Label("Custom Menu");
        title.setLayoutX(100);
        title.setPrefWidth(scene.getWidth() - 200);
        title.setLayoutY(50);
        title.setFont(Font.font("Algerian", FontWeight.BOLD, 56));
        title.setAlignment(Pos.CENTER);
        title.setTextFill(Color.WHITE);
        title.setOpacity(0.8);
        root.getChildren().add(title);

        HBox buttonsBox = new HBox();
        buttonsBox.setLayoutX(100);
        buttonsBox.setPrefWidth(scene.getWidth() - 200);
        buttonsBox.setLayoutY(scene.getHeight() / 2 - 100);
        buttonsBox.setPrefHeight(200);
        buttonsBox.setAlignment(Pos.CENTER);
        buttonsBox.setSpacing(50);
        root.getChildren().add(buttonsBox);

        ImageView customCardButton = new ImageView(Dictionary.CUSTOM_CARD_BUTTON_OFF);
        customCardButton.setPreserveRatio(true);
        customCardButton.setFitWidth(200);
        customCardButton.setOnMouseEntered(event -> {
            customCardButton.setImage(Dictionary.CUSTOM_CARD_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        customCardButton.setOnMouseExited(event -> {
            customCardButton.setImage(Dictionary.CUSTOM_CARD_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        customCardButton.setOnMouseClicked(event -> {
            customCard.setupScene();
            customCard.run();
        });
        buttonsBox.getChildren().add(customCardButton);

        ImageView customItemButton = new ImageView(Dictionary.CUSTOM_ITEM_BUTTON_OFF);
        customItemButton.setPreserveRatio(true);
        customItemButton.setFitWidth(200);
        customItemButton.setOnMouseEntered(event -> {
            customItemButton.setImage(Dictionary.CUSTOM_ITEM_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        customItemButton.setOnMouseExited(event -> {
            customItemButton.setImage(Dictionary.CUSTOM_ITEM_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        customItemButton.setOnMouseClicked(event -> {
            customItem.run();
        });
        buttonsBox.getChildren().add(customItemButton);

        ImageView customAmuletButton = new ImageView(Dictionary.CUSTOM_AMULET_BUTTON_OFF);
        customAmuletButton.setPreserveRatio(true);
        customAmuletButton.setFitWidth(200);
        customAmuletButton.setOnMouseEntered(event -> {
            customAmuletButton.setImage(Dictionary.CUSTOM_AMULET_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        customAmuletButton.setOnMouseExited(event -> {
            customAmuletButton.setImage(Dictionary.CUSTOM_AMULET_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        customAmuletButton.setOnMouseClicked(event -> {
            customAmulet.run();
        });
        buttonsBox.getChildren().add(customAmuletButton);

        // background music
        backgroundMusic = new MediaPlayer(Dictionary.CUSTOM_BACKGROUND_MUSIC);
        backgroundMusic.setCycleCount(Timeline.INDEFINITE);
    }
}
