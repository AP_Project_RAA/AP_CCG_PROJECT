package states;

import enums.Breed;
import enums.Field;
import enums.Side;
import enums.SpellType;
import gameObjects.SpellPack;
import gameObjects.cards.monsterCards.General;
import gameObjects.cards.monsterCards.Hero;
import gameObjects.cards.monsterCards.Normal;
import gameObjects.cards.monsterCards.SpellCaster;
import gameObjects.cards.spellCards.AuraSpell;
import gameObjects.cards.spellCards.ContinuousSpell;
import gameObjects.cards.spellCards.InstantSpell;
import gameObjects.spells.*;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import main.Dictionary;
import main.Saver;

import java.io.File;
import java.util.ArrayList;


public class CustomCard {
    private Stage stage;
    private Group root;
    private Scene scene;
    private CustomMenu customMenu;
    private boolean isMakingMonsterCard = false, isMakingSpellCard = false;
    private String monsterCardType = "NONE", spellCardType = "NONE";
    private ImageView monsterCardButton, spellCardButton;
    private VBox cardsTypeBox2;
    private Label cardsTypeTitle2;
    private Rectangle cardsTypeBox2bg;
    private ImageView normalButton, spellCasterButton, heroButton, generalButton;
    private TextField hpField, apField, mpField, speciality, monsterBreed;
    private ImageView instantButton, auraButton, continuousButton;
    private Rectangle monsterCardDatabg;
    private VBox monsterCardDataBox;
    private Label monsterCardDataTitle;
    private Rectangle miniToolBoxbg, toolBoxbg;
    private VBox miniSpellToolBox, spellToolBox;
    private Label spellToolBoxTitle;
    private String spellType = "NONE";
    private ImageView healerButton, attackerButton, apChangerButton, cardMoverButton;
    private String addingSpell = "NONE";
    private ImageView addBattleCryButton, addSpellButton, addWillButton;
    private Rectangle spellDatabg;
    private VBox spellDataVbox;
    private Label spellDataTitle;
    private ImageView selectiveButton, randomButton, allButton;
    private String sideString = "NONE";
    private ImageView friendlyButton, enemyButton, bothButton;
    private HBox sideBox;
    private String spellType2 = "NONE";
    private ImageView elvenButton, atlantianButton, dragonBreedButton, demonicButton;
    private ImageView normalTypeButton, spellCasterTypeButton, generalTypeButton, heroTypeButton;
    private TextField spellAmount;
    private ImageView instantTypeButton, contsTypeButton, auraTypeButton;
    private Label spellCardLabel;
    private HBox spellCardTypeBox;
    private String source, dest, sourceSideString, destSideString;
    private HBox sourceBox, destBox;
    private TextField spellPackName, cardName;
    private Rectangle eSpellsBoxbg;
    private Label eSpellsBoxTitle;
    private ScrollPane eSpellsPane;
    private boolean isAddingExisting = false;
    private VBox spellCardDataBox;
    private Label spellCardDataTitle;

    private SpellPack battleCry, spell, will;
    private ArrayList<Spell> spells;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setCustomMenu(CustomMenu customMenu) {
        this.customMenu = customMenu;
    }

    public void run() {
        stage.setScene(scene);
        stage.show();
    }

    public void setupScene() {
        root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        ImageView custumCardbg = new ImageView(Dictionary.CUSTOM_CARD_BG_3);
        custumCardbg.fitWidthProperty().bind(scene.widthProperty());
        custumCardbg.fitHeightProperty().bind(scene.heightProperty());
        root.getChildren().add(custumCardbg);

        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            customMenu.run();
        });
        root.getChildren().add(backButtonImageView);

        Label title = new Label("Custom Cards Factory");
        title.setLayoutX(50);
        title.setPrefWidth(scene.getWidth() - 600);
        title.setLayoutY(50);
        title.setFont(Font.font("Algerian", FontWeight.BOLD, 56));
        title.setAlignment(Pos.CENTER);
        title.setTextFill(Color.WHITE);
        title.setOpacity(0.8);
        root.getChildren().add(title);

        Rectangle cardsTypeBoxbg = new Rectangle(20, 150, 160, scene.getHeight() - 300);
        cardsTypeBoxbg.setFill(Color.BLACK);
        cardsTypeBoxbg.setOpacity(0.5);
        cardsTypeBoxbg.setArcWidth(15);
        cardsTypeBoxbg.setArcHeight(15);
        root.getChildren().add(cardsTypeBoxbg);

        Label cardsTypeTitle = new Label("Choose custom card type:");
        cardsTypeTitle.setLayoutX(25);
        cardsTypeTitle.setPrefWidth(150);
        cardsTypeTitle.setLayoutY(150);
        cardsTypeTitle.setTextAlignment(TextAlignment.CENTER);
        cardsTypeTitle.setAlignment(Pos.CENTER);
        cardsTypeTitle.setWrapText(true);
        cardsTypeTitle.setFont(Font.font("Algerian", 18));
        cardsTypeTitle.setTextFill(Color.WHITE);
        root.getChildren().add(cardsTypeTitle);

        VBox cardsTypeBox = new VBox();
        cardsTypeBox.setLayoutX(20);
        cardsTypeBox.setPrefWidth(160);
        cardsTypeBox.setLayoutY(150);
        cardsTypeBox.setPrefHeight(scene.getHeight() - 300);
        cardsTypeBox.setAlignment(Pos.CENTER);
        cardsTypeBox.setSpacing(50);
        root.getChildren().add(cardsTypeBox);

        monsterCardButton = new ImageView(Dictionary.MONSTERCARD_BUTTON_OFF);
        monsterCardButton.setPreserveRatio(true);
        monsterCardButton.setFitWidth(150);
        monsterCardButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        monsterCardButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        monsterCardButton.setOnMouseClicked(event -> {
            if (isMakingMonsterCard) {
                isMakingMonsterCard = false;
                monsterCardButton.setImage(Dictionary.MONSTERCARD_BUTTON_OFF);
                clearMonsterCardTypeBox();
            } else {
                isMakingMonsterCard = true;
                isMakingSpellCard = false;
                monsterCardButton.setImage(Dictionary.MONSTERCARD_BUTTON_ON);
                spellCardButton.setImage(Dictionary.SPELLCARD_BUTTON_OFF);
                clearSpellCardTypeBox();
                setupMonsterCardTypeBox();
            }
        });
        cardsTypeBox.getChildren().add(monsterCardButton);

        spellCardButton = new ImageView(Dictionary.SPELLCARD_BUTTON_OFF);
        spellCardButton.setPreserveRatio(true);
        spellCardButton.setFitWidth(150);
        spellCardButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        spellCardButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        spellCardButton.setOnMouseClicked(event -> {
            if (isMakingSpellCard) {
                isMakingSpellCard = false;
                spellCardButton.setImage(Dictionary.SPELLCARD_BUTTON_OFF);
                clearSpellCardTypeBox();
            } else {
                isMakingSpellCard = true;
                isMakingMonsterCard = false;
                spellCardButton.setImage(Dictionary.SPELLCARD_BUTTON_ON);
                monsterCardButton.setImage(Dictionary.MONSTERCARD_BUTTON_OFF);
                clearMonsterCardTypeBox();
                setupSpellCardTypeBox();
            }
        });
        cardsTypeBox.getChildren().add(spellCardButton);


        //making monstercardtype and spellcardtype buttons
        //setting up monster cards
        setupMonsterCardButtons();

        //setting up spell cards
        setupSpellCardButtons();

        setupMonsterCardDataBox("NONE");

        setupSpellminiToolBox();

        setupExistingSpellsBox();
        setupSpellToolBox();

        setupSpellDataBox();
    }

    private void setupSpellCardTypeBox() {
        cardsTypeBox2.getChildren().clear();
        cardsTypeBox2.setVisible(true);
        cardsTypeBox2bg.setVisible(true);
        cardsTypeTitle2.setVisible(true);

        cardsTypeTitle2.setText("Choose SpellCard Type:");

        cardsTypeBox2.setSpacing(70);
        cardsTypeBox2.getChildren().addAll(instantButton, auraButton, continuousButton);
    }

    private void clearSpellCardTypeBox() {
        cardsTypeBox2.getChildren().clear();
        cardsTypeBox2.setVisible(false);
        cardsTypeTitle2.setVisible(false);
        cardsTypeBox2bg.setVisible(false);
    }

    private void setupMonsterCardTypeBox() {
        cardsTypeBox2.getChildren().clear();

        cardsTypeBox2bg.setVisible(true);

        cardsTypeTitle2.setText("Choose MonsterCard Type:");
        cardsTypeTitle2.setVisible(true);

        cardsTypeBox2.setVisible(true);
        cardsTypeBox2.setSpacing(50);
        cardsTypeBox2.getChildren().addAll(normalButton, spellCasterButton, generalButton, heroButton);

    }

    private void clearMonsterCardTypeBox() {
        cardsTypeBox2.getChildren().clear();
        cardsTypeBox2.setVisible(false);
        cardsTypeTitle2.setVisible(false);
        cardsTypeBox2bg.setVisible(false);
    }

    private void setupMonsterCardButtons() {
        cardsTypeBox2bg = new Rectangle(190, 150, 160, scene.getHeight() - 300);
        cardsTypeBox2bg.setArcHeight(15);
        cardsTypeBox2bg.setArcWidth(15);
        cardsTypeBox2bg.setFill(Color.BLACK);
        cardsTypeBox2bg.setOpacity(0.5);
        cardsTypeBox2bg.setVisible(false);
        root.getChildren().add(cardsTypeBox2bg);

        cardsTypeTitle2 = new Label();
        cardsTypeTitle2.setLayoutX(195);
        cardsTypeTitle2.setPrefWidth(150);
        cardsTypeTitle2.setLayoutY(150);
        cardsTypeTitle2.setWrapText(true);
        cardsTypeTitle2.setTextFill(Color.WHITE);
        cardsTypeTitle2.setAlignment(Pos.CENTER);
        cardsTypeTitle2.setTextAlignment(TextAlignment.CENTER);
        cardsTypeTitle2.setFont(Font.font("Algerian", 18));
        cardsTypeTitle2.setVisible(false);
        root.getChildren().add(cardsTypeTitle2);

        cardsTypeBox2 = new VBox();
        cardsTypeBox2.setLayoutX(190);
        cardsTypeBox2.setPrefWidth(160);
        cardsTypeBox2.setLayoutY(150);
        cardsTypeBox2.setPrefHeight(scene.getHeight() - 300);
        cardsTypeBox2.setAlignment(Pos.CENTER);
        cardsTypeBox2.setSpacing(50);
        cardsTypeBox2.setVisible(false);
        root.getChildren().add(cardsTypeBox2);

        normalButton = new ImageView(Dictionary.NORMAL_BUTTON_OFF);
        normalButton.setPreserveRatio(true);
        normalButton.setFitWidth(150);
        normalButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        normalButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        normalButton.setOnMouseClicked(event -> {
            if (monsterCardType.equals("NORMAL")) {
                monsterCardType = "NONE";
                normalButton.setImage(Dictionary.NORMAL_BUTTON_OFF);

                clearMonsterCardDataBox();
            } else {
                monsterCardType = "NORMAL";
                normalButton.setImage(Dictionary.NORMAL_BUTTON_ON);
                spellCasterButton.setImage(Dictionary.SPELLCASTER_BUTTON_OFF);
                heroButton.setImage(Dictionary.HERO_BUTTON_OFF);
                generalButton.setImage(Dictionary.GENERAL_BUTTON_OFF);

                clearMonsterCardDataBox();
                setupMonsterCardDataBox("NORMAL");
                showMonsterCardDataBox();
            }
        });

        spellCasterButton = new ImageView(Dictionary.SPELLCASTER_BUTTON_OFF);
        spellCasterButton.setPreserveRatio(true);
        spellCasterButton.setFitWidth(150);
        spellCasterButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        spellCasterButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        spellCasterButton.setOnMouseClicked(event -> {
            if (monsterCardType.equals("SPELLCASTER")) {
                monsterCardType = "NONE";
                spellCasterButton.setImage(Dictionary.SPELLCASTER_BUTTON_OFF);
                clearMonsterCardDataBox();
            } else {
                monsterCardType = "SPELLCASTER";
                spellCasterButton.setImage(Dictionary.SPELLCASTER_BUTTON_ON);
                normalButton.setImage(Dictionary.NORMAL_BUTTON_OFF);
                heroButton.setImage(Dictionary.HERO_BUTTON_OFF);
                generalButton.setImage(Dictionary.GENERAL_BUTTON_OFF);

                clearMonsterCardDataBox();
                setupMonsterCardDataBox("SPELLCASTER");
                showMonsterCardDataBox();
            }
        });

        generalButton = new ImageView(Dictionary.GENERAL_BUTTON_OFF);
        generalButton.setPreserveRatio(true);
        generalButton.setFitWidth(150);
        generalButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        generalButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        generalButton.setOnMouseClicked(event -> {
            if (monsterCardType.equals("GENERAL")) {
                monsterCardType = "NONE";
                generalButton.setImage(Dictionary.GENERAL_BUTTON_OFF);
                clearMonsterCardDataBox();
            } else {
                monsterCardType = "GENERAL";
                generalButton.setImage(Dictionary.GENERAL_BUTTON_ON);
                normalButton.setImage(Dictionary.NORMAL_BUTTON_OFF);
                spellCasterButton.setImage(Dictionary.SPELLCASTER_BUTTON_OFF);
                heroButton.setImage(Dictionary.HERO_BUTTON_OFF);

                clearMonsterCardDataBox();
                setupMonsterCardDataBox("GENERAL");
                showMonsterCardDataBox();
            }
        });

        heroButton = new ImageView(Dictionary.HERO_BUTTON_OFF);
        heroButton.setPreserveRatio(true);
        heroButton.setFitWidth(150);
        heroButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        heroButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        heroButton.setOnMouseClicked(event -> {
            if (monsterCardType.equals("HERO")) {
                monsterCardType = "NONE";
                heroButton.setImage(Dictionary.HERO_BUTTON_OFF);
                clearMonsterCardDataBox();
            } else {
                monsterCardType = "HERO";
                heroButton.setImage(Dictionary.HERO_BUTTON_ON);
                normalButton.setImage(Dictionary.NORMAL_BUTTON_OFF);
                spellCasterButton.setImage(Dictionary.SPELLCASTER_BUTTON_OFF);
                generalButton.setImage(Dictionary.GENERAL_BUTTON_OFF);

                clearMonsterCardDataBox();
                setupMonsterCardDataBox("HERO");
                showMonsterCardDataBox();
            }
        });
    }

    private void setupSpellCardButtons() {
        instantButton = new ImageView(Dictionary.INSTANT_BUTTON_OFF);
        instantButton.setPreserveRatio(true);
        instantButton.setFitWidth(150);
        instantButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        instantButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        instantButton.setOnMouseClicked(event -> {
            if (spellCardType.equals("INSTANT")) {
                spellCardType = "NONE";
                instantButton.setImage(Dictionary.INSTANT_BUTTON_OFF);
            } else {
                spellCardType = "INSTANT";
                instantButton.setImage(Dictionary.INSTANT_BUTTON_ON);
                auraButton.setImage(Dictionary.AURA_BUTTON_OFF);
                continuousButton.setImage(Dictionary.CONTS_BUTTON_OFF);
                setupSpellCardDataBox();
                showSpellCardDataBox();
            }
        });

        auraButton = new ImageView(Dictionary.AURA_BUTTON_OFF);
        auraButton.setPreserveRatio(true);
        auraButton.setFitWidth(150);
        auraButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        auraButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        auraButton.setOnMouseClicked(event -> {
            if (spellCardType.equals("AURA")) {
                spellCardType = "NONE";
                auraButton.setImage(Dictionary.AURA_BUTTON_OFF);
            } else {
                spellCardType = "AURA";
                auraButton.setImage(Dictionary.AURA_BUTTON_ON);
                instantButton.setImage(Dictionary.INSTANT_BUTTON_OFF);
                continuousButton.setImage(Dictionary.CONTS_BUTTON_OFF);
                setupSpellCardDataBox();
                showSpellCardDataBox();
            }
        });

        continuousButton = new ImageView(Dictionary.CONTS_BUTTON_OFF);
        continuousButton.setPreserveRatio(true);
        continuousButton.setFitWidth(150);
        continuousButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        continuousButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        continuousButton.setOnMouseClicked(event -> {
            if (spellCardType.equals("CONTINUOUS")) {
                spellCardType = "NONE";
                continuousButton.setImage(Dictionary.CONTS_BUTTON_OFF);
            } else {
                spellCardType = "CONTINUOUS";
                continuousButton.setImage(Dictionary.CONTS_BUTTON_ON);
                instantButton.setImage(Dictionary.INSTANT_BUTTON_OFF);
                auraButton.setImage(Dictionary.AURA_BUTTON_OFF);
                setupSpellCardDataBox();
                showSpellCardDataBox();
            }
        });
    }

    private void setupMonsterCardDataBox(String cardType) {
        monsterCardDatabg = new Rectangle(360, 150, 160, scene.getHeight() - 300);
        monsterCardDatabg.setFill(Color.BLACK);
        monsterCardDatabg.setOpacity(0.5);
        monsterCardDatabg.setVisible(false);
        monsterCardDatabg.setArcWidth(15);
        monsterCardDatabg.setArcHeight(15);
        root.getChildren().addAll(monsterCardDatabg);

        monsterCardDataTitle = new Label("Enter MonsterCard Data:");
        monsterCardDataTitle.setLayoutX(365);
        monsterCardDataTitle.setLayoutY(150);
        monsterCardDataTitle.setPrefWidth(150);
        monsterCardDataTitle.setWrapText(true);
        monsterCardDataTitle.setAlignment(Pos.CENTER);
        monsterCardDataTitle.setTextAlignment(TextAlignment.CENTER);
        monsterCardDataTitle.setFont(Font.font("Algerian", 18));
        monsterCardDataTitle.setTextFill(Color.WHITE);
        monsterCardDataTitle.setVisible(false);
        root.getChildren().add(monsterCardDataTitle);


        monsterCardDataBox = new VBox();
        monsterCardDataBox.setLayoutX(360);
        monsterCardDataBox.setPrefWidth(160);
        monsterCardDataBox.setLayoutY(150);
        monsterCardDataBox.setPrefHeight(scene.getHeight() - 300);
        monsterCardDataBox.setAlignment(Pos.CENTER);
        monsterCardDataBox.setSpacing(5);
        monsterCardDataBox.setVisible(false);
        root.getChildren().add(monsterCardDataBox);

        cardName = new TextField("CARD NAME");
        cardName.setPrefWidth(150);
        cardName.setAlignment(Pos.CENTER);
        cardName.setFont(Font.font("Times New Roman", 14));
        monsterCardDataBox.getChildren().add(cardName);

        HBox hpBox = new HBox();
        hpBox.setPrefWidth(150);
        hpBox.setSpacing(10);
        monsterCardDataBox.getChildren().add(hpBox);

        Label hpLabel = new Label("HP:");
        hpLabel.setFont(Font.font("Times New Roman", 14));
        hpLabel.setTextFill(Color.WHITE);
        hpLabel.setAlignment(Pos.CENTER);
        hpLabel.setPrefWidth(70);

        hpField = new TextField();
        hpField.setText("-");
        hpField.setAlignment(Pos.CENTER);
        hpField.setPrefWidth(70);
        hpBox.getChildren().addAll(hpLabel, hpField);

        HBox apBox = new HBox();
        apBox.setPrefWidth(150);
        apBox.setSpacing(10);
        monsterCardDataBox.getChildren().addAll(apBox);

        Label apLabel = new Label("AP:");
        apLabel.setFont(Font.font("Times New Roman", 14));
        apLabel.setTextFill(Color.WHITE);
        apLabel.setAlignment(Pos.CENTER);
        apLabel.setPrefWidth(70);

        apField = new TextField();
        apField.setText("-");
        apField.setAlignment(Pos.CENTER);
        apField.setPrefWidth(70);
        apBox.getChildren().addAll(apLabel, apField);

        HBox mpBox = new HBox();
        mpBox.setPrefWidth(150);
        mpBox.setSpacing(10);
        monsterCardDataBox.getChildren().add(mpBox);

        Label mpLabel = new Label("MP:");
        mpLabel.setFont(Font.font("Times New Roman", 14));
        mpLabel.setTextFill(Color.WHITE);
        mpLabel.setAlignment(Pos.CENTER);
        mpLabel.setPrefWidth(70);

        mpField = new TextField();
        mpField.setText("-");
        mpField.setAlignment(Pos.CENTER);
        mpField.setPrefWidth(70);
        mpBox.getChildren().addAll(mpLabel, mpField);

        speciality = new TextField("IS DEFENSIVE - IS NIMBLE");
        speciality.setPrefWidth(160);
        speciality.setFont(Font.font("Times New Roman", 11));
        speciality.setAlignment(Pos.CENTER);
        monsterCardDataBox.getChildren().add(speciality);

        monsterBreed = new TextField("ElVEN|DRAGONBREED|ATLANTIAN|DEMONIC");
        monsterBreed.setPrefWidth(160);
        monsterBreed.setFont(Font.font("Times New Roman", 11));
        monsterBreed.setAlignment(Pos.CENTER);
        monsterCardDataBox.getChildren().add(monsterBreed);

        battleCry = null;
        spell = null;
        will = null;

        //buttons
        //add battlecry
        addBattleCryButton = new ImageView(Dictionary.ADD_BATTLECRY_BUTTON_OFF);
        addBattleCryButton.setPreserveRatio(true);
        addBattleCryButton.setFitWidth(150);
        addBattleCryButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        addBattleCryButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        addBattleCryButton.setOnMouseClicked(event -> {
            if (addingSpell.equals("BATTLECRY")) {
                addingSpell = "NONE";
                addBattleCryButton.setImage(Dictionary.ADD_BATTLECRY_BUTTON_OFF);
                hideMiniSpellToolBox();
                hideSpellToolBox();
            } else {
                addingSpell = "BATTLECRY";
                addBattleCryButton.setImage(Dictionary.ADD_BATTLECRY_BUTTON_ON);
                addSpellButton.setImage(Dictionary.ADDSPELL_BUTTON_OFF);
                addWillButton.setImage(Dictionary.ADD_WILL_BUTTON_OFF);
                hideSpellToolBox();
                showMiniSpellToolBox();
            }
        });

        //add spell
        addSpellButton = new ImageView(Dictionary.ADDSPELL_BUTTON_OFF);
        addSpellButton.setPreserveRatio(true);
        addSpellButton.setFitWidth(150);
        addSpellButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        addSpellButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        addSpellButton.setOnMouseClicked(event -> {
            if (addingSpell.equals("SPELL")) {
                addingSpell = "NONE";
                addSpellButton.setImage(Dictionary.ADDSPELL_BUTTON_OFF);
                hideSpellToolBox();
                hideMiniSpellToolBox();
            } else {
                addingSpell = "SPELL";
                addSpellButton.setImage(Dictionary.ADDSPELL_BUTTON_ON);
                addBattleCryButton.setImage(Dictionary.ADD_BATTLECRY_BUTTON_OFF);
                addWillButton.setImage(Dictionary.ADD_WILL_BUTTON_OFF);
                hideSpellToolBox();
                showMiniSpellToolBox();
            }
        });

        //add will
        addWillButton = new ImageView(Dictionary.ADD_WILL_BUTTON_OFF);
        addWillButton.setPreserveRatio(true);
        addWillButton.setFitWidth(150);
        addWillButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        addWillButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        addWillButton.setOnMouseClicked(event -> {
            if (addingSpell.equals("WILL")) {
                addingSpell = "NONE";
                addWillButton.setImage(Dictionary.ADD_WILL_BUTTON_OFF);
                hideSpellToolBox();
                hideMiniSpellToolBox();
            } else {
                addingSpell = "WILL";
                addWillButton.setImage(Dictionary.ADD_WILL_BUTTON_ON);
                addBattleCryButton.setImage(Dictionary.ADD_BATTLECRY_BUTTON_OFF);
                addSpellButton.setImage(Dictionary.ADDSPELL_BUTTON_OFF);
                hideSpellToolBox();
                showMiniSpellToolBox();
            }
        });

        if (cardType.equals("GENERAL") || cardType.equals("HERO"))
            monsterCardDataBox.getChildren().add(addBattleCryButton);

        if (cardType.equals("SPELLCASTER") || cardType.equals("HERO"))
            monsterCardDataBox.getChildren().add(addSpellButton);

        if (cardType.equals("GENERAL") || cardType.equals("HERO"))
            monsterCardDataBox.getChildren().add(addWillButton);

        //make card
        ImageView makeCardButton = new ImageView(Dictionary.MAKECARD_BUTTON_OFF);
        makeCardButton.setPreserveRatio(true);
        makeCardButton.setFitWidth(150);
        makeCardButton.setOnMouseEntered(event -> {
            makeCardButton.setImage(Dictionary.MAKECARD_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        makeCardButton.setOnMouseExited(event -> {
            makeCardButton.setImage(Dictionary.MAKECARD_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        makeCardButton.setOnMouseClicked(event -> {
            makeMonsterCard();
        });
        monsterCardDataBox.getChildren().add(makeCardButton);
    }

    private void showMonsterCardDataBox() {
        monsterCardDatabg.setVisible(true);
        monsterCardDataTitle.setVisible(true);
        monsterCardDataBox.setVisible(true);
        addBattleCryButton.setDisable(false);
        addBattleCryButton.setOpacity(1);
        addSpellButton.setDisable(false);
        addSpellButton.setOpacity(1);
        addWillButton.setDisable(false);
        addWillButton.setOpacity(1);
    }

    private void clearMonsterCardDataBox() {
        root.getChildren().remove(monsterCardDatabg);
        monsterCardDataBox.setVisible(false);
        monsterCardDataBox.getChildren().clear();
        monsterCardDataTitle.setVisible(false);
    }

    private void setupSpellCardDataBox() {
        monsterCardDatabg = new Rectangle(360, 150, 160, scene.getHeight() - 300);
        monsterCardDatabg.setFill(Color.BLACK);
        monsterCardDatabg.setOpacity(0.5);
        monsterCardDatabg.setVisible(false);
        monsterCardDatabg.setArcWidth(15);
        monsterCardDatabg.setArcHeight(15);
        root.getChildren().add(monsterCardDatabg);

        spellCardDataTitle = new Label("Enter MonsterCard Data:");
        spellCardDataTitle.setLayoutX(365);
        spellCardDataTitle.setLayoutY(150);
        spellCardDataTitle.setPrefWidth(150);
        spellCardDataTitle.setWrapText(true);
        spellCardDataTitle.setAlignment(Pos.CENTER);
        spellCardDataTitle.setTextAlignment(TextAlignment.CENTER);
        spellCardDataTitle.setFont(Font.font("Algerian", 18));
        spellCardDataTitle.setTextFill(Color.WHITE);
        spellCardDataTitle.setVisible(false);
        root.getChildren().add(spellCardDataTitle);


        spellCardDataBox = new VBox();
        spellCardDataBox.setLayoutX(360);
        spellCardDataBox.setPrefWidth(160);
        spellCardDataBox.setLayoutY(150);
        spellCardDataBox.setPrefHeight(scene.getHeight() - 300);
        spellCardDataBox.setAlignment(Pos.CENTER);
        spellCardDataBox.setSpacing(5);
        spellCardDataBox.setVisible(false);
        root.getChildren().add(spellCardDataBox);

        cardName = new TextField("CARD NAME");
        cardName.setPrefWidth(150);
        cardName.setAlignment(Pos.CENTER);
        cardName.setFont(Font.font("Times New Roman", 14));
        spellCardDataBox.getChildren().add(cardName);

        HBox mpBox = new HBox();
        mpBox.setPrefWidth(150);
        mpBox.setSpacing(10);
        spellCardDataBox.getChildren().add(mpBox);

        Label mpLabel = new Label("MP:");
        mpLabel.setFont(Font.font("Times New Roman", 14));
        mpLabel.setTextFill(Color.WHITE);
        mpLabel.setAlignment(Pos.CENTER);
        mpLabel.setPrefWidth(70);

        mpField = new TextField();
        mpField.setText("-");
        mpField.setAlignment(Pos.CENTER);
        mpField.setPrefWidth(70);
        mpBox.getChildren().addAll(mpLabel, mpField);


        spell = null;

        //buttons
        //add spell
        addSpellButton = new ImageView(Dictionary.ADDSPELL_BUTTON_OFF);
        addSpellButton.setPreserveRatio(true);
        addSpellButton.setFitWidth(150);
        addSpellButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        addSpellButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        addSpellButton.setOnMouseClicked(event -> {
            if (addingSpell.equals("SPELL")) {
                addingSpell = "NONE";
                addSpellButton.setImage(Dictionary.ADDSPELL_BUTTON_OFF);
                hideSpellToolBox();
                hideMiniSpellToolBox();
            } else {
                addingSpell = "SPELL";
                addSpellButton.setImage(Dictionary.ADDSPELL_BUTTON_ON);
                hideSpellToolBox();
                showMiniSpellToolBox();
            }
        });
        spellCardDataBox.getChildren().add(addSpellButton);

        //make card
        ImageView makeCardButton = new ImageView(Dictionary.MAKECARD_BUTTON_OFF);
        makeCardButton.setPreserveRatio(true);
        makeCardButton.setFitWidth(150);
        makeCardButton.setOnMouseEntered(event -> {
            makeCardButton.setImage(Dictionary.MAKECARD_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        makeCardButton.setOnMouseExited(event -> {
            makeCardButton.setImage(Dictionary.MAKECARD_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        makeCardButton.setOnMouseClicked(event -> {
            makeSpellCard();
        });
        spellCardDataBox.getChildren().add(makeCardButton);
    }

    private void showSpellCardDataBox() {
        monsterCardDatabg.setVisible(true);
        spellCardDataTitle.setVisible(true);
        spellCardDataBox.setVisible(true);
    }

    private void hideSpellCardDataBox() {
        if (root.getChildren().contains(monsterCardDatabg))
            root.getChildren().remove(monsterCardDatabg);

        if (root.getChildren().contains(spellCardDataTitle))
            root.getChildren().remove(spellCardDataTitle);

        if (root.getChildren().contains(spellCardDataBox))
            root.getChildren().remove(spellCardDataBox);
    }

    private void setupSpellminiToolBox() {
        miniToolBoxbg = new Rectangle(530, 150, 160, scene.getHeight() - 300);
        miniToolBoxbg.setFill(Color.BLACK);
        miniToolBoxbg.setOpacity(0.5);
        miniToolBoxbg.setArcHeight(15);
        miniToolBoxbg.setArcWidth(15);
        miniToolBoxbg.setVisible(false);
        root.getChildren().add(miniToolBoxbg);

        miniSpellToolBox = new VBox();
        miniSpellToolBox.setLayoutX(530);
        miniSpellToolBox.setPrefWidth(160);
        miniSpellToolBox.setLayoutY(150);
        miniSpellToolBox.setPrefHeight(scene.getHeight() - 300);
        miniSpellToolBox.setAlignment(Pos.CENTER);
        miniSpellToolBox.setSpacing(50);
        miniSpellToolBox.setVisible(false);
        root.getChildren().add(miniSpellToolBox);

        ImageView addExistingButton = new ImageView(Dictionary.ADD_EXISTING_BUTTON_OFF);
        addExistingButton.setPreserveRatio(true);
        addExistingButton.setFitWidth(150);
        addExistingButton.setOnMouseEntered(event -> {
            addExistingButton.setImage(Dictionary.ADD_EXISTING_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        addExistingButton.setOnMouseExited(event -> {
            addExistingButton.setImage(Dictionary.ADD_EXISTING_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        addExistingButton.setOnMouseClicked(event -> {
            if (isAddingExisting) {
                isAddingExisting = false;
                hideExistingSpellsBox();
            } else {
                isAddingExisting = true;
                hideSpellToolBox();
                setupExistingSpellsBox();
                showExistingSpellsBox();
            }
        });
        miniSpellToolBox.getChildren().add(addExistingButton);

        ImageView createNewButton = new ImageView(Dictionary.CREATE_NEW_BUTTON_OFF);
        createNewButton.setPreserveRatio(true);
        createNewButton.setFitWidth(150);
        createNewButton.setOnMouseEntered(event -> {
            createNewButton.setImage(Dictionary.CREATE_NEW_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        createNewButton.setOnMouseExited(event -> {
            createNewButton.setImage(Dictionary.CREATE_NEW_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        createNewButton.setOnMouseClicked(event -> {
            if (spellToolBox.isVisible())
                hideSpellToolBox();
            else {
                showSpellToolBox();
                spells = new ArrayList<>();
            }
        });
        miniSpellToolBox.getChildren().add(createNewButton);
    }

    private void showMiniSpellToolBox() {
        miniSpellToolBox.setVisible(true);
        miniToolBoxbg.setVisible(true);
    }

    private void hideMiniSpellToolBox() {
        miniToolBoxbg.setVisible(false);
        miniSpellToolBox.setVisible(false);
    }

    private void setupExistingSpellsBox() {
        eSpellsBoxbg = new Rectangle(700, 150, 160, scene.getHeight() - 300);
        eSpellsBoxbg.setFill(Color.BLACK);
        eSpellsBoxbg.setOpacity(0.5);
        eSpellsBoxbg.setArcWidth(15);
        eSpellsBoxbg.setArcHeight(15);
        eSpellsBoxbg.setVisible(false);
        root.getChildren().add(eSpellsBoxbg);

        eSpellsBoxTitle = new Label("Existing Spells");
        eSpellsBoxTitle.setLayoutX(705);
        eSpellsBoxTitle.setLayoutY(150);
        eSpellsBoxTitle.setPrefWidth(150);
        eSpellsBoxTitle.setFont(Font.font("Algerian", 18));
        eSpellsBoxTitle.setTextFill(Color.WHITE);
        eSpellsBoxTitle.setWrapText(true);
        eSpellsBoxTitle.setAlignment(Pos.CENTER);
        eSpellsBoxTitle.setTextAlignment(TextAlignment.CENTER);
        eSpellsBoxTitle.setVisible(false);
        root.getChildren().add(eSpellsBoxTitle);

        eSpellsPane = new ScrollPane();
        eSpellsPane.setLayoutX(700);
        eSpellsPane.setLayoutY(250);
        eSpellsPane.setPrefViewportHeight(scene.getHeight() - 400);
        eSpellsPane.setPrefViewportWidth(160);
        eSpellsPane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        eSpellsPane.setVisible(false);
        root.getChildren().add(eSpellsPane);

        VBox eSpellsBox = new VBox();
        eSpellsBox.setAlignment(Pos.CENTER);
        eSpellsBox.setSpacing(10);
        ArrayList<SpellPack> eSpells = Saver.loadSpellPacks();
        for (SpellPack spellPack : eSpells) {
            Label spellPackLabel = new Label(spellPack.getName());
            spellPackLabel.setTranslateX(10);
            spellPackLabel.setPrefWidth(140);
            spellPackLabel.setWrapText(true);
            spellPackLabel.setFont(Font.font("Times New Roman", 16));
            spellPackLabel.setStyle("-fx-background-color: WHITE");
            spellPackLabel.setAlignment(Pos.CENTER);
            spellPackLabel.setTextAlignment(TextAlignment.CENTER);
            spellPackLabel.setOnMouseEntered(event -> {
                scene.setCursor(Cursor.HAND);
                spellPackLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
            });
            spellPackLabel.setOnMouseExited(event -> {
                scene.setCursor(Cursor.DEFAULT);
                spellPackLabel.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
            });
            spellPackLabel.setOnMouseClicked(event -> {
                spells = new ArrayList<>();
                spells.addAll(spellPack.getSpells());
                createSpellPack();
            });
            eSpellsBox.getChildren().add(spellPackLabel);
        }
        eSpellsPane.setContent(eSpellsBox);
    }

    private void showExistingSpellsBox() {
        eSpellsBoxbg.setVisible(true);
        eSpellsBoxTitle.setVisible(true);
        eSpellsPane.setVisible(true);
    }

    private void hideExistingSpellsBox() {
        if (root.getChildren().contains(eSpellsBoxbg))
            root.getChildren().remove(eSpellsBoxbg);
        if (root.getChildren().contains(eSpellsBoxTitle))
            root.getChildren().remove(eSpellsBoxTitle);
        if (root.getChildren().contains(eSpellsPane))
            root.getChildren().remove(eSpellsPane);
    }

    private void setupSpellToolBox() {
        //700 150
        toolBoxbg = new Rectangle(700, 150, 160, scene.getHeight() - 300);
        toolBoxbg.setFill(Color.BLACK);
        toolBoxbg.setOpacity(0.5);
        toolBoxbg.setArcWidth(15);
        toolBoxbg.setArcHeight(15);
        toolBoxbg.setVisible(false);
        root.getChildren().add(toolBoxbg);

        spellToolBoxTitle = new Label("Choose Spell Types:");
        spellToolBoxTitle.setLayoutX(705);
        spellToolBoxTitle.setPrefWidth(150);
        spellToolBoxTitle.setLayoutY(150);
        spellToolBoxTitle.setWrapText(true);
        spellToolBoxTitle.setTextFill(Color.WHITE);
        spellToolBoxTitle.setFont(Font.font("Algerian", 18));
        spellToolBoxTitle.setAlignment(Pos.CENTER);
        spellToolBoxTitle.setTextAlignment(TextAlignment.CENTER);
        spellToolBoxTitle.setVisible(false);
        root.getChildren().add(spellToolBoxTitle);

        spellToolBox = new VBox();
        spellToolBox.setLayoutX(700);
        spellToolBox.setLayoutY(150);
        spellToolBox.setPrefWidth(160);
        spellToolBox.setPrefHeight(scene.getHeight() - 300);
        spellToolBox.setAlignment(Pos.CENTER);
        spellToolBox.setSpacing(20);
        spellToolBox.setVisible(false);
        root.getChildren().add(spellToolBox);

        spellPackName = new TextField("SPELL NAME");
        spellPackName.setPrefWidth(150);
        spellPackName.setFont(Font.font("Times New Roman", 14));
        spellPackName.setAlignment(Pos.CENTER);
        spellToolBox.getChildren().add(spellPackName);


        healerButton = new ImageView(Dictionary.HEALER_BUTTON_OFF);
        healerButton.setPreserveRatio(true);
        healerButton.setFitWidth(150);
        healerButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        healerButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        healerButton.setOnMouseClicked(event -> {
            if (spellType.equals("HEALER")) {
                spellType = "NONE";
                healerButton.setImage(Dictionary.HEALER_BUTTON_OFF);
            } else {
                spellType = "HEALER";
                healerButton.setImage(Dictionary.HEALER_BUTTON_ON);
                attackerButton.setImage(Dictionary.ATTACKER_BUTTON_OFF);
                apChangerButton.setImage(Dictionary.APCHANGER_BUTTON_OFF);
                cardMoverButton.setImage(Dictionary.CARDMOVER_BUTTON_OFF);

                hideSpellDataBox();
                setupSpellDataBox();
                showSpellDataBox();
            }
        });

        attackerButton = new ImageView(Dictionary.ATTACKER_BUTTON_OFF);
        attackerButton.setPreserveRatio(true);
        attackerButton.setFitWidth(150);
        attackerButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        attackerButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        attackerButton.setOnMouseClicked(event -> {
            if (spellType.equals("ATTACKER")) {
                spellType = "NONE";
                attackerButton.setImage(Dictionary.ATTACKER_BUTTON_OFF);
            } else {
                spellType = "ATTACKER";
                attackerButton.setImage(Dictionary.ATTACKER_BUTTON_ON);
                healerButton.setImage(Dictionary.HEALER_BUTTON_OFF);
                apChangerButton.setImage(Dictionary.APCHANGER_BUTTON_OFF);
                cardMoverButton.setImage(Dictionary.CARDMOVER_BUTTON_OFF);

                hideSpellDataBox();
                setupSpellDataBox();
                showSpellDataBox();
            }
        });

        apChangerButton = new ImageView(Dictionary.APCHANGER_BUTTON_OFF);
        apChangerButton.setPreserveRatio(true);
        apChangerButton.setFitWidth(150);
        apChangerButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        apChangerButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        apChangerButton.setOnMouseClicked(event -> {
            if (spellType.equals("APCHANGER")) {
                spellType = "NONE";
                apChangerButton.setImage(Dictionary.APCHANGER_BUTTON_OFF);
            } else {
                spellType = "APCHANGER";
                apChangerButton.setImage(Dictionary.APCHANGER_BUTTON_ON);
                healerButton.setImage(Dictionary.HEALER_BUTTON_OFF);
                attackerButton.setImage(Dictionary.ATTACKER_BUTTON_OFF);
                cardMoverButton.setImage(Dictionary.CARDMOVER_BUTTON_OFF);

                hideSpellDataBox();
                setupSpellDataBox();
                showSpellDataBox();
            }
        });

        cardMoverButton = new ImageView(Dictionary.CARDMOVER_BUTTON_OFF);
        cardMoverButton.setPreserveRatio(true);
        cardMoverButton.setFitWidth(150);
        cardMoverButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        cardMoverButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        cardMoverButton.setOnMouseClicked(event -> {
            if (spellType.equals("CARDMOVER")) {
                spellType = "NONE";
                cardMoverButton.setImage(Dictionary.CARDMOVER_BUTTON_OFF);
            } else {
                spellType = "CARDMOVER";
                cardMoverButton.setImage(Dictionary.CARDMOVER_BUTTON_ON);
                healerButton.setImage(Dictionary.HEALER_BUTTON_OFF);
                attackerButton.setImage(Dictionary.ATTACKER_BUTTON_OFF);
                apChangerButton.setImage(Dictionary.APCHANGER_BUTTON_OFF);

                hideSpellDataBox();
                setupSpellDataBox();
                showSpellDataBox();
            }
        });
        spellToolBox.getChildren().addAll(healerButton, attackerButton, apChangerButton, cardMoverButton);

        ImageView createButton = new ImageView(Dictionary.CREATE_BLUE_BUTTON_OFF);
        createButton.setPreserveRatio(true);
        createButton.setFitWidth(150);
        createButton.setOnMouseEntered(event -> {
            scene.setCursor(Cursor.HAND);
            createButton.setImage(Dictionary.CREATE_BLUE_BUTTON_ON);
        });
        createButton.setOnMouseExited(event -> {
            scene.setCursor(Cursor.DEFAULT);
            createButton.setImage(Dictionary.CREATE_BLUE_BUTTON_OFF);
        });
        createButton.setOnMouseClicked(event -> {
            createSpellPack();
        });
        spellToolBox.getChildren().addAll(createButton);
    }

    private void showSpellToolBox() {
        toolBoxbg.setVisible(true);
        spellToolBoxTitle.setVisible(true);
        spellToolBox.setVisible(true);

        spellPackName.setText("SPELL NAME");

        healerButton.setOpacity(1);
        healerButton.setImage(Dictionary.HEALER_BUTTON_OFF);
        healerButton.setDisable(false);
        attackerButton.setOpacity(1);
        attackerButton.setImage(Dictionary.ATTACKER_BUTTON_OFF);
        attackerButton.setDisable(false);
        apChangerButton.setOpacity(1);
        apChangerButton.setImage(Dictionary.APCHANGER_BUTTON_OFF);
        apChangerButton.setDisable(false);
        cardMoverButton.setOpacity(1);
        cardMoverButton.setImage(Dictionary.CARDMOVER_BUTTON_OFF);
        cardMoverButton.setDisable(false);
    }

    private void hideSpellToolBox() {
        spellToolBox.setVisible(false);
        spellToolBoxTitle.setVisible(false);
        toolBoxbg.setVisible(false);
    }

    private void setupSpellDataBox() {
        spellDatabg = new Rectangle(870, 100, 310, scene.getHeight() - 200);
        spellDatabg.setFill(Color.BLACK);
        spellDatabg.setArcHeight(15);
        spellDatabg.setArcWidth(15);
        spellDatabg.setOpacity(0.5);
        spellDatabg.setVisible(false);
        root.getChildren().add(spellDatabg);

        spellDataVbox = new VBox();
        spellDataVbox.setLayoutX(870);
        spellDataVbox.setPrefWidth(300);
        spellDataVbox.setLayoutY(100);
        spellDataVbox.setPrefHeight(scene.getHeight() - 200);
        spellDataVbox.setAlignment(Pos.CENTER);
        spellDataVbox.setSpacing(10);
        spellDataVbox.setVisible(false);
        root.getChildren().add(spellDataVbox);

        spellDataTitle = new Label("Title");
        spellDataTitle.setPrefWidth(300);
        spellDataTitle.setWrapText(true);
        spellDataTitle.setAlignment(Pos.CENTER);
        spellDataTitle.setTextAlignment(TextAlignment.CENTER);
        spellDataTitle.setTextFill(Color.WHITE);
        spellDataTitle.setFont(Font.font("Algerian", 14));
        spellDataVbox.getChildren().add(spellDataTitle);

        HBox spellTypeBox = new HBox();
        spellTypeBox.setPrefWidth(300);
        spellTypeBox.setAlignment(Pos.CENTER);
        spellDataVbox.getChildren().add(spellTypeBox);

        selectiveButton = new ImageView(Dictionary.SELECTIVE_BUTTON_OFF);
        selectiveButton.setPreserveRatio(true);
        selectiveButton.setFitWidth(90);
        selectiveButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        selectiveButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        selectiveButton.setOnMouseClicked(event -> {
            if (spellType2.equals("SELECTIVE")) {
                spellType2 = "NONE";
                selectiveButton.setImage(Dictionary.SELECTIVE_BUTTON_OFF);
            } else {
                spellType2 = "SELECTIVE";
                selectiveButton.setImage(Dictionary.SELECTIVE_BUTTON_ON);
                randomButton.setImage(Dictionary.RANDOM_BUTTON_OFF);
                allButton.setImage(Dictionary.ALL_BUTTON_OFF);
            }
        });
        spellTypeBox.getChildren().add(selectiveButton);

        randomButton = new ImageView(Dictionary.RANDOM_BUTTON_OFF);
        randomButton.setPreserveRatio(true);
        randomButton.setFitWidth(90);
        randomButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        randomButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        randomButton.setOnMouseClicked(event -> {
            if (spellType2.equals("RANDOM")) {
                spellType2 = "NONE";
                randomButton.setImage(Dictionary.RANDOM_BUTTON_OFF);
            } else {
                spellType2 = "RANDOM";
                randomButton.setImage(Dictionary.RANDOM_BUTTON_ON);
                selectiveButton.setImage(Dictionary.SELECTIVE_BUTTON_OFF);
                allButton.setImage(Dictionary.ALL_BUTTON_OFF);
            }
        });
        spellTypeBox.getChildren().add(randomButton);

        allButton = new ImageView(Dictionary.ALL_BUTTON_OFF);
        allButton.setPreserveRatio(true);
        allButton.setFitWidth(90);
        allButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        allButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        allButton.setOnMouseClicked(event -> {
            if (spellType2.equals("ALL")) {
                spellType2 = "NONE";
                allButton.setImage(Dictionary.ALL_BUTTON_OFF);
            } else {
                spellType2 = "ALL";
                allButton.setImage(Dictionary.ALL_BUTTON_ON);
                selectiveButton.setImage(Dictionary.SELECTIVE_BUTTON_OFF);
                randomButton.setImage(Dictionary.RANDOM_BUTTON_OFF);
            }
        });
        spellTypeBox.getChildren().add(allButton);

        sideBox = new HBox();
        sideBox.setPrefWidth(300);
        sideBox.setAlignment(Pos.CENTER);
        sideBox.setVisible(false);
        spellDataVbox.getChildren().add(sideBox);

        friendlyButton = new ImageView(Dictionary.FRIENDLY_BUTTON_OFF);
        friendlyButton.setPreserveRatio(true);
        friendlyButton.setFitWidth(90);
        friendlyButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        friendlyButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        friendlyButton.setOnMouseClicked(event -> {
            if (sideString.equals("FRIENDLY")) {
                sideString = "NONE";
                friendlyButton.setImage(Dictionary.FRIENDLY_BUTTON_OFF);
            } else {
                sideString = "FRIENDLY";
                friendlyButton.setImage(Dictionary.FRIENDLY_BUTTON_ON);
                enemyButton.setImage(Dictionary.ENEMY_BUTTON_OFF);
                bothButton.setImage(Dictionary.BOTH_BUTTON_OFF);
            }
        });

        enemyButton = new ImageView(Dictionary.ENEMY_BUTTON_OFF);
        enemyButton.setPreserveRatio(true);
        enemyButton.setFitWidth(90);
        enemyButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        enemyButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        enemyButton.setOnMouseClicked(event -> {
            if (sideString.equals("ENEMY")) {
                sideString = "NONE";
                enemyButton.setImage(Dictionary.ENEMY_BUTTON_OFF);
            } else {
                sideString = "ENEMY";
                enemyButton.setImage(Dictionary.ENEMY_BUTTON_ON);
                friendlyButton.setImage(Dictionary.FRIENDLY_BUTTON_OFF);
                bothButton.setImage(Dictionary.BOTH_BUTTON_OFF);
            }
        });

        bothButton = new ImageView(Dictionary.BOTH_BUTTON_OFF);
        bothButton.setPreserveRatio(true);
        bothButton.setFitWidth(90);
        bothButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        bothButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        bothButton.setOnMouseClicked(event -> {
            if (sideString.equals("BOTH")) {
                sideString = "NONE";
                bothButton.setImage(Dictionary.BOTH_BUTTON_OFF);
            } else {
                sideString = "BOTH";
                bothButton.setImage(Dictionary.BOTH_BUTTON_ON);
                friendlyButton.setImage(Dictionary.FRIENDLY_BUTTON_OFF);
                enemyButton.setImage(Dictionary.ENEMY_BUTTON_OFF);
            }
        });
        sideBox.getChildren().addAll(friendlyButton, enemyButton, bothButton);

        VBox breedBox = new VBox();
        breedBox.setAlignment(Pos.CENTER);
        breedBox.setSpacing(5);

        HBox breedBox1 = new HBox();
        breedBox1.setAlignment(Pos.CENTER);
        breedBox1.setPrefWidth(300);
        breedBox1.setSpacing(5);

        HBox breedBox2 = new HBox();
        breedBox2.setAlignment(Pos.CENTER);
        breedBox2.setPrefWidth(300);
        breedBox2.setSpacing(5);

        breedBox.getChildren().addAll(breedBox1, breedBox2);
        spellDataVbox.getChildren().add(breedBox);

        elvenButton = new ImageView(Dictionary.ELVEN_BUTTON_OFF);
        elvenButton.setPreserveRatio(true);
        elvenButton.setFitWidth(110);
        elvenButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        elvenButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        elvenButton.setOnMouseClicked(event -> {
            if (elvenButton.getImage().equals(Dictionary.ELVEN_BUTTON_OFF))
                elvenButton.setImage(Dictionary.ELVEN_BUTTON_ON);
            else
                elvenButton.setImage(Dictionary.ELVEN_BUTTON_OFF);
        });

        atlantianButton = new ImageView(Dictionary.ATLANTIAN_BUTTON_OFF);
        atlantianButton.setPreserveRatio(true);
        atlantianButton.setFitWidth(110);
        atlantianButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        atlantianButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        atlantianButton.setOnMouseClicked(event -> {
            if (atlantianButton.getImage().equals(Dictionary.ATLANTIAN_BUTTON_OFF))
                atlantianButton.setImage(Dictionary.ATLANTIAN_BUTTON_ON);
            else
                atlantianButton.setImage(Dictionary.ATLANTIAN_BUTTON_OFF);
        });

        breedBox1.getChildren().addAll(elvenButton, atlantianButton);

        dragonBreedButton = new ImageView(Dictionary.DRAGONBREED_BUTTON_OFF);
        dragonBreedButton.setPreserveRatio(true);
        dragonBreedButton.setFitWidth(110);
        dragonBreedButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        dragonBreedButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        dragonBreedButton.setOnMouseClicked(event -> {
            if (dragonBreedButton.getImage().equals(Dictionary.DRAGONBREED_BUTTON_OFF))
                dragonBreedButton.setImage(Dictionary.DRAGONBREED_BUTTON_ON);
            else
                dragonBreedButton.setImage(Dictionary.DRAGONBREED_BUTTON_OFF);
        });

        demonicButton = new ImageView(Dictionary.DEMONIC_BUTTON_OFF);
        demonicButton.setPreserveRatio(true);
        demonicButton.setFitWidth(110);
        demonicButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        demonicButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        demonicButton.setOnMouseClicked(event -> {
            if (demonicButton.getImage().equals(Dictionary.DEMONIC_BUTTON_OFF))
                demonicButton.setImage(Dictionary.DEMONIC_BUTTON_ON);
            else
                demonicButton.setImage(Dictionary.DEMONIC_BUTTON_OFF);
        });
        breedBox2.getChildren().addAll(dragonBreedButton, demonicButton);

        VBox monsterTypeBox = new VBox();
        monsterTypeBox.setAlignment(Pos.CENTER);
        monsterTypeBox.setSpacing(5);

        HBox monsterTypeBox1 = new HBox();
        monsterTypeBox1.setAlignment(Pos.CENTER);
        monsterTypeBox1.setSpacing(5);

        HBox monsterTypeBox2 = new HBox();
        monsterTypeBox2.setAlignment(Pos.CENTER);
        monsterTypeBox2.setSpacing(5);

        monsterTypeBox.getChildren().addAll(monsterTypeBox1, monsterTypeBox2);
        spellDataVbox.getChildren().addAll(monsterTypeBox);

        //monster type buttons
        normalTypeButton = new ImageView(Dictionary.NORMAL_BUTTON_OFF);
        normalTypeButton.setPreserveRatio(true);
        normalTypeButton.setFitWidth(110);
        normalTypeButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        normalTypeButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        normalTypeButton.setOnMouseClicked(event -> {
            if (normalTypeButton.getImage().equals(Dictionary.NORMAL_BUTTON_OFF))
                normalTypeButton.setImage(Dictionary.NORMAL_BUTTON_ON);
            else
                normalTypeButton.setImage(Dictionary.NORMAL_BUTTON_OFF);
        });

        spellCasterTypeButton = new ImageView(Dictionary.SPELLCASTER_BUTTON_OFF);
        spellCasterTypeButton.setPreserveRatio(true);
        spellCasterTypeButton.setFitWidth(110);
        spellCasterTypeButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        spellCasterTypeButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        spellCasterTypeButton.setOnMouseClicked(event -> {
            if (spellCasterTypeButton.getImage().equals(Dictionary.SPELLCASTER_BUTTON_OFF))
                spellCasterTypeButton.setImage(Dictionary.SPELLCASTER_BUTTON_ON);
            else
                spellCasterTypeButton.setImage(Dictionary.SPELLCASTER_BUTTON_OFF);
        });

        monsterTypeBox1.getChildren().addAll(normalTypeButton, spellCasterTypeButton);

        generalTypeButton = new ImageView(Dictionary.GENERAL_BUTTON_OFF);
        generalTypeButton.setPreserveRatio(true);
        generalTypeButton.setFitWidth(110);
        generalTypeButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        generalTypeButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        generalTypeButton.setOnMouseClicked(event -> {
            if (generalTypeButton.getImage().equals(Dictionary.GENERAL_BUTTON_OFF))
                generalTypeButton.setImage(Dictionary.GENERAL_BUTTON_ON);
            else
                generalTypeButton.setImage(Dictionary.GENERAL_BUTTON_OFF);
        });

        heroTypeButton = new ImageView(Dictionary.HERO_BUTTON_OFF);
        heroTypeButton.setPreserveRatio(true);
        heroTypeButton.setFitWidth(110);
        heroTypeButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        heroTypeButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        heroTypeButton.setOnMouseClicked(event -> {
            if (heroTypeButton.getImage().equals(Dictionary.HERO_BUTTON_OFF))
                heroTypeButton.setImage(Dictionary.HERO_BUTTON_ON);
            else
                heroTypeButton.setImage(Dictionary.HERO_BUTTON_OFF);
        });

        monsterTypeBox2.getChildren().addAll(generalTypeButton, heroTypeButton);


        Label monsterLabel = new Label("Monster Cards");
        monsterLabel.setPrefWidth(300);
        monsterLabel.setWrapText(true);
        monsterLabel.setAlignment(Pos.CENTER);
        monsterLabel.setTextAlignment(TextAlignment.CENTER);
        monsterLabel.setTextFill(Color.WHITE);
        monsterLabel.setFont(Font.font("Algerian", 14));
        spellDataVbox.getChildren().add(monsterLabel);

        spellAmount = new TextField();
        spellAmount.setPrefWidth(100);
        spellAmount.setAlignment(Pos.CENTER);
        spellAmount.setFont(Font.font("Times New Roman", 14));
        spellAmount.setVisible(false);
        spellDataVbox.getChildren().add(spellAmount);

        spellCardTypeBox = new HBox();
        spellCardTypeBox.setAlignment(Pos.CENTER);
        spellCardTypeBox.setVisible(false);
        spellDataVbox.getChildren().add(spellCardTypeBox);

        instantTypeButton = new ImageView(Dictionary.INSTANT_BUTTON_OFF_2);
        instantTypeButton.setPreserveRatio(true);
        instantTypeButton.setFitWidth(90);
        instantTypeButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        instantTypeButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        instantTypeButton.setOnMouseClicked(event -> {
            if (instantTypeButton.getImage().equals(Dictionary.INSTANT_BUTTON_OFF_2))
                instantTypeButton.setImage(Dictionary.INSTANT_BUTTON_ON_2);
            else
                instantTypeButton.setImage(Dictionary.INSTANT_BUTTON_OFF_2);
        });

        contsTypeButton = new ImageView(Dictionary.CONTS_BUTTON_OFF_2);
        contsTypeButton.setPreserveRatio(true);
        contsTypeButton.setFitWidth(90);
        contsTypeButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        contsTypeButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        contsTypeButton.setOnMouseClicked(event -> {
            if (contsTypeButton.getImage().equals(Dictionary.CONTS_BUTTON_OFF_2))
                contsTypeButton.setImage(Dictionary.CONTS_BUTTON_ON_2);
            else
                contsTypeButton.setImage(Dictionary.CONTS_BUTTON_OFF_2);
        });

        auraTypeButton = new ImageView(Dictionary.AURA_BUTTON_OFF_2);
        auraTypeButton.setPreserveRatio(true);
        auraTypeButton.setFitWidth(90);
        auraTypeButton.setOnMouseEntered(event -> scene.setCursor(Cursor.HAND));
        auraTypeButton.setOnMouseExited(event -> scene.setCursor(Cursor.DEFAULT));
        auraTypeButton.setOnMouseClicked(event -> {
            if (auraTypeButton.getImage().equals(Dictionary.AURA_BUTTON_OFF_2))
                auraTypeButton.setImage(Dictionary.AURA_BUTTON_ON_2);
            else
                auraTypeButton.setImage(Dictionary.AURA_BUTTON_OFF_2);
        });

        spellCardTypeBox.getChildren().addAll(instantTypeButton, contsTypeButton, auraTypeButton);

        spellCardLabel = new Label("Spell Cards");
        spellCardLabel.setPrefWidth(300);
        spellCardLabel.setFont(Font.font("Algerian", 14));
        spellCardLabel.setTextFill(Color.WHITE);
        spellCardLabel.setAlignment(Pos.CENTER);
        spellCardLabel.setVisible(false);
        spellDataVbox.getChildren().add(spellCardLabel);

        sourceBox = new HBox();
        sourceBox.setSpacing(10);
        sourceBox.setAlignment(Pos.CENTER);
        sourceBox.setVisible(false);
        spellDataVbox.getChildren().add(sourceBox);

        Label sourceLabel = new Label("From:");
        sourceLabel.setTextFill(Color.WHITE);
        sourceLabel.setPrefWidth(50);
        sourceLabel.setAlignment(Pos.CENTER);
        sourceLabel.setFont(Font.font("Times New Roman", 14));
        sourceBox.getChildren().add(sourceLabel);

        MenuBar sourceSidebar = new MenuBar();
        sourceSidebar.setPrefWidth(100);
        Menu sourceSideMenu = new Menu("SIDE");
        MenuItem[] sourceSideItems = new MenuItem[2];
        String[] sourceSides = {"FRIENDLY", "ENEMY"};
        for (int i = 0; i < 2; i++) {
            sourceSideItems[i] = new MenuItem(sourceSides[i]);
            int finalI = i;
            sourceSideItems[i].setOnAction(event -> {
                sourceSideMenu.setText(sourceSides[finalI]);
                sourceSideString = sourceSides[finalI];
            });
        }
        sourceSideMenu.getItems().addAll(sourceSideItems);
        sourceSidebar.getMenus().add(sourceSideMenu);
        sourceBox.getChildren().add(sourceSidebar);

        MenuBar sourcebar = new MenuBar();
        sourcebar.setPrefWidth(100);
        Menu sourceMenu = new Menu("SOURCE");
        MenuItem[] sourceItems = new MenuItem[5];
        String[] sourcePlaces = {"MONSTERFIELD", "SPELLFIELD", "DECK", "HAND", "GRAVEYARD"};
        for (int i = 0; i < 5; i++) {
            sourceItems[i] = new MenuItem(sourcePlaces[i]);
            int finalI = i;
            sourceItems[i].setOnAction(event -> {
                sourceMenu.setText(sourcePlaces[finalI]);
                source = sourcePlaces[finalI];
            });
        }
        sourceMenu.getItems().addAll(sourceItems);
        sourcebar.getMenus().add(sourceMenu);
        sourceBox.getChildren().add(sourcebar);

        destBox = new HBox();
        destBox.setSpacing(10);
        destBox.setAlignment(Pos.CENTER);
        destBox.setVisible(false);
        spellDataVbox.getChildren().add(destBox);

        Label destLabel = new Label("To:");
        destLabel.setTextFill(Color.WHITE);
        destLabel.setPrefWidth(50);
        destLabel.setAlignment(Pos.CENTER);
        destLabel.setFont(Font.font("Times New Roman", 14));
        destBox.getChildren().add(destLabel);

        MenuBar destSideMenuBar = new MenuBar();
        destSideMenuBar.setPrefWidth(100);
        Menu destSideMenu = new Menu("SIDE");
        MenuItem[] destSideItems = new MenuItem[2];
        String[] destSides = {"FRIENDLY", "ENEMY"};
        for (int i = 0; i < 2; i++) {
            destSideItems[i] = new MenuItem(destSides[i]);
            int finalI = i;
            destSideItems[i].setOnAction(event -> {
                destSideMenu.setText(destSides[finalI]);
                destSideString = destSides[finalI];
            });
        }
        destSideMenu.getItems().addAll(destSideItems);
        destSideMenuBar.getMenus().add(destSideMenu);
        destBox.getChildren().add(destSideMenuBar);

        MenuBar destMenuBar = new MenuBar();
        destMenuBar.setPrefWidth(100);
        Menu destMenu = new Menu("DESTINATION");
        MenuItem[] destItems = new MenuItem[5];
        String[] destPlaces = {"MONSTERFIELD", "SPELLFIELD", "DECK", "HAND", "GRAVEYARD"};
        for (int i = 0; i < 5; i++) {
            destItems[i] = new MenuItem(destPlaces[i]);
            int finalI = i;
            destItems[i].setOnAction(event -> {
                destMenu.setText(destPlaces[finalI]);
                dest = destPlaces[finalI];
            });
        }
        destMenu.getItems().addAll(destItems);
        destMenuBar.getMenus().add(destMenu);
        destBox.getChildren().add(destMenuBar);


        ImageView createButton = new ImageView(Dictionary.CREATE_SPELL_BUTTON_OFF);
        createButton.setPreserveRatio(true);
        createButton.setFitWidth(150);
        createButton.setOnMouseEntered(event -> {
            scene.setCursor(Cursor.HAND);
            createButton.setImage(Dictionary.CREATE_SPELL_BUTTON_ON);
        });
        createButton.setOnMouseExited(event -> {
            scene.setCursor(Cursor.DEFAULT);
            createButton.setImage(Dictionary.CREATE_SPELL_BUTTON_OFF);
        });
        createButton.setOnMouseClicked(event -> {
            createSpell();
        });
        spellDataVbox.getChildren().add(createButton);
    }

    private void showSpellDataBox() {
        switch (spellType) {
            case "HEALER":
                spellDataTitle.setText("Heal");
                spellAmount.setText("HEAL AMOUNT");
                spellAmount.setVisible(true);
                break;
            case "ATTACKER":
                spellDataTitle.setText("Attack");
                spellAmount.setText("ATTACK AMOUNT");
                spellAmount.setVisible(true);
                break;
            case "APCHANGER":
                spellDataTitle.setText("Change AP of");
                spellAmount.setText("CHANGE AMOUNT");
                sideBox.setVisible(true);
                spellAmount.setVisible(true);
                break;
            case "CARDMOVER":
                spellDataTitle.setText("Move");
                spellCardTypeBox.setVisible(true);
                spellCardLabel.setVisible(true);
                sourceBox.setVisible(true);
                destBox.setVisible(true);
                break;
        }
        spellDatabg.setVisible(true);
        spellDataVbox.setVisible(true);
    }

    private void hideSpellDataBox() {
        root.getChildren().remove(spellDatabg);
        spellDataVbox.getChildren().clear();
    }

    private void createSpell() {
        SpellType spellTypeEnum = SpellType.ALL;
        switch (spellType2) {
            case "SELECTIVE":
                spellTypeEnum = SpellType.SELECTIVE;
                break;
            case "RANDOM":
                spellTypeEnum = SpellType.RANDOM;
                break;
            case "ALL":
                spellTypeEnum = SpellType.ALL;
                break;
        }

        int noOfCasts = 1;

        String breed = "";
        breed = breed + elvenButton.getImage().equals(Dictionary.SELECTIVE_BUTTON_ON);
        breed = breed + dragonBreedButton.getImage().equals(Dictionary.DRAGONBREED_BUTTON_ON);
        breed = breed + atlantianButton.getImage().equals(Dictionary.ATLANTIAN_BUTTON_ON);
        breed = breed + demonicButton.getImage().equals(Dictionary.DEMONIC_BUTTON_ON);

        String monsterCardType = "";
        monsterCardType += normalTypeButton.getImage().equals(Dictionary.NORMAL_BUTTON_ON);
        monsterCardType += spellCasterTypeButton.getImage().equals(Dictionary.SPELLCASTER_BUTTON_ON);
        monsterCardType += generalTypeButton.getImage().equals(Dictionary.GENERAL_BUTTON_ON);
        monsterCardType += heroTypeButton.getImage().equals(Dictionary.HERO_BUTTON_ON);

        String spellCardType = "";
        spellCardType += instantTypeButton.getImage().equals(Dictionary.INSTANT_BUTTON_ON_2);
        spellCardType += contsTypeButton.getImage().equals(Dictionary.CONTS_BUTTON_ON_2);
        spellCardType += auraTypeButton.getImage().equals(Dictionary.AURA_BUTTON_ON_2);

        String effect = "00";


        switch (spellType) {
            case "HEALER":
                Healer healer = new Healer(spellTypeEnum, 1, breed, monsterCardType, spellCardType, Integer.valueOf(spellAmount.getText()), effect);
                spells.add(healer);
                healerButton.setOpacity(0.5);
                healerButton.setDisable(true);
                hideSpellDataBox();
                break;
            case "ATTACKER":
                Attacker attacker = new Attacker(spellTypeEnum, 1, breed, monsterCardType, spellCardType, Integer.valueOf(spellAmount.getText()), effect);
                spells.add(attacker);
                attackerButton.setOpacity(0.5);
                attackerButton.setDisable(true);
                hideSpellDataBox();
                break;
            case "APCHANGER":
                Side side = Side.FRIENDLY;
                switch (sideString) {
                    case "FRIENDLY":
                        side = Side.FRIENDLY;
                        break;
                    case "ENEMY":
                        side = Side.ENEMY;
                        break;
                    case "BOTH":
                        side = side.BOTH;
                        break;
                }
                ApChanger apChanger = new ApChanger(spellTypeEnum, 1, breed, monsterCardType, spellCardType, Integer.valueOf(spellAmount.getText()), side);
                spells.add(apChanger);
                apChangerButton.setOpacity(0.5);
                apChangerButton.setDisable(true);
                hideSpellDataBox();
                break;
            case "CARDMOVER":
                Field sourcePlace = Field.MONSTERFIELD, destPlace = Field.MONSTERFIELD;
                Side sourceSide = Side.FRIENDLY, destSide = Side.FRIENDLY;
                switch (source) {
                    case "MONSTERFIELD":
                        sourcePlace = Field.MONSTERFIELD;
                        break;
                    case "SPELLFIELD":
                        sourcePlace = Field.SPELLFIELD;
                        break;
                    case "DECK":
                        sourcePlace = Field.DECK;
                        break;
                    case "HAND":
                        sourcePlace = Field.HAND;
                        break;
                    case "GRAVEYARD":
                        sourcePlace = Field.GRAVEYARD;
                        break;
                }
                switch (dest) {
                    case "MONSTERFIELD":
                        destPlace = Field.MONSTERFIELD;
                        break;
                    case "SPELLFIELD":
                        destPlace = Field.SPELLFIELD;
                        break;
                    case "DECK":
                        destPlace = Field.DECK;
                        break;
                    case "HAND":
                        destPlace = Field.HAND;
                        break;
                    case "GRAVEYARD":
                        destPlace = Field.GRAVEYARD;
                        break;
                }
                switch (sourceSideString) {
                    case "FRIENDLY":
                        sourceSide = Side.FRIENDLY;
                        break;
                    case "ENEMY":
                        sourceSide = Side.ENEMY;
                        break;
                }
                switch (destSideString) {
                    case "FRIENDLY":
                        destSide = Side.FRIENDLY;
                        break;
                    case "ENEMY":
                        destSide = Side.ENEMY;
                        break;
                }

                CardMover cardMover = new CardMover(spellTypeEnum, 1, breed, monsterCardType, spellCardType, sourceSide, sourcePlace, destSide, destPlace);
                spells.add(cardMover);
                cardMoverButton.setOpacity(0.5);
                cardMoverButton.setDisable(true);
                hideSpellDataBox();
                break;
        }
        System.out.println(spells.size());
    }

    private void createSpellPack() {
        switch (addingSpell) {
            case "BATTLECRY":
                battleCry = new SpellPack(spells);
                battleCry.setName(spellPackName.getText());
                addBattleCryButton.setOpacity(0.5);
                addBattleCryButton.setDisable(true);

                if (!isAddingExisting)
                    Saver.saveSpellPack(battleCry);

                hideExistingSpellsBox();
                hideSpellToolBox();
                hideMiniSpellToolBox();
                break;
            case "SPELL":
                spell = new SpellPack(spells);
                spell.setName(spellPackName.getText());
                addSpellButton.setOpacity(0.5);
                addSpellButton.setDisable(true);

                if (!isAddingExisting)
                    Saver.saveSpellPack(spell);

                hideExistingSpellsBox();
                hideSpellToolBox();
                hideMiniSpellToolBox();
                break;
            case "WILL":
                will = new SpellPack(spells);
                will.setName(spellPackName.getText());
                addWillButton.setOpacity(0.5);
                addWillButton.setDisable(true);

                if (!isAddingExisting)
                    Saver.saveSpellPack(will);

                hideExistingSpellsBox();
                hideSpellToolBox();
                hideMiniSpellToolBox();
                break;
        }
        spells = null;
    }

    private void makeMonsterCard() {
        if (cardName.getText().equals(""))
            return;

        int hp = Integer.valueOf(hpField.getText());
        int ap = Integer.valueOf(apField.getText());
        int mp = Integer.valueOf(mpField.getText());

        Breed breed = Breed.DEMONIC;
        switch (monsterBreed.getText()) {
            case "ELEVEN":
                breed = Breed.ELVEN;
                break;
            case "DRAGONBREED":
                breed = Breed.DRAGONBREED;
                break;
            case "ATLANTIAN":
                breed = Breed.ATLANTIAN;
                break;
        }
        switch (monsterCardType) {
            case "NORMAL":
                Normal normal = new Normal(cardName.getText(), mp, hp, ap, speciality.getText(), breed);
                Saver.saveMonsterCard(normal);
                break;
            case "SPELLCASTER":
                if (spell == null)
                    return;
                SpellCaster spellCaster = new SpellCaster(cardName.getText(), mp, hp, ap, speciality.getText(), breed, spell, spell.getName());
                Saver.saveMonsterCard(spellCaster);
                break;
            case "GENERAL":
                if (battleCry == null || will == null)
                    return;
                General general = new General(cardName.getText(), mp, hp, ap, speciality.getText(), breed, battleCry, will, battleCry.getName(), will.getName());
                Saver.saveMonsterCard(general);
                break;
            case "HERO":
                if (battleCry == null || spell == null || will == null)
                    return;
                Hero hero = new Hero(cardName.getText(), mp, hp, ap, speciality.getText(), breed, battleCry, spell, will, battleCry.getName(), spell.getName(), will.getName());
                Saver.saveMonsterCard(hero);
                break;
        }
        Saver.loadCards();
        clearMonsterCardDataBox();
        clearMonsterCardTypeBox();
        setupMonsterCardTypeBox();
    }

    private void makeSpellCard() {
        int mp = Integer.valueOf(mpField.getText());

        switch (spellCardType) {
            case "INSTANT":
                InstantSpell instantSpell = new InstantSpell(cardName.getText(), mp, spell, spell.getName());
                Saver.saveSpellCard(instantSpell);
                break;
            case "CONTINUOUS":
                ContinuousSpell continuousSpell = new ContinuousSpell(cardName.getText(), mp, spell, spell.getName());
                Saver.saveSpellCard(continuousSpell);
                break;
            case "AURA":
                AuraSpell auraSpell = new AuraSpell(cardName.getText(), mp, spell, spell.getName());
                Saver.saveSpellCard(auraSpell);
                break;
        }

        Saver.loadCards();
        hideSpellCardDataBox();
        clearSpellCardTypeBox();
        setupSpellCardTypeBox();
    }
}
