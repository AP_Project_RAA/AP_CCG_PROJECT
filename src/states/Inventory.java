package states;

import gameObjects.Amulet;
import gameObjects.Item;
import gameObjects.cards.Card;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import main.Dictionary;
import main.Player;
import main.Saver;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

public class Inventory implements State, Serializable {
    private static final long serialVersionUID = 6L;

    private ArrayList<Card> cards;
    private ArrayList<Item> items;
    private ArrayList<Amulet> amulets;
    transient private MainMenu mainMenu;
    private Player player;
    transient private Stage stage;
    transient private Scene scene;
    transient private Scene menuScene, itemInventoryScene, amuletInventoryScene, cardInventoryScene;
    transient private Group menuGroup, itemInventoryGroup, amuletInventoryGroup, cardInventoryGroup;
    transient private Label itemInfoLabel;

    public Inventory() {
        cards = new ArrayList<>();
        items = new ArrayList<>();
        amulets = new ArrayList<>();
    }

    @Override
    public void run() {
        stage.setScene(scene);
        stage.show();
        Saver.save();
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public ArrayList<Amulet> getAmulets() {
        return amulets;
    }

    public void setAmulets(ArrayList<Amulet> amulets) {
        this.amulets = amulets;
    }

    public void setMainMenu(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setupScene() {
        //MenuScene
        menuGroup = new Group();
        menuScene = new Scene(menuGroup, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        ImageView background = new ImageView(new Image(new File("images\\inventorybg.jpg").toURI().toString()));
        background.fitWidthProperty().bind(menuScene.widthProperty());
        menuGroup.getChildren().add(background);

        Label pageName = new Label("Inventory");
        pageName.setPrefWidth(500);
        pageName.setAlignment(Pos.CENTER);
        pageName.setFont(Font.font("Algerian", FontWeight.BOLD, 56));
        pageName.setLayoutX(menuScene.getWidth() / 2 - 250);
        pageName.setLayoutY(20);
        menuGroup.getChildren().add(pageName);

        ImageView cardInventoryButton = new ImageView(Dictionary.CARD_INVENTORY_BUTTON_1);
        cardInventoryButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setupCardInventoryScene();
                scene = cardInventoryScene;
                Dictionary.playTurnPageSound();
                run();
            }
        });
        cardInventoryButton.setOnMouseEntered(event -> {
            cardInventoryButton.setImage(Dictionary.CARD_INVENTORY_BUTTON_2);
            menuScene.setCursor(Cursor.HAND);
        });
        cardInventoryButton.setOnMouseExited(event -> {
            cardInventoryButton.setImage(Dictionary.CARD_INVENTORY_BUTTON_1);
            menuScene.setCursor(Cursor.DEFAULT);
        });

        ImageView itemInventoryButton = new ImageView(Dictionary.ITEM_INVENTORY_BUTTON_1);
        itemInventoryButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setupItemInventoryScene();
                scene = itemInventoryScene;
                Dictionary.playTurnPageSound();
                run();
            }
        });
        itemInventoryButton.setOnMouseEntered(event -> {
            itemInventoryButton.setImage(Dictionary.ITEM_INVENTORY_BUTTON_2);
            menuScene.setCursor(Cursor.HAND);
        });
        itemInventoryButton.setOnMouseExited(event -> {
            itemInventoryButton.setImage(Dictionary.ITEM_INVENTORY_BUTTON_1);
            menuScene.setCursor(Cursor.DEFAULT);
        });

        ImageView amuletInventoryButton = new ImageView(Dictionary.AMULET_INVENTORY_BUTTON_1);
        amuletInventoryButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setupAmuletInventoryScene();
                scene = amuletInventoryScene;
                Dictionary.playTurnPageSound();
                run();
            }
        });
        amuletInventoryButton.setOnMouseEntered(event -> {
            amuletInventoryButton.setImage(Dictionary.AMULET_INVENTORY_BUTTON_2);
            menuScene.setCursor(Cursor.HAND);
        });
        amuletInventoryButton.setOnMouseExited(event -> {
            amuletInventoryButton.setImage(Dictionary.AMULET_INVENTORY_BUTTON_1);
            menuScene.setCursor(Cursor.DEFAULT);
        });

        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER);
        hbox.setPrefWidth(1000);
        hbox.setPrefHeight(100);
        hbox.setLayoutX(menuScene.getWidth() / 2 - 500);
        hbox.setLayoutY(menuScene.getHeight() / 2 - 50);
        hbox.setSpacing(50);
        hbox.getChildren().addAll(cardInventoryButton, itemInventoryButton, amuletInventoryButton);
        menuGroup.getChildren().add(hbox);

        // back button
        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            mainMenu.run();
        });
        menuGroup.getChildren().add(backButtonImageView);

        scene = menuScene;

        setupItemInventoryScene();
        setupAmuletInventoryScene();
        setupCardInventoryScene();
    }

    private void setupItemInventoryScene() {
        itemInventoryGroup = new Group();
        itemInventoryScene = new Scene(itemInventoryGroup, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);
        setupBasicPage(itemInventoryGroup, itemInventoryScene, "Item  Inventory");

        HBox hBox = new HBox();
        for (Item item : items) {
            hBox.getChildren().add(item.getItemGroup());
            item.getItemGroup().setOnMouseClicked(event -> {
                itemInfoLabel.setText(item.getInfo());
                Dictionary.playClickSound();
            });
        }

        hBox.setPrefWidth(Dictionary.WINDOW_WIDTH - 256);
        hBox.setTranslateX(-10);
        hBox.setSpacing(16);
        hBox.setAlignment(Pos.CENTER);

        ImageView scrollpaper = new ImageView(Dictionary.SCROLL_PAPER_1);
        scrollpaper.setTranslateX(100);
        scrollpaper.setTranslateY(150);
        scrollpaper.setFitWidth(1160);
        scrollpaper.setFitHeight(200);
        itemInventoryGroup.getChildren().add(scrollpaper);

        ScrollPane spane = new ScrollPane();
        spane.setLayoutX(118);
        spane.setLayoutY(180);
        spane.setMaxWidth(Dictionary.WINDOW_WIDTH - 258);
        spane.setMinHeight(160);
        spane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        spane.setContent(hBox);
        itemInventoryGroup.getChildren().add(spane);

        itemInfoLabel = new Label();
        itemInfoLabel.setWrapText(true);
        itemInfoLabel.setPrefWidth(itemInventoryScene.getWidth() - 900);
        itemInfoLabel.setPrefHeight(280);
        itemInfoLabel.setTranslateY(35);
        itemInfoLabel.setAlignment(Pos.CENTER);
        itemInfoLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 14));

        ImageView infoPaper = new ImageView(new Image(new File("images\\scroll-paper2.png").toURI().toString()));
        infoPaper.setFitWidth(itemInventoryScene.getWidth() - 900);
        infoPaper.setFitHeight(350);
        infoPaper.setLayoutX(450);
        infoPaper.setLayoutY(380);
        infoPaper.setOpacity(0.9);
        itemInventoryGroup.getChildren().add(infoPaper);

        ScrollPane infoPane = new ScrollPane();
        infoPane.setContent(itemInfoLabel);
        infoPane.setLayoutY(380);
        infoPane.setLayoutX(450);
        infoPane.setPrefViewportWidth(itemInventoryScene.getWidth() - 900);
        infoPane.setPrefViewportHeight(350);
        infoPane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());

        itemInventoryGroup.getChildren().add(infoPane);
    }

    transient private Label amuletInfoContent;
    transient private Label selectedAmuletLabel;
    private Amulet selectedAmulet;
    private int selectedAmuletid;

    private void setupAmuletInventoryScene() {
        amuletInventoryGroup = new Group();
        amuletInventoryScene = new Scene(amuletInventoryGroup, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);
        setupBasicPage(amuletInventoryGroup, amuletInventoryScene, "Amulet  Inventory");

        amuletInfoContent = new Label("Info will be shown here.");
        selectedAmuletLabel = new Label("Selceted Amulet");

        ImageView scrollpaper = new ImageView(Dictionary.SCROLL_PAPER_1);
        scrollpaper.setTranslateX(100);
        scrollpaper.setTranslateY(150);
        scrollpaper.setFitWidth(amuletInventoryScene.getWidth() - 500);
        scrollpaper.setFitHeight(200);
        amuletInventoryGroup.getChildren().add(scrollpaper);

        HBox hBox = new HBox();
        hBox.setLayoutX(130);
        hBox.setTranslateY(30);
        hBox.setPrefWidth(amuletInventoryScene.getWidth() - 540);
        hBox.setPrefHeight(160);
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(16);

        for (int i = 0; i < amulets.size(); i++) {
            Group amuletGroup = amulets.get(i).getAmuletGroup();
            hBox.getChildren().add(amuletGroup);
            Amulet amulet = amulets.get(i);
            int FinalI = i;
            amuletGroup.setOnMouseClicked(event -> {
                amuletInfoContent.setText(amulet.getInfo());
                selectedAmuletLabel.setText(amulet.getName());
                selectedAmulet = amulet;
                selectedAmuletid = FinalI;
                Dictionary.playClickSound();
            });
        }

        ScrollPane spane = new ScrollPane();
        spane.setLayoutX(113);
        spane.setLayoutY(150);
        spane.setPrefViewportWidth(811);
        spane.setMinViewportHeight(176);
        spane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        spane.setContent(hBox);
        amuletInventoryGroup.getChildren().add(spane);

        Label scrollTitle = new Label("Your Amulets");
        scrollTitle.setLayoutX(100);
        scrollTitle.setLayoutY(160);
        scrollTitle.setPrefWidth(amuletInventoryScene.getWidth() - 500);
        scrollTitle.setAlignment(Pos.CENTER);
        scrollTitle.setFont(Font.font("Algerian", 28));
        scrollTitle.setTextFill(Paint.valueOf("#664422"));
        amuletInventoryGroup.getChildren().add(scrollTitle);

        ImageView equippedImage = new ImageView(Dictionary.SCROLL_PAPER_1);
        equippedImage.setLayoutX(amuletInventoryScene.getWidth() - 380);
        equippedImage.setLayoutY(150);
        equippedImage.setFitWidth(280);
        equippedImage.setFitHeight(200);
        amuletInventoryGroup.getChildren().add(equippedImage);

        Label equippedTitle = new Label("Equipped Amulet");
        equippedTitle.setLayoutX(amuletInventoryScene.getWidth() - 380);
        equippedTitle.setLayoutY(160);
        equippedTitle.setPrefWidth(280);
        equippedTitle.setAlignment(Pos.CENTER);
        equippedTitle.setFont(Font.font("Algerian", 28));
        equippedTitle.setTextFill(Paint.valueOf("#664422"));
        amuletInventoryGroup.getChildren().add(equippedTitle);

        HBox equAmuletBox = new HBox();
        equAmuletBox.setLayoutX(amuletInventoryScene.getWidth() - 380);
        equAmuletBox.setLayoutY(160);
        equAmuletBox.setPrefWidth(280);
        equAmuletBox.setPrefHeight(200);
        equAmuletBox.setAlignment(Pos.CENTER);
        amuletInventoryGroup.getChildren().add(equAmuletBox);

        if (player.getAmulet() != null) {
            Group equAmuletGroup = player.getAmulet().getAmuletGroup();
            equAmuletBox.getChildren().add(equAmuletGroup);
        }

        ImageView equipButton = new ImageView(Dictionary.EQUIP_BUTTON_1);
        equipButton.setPreserveRatio(true);
        equipButton.setFitWidth(200);
        equipButton.setTranslateY(350);
        equipButton.setTranslateX(amuletInventoryScene.getWidth() - 350);
        equipButton.setOnMouseEntered(event -> {
            equipButton.setImage(Dictionary.EQUIP_BUTTON_2);
            scene.setCursor(Cursor.HAND);
        });
        equipButton.setOnMouseExited(event -> {
            equipButton.setImage(Dictionary.EQUIP_BUTTON_1);
            scene.setCursor(Cursor.DEFAULT);
        });
        equipButton.setOnMouseClicked(event -> {
            if (selectedAmulet == null) {
                amuletInfoContent.setText("You should select an amulet first.");
            } else if (player.getAmulet() == null) {
                player.setAmulet(selectedAmulet);
                amulets.remove(selectedAmuletid);
                selectedAmulet = null;
                Dictionary.playClickSound();
                setupAmuletInventoryScene();
                scene = amuletInventoryScene;
                run();
            } else {
                amulets.remove(selectedAmuletid);
                Amulet playerAmulet = player.getAmulet();
                amulets.add(playerAmulet);
                player.setAmulet(selectedAmulet);
                selectedAmulet = null;
                Dictionary.playClickSound();
                setupAmuletInventoryScene();
                scene = amuletInventoryScene;
                run();
            }
        });
        amuletInventoryGroup.getChildren().add(equipButton);

        selectedAmuletLabel.setLayoutX(amuletInventoryScene.getWidth() - 380);
        selectedAmuletLabel.setLayoutY(460);
        selectedAmuletLabel.setPrefWidth(280);
        selectedAmuletLabel.setPrefHeight(40);
        selectedAmuletLabel.setAlignment(Pos.CENTER);
        selectedAmuletLabel.setFont(Font.font("Algerian", 24));
        selectedAmuletLabel.setStyle("-fx-background-color: #fff9;" +
                "-fx-background-radius: 6px");
        amuletInventoryGroup.getChildren().add(selectedAmuletLabel);

        ImageView amuletInfoImage = new ImageView(Dictionary.SCROLL_PAPER_2);
        amuletInfoImage.setLayoutX(250);
        amuletInfoImage.setLayoutY(400);
        amuletInfoImage.setFitWidth(amuletInventoryScene.getWidth() - 800);
        amuletInfoImage.setFitHeight(250);
        amuletInventoryGroup.getChildren().add(amuletInfoImage);

        Label amuletInfoTitle = new Label("Amulet Info");
        amuletInfoTitle.setFont(Font.font("Algerian", FontWeight.BOLD, 24));
        amuletInfoTitle.setAlignment(Pos.CENTER);
        amuletInfoTitle.setLayoutX(250);
        amuletInfoTitle.setLayoutY(430);
        amuletInfoTitle.setPrefWidth(amuletInventoryScene.getWidth() - 800);
        amuletInventoryGroup.getChildren().add(amuletInfoTitle);

        amuletInfoContent.setFont(Font.font("Times New Roman", FontWeight.BOLD, 14));
        amuletInfoContent.setAlignment(Pos.CENTER);
        amuletInfoContent.setLayoutX(250);
        amuletInfoContent.setPrefWidth(amuletInventoryScene.getWidth() - 800);
        amuletInfoContent.setLayoutY(440);
        amuletInfoContent.setPrefHeight(170);
        amuletInventoryGroup.getChildren().add(amuletInfoContent);

    }

    private Card selectedCardi, selectedCardd;
    private int iCardid, dCardid;
    transient private Label infoPaneContent;
    transient private Label selectedCardLabel;
    transient private HBox dCardsBox = null;
    transient private ScrollPane deckCardsPane = new ScrollPane();
    transient private HBox iCardsBox = null;
    transient private ScrollPane inventoryCardsPane = new ScrollPane();

    private void setupCardInventoryScene() {
        cardInventoryGroup = new Group();
        cardInventoryScene = new Scene(cardInventoryGroup, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);
        setupBasicPage(cardInventoryGroup, cardInventoryScene, "Card  Inventory");

        //cards in Inventory
        ImageView iCardsbg = new ImageView(Dictionary.SCROLL_PAPER_1);
        iCardsbg.setLayoutX(100);
        iCardsbg.setLayoutY(90);
        iCardsbg.setFitWidth(cardInventoryScene.getWidth() - 200);
        iCardsbg.setFitHeight(170);
        iCardsbg.setOpacity(0.8);
        cardInventoryGroup.getChildren().add(iCardsbg);

        Label iCardsTitle = new Label("Inventory");
        iCardsTitle.setLayoutX(100);
        iCardsTitle.setPrefWidth(cardInventoryScene.getWidth() - 200);
        iCardsTitle.setLayoutY(90);
        iCardsTitle.setFont(Font.font("Algerian", FontWeight.BOLD, 24));
        iCardsTitle.setAlignment(Pos.CENTER);
        cardInventoryGroup.getChildren().add(iCardsTitle);

        renewCardsBox(iCardsBox, 95, cards, inventoryCardsPane);
        cardInventoryGroup.getChildren().add(inventoryCardsPane);

        //cards in Deck
        ImageView dCardsbg = new ImageView(Dictionary.SCROLL_PAPER_1);
        dCardsbg.setLayoutX(100);
        dCardsbg.setLayoutY(280);
        dCardsbg.setFitWidth(cardInventoryScene.getWidth() - 200);
        dCardsbg.setFitHeight(170);
        dCardsbg.setOpacity(0.8);
        cardInventoryGroup.getChildren().add(dCardsbg);

        Label dCardsTitle = new Label("Deck");
        dCardsTitle.setLayoutX(100);
        dCardsTitle.setPrefWidth(cardInventoryScene.getWidth() - 200);
        dCardsTitle.setLayoutY(280);
        dCardsTitle.setFont(Font.font("Algerian", FontWeight.BOLD, 24));
        dCardsTitle.setAlignment(Pos.CENTER);
        cardInventoryGroup.getChildren().add(dCardsTitle);

        ArrayList<Card> deckCards = player.getDeck().getCards();
        renewCardsBox(dCardsBox, 285, deckCards, deckCardsPane);
        cardInventoryGroup.getChildren().add(deckCardsPane);

        //add to deck button
        ImageView atdButton = new ImageView(Dictionary.atdButton_1);
        atdButton.setLayoutX(1050);
        atdButton.setLayoutY(470);
        atdButton.setPreserveRatio(true);
        atdButton.setFitHeight(70);
        atdButton.setOnMouseEntered(event -> {
            atdButton.setImage(Dictionary.atdButton_2);
            cardInventoryScene.setCursor(Cursor.HAND);
        });
        atdButton.setOnMouseExited(event -> {
            atdButton.setImage(Dictionary.atdButton_1);
            cardInventoryScene.setCursor(Cursor.DEFAULT);
        });
        atdButton.setOnMouseClicked(event -> {
            if (selectedCardi != null && deckCards.size() < 30) {
                deckCards.add(selectedCardi);
                cards.remove(iCardid);
                selectedCardi = null;
                selectedCardLabel.setText("No Card Selected.");
                Dictionary.playClickSound();
                setupCardInventoryScene();
                scene = cardInventoryScene;
                run();
            } else if (selectedCardi == null)
                infoPaneContent.setText("You should select a card first.");
            else if (deckCards.size() == 30)
                infoPaneContent.setText("Your Deck is full.");
        });
        cardInventoryGroup.getChildren().add(atdButton);

        //remove from deck button
        ImageView rfdButton = new ImageView(Dictionary.rfdButton_1);
        rfdButton.setLayoutX(1050);
        rfdButton.setLayoutY(540);
        rfdButton.setPreserveRatio(true);
        rfdButton.setFitHeight(70);
        rfdButton.setOnMouseEntered(event -> {
            rfdButton.setImage(Dictionary.rfdButton_2);
            cardInventoryScene.setCursor(Cursor.HAND);
        });
        rfdButton.setOnMouseExited(event -> {
            rfdButton.setImage(Dictionary.rfdButton_1);
            cardInventoryScene.setCursor(Cursor.DEFAULT);
        });
        rfdButton.setOnMouseClicked(event -> {
            if (selectedCardd != null && deckCards.size() > 0) {
                cards.add(selectedCardd);
                deckCards.remove(dCardid);
                selectedCardd = null;
                selectedCardLabel.setText("No Card Selected.");
                Dictionary.playClickSound();
                setupCardInventoryScene();
                scene = cardInventoryScene;
                run();
            } else if (selectedCardi == null)
                infoPaneContent.setText("You should select a card first.");
            else if (deckCards.size() == 30)
                infoPaneContent.setText("Your Deck is full.");
        });
        cardInventoryGroup.getChildren().add(rfdButton);

        //selected card
        selectedCardLabel = new Label("Selected Card");
        selectedCardLabel.setLayoutX(1030);
        selectedCardLabel.setLayoutY(610);
        selectedCardLabel.setPrefWidth(243);
        selectedCardLabel.setPrefHeight(40);
        selectedCardLabel.setAlignment(Pos.CENTER);
        selectedCardLabel.setFont(Font.font("Algerian", 24));
        selectedCardLabel.setStyle("-fx-background-color: #fff9;" +
                "-fx-background-radius: 6px");
        cardInventoryGroup.getChildren().add(selectedCardLabel);

        //info content
        ImageView infoScrollImage = new ImageView(Dictionary.SCROLL_PAPER_2);
        infoScrollImage.setLayoutX(300);
        infoScrollImage.setLayoutY(480);
        infoScrollImage.setFitWidth(400);
        infoScrollImage.setFitHeight(270);
        cardInventoryGroup.getChildren().add(infoScrollImage);

        infoPaneContent = new Label("Info will be shown here.");
        infoPaneContent.setAlignment(Pos.CENTER);
        infoPaneContent.setFont(Font.font("Times New Roman", FontWeight.BOLD, 14));
        infoPaneContent.setLayoutX(355);
        infoPaneContent.setLayoutY(480);
        infoPaneContent.setPrefWidth(290);
        infoPaneContent.setPrefHeight(270);
        infoPaneContent.setWrapText(true);
        cardInventoryGroup.getChildren().add(infoPaneContent);
    }

    private void renewCardsBox(HBox CardsBox, int y, ArrayList<Card> cards, ScrollPane pane) {

        CardsBox = new HBox();
        CardsBox.setAlignment(Pos.CENTER);
        CardsBox.setLayoutX(115);
        CardsBox.setLayoutY(y);
        CardsBox.setTranslateY(16);
        CardsBox.setSpacing(16);
        CardsBox.setPrefWidth(cardInventoryScene.getWidth() - 265);

        for (int i = 0; i < cards.size(); i++) {
            Card card = cards.get(i);
            Group cardGroup = card.getCardGroup();
            CardsBox.getChildren().add(cardGroup);
            int finalI = i;
            cardGroup.setOnMouseClicked(event -> {
                if (cards == this.cards) {
                    selectedCardi = card;
                    iCardid = finalI;
                } else {
                    selectedCardd = card;
                    dCardid = finalI;
                }
                selectedCardLabel.setText(card.getName());
                infoPaneContent.setText(card.getInfo());
                Dictionary.playClickSound();
            });
        }

        pane.setLayoutX(115);
        pane.setLayoutY(y);
        pane.setPrefViewportWidth(cardInventoryScene.getWidth() - 265);
        pane.setPrefViewportHeight(140);
        pane.setContent(CardsBox);
        pane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
    }

    public void setupBasicPage(Group group, Scene scene, String title) {
        //Background
        ImageView background = new ImageView(new Image(new File("images\\inventorybg.jpg").toURI().toString()));
        //background.setPreserveRatio(true);
        background.fitWidthProperty().bind(scene.widthProperty());
        group.getChildren().add(background);

        //back button
        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            this.scene = menuScene;
            Dictionary.playTurnPageSound();
            run();
        });
        group.getChildren().add(backButtonImageView);

        //Page Title
        Label pageName = new Label(title);
        pageName.setPrefWidth(600);
        pageName.setAlignment(Pos.CENTER);
        pageName.setLayoutX(scene.getWidth() / 2 - 300);
        pageName.setLayoutY(20);
        pageName.setFont(Font.font("Algerian", FontWeight.BOLD, 56));
        group.getChildren().add(pageName);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Inventory getCopy() {
        Inventory inventory = new Inventory();
        inventory.mainMenu = this.mainMenu;
        inventory.stage = this.stage;
        inventory.player = this.player;

        ArrayList<Item> itemsCopy = new ArrayList<>();
        for (Item item : items) {
            itemsCopy.add(item.getCopy());
        }
        inventory.setItems(itemsCopy);

        ArrayList<Amulet> amuletsCopy = new ArrayList<>();
        for (Amulet amulet : amulets) {
            amuletsCopy.add(amulet.getCopy());
        }
        inventory.setAmulets(amuletsCopy);

        ArrayList<Card> cardsCopy = new ArrayList<>();
        for (Card card : cards) {
            cardsCopy.add(card.getCopy());
        }
        inventory.setCards(cardsCopy);

        return inventory;
    }
}