package states;

import javafx.animation.Timeline;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import main.Dictionary;
import main.Saver;

public class ShopMenu implements State {

    private State preState;
    private CardShop cardShop;
    private ItemShop itemShop;
    private AmuletShop amuletShop;
    private MainMenu mainMenu;
    private Stage stage;
    private Scene scene;
    private MediaPlayer backgroundMusic;

    @Override
    public void run() {
        scene.setCursor(Cursor.DEFAULT);
        stage.setScene(scene);
        stage.show();
        backgroundMusic.play();
        Saver.save();
    }

    public void setupScene() {
        Group root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        // background
        ImageView imageView = new ImageView(Dictionary.SHOP_MENU_BACKGROUND);
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(scene.widthProperty());
        root.getChildren().add(imageView);

        // back button
        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            backgroundMusic.stop();
            Dictionary.playClickSound();
            preState.run();
        });
        root.getChildren().add(backButtonImageView);

        // card shop button
        ImageView btn1ImageView = new ImageView(Dictionary.SHOP_MENU_BTN1);
        btn1ImageView.setLayoutX(100);
        btn1ImageView.setLayoutY(0);
        btn1ImageView.setOnMouseEntered(event -> btn1ImageView.setImage(Dictionary.SHOP_MENU_BTN1_HOVER));
        btn1ImageView.setOnMouseExited(event -> btn1ImageView.setImage(Dictionary.SHOP_MENU_BTN1));
        btn1ImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            cardShop.run();
        });
        root.getChildren().add(btn1ImageView);

        // item shop button
        ImageView btn2ImageView = new ImageView(Dictionary.SHOP_MENU_BTN2);
        btn2ImageView.setLayoutX(100);
        btn2ImageView.setLayoutY(200);
        btn2ImageView.setOnMouseEntered(event -> btn2ImageView.setImage(Dictionary.SHOP_MENU_BTN2_HOVER));
        btn2ImageView.setOnMouseExited(event -> btn2ImageView.setImage(Dictionary.SHOP_MENU_BTN2));
        btn2ImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            itemShop.run();
        });
        root.getChildren().add(btn2ImageView);

        // amulet shop button
        ImageView btn3ImageView = new ImageView(Dictionary.SHOP_MENU_BTN3);
        btn3ImageView.setLayoutX(100);
        btn3ImageView.setLayoutY(400);
        btn3ImageView.setOnMouseEntered(event -> btn3ImageView.setImage(Dictionary.SHOP_MENU_BTN3_HOVER));
        btn3ImageView.setOnMouseExited(event -> btn3ImageView.setImage(Dictionary.SHOP_MENU_BTN3));
        btn3ImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            amuletShop.run();
        });
        root.getChildren().add(btn3ImageView);

        // background music
        backgroundMusic = new MediaPlayer(Dictionary.SHOP_BACKGROUND_MUSIC);
        backgroundMusic.setCycleCount(Timeline.INDEFINITE);
        backgroundMusic.setVolume(0.3);
    }

    public void setCardShop(CardShop cardShop) {
        this.cardShop = cardShop;
    }

    public void setItemShop(ItemShop itemShop) {
        this.itemShop = itemShop;
    }

    public void setAmuletShop(AmuletShop amuletShop) {
        this.amuletShop = amuletShop;
    }

    public void setMainMenu(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    public void setPreState(State preState) {
        this.preState = preState;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}