package states;

import gameObjects.Amulet;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import main.Dictionary;
import main.Player;

import java.util.ArrayList;

public class AmuletShop implements State {

    private ArrayList<Amulet> amuletsInShop;
    private Player player;
    private ShopMenu shopMenu;
    private Stage stage;
    private Scene scene;
    private Label creditLabel;
    private Label infoContent;
    private Label messageLabel;
    private Amulet selectedAmulet = null;
    private ImageView buyButtonImageView;
    private ImageView sellButtonImageView;

    public AmuletShop(ArrayList<Amulet> amulets){
        this.amuletsInShop = amulets;
    }

    @Override
    public void run(){
        refreshScene();
        stage.setScene(scene);
        stage.show();
    }

    public void buy(Amulet amulet){
        if(player.getCredit() >= amulet.getPrice() && !player.getInventory().getAmulets().contains(amulet)) {
            player.setCredit(player.getCredit() - amulet.getPrice());
            player.getInventory().getAmulets().add(amulet.getCopy());
        }
    }

    public void sell(Amulet amulet){
        if(player.getInventory().getAmulets().contains(amulet)) {
            player.setCredit(player.getCredit() + amulet.getPrice() / 2);
            player.getInventory().getAmulets().remove(amulet);
        } else if(player.getAmulet() != null && player.getAmulet().equals(amulet)) {
            player.setCredit(player.getCredit() + amulet.getPrice() / 2);
            player.removeAmuletAndApplyChanges();
        }
    }

    public void setupScene(){
        Group root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        // background
        ImageView imageView = new ImageView(Dictionary.SHOP_MENU_BACKGROUND);
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(scene.widthProperty());
        root.getChildren().add(imageView);

        // amulet shop label
        Text cardShopText = new Text("Amulet Shop");
        cardShopText.setFont(Font.font("algerian", 48));
        cardShopText.setFill(Color.ORANGE);
        cardShopText.layoutXProperty().bind(scene.widthProperty().
                subtract(cardShopText.layoutBoundsProperty().get().getWidth()).divide(2));
        cardShopText.setY(64);
        root.getChildren().add(cardShopText);

        // back button
        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            selectedAmulet = null;
            messageLabel.setText("select an amulet...");
            infoContent.setText("");
            buyButtonImageView.setImage(Dictionary.BUY_BUTTON_DISABLE);
            buyButtonImageView.setDisable(true);
            sellButtonImageView.setImage(Dictionary.SELL_BUTTON_DISABLE);
            sellButtonImageView.setDisable(true);
            Dictionary.playClickSound();
            shopMenu.run();
        });
        root.getChildren().add(backButtonImageView);

        // credit label
        creditLabel = new Label(Integer.toString(player.getCredit()));
        creditLabel.setFont(Font.font("algerian", 28));
        creditLabel.setTextFill(Color.GOLD);
        creditLabel.setLayoutX(1240);
        creditLabel.setLayoutY(36);
        ImageView coinImageView = new ImageView(Dictionary.COIN_IMAGE);
        coinImageView.setFitWidth(40);
        coinImageView.setFitHeight(40);
        coinImageView.setX(1190);
        coinImageView.setY(34);
        root.getChildren().addAll(coinImageView, creditLabel);

        // info text area
        ImageView infoScrollPaperImageView = new ImageView(Dictionary.SCROLL_PAPER_2);
        infoScrollPaperImageView.setLayoutX(28);
        infoScrollPaperImageView.setLayoutY(452);
        infoScrollPaperImageView.setFitWidth(600);
        infoScrollPaperImageView.setFitHeight(230);
        infoContent = new Label();
        infoContent.setWrapText(true);
        infoContent.setMaxWidth(420);
        infoContent.setLayoutX(120);
        infoContent.setLayoutY(490);
        infoContent.setFont(Font.font("algerian", 18));
        root.getChildren().addAll(infoScrollPaperImageView, infoContent);

        // amulets area
        ImageView amuletsInShopScrollPaperImageView = new ImageView(Dictionary.SCROLL_PAPER_1);
        amuletsInShopScrollPaperImageView.setLayoutX(188);
        amuletsInShopScrollPaperImageView.setLayoutY(150);
        amuletsInShopScrollPaperImageView.setFitWidth(1000);
        amuletsInShopScrollPaperImageView.setFitHeight(190);
        HBox amuletsInShopHBox = new HBox();
        for(Amulet amulet : amuletsInShop){
            amuletsInShopHBox.getChildren().add(amulet.getAmuletGroup());
            if(!amulet.getName().equals("Demon King's Crown")) {
                amulet.getAmuletGroup().setOnMouseClicked(event -> {
                    selectedAmulet = amulet;
                    messageLabel.setText(amulet.getName() + "\nPrice: " + selectedAmulet.getPrice());
                    Dictionary.playClickSound();
                    refreshScene();
                });
                if(!player.getInventory().getAmulets().contains(amulet) && (player.getAmulet() == null || !player.getAmulet().equals(amulet))) {
                    amulet.getAmuletGroup().setOpacity(0.5);
                }
            } else {
                // Demon King's Crown
                amulet.getAmuletGroup().setOnMouseClicked(event -> {
                    selectedAmulet = amulet;
                    messageLabel.setText(Dictionary.NON_PURCHASABLE);
                    buyButtonImageView.setImage(Dictionary.BUY_BUTTON_DISABLE);
                    buyButtonImageView.setDisable(true);
                    if(player.getInventory().getAmulets().contains(amulet)) {
                        sellButtonImageView.setImage(Dictionary.SELL_BUTTON);
                        sellButtonImageView.setDisable(false);
                    } else {
                        sellButtonImageView.setImage(Dictionary.SELL_BUTTON_DISABLE);
                        sellButtonImageView.setDisable(true);
                    }
                    infoContent.setText(amulet.getInfo());
                    Dictionary.playClickSound();
                });
                amulet.getAmuletGroup().setOpacity(0.5);
            }
        }
        amuletsInShopHBox.setPrefWidth(Dictionary.WINDOW_WIDTH);
        amuletsInShopHBox.setAlignment(Pos.CENTER);
        amuletsInShopHBox.setSpacing(16);
        amuletsInShopHBox.setLayoutY(180);
        root.getChildren().addAll(amuletsInShopScrollPaperImageView, amuletsInShopHBox);

        // buy button
        buyButtonImageView = new ImageView(Dictionary.BUY_BUTTON_DISABLE);
        buyButtonImageView.setDisable(true);
        buyButtonImageView.setLayoutX(600);
        buyButtonImageView.setLayoutY(270);
        buyButtonImageView.setOnMouseEntered(event -> {
            if(!buyButtonImageView.isDisable()) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON_HOVER);
                scene.setCursor(Cursor.HAND);
            }
        });
        buyButtonImageView.setOnMouseExited(event -> {
            if(!buyButtonImageView.isDisable()) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON);
                scene.setCursor(Cursor.DEFAULT);
            }
        });
        buyButtonImageView.setOnMouseClicked(event -> {
            buy(selectedAmulet);
            Dictionary.playBuySound();
            refreshScene();
        });
        root.getChildren().add(buyButtonImageView);

        // sell button
        sellButtonImageView = new ImageView(Dictionary.SELL_BUTTON_DISABLE);
        sellButtonImageView.setDisable(true);
        sellButtonImageView.setOnMouseEntered(event -> {
            if(!sellButtonImageView.isDisable()) {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON_HOVER);
                scene.setCursor(Cursor.HAND);
            }
        });
        sellButtonImageView.setOnMouseExited(event -> {
            if(!sellButtonImageView.isDisable()) {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON);
                scene.setCursor(Cursor.DEFAULT);
            }
        });
        sellButtonImageView.setOnMouseClicked(event -> {
            sell(selectedAmulet);
            Dictionary.playSellSound();
            refreshScene();
        });
        sellButtonImageView.setLayoutX(800);
        sellButtonImageView.setLayoutY(270);
        root.getChildren().add(sellButtonImageView);

        // message label
        messageLabel = new Label("select an amulet...");
        messageLabel.setDisable(true);
        messageLabel.setLayoutX(720);
        messageLabel.setLayoutY(550);
        messageLabel.setPrefWidth(500);
        messageLabel.setPrefHeight(80);
        messageLabel.setOpacity(0.7);
        messageLabel.setAlignment(Pos.CENTER);
        messageLabel.setTextAlignment(TextAlignment.CENTER);
        messageLabel.setStyle("-fx-background-color: white;" +
                "-fx-background-radius: 10px");
        messageLabel.setFont(Font.font("algerian", 20));
        root.getChildren().add(messageLabel);
    }

    private void refreshScene(){
        creditLabel.setText(Integer.toString(player.getCredit()));
        for(Amulet amulet : amuletsInShop){
            if(player.getInventory().getAmulets().contains(amulet) || (player.getAmulet() != null && player.getAmulet().equals(amulet))) {
                amulet.getAmuletGroup().setOpacity(1);
            } else {
                amulet.getAmuletGroup().setOpacity(0.5);
            }
        }
        if(selectedAmulet != null) {
            if(player.getInventory().getAmulets().contains(selectedAmulet) || (player.getAmulet() != null && player.getAmulet().equals(selectedAmulet))) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON_DISABLE);
                buyButtonImageView.setDisable(true);
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON);
                sellButtonImageView.setDisable(false);
            } else {
                if(selectedAmulet.getPrice() <= player.getCredit()) {
                    buyButtonImageView.setImage(Dictionary.BUY_BUTTON);
                    buyButtonImageView.setDisable(false);
                } else {
                    buyButtonImageView.setImage(Dictionary.BUY_BUTTON_DISABLE);
                    buyButtonImageView.setDisable(true);
                }
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON_DISABLE);
                sellButtonImageView.setDisable(true);
            }
            infoContent.setText(selectedAmulet.getInfo());
        }
    }

    public void setPlayer(Player player){
        this.player = player;
    }

    public void setShopMenu(ShopMenu shopMenu){
        this.shopMenu = shopMenu;
    }

    public ArrayList<Amulet> getAmuletsInShop(){
        return amuletsInShop;
    }

    public void setStage(Stage stage){
        this.stage = stage;
    }
}