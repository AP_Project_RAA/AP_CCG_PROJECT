package states;

public interface State {

    void run();
}