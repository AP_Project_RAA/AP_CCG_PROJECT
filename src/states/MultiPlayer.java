package states;

import javafx.animation.FadeTransition;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import main.Dictionary;
import network.Peer_to_Peer.ClientBattle;
import network.Peer_to_Peer.ServerBattle;

public class MultiPlayer implements State {
    private Stage stage;
    private Scene scene;
    private MediaPlayer backgroundMusic;
    private State preState;
    private ClientBattle clientBattle;
    private ServerBattle serverBattle;

    @Override
    public void run(){
        backgroundMusic.play();
        stage.setScene(scene);
        stage.show();
    }

    public void setupScene(){
        Group root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        ImageView imageView = new ImageView(Dictionary.MULTIPLAYER_BACKGROUND);
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(scene.widthProperty());
        root.getChildren().add(imageView);

        ImageView smokeGifImageView1 = new ImageView(Dictionary.SMOKE_GIF);
        smokeGifImageView1.setFitWidth(Dictionary.WINDOW_WIDTH);
        smokeGifImageView1.setFitHeight(Dictionary.WINDOW_HEIGHT);
        smokeGifImageView1.setOpacity(0.18);
        ImageView smokeGifImageView2 = new ImageView(Dictionary.SMOKE_GIF);
        smokeGifImageView2.setFitWidth(Dictionary.WINDOW_WIDTH);
        smokeGifImageView2.setFitHeight(Dictionary.WINDOW_HEIGHT);
        smokeGifImageView2.setOpacity(0.18);
        smokeGifImageView2.setRotate(180);
        root.getChildren().addAll(smokeGifImageView1, smokeGifImageView2);
        //Name
        TextField name = new TextField("Your Name");
        name.setAlignment(Pos.CENTER);
        name.setPrefWidth(200);
        name.setLayoutX(scene.getWidth()/2 - 100);
        name.setLayoutY(scene.getHeight()/2 - 160);
        root.getChildren().add(name);

        //Server
            //ServerOptions
        Group serverOptions = new Group();
        serverOptions.setLayoutX(scene.getWidth()/4 - 15);
        serverOptions.setLayoutY(scene.getHeight()/2);

        Rectangle serverOBG = new Rectangle(300, 100);
        serverOBG.setFill(Color.BLACK);
        serverOBG.setOpacity(0.5);
        serverOBG.setArcWidth(15);
        serverOBG.setArcHeight(15);
        serverOptions.getChildren().add(serverOBG);

        Text serverPortText = new Text("Port");
        serverPortText.setFill(Color.WHITE);
        serverPortText.setFont(new Font(18));
        serverPortText.setLayoutX(25);
        serverPortText.setLayoutY(25);
        serverOptions.getChildren().add(serverPortText);

        TextField serverPort = new TextField("8080");
        serverPort.setAlignment(Pos.CENTER);
        serverPort.setPrefWidth(200);
        serverPort.setLayoutX(80);
        serverPort.setLayoutY(5);
        serverPort.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, null, null)));
        serverOptions.getChildren().add(serverPort);

        ImageView okBTN1 = new ImageView(Dictionary.OK_BTN);
        okBTN1.setFitWidth(100);
        okBTN1.setFitHeight(50);
        okBTN1.setLayoutX(100);
        okBTN1.setLayoutY(60);
        okBTN1.setOnMouseEntered(event -> {
            okBTN1.setImage(Dictionary.OK_BTN_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        okBTN1.setOnMouseExited(event -> {
            okBTN1.setImage(Dictionary.OK_BTN);
            scene.setCursor(Cursor.DEFAULT);
        });
        okBTN1.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            backgroundMusic.stop();
            serverBattle.getPlayer().setName(name.getText());
            serverBattle.getServer().setPort(Integer.parseInt(serverPort.getText()));
            serverBattle.getServer().setDaemon(true);
            serverBattle.getServer().start();
        });
        serverOptions.getChildren().add(okBTN1);
        serverOptions.setVisible(false);

            //Path
        Line serverP1 = new Line(150,-50,150,75);
        Line serverP2 = new Line(150,75,150,-50);
        PathTransition serverTransition = new PathTransition();
        serverTransition.setPath(serverP2);
        serverTransition.setDuration(Duration.millis(500));
        serverTransition.setNode(serverOptions);
        serverTransition.setAutoReverse(false);

        root.getChildren().add(serverOptions);
        //ServerBTN
        ImageView btn1ImageView = new ImageView(Dictionary.SERVER_BTN);
        btn1ImageView.setLayoutX(160);
        btn1ImageView.setLayoutY(0);
        btn1ImageView.setOnMouseEntered(event -> {
            btn1ImageView.setImage(Dictionary.SERVER_BTN_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        btn1ImageView.setOnMouseExited(event -> {
            btn1ImageView.setImage(Dictionary.SERVER_BTN);
            scene.setCursor(Cursor.DEFAULT);
        });
        btn1ImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            if(serverTransition.getPath().equals(serverP1)){
                serverTransition.setPath(serverP2);
                serverTransition.play();

                FadeTransition pt = new FadeTransition();
                pt.setDuration(Duration.millis(500));
                pt.setNode(serverOptions);
                pt.setFromValue(1);
                pt.setToValue(0);
                pt.play();
            }else{
                serverTransition.setPath(serverP1);
                serverOptions.setVisible(true);
                serverTransition.play();

                FadeTransition pt = new FadeTransition();
                pt.setDuration(Duration.millis(500));
                pt.setNode(serverOptions);
                pt.setFromValue(0);
                pt.setToValue(1);
                pt.play();
            }
        });
        root.getChildren().add(btn1ImageView);
        //Client
            //ClientOptions
        Group clientOptions = new Group();
        clientOptions.setLayoutX(scene.getWidth()/2 + 50);
        clientOptions.setLayoutY(scene.getHeight()/2);

        Rectangle clientOBG = new Rectangle(300, 100);
        clientOBG.setFill(Color.BLACK);
        clientOBG.setOpacity(0.5);
        clientOBG.setArcWidth(15);
        clientOBG.setArcHeight(15);
        clientOptions.getChildren().add(clientOBG);

        Text clientPortText = new Text("Port");
        clientPortText.setFill(Color.WHITE);
        clientPortText.setFont(new Font(18));
        clientPortText.setLayoutX(25);
        clientPortText.setLayoutY(25);
        clientOptions.getChildren().add(clientPortText);

        TextField clientPort = new TextField("8080");
        clientPort.setAlignment(Pos.CENTER);
        clientPort.setPrefWidth(200);
        clientPort.setLayoutX(80);
        clientPort.setLayoutY(5);
        clientPort.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, null, null)));
        clientOptions.getChildren().add(clientPort);

        Text clientServerText = new Text("Server");
        clientServerText.setFill(Color.WHITE);
        clientServerText.setFont(new Font(18));
        clientServerText.setLayoutX(15);
        clientServerText.setLayoutY(55);
        clientOptions.getChildren().add(clientServerText);

        TextField clientServer = new TextField("localhost");
        clientServer.setAlignment(Pos.CENTER);
        clientServer.setPrefWidth(200);
        clientServer.setLayoutX(80);
        clientServer.setLayoutY(35);
        clientServer.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, null, null)));
        clientOptions.getChildren().add(clientServer);

        ImageView okBTN2 = new ImageView(Dictionary.OK_BTN);
        okBTN2.setFitWidth(100);
        okBTN2.setFitHeight(50);
        okBTN2.setLayoutX(100);
        okBTN2.setLayoutY(60);
        okBTN2.setOnMouseEntered(event -> {
            okBTN2.setImage(Dictionary.OK_BTN_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        okBTN2.setOnMouseExited(event -> {
            okBTN2.setImage(Dictionary.OK_BTN);
            scene.setCursor(Cursor.DEFAULT);
        });
        okBTN2.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            backgroundMusic.stop();
            clientBattle.getPlayer().setName(name.getText());
            clientBattle.getClient().setPort(Integer.parseInt(serverPort.getText()));
            clientBattle.getClient().setDaemon(true);
            clientBattle.getClient().start();
        });
        clientOptions.getChildren().add(okBTN2);
        clientOptions.setVisible(false);

            //Path
        Line clientP1 = new Line(150,-50,150,75);
        Line clientP2 = new Line(150,75,150,-50);
        PathTransition clientTransition = new PathTransition();
        clientTransition.setPath(clientP2);
        clientTransition.setDuration(Duration.millis(500));
        clientTransition.setNode(clientOptions);
        clientTransition.setAutoReverse(false);

        root.getChildren().add(clientOptions);
            //ClientBTN
        ImageView btn2ImageView = new ImageView(Dictionary.CLIENT_BTN);
        btn2ImageView.setLayoutX(565);
        btn2ImageView.setLayoutY(0);
        btn2ImageView.setOnMouseEntered(event -> {
            btn2ImageView.setImage(Dictionary.CLIENT_BTN_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        btn2ImageView.setOnMouseExited(event -> {
            btn2ImageView.setImage(Dictionary.CLIENT_BTN);
            scene.setCursor(Cursor.DEFAULT);
        });
        btn2ImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            if(clientTransition.getPath().equals(clientP1)){
                clientTransition.setPath(clientP2);
                clientTransition.play();

                FadeTransition pt = new FadeTransition();
                pt.setDuration(Duration.millis(500));
                pt.setNode(clientOptions);
                pt.setFromValue(1);
                pt.setToValue(0);
                pt.play();
            }else{
                clientTransition.setPath(clientP1);
                clientOptions.setVisible(true);
                clientTransition.play();

                FadeTransition pt = new FadeTransition();
                pt.setDuration(Duration.millis(500));
                pt.setNode(clientOptions);
                pt.setFromValue(0);
                pt.setToValue(1);
                pt.play();
            }
        });
        root.getChildren().add(btn2ImageView);
        //Back
        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            preState.run();
            backgroundMusic.stop();
        });
        root.getChildren().add(backButtonImageView);

        // background music
        backgroundMusic = new MediaPlayer(Dictionary.MAIN_MENU_BACKGROUND_MUSIC);
        backgroundMusic.setCycleCount(Timeline.INDEFINITE);
    }

    public void setStage(Stage stage){
        this.stage = stage;
    }

    public void setPreState(State preState){
        this.preState = preState;
    }

    public void setServerBattle(ServerBattle serverBattle){
        this.serverBattle = serverBattle;
    }

    public void setClientBattle(ClientBattle clientBattle){
        this.clientBattle = clientBattle;
    }
}
