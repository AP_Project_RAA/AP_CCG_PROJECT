package states;

import gameObjects.cards.Card;
import gameObjects.cards.monsterCards.General;
import gameObjects.cards.monsterCards.Hero;
import gameObjects.cards.monsterCards.Normal;
import gameObjects.cards.monsterCards.SpellCaster;
import gameObjects.cards.spellCards.SpellCard;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import main.Calculator;
import main.Dictionary;
import main.Player;

import java.io.File;
import java.util.ArrayList;

public class CardShop implements State {

    private ArrayList<Card> cardsInShop;
    private Player player;
    private ShopMenu shopMenu;
    private Stage stage;
    private Scene scene;
    transient private Label creditLabel;
    transient private Label infoContent;
    private HBox cardsOwnedHBox;
    private ImageView buyButtonImageView;
    private ImageView sellButtonImageView;
    transient private Label messageLabel;
    private Card selectedCard = null;

    public CardShop(ArrayList<Card> cardsInShop) {
        this.cardsInShop = cardsInShop;
    }

    public ArrayList<Card> getCardsInShop() {
        return cardsInShop;
    }

    @Override
    public void run() {
        refreshScene();
        stage.setScene(scene);
        stage.show();
    }

    public void buy(Card card) {
        if (isAllowedToBuy(card)) {
            player.setCredit(player.getCredit() - card.getPrice());
            player.getInventory().getCards().add(card.getCopy());
        }
    }

    public boolean isAllowedToBuy(Card card) {
        if (card == null) {
            return false;
        }
        ArrayList<Card> totalCards = new ArrayList<>();
        totalCards.addAll(player.getInventory().getCards());
        totalCards.addAll(player.getDeck().getCards());
        int numberOwned = Calculator.instance.numberOfCardInArray(totalCards, card);
        if ((card instanceof SpellCard && card.getMP() < 6 && numberOwned <= 2)
                || (card instanceof SpellCard && card.getMP() >= 6 && numberOwned <= 1)
                || (card instanceof Normal && numberOwned <= 3)
                || (card instanceof SpellCaster && numberOwned <= 1)
                || (card instanceof General && numberOwned <= 1)
                || (card instanceof Hero && numberOwned <= 0)) {
            if (player.getCredit() >= card.getPrice()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void sell(Card card) {
        if (isAllowedToSell(card)) {
            player.setCredit(player.getCredit() + card.getPrice() / 2);
            for (Card c : player.getInventory().getCards()) {
                if (c.getName().equals(card.getName())) {
                    player.getInventory().getCards().remove(c);
                    return;
                }
            }
            for (Card c : player.getDeck().getCards()) {
                if (c.getName().equals(card.getName())) {
                    player.getDeck().getCards().remove(c);
                    return;
                }
            }
        }
    }

    public boolean isAllowedToSell(Card card) {
        for (Card c : player.getInventory().getCards()) {
            if (c.getName().equals(card.getName())) {
                return true;
            }
        }
        for (Card c : player.getDeck().getCards()) {
            if (c.getName().equals(card.getName())) {
                return true;
            }
        }
        return false;
    }

    public void setupScene() {
        Group root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        // background
        ImageView imageView = new ImageView(Dictionary.SHOP_MENU_BACKGROUND);
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(scene.widthProperty());
        root.getChildren().add(imageView);

        // card shop label
        Text cardShopText = new Text("Card Shop");
        cardShopText.setFont(Font.font("algerian", 48));
        cardShopText.setFill(Color.ORANGE);
        cardShopText.layoutXProperty().bind(scene.widthProperty().
                subtract(cardShopText.layoutBoundsProperty().get().getWidth()).divide(2));
        cardShopText.setY(64);
        root.getChildren().add(cardShopText);

        // back button
        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            selectedCard = null;
            messageLabel.setText("select a card from shop...");
            infoContent.setText("");
            buyButtonImageView.setImage(Dictionary.BUY_BUTTON_DISABLE);
            buyButtonImageView.setDisable(true);
            sellButtonImageView.setImage(Dictionary.SELL_BUTTON_DISABLE);
            sellButtonImageView.setDisable(true);
            Dictionary.playClickSound();
            shopMenu.run();
        });
        root.getChildren().add(backButtonImageView);

        // credit label
        creditLabel = new Label(Integer.toString(player.getCredit()));
        creditLabel.setFont(Font.font("algerian", 28));
        creditLabel.setTextFill(Color.GOLD);
        creditLabel.setLayoutX(1240);
        creditLabel.setLayoutY(36);
        ImageView coinImageView = new ImageView(Dictionary.COIN_IMAGE);
        coinImageView.setFitWidth(40);
        coinImageView.setFitHeight(40);
        coinImageView.setX(1190);
        coinImageView.setY(34);
        root.getChildren().addAll(coinImageView, creditLabel);

        // info text area
        ImageView infoScrollPaperImageView = new ImageView(Dictionary.SCROLL_PAPER_2);
        infoScrollPaperImageView.setLayoutX(28);
        infoScrollPaperImageView.setLayoutY(512);
        infoScrollPaperImageView.setFitWidth(600);
        infoScrollPaperImageView.setFitHeight(230);
        ScrollPane infoBarScrollPane = new ScrollPane();
        infoContent = new Label();
        infoContent.setWrapText(true);
        infoContent.setMaxWidth(410);
        infoBarScrollPane.setPrefWidth(430);
        infoBarScrollPane.setMinHeight(183);
        infoBarScrollPane.setMaxHeight(183);
        infoBarScrollPane.setLayoutX(116);
        infoBarScrollPane.setLayoutY(535);
        infoBarScrollPane.setContent(infoContent);
        infoContent.setFont(Font.font("algerian", 16));
        infoBarScrollPane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        root.getChildren().addAll(infoScrollPaperImageView, infoBarScrollPane);

        // cards in shop area
        ImageView cardsInShopScrollPaperImageView = new ImageView(Dictionary.SCROLL_PAPER_1);
        cardsInShopScrollPaperImageView.setLayoutX(30);
        cardsInShopScrollPaperImageView.setLayoutY(90);
        cardsInShopScrollPaperImageView.setFitWidth(1300);
        cardsInShopScrollPaperImageView.setFitHeight(190);
        Label cardShopLabel = new Label("Shop");
        cardShopLabel.setFont(Font.font("algerian", 28));
        cardShopLabel.setTextFill(Paint.valueOf("#664422"));
        cardShopLabel.setLayoutY(96);
        cardShopLabel.setLayoutX(636);
        ScrollPane cardsInShopScrollPane = new ScrollPane();
        HBox cardsInShopContent = new HBox();
        for (Card card : cardsInShop) {
            card.getCardGroup().setOnMouseClicked(event -> {
                selectedCard = card;
                messageLabel.setText("selected card :  " + card.getName() + "\nPrice: " + selectedCard.getPrice());
                Dictionary.playClickSound();
                refreshScene();
            });
            cardsInShopContent.getChildren().add(card.getCardGroup());
        }
        cardsInShopContent.setSpacing(16);
        cardsInShopContent.setTranslateY(14);
        cardsInShopScrollPane.setMaxWidth(Dictionary.WINDOW_WIDTH - 124);
        cardsInShopScrollPane.setMinHeight(160);
        cardsInShopScrollPane.setLayoutX(50);
        cardsInShopScrollPane.setLayoutY(114);
        cardsInShopScrollPane.setContent(cardsInShopContent);
        cardsInShopScrollPane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        root.getChildren().addAll(cardsInShopScrollPaperImageView, cardShopLabel, cardsInShopScrollPane);

        // player's cards area
        ImageView cardsOwnedScrollPaperImageView = new ImageView(Dictionary.SCROLL_PAPER_1);
        cardsOwnedScrollPaperImageView.setLayoutX(30);
        cardsOwnedScrollPaperImageView.setLayoutY(300);
        cardsOwnedScrollPaperImageView.setFitWidth(1300);
        cardsOwnedScrollPaperImageView.setFitHeight(190);
        Label yourCardsLabel = new Label("Your Cards");
        yourCardsLabel.setFont(Font.font("algerian", 28));
        yourCardsLabel.setTextFill(Paint.valueOf("#664422"));
        yourCardsLabel.setLayoutY(306);
        yourCardsLabel.setLayoutX(590);
        ScrollPane cardsOwnedScrollPane = new ScrollPane();
        cardsOwnedHBox = new HBox();
        showPlayersCardsBox();
        cardsOwnedHBox.setSpacing(16);
        cardsOwnedHBox.setTranslateY(14);
        cardsOwnedScrollPane.setMaxWidth(Dictionary.WINDOW_WIDTH - 124);
        cardsOwnedScrollPane.setMinHeight(160);
        cardsOwnedScrollPane.setLayoutX(50);
        cardsOwnedScrollPane.setLayoutY(324);
        cardsOwnedScrollPane.setContent(cardsOwnedHBox);
        cardsOwnedScrollPane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        root.getChildren().addAll(cardsOwnedScrollPaperImageView, yourCardsLabel, cardsOwnedScrollPane);

        // buy button
        buyButtonImageView = new ImageView(Dictionary.BUY_BUTTON_DISABLE);
        buyButtonImageView.setDisable(true);
        buyButtonImageView.setLayoutX(600);
        buyButtonImageView.setLayoutY(330);
        buyButtonImageView.setOnMouseEntered(event -> {
            if (!buyButtonImageView.isDisable()) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON_HOVER);
                scene.setCursor(Cursor.HAND);
            }
        });
        buyButtonImageView.setOnMouseExited(event -> {
            if (!buyButtonImageView.isDisable()) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON);
                scene.setCursor(Cursor.DEFAULT);
            }
        });
        buyButtonImageView.setOnMouseClicked(event -> {
            buy(selectedCard);
            Dictionary.playBuySound();
            refreshScene();
            if (!buyButtonImageView.isDisable()) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON_HOVER);
            }
        });
        root.getChildren().add(buyButtonImageView);

        // sell button
        sellButtonImageView = new ImageView(Dictionary.SELL_BUTTON_DISABLE);
        sellButtonImageView.setDisable(true);
        sellButtonImageView.setOnMouseEntered(event -> {
            if (!sellButtonImageView.isDisable()) {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON_HOVER);
                scene.setCursor(Cursor.HAND);
            }
        });
        sellButtonImageView.setOnMouseExited(event -> {
            if (!sellButtonImageView.isDisable()) {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON);
                scene.setCursor(Cursor.DEFAULT);
            }
        });
        sellButtonImageView.setOnMouseClicked(event -> {
            sell(selectedCard);
            Dictionary.playSellSound();
            refreshScene();
            if (!sellButtonImageView.isDisable()) {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON_HOVER);
            }
        });
        sellButtonImageView.setLayoutX(800);
        sellButtonImageView.setLayoutY(330);
        root.getChildren().add(sellButtonImageView);

        // message label
        messageLabel = new Label("select a card from shop...");
        messageLabel.setDisable(true);
        messageLabel.setLayoutX(700);
        messageLabel.setLayoutY(610);
        messageLabel.setPrefHeight(80);
        messageLabel.setPrefWidth(520);
        messageLabel.setOpacity(0.7);
        messageLabel.setAlignment(Pos.CENTER);
        messageLabel.setTextAlignment(TextAlignment.CENTER);
        messageLabel.setStyle("-fx-background-color: white;" +
                "-fx-background-radius: 10px");
        messageLabel.setFont(Font.font("algerian", 20));
        root.getChildren().add(messageLabel);
    }

    public void refreshScene() {
        creditLabel.setText(Integer.toString(player.getCredit()));
        cardsOwnedHBox.getChildren().clear();
        showPlayersCardsBox();
        if (selectedCard != null) {
            infoContent.setText(selectedCard.getInfo());
            if (isAllowedToBuy(selectedCard)) {
                buyButtonImageView.setDisable(false);
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON);
            } else {
                buyButtonImageView.setDisable(true);
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON_DISABLE);
                scene.setCursor(Cursor.DEFAULT);
            }
            if (isAllowedToSell(selectedCard)) {
                sellButtonImageView.setDisable(false);
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON);
            } else {
                sellButtonImageView.setDisable(true);
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON_DISABLE);
                scene.setCursor(Cursor.DEFAULT);
            }
        }
    }

    public void showPlayersCardsBox() {
        ArrayList<Card> totalCardsOwned = new ArrayList<>();
        totalCardsOwned.addAll(player.getInventory().getCards());
        totalCardsOwned.addAll(player.getDeck().getCards());
        for (Card card : totalCardsOwned) {
            card.getCardGroup().setOnMouseClicked(event -> {
                infoContent.setText(card.getInfo());
                Dictionary.playClickSound();
            });
            cardsOwnedHBox.getChildren().add(card.getCardGroup());
        }
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setShopMenu(ShopMenu shopMenu) {
        this.shopMenu = shopMenu;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}