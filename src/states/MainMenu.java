package states;

import javafx.animation.FadeTransition;
import javafx.animation.PathTransition;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import main.Battle;
import main.Dictionary;
import map.Map;
import network.MainServer;

public class MainMenu implements State {

    private ShopMenu shopmenu;
    private Inventory inventory;
    private Map map;
    private Battle battle;
    private Stage stage;
    private Scene scene;
    private MediaPlayer backgroundMusic;
    private MultiPlayer multiPlayer;
    private Online online;
    private CustomMenu customMenu;

    @Override
    public void run() {
        backgroundMusic.play();
        stage.setScene(scene);
        stage.show();
    }

    public void setupScene() {
        Group root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        ImageView imageView = new ImageView(Dictionary.MAIN_MENU_BACKGROUND);
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(scene.widthProperty());
        root.getChildren().add(imageView);

        ImageView smokeGifImageView1 = new ImageView(Dictionary.SMOKE_GIF);
        smokeGifImageView1.setFitWidth(Dictionary.WINDOW_WIDTH);
        smokeGifImageView1.setFitHeight(Dictionary.WINDOW_HEIGHT);
        smokeGifImageView1.setOpacity(0.18);
        ImageView smokeGifImageView2 = new ImageView(Dictionary.SMOKE_GIF);
        smokeGifImageView2.setFitWidth(Dictionary.WINDOW_WIDTH);
        smokeGifImageView2.setFitHeight(Dictionary.WINDOW_HEIGHT);
        smokeGifImageView2.setOpacity(0.18);
        smokeGifImageView2.setRotate(180);
        root.getChildren().addAll(smokeGifImageView1, smokeGifImageView2);

        ImageView btn1ImageView = new ImageView(Dictionary.MAIN_MENU_BTN1);
        btn1ImageView.setLayoutX(160);
        btn1ImageView.setLayoutY(-180);
        btn1ImageView.setOnMouseEntered(event -> {
            btn1ImageView.setImage(Dictionary.MAIN_MENU_BTN1_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        btn1ImageView.setOnMouseExited(event -> {
            btn1ImageView.setImage(Dictionary.MAIN_MENU_BTN1);
            scene.setCursor(Cursor.DEFAULT);
        });
        btn1ImageView.setOnMouseClicked(event -> {
            backgroundMusic.stop();
            Dictionary.playClickSound();
            map.run();
        });
        root.getChildren().add(btn1ImageView);

        ImageView btn2ImageView = new ImageView(Dictionary.MAIN_MENU_BTN2);
        btn2ImageView.setLayoutX(160);
        btn2ImageView.setLayoutY(-100);
        btn2ImageView.setOnMouseEntered(event -> {
            btn2ImageView.setImage(Dictionary.MAIN_MENU_BTN2_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        btn2ImageView.setOnMouseExited(event -> {
            btn2ImageView.setImage(Dictionary.MAIN_MENU_BTN2);
            scene.setCursor(Cursor.DEFAULT);
        });
        btn2ImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            backgroundMusic.stop();
            customMenu.run();
        });
        root.getChildren().add(btn2ImageView);
        //ءMultiPlayer
        //Options
        Group options = new Group();
        options.setLayoutX(scene.getWidth() / 2 - 100);
        options.setLayoutY(scene.getHeight() / 2 - 180);

        Rectangle optionsBG = new Rectangle(200, 200);
        optionsBG.setFill(Color.BLACK);
        optionsBG.setOpacity(0.6);
        optionsBG.setArcWidth(15);
        optionsBG.setArcHeight(15);
        options.getChildren().add(optionsBG);

        ImageView onlineBTN = new ImageView(Dictionary.ONLINE);
        onlineBTN.setLayoutX(-207);
        onlineBTN.setLayoutY(-280);
        onlineBTN.setOnMouseEntered(event -> {
            onlineBTN.setImage(Dictionary.ONLINE_HOVER);
            this.scene.setCursor(Cursor.HAND);
        });
        onlineBTN.setOnMouseExited(event -> {
            onlineBTN.setImage(Dictionary.ONLINE);
            this.scene.setCursor(Cursor.DEFAULT);
        });
        onlineBTN.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            backgroundMusic.stop();
            online.run();
        });
        options.getChildren().add(onlineBTN);

        ImageView friends = new ImageView(Dictionary.WITH_FRIENDS);
        friends.setLayoutX(-207);
        friends.setLayoutY(-180);
        friends.setOnMouseEntered(event -> {
            friends.setImage(Dictionary.WITH_FRIENDS_HOVER);
            this.scene.setCursor(Cursor.HAND);
        });
        friends.setOnMouseExited(event -> {
            friends.setImage(Dictionary.WITH_FRIENDS);
            this.scene.setCursor(Cursor.DEFAULT);
        });
        friends.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            backgroundMusic.stop();
            multiPlayer.run();
        });
        options.getChildren().add(friends);
        options.setOpacity(0);

        Line p1 = new Line(-80, 100, 130, 100);
        Line p2 = new Line(130, 100, -80, 100);
        PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.millis(500));
        pathTransition.setPath(p2);
        pathTransition.setNode(options);

        root.getChildren().add(options);
        //BTN
        ImageView btn3ImageView = new ImageView(Dictionary.MAIN_MENU_BTN3);
        btn3ImageView.setLayoutX(160);
        btn3ImageView.setLayoutY(-20);
        btn3ImageView.setOnMouseEntered(event -> {
            btn3ImageView.setImage(Dictionary.MAIN_MENU_BTN3_HOVER);
            this.scene.setCursor(Cursor.HAND);
        });
        btn3ImageView.setOnMouseExited(event -> {
            btn3ImageView.setImage(Dictionary.MAIN_MENU_BTN3);
            this.scene.setCursor(Cursor.DEFAULT);
        });
        btn3ImageView.setOnMouseClicked(event -> {
            if (pathTransition.getPath().equals(p2)) {
                pathTransition.setPath(p1);
                FadeTransition ft = new FadeTransition();
                ft.setDuration(Duration.millis(500));
                ft.setFromValue(0);
                ft.setToValue(1);
                ft.setNode(options);
                ft.play();
            } else {
                pathTransition.setPath(p2);
                FadeTransition ft = new FadeTransition();
                ft.setDuration(Duration.millis(500));
                ft.setFromValue(1);
                ft.setToValue(0);
                ft.setNode(options);
                ft.play();
            }
            Dictionary.playClickSound();
            pathTransition.play();
        });
        root.getChildren().add(btn3ImageView);

        ImageView btn4ImageView = new ImageView(Dictionary.MAIN_MENU_BTN4);
        btn4ImageView.setLayoutX(160);
        btn4ImageView.setLayoutY(60);
        btn4ImageView.setOnMouseEntered(event -> {
            btn4ImageView.setImage(Dictionary.MAIN_MENU_BTN4_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        btn4ImageView.setOnMouseExited(event -> {
            btn4ImageView.setImage(Dictionary.MAIN_MENU_BTN4);
            scene.setCursor(Cursor.DEFAULT);
        });
        btn4ImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
        });
        root.getChildren().add(btn4ImageView);

        ImageView btn5ImageView = new ImageView(Dictionary.MAIN_MENU_BTN5);
        btn5ImageView.setLayoutX(160);
        btn5ImageView.setLayoutY(140);
        btn5ImageView.setOnMouseEntered(event -> {
            btn5ImageView.setImage(Dictionary.MAIN_MENU_BTN5_HOVER);
            scene.setCursor(Cursor.HAND);
        });
        btn5ImageView.setOnMouseExited(event -> {
            btn5ImageView.setImage(Dictionary.MAIN_MENU_BTN5);
            scene.setCursor(Cursor.DEFAULT);
        });
        btn5ImageView.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            System.exit(0);
        });
        root.getChildren().add(btn5ImageView);

        Button tempButtontoInventory = new Button("Inventory");
        tempButtontoInventory.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            backgroundMusic.stop();
            inventory.run();
        });
        tempButtontoInventory.setLayoutX(1040);
        tempButtontoInventory.setLayoutY(720);
        root.getChildren().add(tempButtontoInventory);

        Button tempButtonToShop = new Button("Shop");
        tempButtonToShop.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            backgroundMusic.stop();
            shopmenu.setPreState(this);
            shopmenu.run();
        });
        tempButtonToShop.setLayoutX(1130);
        tempButtonToShop.setLayoutY(720);
        root.getChildren().add(tempButtonToShop);

        Button tempButtonToMainServer = new Button("MainServer");
        tempButtonToMainServer.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            //backgroundMusic.stop();
            //inventory.run();
            new MainServer(8080).start();
        });
        tempButtonToMainServer.setLayoutX(1200);
        tempButtonToMainServer.setLayoutY(720);
        root.getChildren().add(tempButtonToMainServer);

        Button tempButtonToBattle = new Button("Battle");
        tempButtonToBattle.setLayoutX(1300);
        tempButtonToBattle.setLayoutY(720);
        tempButtonToBattle.setOnMouseClicked(event -> {
            Dictionary.playClickSound();
            backgroundMusic.stop();
            battle.setupScene();
            battle.run();
        });
        root.getChildren().add(tempButtonToBattle);

        // background music
        backgroundMusic = new MediaPlayer(Dictionary.MAIN_MENU_BACKGROUND_MUSIC);
        backgroundMusic.setCycleCount(MediaPlayer.INDEFINITE);
    }

    public void setShopmenu(ShopMenu shopmenu) {
        this.shopmenu = shopmenu;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setBattle(Battle battle) {
        this.battle = battle;
    }

    public void setMultiPlayer(MultiPlayer multiPlayer) {
        this.multiPlayer = multiPlayer;
    }

    public void setOnline(Online online) {
        this.online = online;
    }

    public MediaPlayer getBackgroundMusic() {
        return backgroundMusic;
    }

    public void setCustomMenu(CustomMenu customMenu) {
        this.customMenu = customMenu;
    }
}
