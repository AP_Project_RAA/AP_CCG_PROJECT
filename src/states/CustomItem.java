package states;

import gameObjects.Item;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import main.Dictionary;
import main.Player;

import java.io.File;

public class CustomItem {
    private CustomMenu customMenu;
    private Player player;
    private ItemShop itemShop;
    private Stage stage;
    private Scene scene;
    private Label creditLabel;
    private String selectedColor;

    public void run() {
        creditLabel.setText(Integer.toString(player.getCredit()));
        stage.setScene(scene);
        stage.show();
    }

    public void setupScene() {
        Group root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        // background image
        ImageView backgroundImageView = new ImageView(Dictionary.CUSTOM_ITEM_BACKGROUND);
        backgroundImageView.setPreserveRatio(true);
        backgroundImageView.setFitWidth(Dictionary.WINDOW_WIDTH);
        root.getChildren().add(backgroundImageView);

        // title
        Label title = new Label("Item  Making  Room");
        title.setPrefWidth(Dictionary.WINDOW_WIDTH);
        title.setAlignment(Pos.CENTER);
        title.setLayoutY(40);
        title.setFont(Font.font("algerian", 50));
        title.setTextFill(Color.DARKRED);
        root.getChildren().add(title);

        // credit label
        creditLabel = new Label(Integer.toString(player.getCredit()));
        creditLabel.setFont(Font.font("algerian", 28));
        creditLabel.setTextFill(Color.GOLD);
        creditLabel.setLayoutX(1240);
        creditLabel.setLayoutY(36);
        ImageView coinImageView = new ImageView(Dictionary.COIN_IMAGE);
        coinImageView.setFitWidth(40);
        coinImageView.setFitHeight(40);
        coinImageView.setX(1190);
        coinImageView.setY(34);
        root.getChildren().addAll(coinImageView, creditLabel);

        // paper image
        ImageView paperImageView = new ImageView(Dictionary.SCROLL_PAPER_3);
        paperImageView.setLayoutX(400);
        paperImageView.setLayoutY(120);
        paperImageView.setFitWidth(560);
        paperImageView.setFitHeight(600);
        root.getChildren().add(paperImageView);

        // HP slider and label
        Slider HPSlider = new Slider(0, 40, 0);
        HPSlider.setLayoutX(500);
        HPSlider.setLayoutY(240);
        HPSlider.setPrefWidth(200);
        HPSlider.getStylesheets().add(new File("styles\\sliderStyle.css").toURI().toString());
        Label HPLabel = new Label("HP :");
        HPLabel.setLayoutX(732);
        HPLabel.setLayoutY(236);
        HPLabel.setFont(Font.font("algerian", 20));
        Label HPValueLabel = new Label("0");
        HPValueLabel.setLayoutX(780);
        HPValueLabel.setLayoutY(236);
        HPValueLabel.setFont(Font.font("algerian", 20));
        root.getChildren().addAll(HPSlider, HPLabel, HPValueLabel);

        // MP slider and label
        Slider MPSlider = new Slider(0, 10, 0);
        MPSlider.setLayoutX(500);
        MPSlider.setLayoutY(300);
        MPSlider.setPrefWidth(200);
        MPSlider.getStylesheets().add(new File("styles\\sliderStyle.css").toURI().toString());
        Label MPLabel = new Label("MP :");
        MPLabel.setLayoutX(732);
        MPLabel.setLayoutY(296);
        MPLabel.setFont(Font.font("algerian", 20));
        Label MPValueLabel = new Label("0");
        MPValueLabel.setLayoutX(780);
        MPValueLabel.setLayoutY(296);
        MPValueLabel.setFont(Font.font("algerian", 20));
        root.getChildren().addAll(MPSlider, MPLabel, MPValueLabel);

        // name textfield
        Label nameLabel = new Label("name");
        nameLabel.setLayoutX(500);
        nameLabel.setLayoutY(360);
        nameLabel.setFont(Font.font("algerian", 20));
        TextField nameTextField = new TextField();
        nameTextField.setLayoutX(570);
        nameTextField.setLayoutY(360);
        nameTextField.lengthProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() > oldValue.intValue() && nameTextField.getText().length() >= 18)
                nameTextField.setText(nameTextField.getText().substring(0, 18));
        });
        nameTextField.setStyle("-fx-background-color: #fec8;" +
                "-fx-background-radius: 6px;");
        root.getChildren().addAll(nameLabel, nameTextField);

        // price label
        Label priceLabel = new Label("price :");
        priceLabel.setLayoutX(500);
        priceLabel.setLayoutY(440);
        priceLabel.setFont(Font.font("algerian", 28));
        Label priceValueLabel = new Label("0");
        priceValueLabel.setLayoutX(608);
        priceValueLabel.setLayoutY(440);
        priceValueLabel.setFont(Font.font("algerian", 28));
        root.getChildren().addAll(priceLabel, priceValueLabel);

        // adding listeners to sliders
        HPSlider.valueProperty().addListener((arg0, oldValue, newValue) -> {
            HPValueLabel.setText(String.valueOf((int) HPSlider.getValue() * 100));
            priceValueLabel.setText(String.valueOf(Integer.parseInt(priceValueLabel.getText()) + (newValue.intValue() - oldValue.intValue()) * 200));
        });
        MPSlider.valueProperty().addListener((arg0, oldValue, newValue) -> {
            MPValueLabel.setText(String.valueOf((int) MPSlider.getValue()));
            priceValueLabel.setText(String.valueOf(Integer.parseInt(priceValueLabel.getText()) + (newValue.intValue() - oldValue.intValue()) * 500));
        });

        // preview of item
        ImageView customItemPreview = new ImageView();
        customItemPreview.setLayoutX(736);
        customItemPreview.setLayoutY(430);
        customItemPreview.setFitWidth(120);
        customItemPreview.setFitHeight(180);
        root.getChildren().add(customItemPreview);

        // color choosing menu
        MenuBar colorPickingMenuBar = new MenuBar();
        Menu colorPickingMenu = new Menu("color");
        MenuItem[] colorMenuItems = new MenuItem[10];
        String[] colorNames = {"black", "blue", "gold", "green", "lightBlue", "lightGreen", "orange", "purple", "red", "yellow"};
        for (int i = 0; i < 10; i++) {
            colorMenuItems[i] = new MenuItem(colorNames[i]);
            int finalI = i;
            colorMenuItems[i].setOnAction(event -> {
                selectedColor = colorMenuItems[finalI].getText();
                colorPickingMenu.setText(colorMenuItems[finalI].getText());
                colorPickingMenuBar.setStyle("-fx-background-color: " + colorNames[finalI] + ";" +
                        "-fx-background-radius: 6px;");
                customItemPreview.setImage(new Image(new File("images\\custom items\\" + colorMenuItems[finalI].getText() + " custom item.png").toURI().toString()));
            });
        }
        colorPickingMenu.getItems().addAll(colorMenuItems);
        colorPickingMenuBar.getMenus().add(colorPickingMenu);
        colorPickingMenuBar.setLayoutX(760);
        colorPickingMenuBar.setLayoutY(360);
        colorPickingMenuBar.setStyle("-fx-background-radius: 6px;");
        root.getChildren().add(colorPickingMenuBar);

        // message box
        Group messageGroup = new Group();
        messageGroup.setVisible(false);
        Rectangle messageBox = new Rectangle();
        messageBox.setWidth(400);
        messageBox.setHeight(80);
        messageBox.setStyle("-fx-fill: #0009;");
        messageBox.setArcHeight(20);
        messageBox.setArcWidth(20);
        messageBox.setLayoutX(480);
        messageBox.setLayoutY(388);
        Label message = new Label();
        message.setTextAlignment(TextAlignment.CENTER);
        message.setAlignment(Pos.CENTER);
        message.setPrefWidth(400);
        message.setLayoutX(480);
        message.setLayoutY(398);
        message.setFont(Font.font("algerian", 15));
        message.setTextFill(Color.WHITE);
        Button OKButton = new Button("OK");
        OKButton.setLayoutX(660);
        OKButton.setLayoutY(428);
        OKButton.setOnMouseClicked(event -> messageGroup.setVisible(false));
        messageGroup.getChildren().addAll(messageBox, message, OKButton);
        root.getChildren().add(messageGroup);

        // create button
        ImageView createButtonImageView = new ImageView(Dictionary.CREATE_BUTTON_OFF);
        createButtonImageView.setLayoutX(500);
        createButtonImageView.setLayoutY(520);
        createButtonImageView.setOnMouseEntered(event -> {
            createButtonImageView.setImage(Dictionary.CREATE_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        createButtonImageView.setOnMouseExited(event -> {
            createButtonImageView.setImage(Dictionary.CREATE_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        createButtonImageView.setOnMouseClicked(event -> {
            boolean isNameUsed = false;
            for (Item item : itemShop.getItemsInShop()) {
                if (item.getName().equals(nameTextField.getText())) {
                    isNameUsed = true;
                    break;
                }
            }
            if (!nameTextField.getText().equals("") && Integer.parseInt(priceValueLabel.getText()) != 0 && selectedColor != null && !isNameUsed) {
                create(nameTextField.getText(), Integer.parseInt(priceValueLabel.getText()),
                        Integer.parseInt(HPValueLabel.getText()), Integer.parseInt(MPValueLabel.getText()));
            } else {
                if (Integer.parseInt(priceValueLabel.getText()) == 0) {
                    message.setText("you have to set HP or MP effect first!");
                } else if (nameTextField.getText().equals("")) {
                    message.setText("you have to put a name on your item first!");
                } else if (selectedColor == null) {
                    message.setText("you have to choose a color first!");
                } else if (isNameUsed) {
                    message.setText("the name you entered has already been used!");
                }
                messageGroup.setVisible(true);
            }
            creditLabel.setText(Integer.toString(player.getCredit()));
        });
        root.getChildren().add(createButtonImageView);

        // back button
        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            HPSlider.setValue(0);
            MPSlider.setValue(0);
            selectedColor = null;
            customItemPreview.setImage(null);
            colorPickingMenu.setText("color");
            colorPickingMenuBar.setStyle("-fx-background-radius: 6px;");
            messageGroup.setVisible(false);
            nameTextField.setText("");
            customMenu.run();
        });
        root.getChildren().add(backButtonImageView);
    }

    private void create(String name, int price, int HP, int MP) {
        if (player.getCredit() >= price) {
            player.setCredit(player.getCredit() - price);
            player.getInventory().getItems().add(new Item(name, price, HP, MP, selectedColor));
            player.getItems().add(new Item(name, price, HP, MP, selectedColor));
            Dictionary.playCreateItemSound();
        }
    }

    public void setCustomMenu(CustomMenu customMenu) {
        this.customMenu = customMenu;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setItemShop(ItemShop itemShop) {
        this.itemShop = itemShop;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
