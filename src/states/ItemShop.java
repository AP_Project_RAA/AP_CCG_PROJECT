package states;

import gameObjects.Item;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import main.Dictionary;
import main.Player;

import java.io.File;
import java.util.ArrayList;

public class ItemShop implements State {

    private ArrayList<Item> itemsInShop;
    private Player player;
    private ShopMenu shopMenu;
    private Stage stage;
    private Scene scene;
    private Label creditLabel;
    private Label infoContent;
    private Label messageLabel;
    private HBox itemsOwnedHBox;
    private ImageView buyButtonImageView;
    private ImageView sellButtonImageView;
    private Item selectedItem = null;

    public ItemShop(ArrayList<Item> itemsInShop) {
        this.itemsInShop = itemsInShop;
    }

    public ArrayList<Item> getItemsInShop() {
        return itemsInShop;
    }

    @Override
    public void run() {
        refreshScene();
        stage.setScene(scene);
        stage.show();
    }

    public void buy(Item item) {
        if (player.getCredit() >= item.getPrice()) {
            player.setCredit(player.getCredit() - item.getPrice());
            player.getInventory().getItems().add(item.getCopy());
            player.getItems().add(item.getCopy());
        }
    }

    public void sell(Item item) {
        for (Item i : player.getInventory().getItems()) {
            if (i.getName().equals(item.getName())) {
                player.setCredit(player.getCredit() + item.getPrice() / 2);
                player.getInventory().getItems().remove(i);
                player.getItems().remove(i);
                return;
            }
        }
    }

    public void setupScene() {
        Group root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        // background
        ImageView imageView = new ImageView(Dictionary.SHOP_MENU_BACKGROUND);
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(scene.widthProperty());
        root.getChildren().add(imageView);

        // item shop label
        Text cardShopText = new Text("Item Shop");
        cardShopText.setFont(Font.font("algerian", 48));
        cardShopText.setFill(Color.ORANGE);
        cardShopText.layoutXProperty().bind(scene.widthProperty().
                subtract(cardShopText.layoutBoundsProperty().get().getWidth()).divide(2));
        cardShopText.setY(64);
        root.getChildren().add(cardShopText);

        // back button
        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            selectedItem = null;
            messageLabel.setText("select an item from shop...");
            infoContent.setText("");
            buyButtonImageView.setImage(Dictionary.BUY_BUTTON_DISABLE);
            buyButtonImageView.setDisable(true);
            sellButtonImageView.setImage(Dictionary.SELL_BUTTON_DISABLE);
            sellButtonImageView.setDisable(true);
            Dictionary.playClickSound();
            shopMenu.run();
        });
        root.getChildren().add(backButtonImageView);

        // credit label
        creditLabel = new Label(Integer.toString(player.getCredit()));
        creditLabel.setFont(Font.font("algerian", 28));
        creditLabel.setTextFill(Color.GOLD);
        creditLabel.setLayoutX(1240);
        creditLabel.setLayoutY(36);
        ImageView coinImageView = new ImageView(Dictionary.COIN_IMAGE);
        coinImageView.setFitWidth(40);
        coinImageView.setFitHeight(40);
        coinImageView.setX(1190);
        coinImageView.setY(34);
        root.getChildren().addAll(coinImageView, creditLabel);

        // info text area
        ImageView infoScrollPaperImageView = new ImageView(Dictionary.SCROLL_PAPER_2);
        infoScrollPaperImageView.setLayoutX(28);
        infoScrollPaperImageView.setLayoutY(512);
        infoScrollPaperImageView.setFitWidth(600);
        infoScrollPaperImageView.setFitHeight(230);
        infoContent = new Label();
        infoContent.setWrapText(true);
        infoContent.setMaxWidth(420);
        infoContent.setLayoutX(120);
        infoContent.setLayoutY(550);
        infoContent.setFont(Font.font("algerian", 18));
        root.getChildren().addAll(infoScrollPaperImageView, infoContent);

        // items in shop area
        ImageView itemsInShopScrollPaperImageView = new ImageView(Dictionary.SCROLL_PAPER_1);
        itemsInShopScrollPaperImageView.setLayoutX(190);
        itemsInShopScrollPaperImageView.setLayoutY(90);
        itemsInShopScrollPaperImageView.setFitWidth(1000);
        itemsInShopScrollPaperImageView.setFitHeight(190);
        Label itemShopLabel = new Label("Shop");
        itemShopLabel.setFont(Font.font("algerian", 28));
        itemShopLabel.setTextFill(Paint.valueOf("#664422"));
        itemShopLabel.setLayoutY(96);
        itemShopLabel.setLayoutX(636);
        HBox itemsInShopHBox = new HBox();
        for (Item item : itemsInShop) {
            item.getItemGroup().setOnMouseClicked(event -> {
                selectedItem = item;
                messageLabel.setText("selected item :  " + item.getName() + "\nPrice: " + selectedItem.getPrice());
                Dictionary.playClickSound();
                refreshScene();
            });
            itemsInShopHBox.getChildren().add(item.getItemGroup());
        }
        itemsInShopHBox.setPrefWidth(Dictionary.WINDOW_WIDTH);
        itemsInShopHBox.setAlignment(Pos.CENTER);
        itemsInShopHBox.setSpacing(16);
        itemsInShopHBox.setLayoutY(130);
        root.getChildren().addAll(itemsInShopScrollPaperImageView, itemShopLabel, itemsInShopHBox);

        // player's items area
        ImageView itemsOwnedScrollPaperImageView = new ImageView(Dictionary.SCROLL_PAPER_1);
        itemsOwnedScrollPaperImageView.setLayoutX(30);
        itemsOwnedScrollPaperImageView.setLayoutY(300);
        itemsOwnedScrollPaperImageView.setFitWidth(1300);
        itemsOwnedScrollPaperImageView.setFitHeight(190);
        Label yourItemsLabel = new Label("Your Items");
        yourItemsLabel.setFont(Font.font("algerian", 28));
        yourItemsLabel.setTextFill(Paint.valueOf("#664422"));
        yourItemsLabel.setLayoutY(306);
        yourItemsLabel.setLayoutX(590);
        ScrollPane itemsOwnedScrollPane = new ScrollPane();
        itemsOwnedHBox = new HBox();
        showPlayersItems();
        itemsOwnedHBox.setSpacing(16);
        itemsOwnedHBox.setTranslateY(14);
        itemsOwnedScrollPane.setMaxWidth(Dictionary.WINDOW_WIDTH - 124);
        itemsOwnedScrollPane.setMinHeight(160);
        itemsOwnedScrollPane.setLayoutX(50);
        itemsOwnedScrollPane.setLayoutY(324);
        itemsOwnedScrollPane.setContent(itemsOwnedHBox);
        itemsOwnedScrollPane.getStylesheets().add(new File("styles\\scrollPaneStyle.css").toURI().toString());
        root.getChildren().addAll(itemsOwnedScrollPaperImageView, yourItemsLabel, itemsOwnedScrollPane);

        // buy button
        buyButtonImageView = new ImageView(Dictionary.BUY_BUTTON_DISABLE);
        buyButtonImageView.setDisable(true);
        buyButtonImageView.setLayoutX(600);
        buyButtonImageView.setLayoutY(330);
        buyButtonImageView.setOnMouseEntered(event -> {
            if (!buyButtonImageView.isDisable()) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON_HOVER);
                scene.setCursor(Cursor.HAND);
            }
        });
        buyButtonImageView.setOnMouseExited(event -> {
            if (!buyButtonImageView.isDisable()) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON);
                scene.setCursor(Cursor.DEFAULT);
            }
        });
        buyButtonImageView.setOnMouseClicked(event -> {
            buy(selectedItem);
            Dictionary.playBuySound();
            refreshScene();
            if (!buyButtonImageView.isDisable()) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON_HOVER);
            }
        });
        root.getChildren().add(buyButtonImageView);

        // sell button
        sellButtonImageView = new ImageView(Dictionary.SELL_BUTTON_DISABLE);
        sellButtonImageView.setDisable(true);
        sellButtonImageView.setOnMouseEntered(event -> {
            if (!sellButtonImageView.isDisable()) {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON_HOVER);
                scene.setCursor(Cursor.HAND);
            }
        });
        sellButtonImageView.setOnMouseExited(event -> {
            if (!sellButtonImageView.isDisable()) {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON);
                scene.setCursor(Cursor.DEFAULT);
            }
        });
        sellButtonImageView.setOnMouseClicked(event -> {
            sell(selectedItem);
            Dictionary.playSellSound();
            refreshScene();
            if (!sellButtonImageView.isDisable()) {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON_HOVER);
            }
        });
        sellButtonImageView.setLayoutX(800);
        sellButtonImageView.setLayoutY(330);
        root.getChildren().add(sellButtonImageView);

        // message label
        messageLabel = new Label("select an item from shop...");
        messageLabel.setDisable(true);
        messageLabel.setLayoutX(730);
        messageLabel.setLayoutY(610);
        messageLabel.setPrefHeight(80);
        messageLabel.setPrefWidth(440);
        messageLabel.setOpacity(0.7);
        messageLabel.setAlignment(Pos.CENTER);
        messageLabel.setTextAlignment(TextAlignment.CENTER);
        messageLabel.setStyle("-fx-background-color: white;" +
                "-fx-background-radius: 10px");
        messageLabel.setFont(Font.font("algerian", 20));
        root.getChildren().add(messageLabel);
    }

    private void refreshScene() {
        creditLabel.setText(Integer.toString(player.getCredit()));
        itemsOwnedHBox.getChildren().clear();
        showPlayersItems();
        if (selectedItem != null) {
            infoContent.setText(selectedItem.getInfo());
            if (player.getCredit() >= selectedItem.getPrice()) {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON);
                buyButtonImageView.setDisable(false);
            } else {
                buyButtonImageView.setImage(Dictionary.BUY_BUTTON_DISABLE);
                buyButtonImageView.setDisable(true);
            }
            boolean hasSelectedItem = false;
            for (Item i : player.getInventory().getItems()) {
                if (i.getName().equals(selectedItem.getName())) {
                    hasSelectedItem = true;
                    break;
                }
            }
            if (hasSelectedItem) {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON);
                sellButtonImageView.setDisable(false);
            } else {
                sellButtonImageView.setImage(Dictionary.SELL_BUTTON_DISABLE);
                sellButtonImageView.setDisable(true);
            }
        }
    }

    public void showPlayersItems() {
        for (Item item : player.getInventory().getItems()) {
            item.getItemGroup().setOnMouseClicked(event -> {
                infoContent.setText(item.getInfo());
                Dictionary.playClickSound();
            });
            itemsOwnedHBox.getChildren().add(item.getItemGroup());
        }
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setShopMenu(ShopMenu shopMenu) {
        this.shopMenu = shopMenu;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}