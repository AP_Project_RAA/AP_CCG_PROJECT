package states;

import gameObjects.Amulet;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.Dictionary;
import main.Player;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class CustomAmulet {
    private CustomMenu customMenu;
    private Player player;
    private Stage stage;
    private Scene scene;
    private Label creditLabel;
    private ImageView previewImageView;
    private File imageFile;

    public void run() {
        creditLabel.setText(Integer.toString(player.getCredit()));
        stage.setScene(scene);
        stage.show();
    }

    public void setupScene() {
        Group root = new Group();
        scene = new Scene(root, Dictionary.WINDOW_WIDTH, Dictionary.WINDOW_HEIGHT);

        // background image
        ImageView backgroundImageView = new ImageView(Dictionary.CUSTOM_AMULET_BACKGROUND);
        backgroundImageView.setPreserveRatio(true);
        backgroundImageView.setFitHeight(Dictionary.WINDOW_HEIGHT);
        root.getChildren().add(backgroundImageView);

        // title
        Label title = new Label("Amulet  Factory");
        title.setPrefWidth(Dictionary.WINDOW_WIDTH);
        title.setAlignment(Pos.CENTER);
        title.setLayoutY(40);
        title.setFont(Font.font("algerian", 50));
        title.setTextFill(Color.DARKRED);
        root.getChildren().add(title);

        // credit label
        creditLabel = new Label(Integer.toString(player.getCredit()));
        creditLabel.setFont(Font.font("algerian", 28));
        creditLabel.setTextFill(Color.GOLD);
        creditLabel.setLayoutX(1240);
        creditLabel.setLayoutY(36);
        ImageView coinImageView = new ImageView(Dictionary.COIN_IMAGE);
        coinImageView.setFitWidth(40);
        coinImageView.setFitHeight(40);
        coinImageView.setX(1190);
        coinImageView.setY(34);
        root.getChildren().addAll(coinImageView, creditLabel);

        // stone image
        ImageView stoneImageView = new ImageView(Dictionary.SIDE_PANE);
        stoneImageView.setLayoutX(460);
        stoneImageView.setLayoutY(120);
        stoneImageView.setFitWidth(440);
        stoneImageView.setFitHeight(620);
        root.getChildren().add(stoneImageView);

        // maxHP slider and label
        Slider maxHPSlider = new Slider(0, 40, 0);
        maxHPSlider.setLayoutX(580);
        maxHPSlider.setLayoutY(180);
        maxHPSlider.setPrefWidth(200);
        maxHPSlider.getStylesheets().add(new File("styles\\sliderStyle.css").toURI().toString());
        Label maxHPLabel = new Label("max-HP Effect :");
        maxHPLabel.setLayoutX(590);
        maxHPLabel.setLayoutY(210);
        maxHPLabel.setTextFill(Color.WHITE);
        maxHPLabel.setFont(Font.font("algerian", 16));
        Label maxHPValueLabel = new Label("0");
        maxHPValueLabel.setLayoutX(730);
        maxHPValueLabel.setLayoutY(210);
        maxHPValueLabel.setTextFill(Color.WHITE);
        maxHPValueLabel.setFont(Font.font("algerian", 16));
        root.getChildren().addAll(maxHPSlider, maxHPLabel, maxHPValueLabel);

        // maxMP slider and label
        Slider maxMPSlider = new Slider(0, 5, 0);
        maxMPSlider.setLayoutX(580);
        maxMPSlider.setLayoutY(260);
        maxMPSlider.setPrefWidth(200);
        maxMPSlider.getStylesheets().add(new File("styles\\sliderStyle.css").toURI().toString());
        Label maxMPLabel = new Label("max-MP Effect :");
        maxMPLabel.setLayoutX(590);
        maxMPLabel.setLayoutY(290);
        maxMPLabel.setTextFill(Color.WHITE);
        maxMPLabel.setFont(Font.font("algerian", 16));
        Label maxMPValueLabel = new Label("0");
        maxMPValueLabel.setLayoutX(730);
        maxMPValueLabel.setLayoutY(290);
        maxMPValueLabel.setTextFill(Color.WHITE);
        maxMPValueLabel.setFont(Font.font("algerian", 16));
        root.getChildren().addAll(maxMPSlider, maxMPLabel, maxMPValueLabel);

        // name textfield
        Label nameLabel = new Label("name");
        nameLabel.setLayoutX(590);
        nameLabel.setLayoutY(350);
        nameLabel.setTextFill(Color.WHITE);
        nameLabel.setFont(Font.font("algerian", 16));
        TextField nameTextField = new TextField();
        nameTextField.setLayoutX(644);
        nameTextField.setLayoutY(348);
        nameTextField.setPrefWidth(130);
        nameTextField.lengthProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() > oldValue.intValue() && nameTextField.getText().length() >= 18)
                nameTextField.setText(nameTextField.getText().substring(0, 18));
        });
        nameTextField.setStyle("-fx-background-color: #fffb;" +
                "-fx-background-radius: 6px;");
        root.getChildren().addAll(nameLabel, nameTextField);

        // choose image
        FileChooser fileChooser = new FileChooser();
        Button chooseButton = new Button("choose image");
        chooseButton.setPrefWidth(120);
        chooseButton.setLayoutX(626);
        chooseButton.setLayoutY(400);
        Group previewImageGroup = new Group();
        previewImageGroup.setLayoutX(628);
        previewImageGroup.setLayoutY(440);
        previewImageGroup.prefWidth(116);
        previewImageGroup.prefHeight(128);
        chooseButton.setOnAction(event -> {
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                imageFile = file;
                previewImageView = new ImageView(new Image(file.toURI().toString()));
                previewImageView.setFitWidth(110);
                previewImageView.setFitHeight(110);
                previewImageView.setLayoutX(3);
                previewImageView.setLayoutY(9);
                ImageView frameImageView = new ImageView(new File("images\\amulets\\amulet-frame.png").toURI().toString());
                frameImageView.setFitWidth(116);
                frameImageView.setFitHeight(128);
                previewImageGroup.getChildren().addAll(previewImageView, frameImageView);
            }
        });
        root.getChildren().addAll(chooseButton, previewImageGroup);

        // price label
        Label priceLabel = new Label("price :");
        priceLabel.setLayoutX(590);
        priceLabel.setLayoutY(600);
        priceLabel.setTextFill(Color.WHITE);
        priceLabel.setFont(Font.font("algerian", 28));
        Label priceValueLabel = new Label("0");
        priceValueLabel.setLayoutX(698);
        priceValueLabel.setLayoutY(600);
        priceValueLabel.setTextFill(Color.WHITE);
        priceValueLabel.setFont(Font.font("algerian", 28));
        root.getChildren().addAll(priceLabel, priceValueLabel);

        // adding listeners to sliders
        maxHPSlider.valueProperty().addListener((arg0, oldValue, newValue) -> {
            maxHPValueLabel.setText(String.valueOf((int) maxHPSlider.getValue() * 100));
            priceValueLabel.setText(String.valueOf(Integer.parseInt(priceValueLabel.getText()) + (newValue.intValue() - oldValue.intValue()) * 400));
        });
        maxMPSlider.valueProperty().addListener((arg0, oldValue, newValue) -> {
            maxMPValueLabel.setText(String.valueOf((int) maxMPSlider.getValue()));
            priceValueLabel.textProperty().setValue(String.valueOf((int) maxHPSlider.getValue() * 400
                    + (int) Math.pow(2, newValue.intValue()) * 1000 * Math.min(1, newValue.intValue())));
        });

        // message box
        Group messageGroup = new Group();
        messageGroup.setVisible(false);
        Rectangle messageBox = new Rectangle();
        messageBox.setWidth(400);
        messageBox.setHeight(80);
        messageBox.setStyle("-fx-fill: #0009;");
        messageBox.setArcHeight(20);
        messageBox.setArcWidth(20);
        messageBox.setLayoutX(480);
        messageBox.setLayoutY(370);
        Label message = new Label();
        message.setTextAlignment(TextAlignment.CENTER);
        message.setAlignment(Pos.CENTER);
        message.setPrefWidth(400);
        message.setLayoutX(480);
        message.setLayoutY(380);
        message.setFont(Font.font("algerian", 15));
        message.setTextFill(Color.WHITE);
        Button OKButton = new Button("OK");
        OKButton.setLayoutX(660);
        OKButton.setLayoutY(410);
        OKButton.setOnMouseClicked(event -> messageGroup.setVisible(false));
        messageGroup.getChildren().addAll(messageBox, message, OKButton);
        root.getChildren().add(messageGroup);

        // create button
        ImageView createButtonImageView = new ImageView(Dictionary.CREATE_BUTTON_OFF);
        createButtonImageView.setLayoutX(628);
        createButtonImageView.setLayoutY(650);
        createButtonImageView.setOnMouseEntered(event -> {
            createButtonImageView.setImage(Dictionary.CREATE_BUTTON_ON);
            scene.setCursor(Cursor.HAND);
        });
        createButtonImageView.setOnMouseExited(event -> {
            createButtonImageView.setImage(Dictionary.CREATE_BUTTON_OFF);
            scene.setCursor(Cursor.DEFAULT);
        });
        createButtonImageView.setOnMouseClicked(event -> {
            boolean isNameUsed = false;
            String[] namesOfAmuletsInShop = {"Iron Pendant", "Gold Pendant", "Diamond Pendant",
                    "Iron Ring", "Gold Ring", "Diamond Ring", "Demon King's Crown"};
            for (String name : namesOfAmuletsInShop) {
                if (name.equals(nameTextField.getText())) {
                    isNameUsed = true;
                    break;
                }
            }
            if (!nameTextField.getText().equals("") && Integer.parseInt(priceValueLabel.getText()) != 0 && !isNameUsed && previewImageView != null) {
                create(nameTextField.getText(), Integer.parseInt(priceValueLabel.getText()), Integer.parseInt(maxHPValueLabel.getText()), Integer.parseInt(maxMPValueLabel.getText()));
            } else {
                if (Integer.parseInt(priceValueLabel.getText()) == 0) {
                    message.setText("you have to set maxHP or maxMP effect first!");
                } else if (nameTextField.getText().equals("")) {
                    message.setText("you have to put a name on your amulet first!");
                } else if (isNameUsed) {
                    message.setText("the name you entered has already been used!");
                } else if (previewImageView == null) {
                    message.setText("please choose an image for your amulet!");
                }
                messageGroup.setVisible(true);
            }
            creditLabel.setText(Integer.toString(player.getCredit()));
        });
        root.getChildren().add(createButtonImageView);

        // back button
        ImageView backButtonImageView = new ImageView(Dictionary.BACK_BUTTON);
        backButtonImageView.setX(20);
        backButtonImageView.setY(20);
        backButtonImageView.setFitHeight(60);
        backButtonImageView.setFitWidth(60);
        backButtonImageView.setOnMouseClicked(event -> {
            maxHPSlider.setValue(0);
            maxMPSlider.setValue(0);
            messageGroup.setVisible(false);
            previewImageGroup.getChildren().clear();
            nameTextField.setText("");
            customMenu.run();
        });
        root.getChildren().add(backButtonImageView);
    }

    private void create(String name, int price, int maxHP, int maxMP) {
        if (player.getCredit() >= price) {
            try {
                File destination = new File("images\\amulets\\" + name + ".png");
                destination.delete();
                Files.copy(imageFile.toPath(), destination.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            player.setCredit(player.getCredit() - price);
            Amulet customAmulet = new Amulet(name, price, maxHP, maxMP, 0);
            player.getInventory().getAmulets().add(customAmulet);
            Dictionary.playCreateAmuletSound();
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setCustomMenu(CustomMenu customMenu) {
        this.customMenu = customMenu;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

}
