package map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class MapHero {
    private int x, y;
    private static int[] allowLocations = {8, 9, 29, 30, 49, 50, 51, 70, 71, 72, 91, 92, 93, 485, 486, 494, 495, 517, 518, 526, 527, 548, 549, 550, 557, 558, 559, 580, 581, 582, 586, 587, 588, 589, 591, 612, 613, 614, 621, 622, 623, 785, 2401};
    private static int[] marketLocations = {1508, 1509, 1510, 1519, 1520, 1521, 1530, 1531, 1532};

    private Animation[] dir = new Animation[4];//{Right, Up, Left, Down}

    MapHero(){
        TextureAtlas hero = new TextureAtlas(Gdx.files.internal("map/Sprites/Hero.atlas"));
        dir[0] = new Animation(1 / 30f,
                hero.findRegion("0144"),
                hero.findRegion("0145"),
                hero.findRegion("0146"),
                hero.findRegion("0147"),
                hero.findRegion("0148"),
                hero.findRegion("0149"),
                hero.findRegion("0150"),
                hero.findRegion("0151"),
                hero.findRegion("0152"));

        dir[1] = new Animation(1 / 30f,
                hero.findRegion("0105"),
                hero.findRegion("0106"),
                hero.findRegion("0107"),
                hero.findRegion("0108"),
                hero.findRegion("0109"),
                hero.findRegion("0110"),
                hero.findRegion("0111"),
                hero.findRegion("0112"),
                hero.findRegion("0113"));

        dir[2] = new Animation(1 / 30f,
                hero.findRegion("0118"),
                hero.findRegion("0119"),
                hero.findRegion("0120"),
                hero.findRegion("0121"),
                hero.findRegion("0122"),
                hero.findRegion("0123"),
                hero.findRegion("0124"),
                hero.findRegion("0125"),
                hero.findRegion("0126"));

        dir[3] = new Animation(1 / 30f,
                hero.findRegion("0131"),
                hero.findRegion("0132"),
                hero.findRegion("0133"),
                hero.findRegion("0134"),
                hero.findRegion("0135"),
                hero.findRegion("0136"),
                hero.findRegion("0137"),
                hero.findRegion("0138"),
                hero.findRegion("0139"));

        x = 256;
        y = 3200 - 512;
    }

    int getX(){
        return x;
    }

    int getY(){
        return y;
    }

    Animation[] getDir(){
        return dir;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    int[] getAllowLocations(){
        return allowLocations;
    }

    int[] getMarketLocations(){
        return marketLocations;
    }
}
