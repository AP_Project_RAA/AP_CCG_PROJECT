package map;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import enums.Locations;
import javafx.application.Platform;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GameMap extends ApplicationAdapter {
    private Map map;
    private Locations[][] locations = new Locations[100][100];
    private String mapPath;

    private MapHero hero;
    private SpriteBatch spriteBatch, miniMapSpriteBatch;
    private float time = 0;
    private Animation heroDir;

    private TiledMap tiledMap;
    private OrthographicCamera camera;
    private OrthographicCamera miniMapCamera;
    private TiledMapRenderer tiledMapRenderer, miniTiledMapRenderer;
    private ShapeRenderer shapeRenderer;

    GameMap(String mapPath, Map map){
        this.map = map;
        this.mapPath = mapPath;
    }

    private Locations[][] getAllowLocations(MapHero hero){
        try {
            FileInputStream in = new FileInputStream(mapPath);
            Scanner scanner = new Scanner(in);
            for(int i = 0; i < 15; i++)
                scanner.nextLine();
            for(int layer = 0; layer < 1; layer++){
                scanner.nextLine();
                scanner.nextLine();
                for(int i = 0; i < 100; i++){
                    String[] numb = scanner.nextLine().split(",");
                    for(int j = 0; j < 100; j++){
                        if(Integer.parseInt(numb[j]) == 785)
                            locations[i][j] = Locations.ALLOW;
                        else
                            locations[i][j] = Locations.BLOCK;
                        /*Missions*/
                        if(i <= 15 && i >= 7 && j <= 45 && j >= 23)
                            locations[i][j] = Locations.MISSION1;
                        else if(i <= 80 && i >= 61 && j <= 39 && j >= 21)
                            locations[i][j] = Locations.MISSION2;
                        else if(i <= 89 && i >= 55 && j <= 91 && j >= 62)
                            locations[i][j] = Locations.MISSION3;
                        else if(i <= 20 && i >= 11 && j <= 89 && j >= 86)
                            locations[i][j] = Locations.MISSION4;
                    }
                }
                scanner.nextLine();
                scanner.nextLine();
            }
            for(int layer = 1; layer < 4; layer++){
                scanner.nextLine();
                scanner.nextLine();
                for(int i = 0; i < 100; i++){
                    String[] numb = scanner.nextLine().split(",");
                    for(int j = 0; j < 100; j++){
                        if(locations[i][j] != Locations.BLOCK && locations[i][j] != Locations.MISSION4) {
                            boolean flag = false;
                            for(int cnt = 0; cnt < hero.getAllowLocations().length; cnt++){
                                if(Long.parseLong(numb[j]) == hero.getAllowLocations()[cnt] || Long.parseLong(numb[j]) == 0) {
                                    flag = true;
                                }
                            }
                            if(!flag)
                                locations[i][j] = Locations.BLOCK;
                        } else if(locations[i][j] == Locations.BLOCK) {
                            boolean flag = false;
                            for(int cnt = 0; cnt < hero.getAllowLocations().length; cnt++){
                                if(Long.parseLong(numb[j]) == hero.getAllowLocations()[cnt]) {
                                    flag = true;
                                }
                            }
                            if(flag)
                                locations[i][j] = Locations.ALLOW;
                        }
                        /*Market*/
                        boolean flag = false;
                        for(int cnt = 0; cnt < hero.getMarketLocations().length; cnt++){
                            if(Long.parseLong(numb[j]) == hero.getMarketLocations()[cnt]) {
                                flag = true;
                            }
                        }
                        if(flag)
                            locations[i][j] = Locations.MARKET;
                    }
                }
                scanner.nextLine();
                scanner.nextLine();
            }
//            for (int i = 0 ; i < 100 ; i++){
//                for(int j = 0 ; j < 100 ; j++){
//                    if(allowLocations[i][j])
//                        System.out.print(1 + ",");
//                    else
//                        System.out.print(0 + ",");
//                }
//                System.out.println("");
//            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        return locations;
    }

    @Override
    public void create(){
        Music mapBackground = Gdx.audio.newMusic(Gdx.files.internal("audio/MapBackground.mp3"));
        mapBackground.play();

        Graphics.DisplayMode mode = Gdx.graphics.getDesktopDisplayMode();
        Gdx.graphics.setDisplayMode(mode.width, mode.height, true);

        spriteBatch = new SpriteBatch();
        hero = new MapHero();
        heroDir = hero.getDir()[0];
        locations = getAllowLocations(hero);

        load();

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, w, h);
        camera.position.set(hero.getX(), hero.getY(), 0);

        miniMapCamera = new OrthographicCamera();
        miniMapSpriteBatch = new SpriteBatch();
        miniMapCamera.setToOrtho(false, w * 8, h * 8);
        miniMapCamera.update();

        camera.update();
        tiledMap = new TmxMapLoader().load(mapPath);
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
        miniTiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);

        shapeRenderer = new ShapeRenderer();
    }

    @Override
    public void render(){
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        miniMapCamera.update();

        tiledMapRenderer.setView(camera);
        tiledMapRenderer.render();

        miniTiledMapRenderer.setView(miniMapCamera);
        miniTiledMapRenderer.render();

        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.begin();
        spriteBatch.draw(heroDir.getKeyFrame(time, true), hero.getX(), hero.getY());
        spriteBatch.end();

        miniMapSpriteBatch.setProjectionMatrix(miniMapCamera.combined);
        miniMapSpriteBatch.begin();
        float width = camera.viewportWidth * camera.zoom, height = camera.viewportHeight * camera.zoom;
        miniMapSpriteBatch.draw(heroDir.getKeyFrame(time, true), hero.getX(), hero.getY());
        miniMapSpriteBatch.end();
        shapeRenderer.setProjectionMatrix(miniMapSpriteBatch.getProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.rect(hero.getX() - width / 2, hero.getY() - height / 2, width, height);
        shapeRenderer.end();

        handleInputs();
    }

    @Override
    public void dispose(){
        shapeRenderer.dispose();
        miniMapSpriteBatch.dispose();
        spriteBatch.dispose();
        tiledMap.dispose();
    }

    private void handleInputs(){
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            heroDir = hero.getDir()[2];
            if(locations[99 - hero.getY() / 32][(hero.getX() - 2) / 32 + 1] != Locations.BLOCK && locations[99 - hero.getY() / 32][(hero.getX() - 2) / 32 + 1] != Locations.MARKET) {
                if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                    hero.setX(hero.getX() - 2);
                    time += Gdx.graphics.getDeltaTime() / 2;
                }
                hero.setX(hero.getX() - 2);
                time += Gdx.graphics.getDeltaTime() / 2;
                camera.position.set(hero.getX(), hero.getY(), 0);
                camera.update();
            } else if(locations[99 - hero.getY() / 32][(hero.getX() - 2) / 32 + 1] == Locations.MARKET) {
                Platform.runLater(() -> {
                    map.goToMarket();
                });
                exit();
            }
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            heroDir = hero.getDir()[0];
            if(locations[99 - hero.getY() / 32][(hero.getX() + 2) / 32 + 1] != Locations.BLOCK && locations[99 - hero.getY() / 32][(hero.getX() + 2) / 32 + 1] != Locations.MARKET) {
                if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                    hero.setX(hero.getX() + 2);
                    time += Gdx.graphics.getDeltaTime() / 2;
                }
                hero.setX(hero.getX() + 2);
                time += Gdx.graphics.getDeltaTime() / 2;
                camera.position.set(hero.getX(), hero.getY(), 0);
                camera.update();
            } else if(locations[99 - hero.getY() / 32][(hero.getX() + 2) / 32 + 1] == Locations.MARKET) {
                Platform.runLater(() -> {
                    map.goToMarket();
                });
                exit();
            }
        }
        if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
            heroDir = hero.getDir()[1];
            if(locations[99 - (hero.getY() + 2) / 32][hero.getX() / 32 + 1] != Locations.BLOCK && locations[99 - (hero.getY() + 2) / 32][hero.getX() / 32 + 1] != Locations.MARKET) {
                if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                    hero.setY(hero.getY() + 2);
                    time += Gdx.graphics.getDeltaTime() / 2;
                }
                hero.setY(hero.getY() + 2);
                time += Gdx.graphics.getDeltaTime() / 2;
                camera.position.set(hero.getX(), hero.getY(), 0);
                camera.update();
            } else if(locations[99 - (hero.getY() + 2) / 32][hero.getX() / 32 + 1] == Locations.MARKET) {
                Platform.runLater(() -> {
                    map.goToMarket();
                });
                exit();
            }
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            heroDir = hero.getDir()[3];
            if(locations[99 - (hero.getY() - 2) / 32][hero.getX() / 32 + 1] != Locations.BLOCK && locations[99 - (hero.getY() - 2) / 32][hero.getX() / 32 + 1] != Locations.MARKET) {
                if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                    hero.setY(hero.getY() - 2);
                    time += Gdx.graphics.getDeltaTime() / 2;
                }
                hero.setY(hero.getY() - 2);
                time += Gdx.graphics.getDeltaTime() / 2;
                camera.position.set(hero.getX(), hero.getY(), 0);
                camera.update();
            } else if(locations[99 - (hero.getY() - 2) / 32][hero.getX() / 32 + 1] == Locations.MARKET) {
                Platform.runLater(() -> {
                    map.goToMarket();
                });
                exit();
            }
        }

        if(locations[99 - hero.getY() / 32][(hero.getX() - 2) / 32] == Locations.MISSION1) {
            Platform.runLater(() -> {
                map.goToBattle();
            });
            exit();
        } else if(locations[99 - hero.getY() / 32][(hero.getX() - 2) / 32] == Locations.MISSION2) {
            Platform.runLater(() -> {
                map.goToBattle();
            });
            exit();
        } else if(locations[99 - hero.getY() / 32][(hero.getX() - 2) / 32] == Locations.MISSION3) {
            Platform.runLater(() -> {
                map.goToBattle();
            });
            exit();
        } else if(locations[99 - hero.getY() / 32][(hero.getX() - 2) / 32] == Locations.MISSION4) {
            Platform.runLater(() -> {
                map.goToBattle();
            });
            exit();
        }

        if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
            if(Gdx.input.isKeyPressed(Input.Keys.MINUS)) {
                camera.zoom += 0.05f;
                if(camera.zoom > 2)
                    camera.zoom = 2;
            }

            if(Gdx.input.isKeyPressed(Input.Keys.PLUS)) {
                camera.zoom -= 0.05f;
                if(camera.zoom < 0.25f)
                    camera.zoom = 0.25f;
            }

            camera.update();
        }

        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            Platform.runLater(() -> {
                map.goToMainMenu();
            });
            exit();
        }
    }

    private void load(){
        mapPath = map.getMapPath();
    }

    private void exit(){
        map.setMapPath(mapPath);
        Gdx.app.exit();
    }
}
