package map;

import main.Battle;
import main.Main;
import states.MainMenu;
import states.ShopMenu;
import states.State;

public class Map implements State {

    private String mapPath;
    private ShopMenu shopMenu;
    private Battle battle;
    private MainMenu mainMenu;

    public Map(String path){
        mapPath = path;
    }

    @Override
    public void run(){
        Main.gameManager.setMapReferences();
        TestGraphics.main(mapPath, this);
    }

    void goToMarket(){
        shopMenu.setPreState(this);
        shopMenu.run();
    }

    void goToBattle(){
        battle.run();
    }

    void goToMainMenu(){
        mainMenu.run();
    }

    public void setShopMenu(ShopMenu shopMenu){
        this.shopMenu = shopMenu;
    }

    public void setBattle(Battle battle){
        this.battle = battle;
    }

    public void setMainMenu(MainMenu mainMenu){
        this.mainMenu = mainMenu;
    }

    public void setMapPath(String path){
        this.mapPath = path;
    }

    public String getMapPath(){
        return mapPath;
    }
}
