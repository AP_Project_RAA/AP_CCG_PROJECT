package map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.OutputStream;
//import java.util.Formatter;

class TestGraphics {
    /*static void atlasMaker(){
        String address = "Map/Sprites/Hero.png";
        try {
            OutputStream out = new FileOutputStream(address.substring(0, address.length()-4) + ".atlas");
            Formatter formatter = new Formatter(out);

            formatter.format("Hero.png\n" + "format: RGBA8888\n" +
                    "filter: Nearest,Nearest\n" +
                    "repeat: none\n");

            int cnt = 1;
            for(int i = 0 ; i < 21; i++)
                for(int j = 0 ; j < 13; j++){
                    formatter.format("%04d\n rotate: false\n xy: %d, %d\n size: 64, 64\n" +
                            "  orig: 64, 64\n" +
                            "  offset: 0, 0\n" +
                            "  index: -1\n", cnt, j*64, i*64);

                    cnt++;
                }
            formatter.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }*/
    static void main(String path, Map map){
        Gdx.app = null;
        Gdx.audio = null;
        Gdx.files = null;
        Gdx.gl = null;
        Gdx.gl20 = null;
        Gdx.gl30 = null;
        Gdx.graphics = null;
        Gdx.input = null;
        Gdx.net = null;

        GameMap gameMap = new GameMap(path, map);
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 1366;
        config.height = 768;
        config.title = "AP_ACG";
        config.vSyncEnabled = true;
        config.forceExit = false;

        new LwjglApplication(gameMap, config);
    }
}
